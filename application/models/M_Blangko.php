<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Blangko extends CI_Model {
		function __construct()
		{
			parent:: __construct();
			$this->yzdb = $this->load->database('YZDB', TRUE);
		}
		// BLANGKO KTP		
		public function get_count_blangko($no_kec,$tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_MONEV_BLANGKO WHERE NO_KEC = $no_kec AND TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->JML;
		}
		public function get_count_inblangko($tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_BLANGKO WHERE TO_CHAR(TANGGAL_MASUK,'DD-MM-YYYY') = '$tgl'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->JML;
		}
		public function get_count_all_blangko($tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_MONEV_BLANGKO WHERE TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->JML;
		}
		public function get_blangko($no_kec,$tgl){
			$sql = "SELECT AMBIL,RUSAK,PENGEMBALIAN FROM SIAK_MONEV_BLANGKO WHERE NO_KEC = $no_kec AND TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r;
		}
		public function get_all_blangko($tgl){
			$sql = "SELECT SUM(AMBIL) AMBIL, SUM(RUSAK) RUSAK, SUM(PENGEMBALIAN) PENGEMBALIAN, SUM(AMBIL) - (SUM(RUSAK) + SUM(PENGEMBALIAN)) TOTAL FROM SIAK_MONEV_BLANGKO WHERE TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r;
		}
		public function get_allin_blangko(){
			$sql = "SELECT 
					(SELECT SUM(JUMLAH) FROM SIAK_BLANGKO WHERE TRUNC(TANGGAL_MASUK,'YYYY') = TRUNC(SYSDATE,'YYYY')) AS MASUK,
					(SELECT SUM(AMBIL) - (SUM(RUSAK) +SUM(PENGEMBALIAN)) AS JUMLAH FROM SIAK_MONEV_BLANGKO WHERE TRUNC(TANGGAL,'YYYY') = TRUNC(SYSDATE,'YYYY')) AS KELUAR,
					(SELECT SUM(RUSAK)AS JUMLAH FROM SIAK_MONEV_BLANGKO WHERE TRUNC(TANGGAL,'YYYY') = TRUNC(SYSDATE,'YYYY')) AS RUSAK,
					(SELECT SUM(JUMLAH) FROM SIAK_BLANGKO ) - (SELECT SUM(AMBIL) - SUM(PENGEMBALIAN) AS JUMLAH FROM SIAK_MONEV_BLANGKO) AS SISA
					FROM DUAL";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r;
		}

		public function save_blangko($no_kec,$tgl,$pengambilan,$rusak,$pengembalian){
			$sql = "INSERT INTO SIAK_MONEV_BLANGKO (NO_KEC,AMBIL,RUSAK,PENGEMBALIAN,TANGGAL,BLANGKO_ID) VALUES ($no_kec,$pengambilan,$rusak,$pengembalian,SYSDATE,BLANGKO_ID.NEXTVAL)";
			$q = $this->yzdb->query($sql);
			return $q;
		}
		public function update_blangko($no_kec,$tgl,$pengambilan,$rusak,$pengembalian){
			$sql = "UPDATE SIAK_MONEV_BLANGKO SET AMBIL = $pengambilan, RUSAK =$rusak, PENGEMBALIAN = $pengembalian WHERE NO_KEC = $no_kec AND TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$q = $this->yzdb->query($sql);
			return $q;
		}
		public function save_inblangko($blangko_masuk,$tgl,$keterangan = '',$user_name){
			$sql = "INSERT INTO SIAK_BLANGKO (JUMLAH,TANGGAL_MASUK,CREATED_BY,KET) VALUES ($blangko_masuk,SYSDATE,'$user_name','$keterangan')";
			$q = $this->yzdb->query($sql);
			return $q;
		}
		public function update_inblangko($blangko_masuk,$tgl,$keterangan = '',$user_name){
			$sql = "UPDATE SIAK_BLANGKO SET JUMLAH = $blangko_masuk, CREATED_BY ='$user_name', KET = '$keterangan' WHERE TO_CHAR(TANGGAL_MASUK,'DD-MM-YYYY') = '$tgl'";
			$q = $this->yzdb->query($sql);
			return $q;
		}
		public function cek_rekap_blangko($tgl_start,$tgl_end){
			$sql = "";
			$sql .= "SELECT 
					  NO_WIL
					  , NAMA_WIL
					  , CASE WHEN AMBIL IS NULL THEN 0 ELSE AMBIL END AS AMBIL 
					  , CASE WHEN RUSAK IS NULL THEN 0 ELSE RUSAK END AS RUSAK 
					  , CASE WHEN PENGEMBALIAN IS NULL THEN 0 ELSE PENGEMBALIAN END AS PENGEMBALIAN 
					  , CASE WHEN TOTAL IS NULL THEN 0 ELSE TOTAL END AS TOTAL 
					FROM SIAK_KODE_WIL A LEFT JOIN 
					(SELECT 
					  A.NO_KEC
					  , SUM(A.AMBIL) AS AMBIL
					  , SUM(A.RUSAK) AS RUSAK
					  , SUM(A.PENGEMBALIAN) AS PENGEMBALIAN
					  , SUM(A.AMBIL) - (SUM(A.RUSAK)+SUM(A.PENGEMBALIAN)) AS TOTAL
					FROM SIAK_MONEV_BLANGKO A
					WHERE 1=1
					AND A.TANGGAL >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.TANGGAL < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					GROUP BY A.NO_KEC ORDER BY A.NO_KEC) B ON A.NO_WIL = B.NO_KEC ORDER BY A.NO_WIL";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function cek_rekap_count_blangko($tgl_start,$tgl_end){
			$sql = "";
			$sql .= "SELECT 
					    CASE WHEN SUM(A.AMBIL) IS NULL THEN 0 ELSE SUM(A.AMBIL) END AS AMBIL
	  					, CASE WHEN SUM(A.RUSAK) IS NULL THEN 0 ELSE SUM(A.RUSAK) END AS RUSAK
	  					, CASE WHEN SUM(A.PENGEMBALIAN) IS NULL THEN 0 ELSE SUM(A.PENGEMBALIAN) END AS PENGEMBALIAN
	  					, CASE WHEN SUM(A.AMBIL) - (SUM(A.RUSAK)+SUM(A.PENGEMBALIAN)) IS NULL THEN 0 ELSE SUM(A.AMBIL) - (SUM(A.RUSAK)+SUM(A.PENGEMBALIAN)) END AS TOTAL
					FROM SIAK_MONEV_BLANGKO A
					WHERE 1=1
					AND A.TANGGAL >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.TANGGAL < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		// BLANGKO KK
		public function get_count_blangko_kk($no_kec,$tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_MONEV_BLANGKO_KK WHERE NO_KEC = $no_kec AND TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->JML;
		}
		public function get_blangko_kk($no_kec,$tgl){
			$sql = "SELECT AMBIL,RUSAK FROM SIAK_MONEV_BLANGKO_KK WHERE NO_KEC = $no_kec AND TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r;
		}
		public function get_count_all_blangko_kk($no_kec,$tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_MONEV_BLANGKO_KK WHERE TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl' AND NO_KEC = $no_kec";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->JML;
		}
		public function get_count_saldo_blangko_kk($no_kec){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_MONEV_BLANGKO_KK WHERE NO_KEC = $no_kec ";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->JML;
		}
		public function get_all_blangko_kk($no_kec,$tgl){
			$sql = "SELECT SUM(AMBIL) AMBIL, SUM(RUSAK) RUSAK, SUM(AMBIL) - SUM(RUSAK) TOTAL FROM SIAK_MONEV_BLANGKO_KK WHERE TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl' AND NO_KEC = $no_kec";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r;
		}
		public function get_saldo_blangko_kk($no_kec){
			$sql = "SELECT  SUM(AMBIL) - SUM(RUSAK) SALDO FROM SIAK_MONEV_BLANGKO_KK WHERE NO_KEC = $no_kec ";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r;
		}
		public function get_allin_kk_blangko(){
			$sql = "SELECT 
					(SELECT SUM(JUMLAH) FROM SIAK_BLANGKO_KK) AS MASUK,
					(SELECT SUM(AMBIL) - SUM(RUSAK) AS JUMLAH FROM SIAK_MONEV_BLANGKO_KK ) AS KELUAR,
					(SELECT SUM(RUSAK)AS JUMLAH FROM SIAK_MONEV_BLANGKO_KK) AS RUSAK,
					(SELECT SUM(JUMLAH) FROM SIAK_BLANGKO_KK) - (SELECT SUM(AMBIL) AS JUMLAH FROM SIAK_MONEV_BLANGKO_KK) AS SISA
					FROM DUAL";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r;
		}
		public function save_blangko_kk($no_kec,$tgl,$pengambilan,$rusak,$user_id){
			$sql = "INSERT INTO SIAK_MONEV_BLANGKO_KK (NO_KEC,AMBIL,RUSAK,PENGEMBALIAN,TANGGAL,BLANGKO_ID,CREATED_BY) VALUES ($no_kec,$pengambilan,$rusak,$pengembalian,SYSDATE,BLANGKO_KK_ID.NEXTVAL,'$user_id')";
			$q = $this->yzdb->query($sql);
			return $q;
		}
		public function update_blangko_kk($no_kec,$tgl,$pengambilan,$rusak,$user_id){
			$sql = "UPDATE SIAK_MONEV_BLANGKO_KK SET AMBIL = $pengambilan, RUSAK =$rusak, CREATED_BY = '$user_id' WHERE NO_KEC = $no_kec AND TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$q = $this->yzdb->query($sql);
			return $q;
		}
		public function save_inblangko_kk($blangko_masuk,$tgl,$keterangan = '',$user_name){
			$sql = "INSERT INTO SIAK_BLANGKO (JUMLAH,TANGGAL_MASUK,CREATED_BY,KET) VALUES ($blangko_masuk,SYSDATE,'$user_name','$keterangan')";
			$q = $this->yzdb->query($sql);
			return $q;
		}
		public function update_inblangko_kk($blangko_masuk,$tgl,$keterangan = '',$user_name){
			$sql = "UPDATE SIAK_BLANGKO SET JUMLAH = $blangko_masuk, CREATED_BY ='$user_name', KET = '$keterangan' WHERE TO_CHAR(TANGGAL_MASUK,'DD-MM-YYYY') = '$tgl'";
			$q = $this->yzdb->query($sql);
			return $q;
		}
		// CETAK DINAS
		public function get_nik($nik){
			$sql = "SELECT COUNT(1) AS JML FROM BIODATA_WNI@DB222 WHERE NIK = $nik";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->JML;
		}
		public function get_data_nik($nik){
			$sql = "SELECT A.NAMA_LGKP, A.NO_KEC, (SELECT B.NAMA_KEC FROM SETUP_KEC@DB222 B WHERE A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC) AS NAMA_KEC FROM BIODATA_WNI@DB222 A WHERE A.NIK = $nik";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function save_cetak($nik,$nama,$no_kec,$nama_kec,$request_by,$receive,$tanggal,$user_name){
			$sql = "INSERT INTO SIAK_CETAK_DINAS (NO,NIK,NAMA_LGKP,NO_KEC,NAMA_KEC,REQ_BY,TGL_CETAK,RECEIVE_BY,CREATED_BY) VALUES (CETAK_DINAS_NO.NEXTVAL,$nik,'$nama',$no_kec,'$nama_kec','$request_by',SYSDATE,'$receive','$user_name')";
			
			$q = $this->yzdb->query($sql);
			return $q;
		}
		public function cek_rekap_cetak_dinas($nik,$nama){
			$sql ="";
			$sql .= " SELECT NO, CASE WHEN NAMA_LGKP IS NULL THEN '-' ELSE NAMA_LGKP END AS NAMA_LGKP, NIK, NO_KEC, NAMA_KEC, REQ_BY, TO_CHAR(TGL_CETAK,'DD-MM-YYYY') AS TGL_CETAK, RECEIVE_BY FROM SIAK_CETAK_DINAS WHERE 1=1";
			if ($nik != ''){
				$sql .= " AND NIK IN ($nik)";	
			}
			if ($nama != ''){
				$sql .= " AND UPPER(NAMA_LGKP) LIKE '".$nama."'";
			}
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function cek_count_cetak_dinas($nik,$nama){
			$sql ="";
			$sql .= "SELECT COUNT(1) AS JML FROM SIAK_CETAK_DINAS WHERE 1=1";
			if ($nik != ''){
				$sql .= " AND NIK IN ($nik)";	
			}
			if ($nama != ''){
				$sql .= " AND UPPER(NAMA_LGKP) LIKE '".$nama."'";
			}
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->JML;
		}
		public function rekap_cetak_dinas($tgl_start,$tgl_end){
			$sql ="";
			$sql .= " SELECT NO, CASE WHEN NAMA_LGKP IS NULL THEN '-' ELSE NAMA_LGKP END AS NAMA_LGKP, NIK, NO_KEC, NAMA_KEC, REQ_BY, TO_CHAR(TGL_CETAK,'DD-MM-YYYY') AS TGL_CETAK, RECEIVE_BY, CREATED_BY FROM SIAK_CETAK_DINAS WHERE 1=1 AND TGL_CETAK >= TO_DATE('$tgl_start','DD-MM-YYYY') AND TGL_CETAK < TO_DATE('$tgl_end','DD-MM-YYYY') +1 ORDER BY NO DESC";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function count_cetak_dinas($tgl_start,$tgl_end){
			$sql ="";
			$sql .= "SELECT COUNT(1) AS JML FROM SIAK_CETAK_DINAS WHERE 1=1 AND TGL_CETAK >= TO_DATE('$tgl_start','DD-MM-YYYY') AND TGL_CETAK < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->JML;
		}
		function get_no_prop()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_prop();
    	}
    	function get_no_kab()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_kab();
    	}
    	function get_siak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_siak_dblink();
    	}
    	function get_rekam_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_rekam_dblink();
    	}
    	function get_cetak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cetak_dblink();
    	}
    	function get_master_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_master_dblink();
    	}
}