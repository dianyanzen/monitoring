<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Dashboard extends CI_Model {
		function __construct()
		{
			parent:: __construct();
            $this->yzdb = $this->load->database('YZDB', TRUE);
			
		}

        public function get_rekam()
        {
            $this->db221 = $this->load->database('DB221', TRUE);
                $sql = "SELECT 
                        SUM( CASE WHEN TO_CHAR(CREATED,'DD/MM/YYYY') = 
                        TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS TODAY 
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'BIO_CAPTURED' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_BIO_CAPTURED
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_SENT_FOR_ENROLLMENT
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'PRINT_READY_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_PRINT_READY_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'DUPLICATE_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_DUPLICATE_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'PROCESSING' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_PROCESSING
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'ADJUDICATE_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_ADJUDICATE_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'ENROLL_FAILURE_AT_CENTRAL' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_ENROLL_FAILURE_AT_CENTRAL
                        , SUM(CASE WHEN CURRENT_STATUS_CODE <> 'SENT_FOR_ENROLLMENT' AND CURRENT_STATUS_CODE <> 'BIO_CAPTURED' AND CURRENT_STATUS_CODE <> 'DUPLICATE_RECORD' AND CURRENT_STATUS_CODE <> 'PRINT_READY_RECORD' AND CURRENT_STATUS_CODE <> 'PROCESSING' AND CURRENT_STATUS_CODE <> 'ADJUDICATE_RECORD' AND CURRENT_STATUS_CODE <> 'ENROLL_FAILURE_AT_CENTRAL' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_OTHER 
                        , SUM( CASE WHEN TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS YESTERDAY
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'BIO_CAPTURED' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_BIO_CAPTURED
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_SENT_FOR_ENROLLMENT
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'PRINT_READY_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_PRINT_READY_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'DUPLICATE_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_DUPLICATE_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'PROCESSING' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_PROCESSING
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'ADJUDICATE_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_ADJUDICATE_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'ENROLL_FAILURE_AT_CENTRAL' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_ENROLL_FAILURE_AT_CENTRAL
                        , SUM(CASE WHEN CURRENT_STATUS_CODE <> 'SENT_FOR_ENROLLMENT' AND CURRENT_STATUS_CODE <> 'BIO_CAPTURED' AND CURRENT_STATUS_CODE <> 'DUPLICATE_RECORD' AND CURRENT_STATUS_CODE <> 'PRINT_READY_RECORD' AND CURRENT_STATUS_CODE <> 'PROCESSING' AND CURRENT_STATUS_CODE <> 'ADJUDICATE_RECORD' AND CURRENT_STATUS_CODE <> 'ENROLL_FAILURE_AT_CENTRAL' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_OTHER 
                        FROM DEMOGRAPHICS WHERE EXISTS(SELECT 1 FROM FACES WHERE DEMOGRAPHICS.NIK = FACES.NIK)";
                $q = $this->db221->query($sql);
                $r = $q->row(0);
                $output = array(
                   "rekam_today"=>$r->TODAY,
                    "rekam_yesterday"=>$r->YESTERDAY,
                    "bio_today"=>$r->N_BIO_CAPTURED,
                    "bio_yesterday"=>$r->Y_BIO_CAPTURED,
                    "sfe_today"=>$r->N_SENT_FOR_ENROLLMENT,
                    "sfe_yesterday"=>$r->Y_SENT_FOR_ENROLLMENT,
                    "prr_today"=>$r->N_PRINT_READY_RECORD,
                    "prr_yesterday"=>$r->Y_PRINT_READY_RECORD,
                    "durec_today"=>$r->N_DUPLICATE_RECORD,
                    "durec_yesterday"=>$r->Y_DUPLICATE_RECORD,
                    "process_today"=>$r->N_PROCESSING,
                    "process_yesterday"=>$r->Y_PROCESSING,
                    "adjudicate_today"=>$r->N_ADJUDICATE_RECORD,
                    "adjudicate_yesterday"=>$r->Y_ADJUDICATE_RECORD,
                    "enroll_failure_today"=>$r->N_ENROLL_FAILURE_AT_CENTRAL,
                    "enroll_failure_yesterday"=>$r->Y_ENROLL_FAILURE_AT_CENTRAL,
                    "other_today"=>$r->N_OTHER,
                    "other_yesterday"=>$r->Y_OTHER
                );
                return $output;
        }
        public function get_user_cetak(){
            $sql ="SELECT USER_ID, TGL, JML FROM VW_SIAK_USER_CETAK";
            $q = $this->yzdb->query($sql);
            $r = $q->result();
            return $r;

        }
        public function get_cetak()
        {
            // $this->db2 = $this->load->database('DB2', TRUE);
                $sql = "SELECT 
                        SUM( CASE WHEN (CASE WHEN TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(A.PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE,'DD/MM/YYYY')) THEN 1 ELSE 0 END) AS HARI_INI, 
                        SUM( CASE WHEN (CASE WHEN TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(A.PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) THEN 1 ELSE 0 END) AS KEMARIN
                        FROM CARD_MANAGEMENT@DB2 A WHERE EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.NIK =B.NIK) 
                        AND (UPPER(A.CREATED_USERNAME) NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB WHERE IS_ACTIVE = 1) OR UPPER(A.LAST_UPDATED_USERNAME) NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB WHERE IS_ACTIVE = 1))
                        ";
        
                $q = $this->db->query($sql);
                $r = $q->row(0);
                $output = array(
                   "cetak_today"=>$r->HARI_INI,
                    "cetak_yesterday"=>$r->KEMARIN
                );
                return $output;
        }
        public function get_kia()
        {
                $sql = "SELECT 
                        SUM( CASE WHEN TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS HARI_INI, 
                        SUM( CASE WHEN TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS KEMARIN
                        FROM T5_SEQN_KIA_PRINT".$this->get_siak_dblink()." A WHERE EXISTS(SELECT 1 FROM BIODATA_WNI".$this->get_siak_dblink()." B WHERE A.NIK =B.NIK) 
                        ";
                $q = $this->yzdb->query($sql);
                $r = $q->row(0);
                $output = array(
                   "kia_today"=>$r->HARI_INI,
                    "kia_yesterday"=>$r->KEMARIN
                );
                return $output;
        }

        public function get_suket()
        {
                $sql = "SELECT 
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET".$this->get_siak_dblink()." A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) N_SUKET,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET".$this->get_siak_dblink()." A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AND NOT EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE,'DD/MM/YYYY'))) N_NREQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET".$this->get_siak_dblink()." A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AND EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE,'DD/MM/YYYY'))) N_DREQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM T7_HIST_SUKET".$this->get_siak_dblink()." A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY'))) AS N_REQ_CETAK,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." A WHERE TO_CHAR(A.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AS N_REQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." A WHERE TO_CHAR(A.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM T7_HIST_SUKET".$this->get_siak_dblink()." B WHERE A.NIK = B.NIK AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY'))) AS N_REQ_BELUM,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET".$this->get_siak_dblink()." A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) Y_SUKET,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET".$this->get_siak_dblink()." A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) AND NOT EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))) Y_NREQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET".$this->get_siak_dblink()." A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) AND EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))) Y_DREQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM T7_HIST_SUKET".$this->get_siak_dblink()." A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))) AS Y_REQ_CETAK,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." A WHERE TO_CHAR(A.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) AS Y_REQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM T5_REQ_CETAK_KTP".$this->get_siak_dblink()." A WHERE TO_CHAR(A.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM T7_HIST_SUKET".$this->get_siak_dblink()." B WHERE A.NIK = B.NIK AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))) AS Y_REQ_BELUM
                            FROM DUAL";
                $q = $this->yzdb->query($sql);
                $r = $q->row(0);
                $output = array(
                   "suket_today"=>$r->N_SUKET,
                    "suket_yesterday"=>$r->Y_SUKET,
                    "dreq_jumlah_yesterday"=>$r->Y_DREQ_JUMLAH,
                    "dreq_jumlah_today"=>$r->N_DREQ_JUMLAH,
                    "nreq_jumlah_today"=>$r->N_NREQ_JUMLAH,
                    "nreq_jumlah_yesterday"=>$r->Y_NREQ_JUMLAH,
                    "req_jumlah_today"=>$r->N_REQ_JUMLAH,
                    "req_jumlah_yesterday"=>$r->Y_REQ_JUMLAH,
                    "req_cetak_today"=>$r->N_REQ_CETAK,
                    "req_cetak_yesterday"=>$r->Y_REQ_CETAK,
                    "req_belum_today"=>$r->N_REQ_BELUM,
                    "req_belum_yesterday"=>$r->Y_REQ_BELUM
                );
                return $output;
        }

        public function get_capil()
        {
        		$sql = "SELECT
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_LAHIR".$this->get_siak_dblink()." A WHERE TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.ADM_AKTA_NO LIKE '%LU%' AND EXISTS(SELECT 1 FROM BIODATA_WNI".$this->get_siak_dblink()." B WHERE A.BAYI_NIK =B.NIK) AND A.ADM_NO_PROV=32AND A.ADM_NO_KAB= ".$this->get_no_kab()." AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D') N_LAHIR_U,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_LAHIR".$this->get_siak_dblink()." A WHERE TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.ADM_AKTA_NO LIKE '%LT%' AND EXISTS(SELECT 1 FROM BIODATA_WNI".$this->get_siak_dblink()." B WHERE A.BAYI_NIK =B.NIK) AND A.ADM_NO_PROV=32AND A.ADM_NO_KAB= ".$this->get_no_kab()." AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D') N_LAHIR_T,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_MATI".$this->get_siak_dblink()."  WHERE  TO_CHAR(PLPR_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND ADM_NO_PROV=32AND ADM_NO_KAB= ".$this->get_no_kab()."  AND MATI_LUAR_NEGERI = 'T' AND MATI_DOM_MATI = 'D') N_MATI,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_KAWIN".$this->get_siak_dblink()."  WHERE  TO_CHAR(KAWIN_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND ADM_NO_PROV =32AND ADM_NO_KAB = ".$this->get_no_kab().") N_KAWIN,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_CERAI".$this->get_siak_dblink()."  WHERE  TO_CHAR(CERAI_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND ADM_NO_PROV =32AND ADM_NO_KAB = ".$this->get_no_kab().") N_CERAI,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_LAHIR".$this->get_siak_dblink()." A WHERE TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.ADM_AKTA_NO LIKE '%LU%' AND EXISTS(SELECT 1 FROM BIODATA_WNI".$this->get_siak_dblink()." B WHERE A.BAYI_NIK =B.NIK) AND A.ADM_NO_PROV=32AND A.ADM_NO_KAB= ".$this->get_no_kab()." AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D') Y_LAHIR_U,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_LAHIR".$this->get_siak_dblink()." A WHERE TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.ADM_AKTA_NO LIKE '%LT%' AND EXISTS(SELECT 1 FROM BIODATA_WNI".$this->get_siak_dblink()." B WHERE A.BAYI_NIK =B.NIK) AND A.ADM_NO_PROV=32AND A.ADM_NO_KAB= ".$this->get_no_kab()." AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D') Y_LAHIR_T,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_MATI".$this->get_siak_dblink()."  WHERE  TO_CHAR(PLPR_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND ADM_NO_PROV=32AND ADM_NO_KAB= ".$this->get_no_kab()."  AND MATI_LUAR_NEGERI = 'T' AND MATI_DOM_MATI = 'D') Y_MATI,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_KAWIN".$this->get_siak_dblink()."  WHERE  TO_CHAR(KAWIN_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND ADM_NO_PROV =32AND ADM_NO_KAB = ".$this->get_no_kab().") Y_KAWIN,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_CERAI".$this->get_siak_dblink()."  WHERE  TO_CHAR(CERAI_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND ADM_NO_PROV =32AND ADM_NO_KAB = ".$this->get_no_kab().") Y_CERAI
                        FROM DUAL";
        		$q = $this->yzdb->query($sql);
                $r = $q->row(0);
                $output = array(
                   "capil_nulahir"=>$r->N_LAHIR_U,
                   "capil_ntlahir"=>$r->N_LAHIR_T,
                   "capil_nmati"=>$r->N_MATI,
                   "capil_nkawin"=>$r->N_KAWIN,
                   "capil_ncerai"=>$r->N_CERAI,
                   "capil_yulahir"=>$r->Y_LAHIR_U,
                   "capil_ytlahir"=>$r->Y_LAHIR_T,
                   "capil_ymati"=>$r->Y_MATI,
                   "capil_ykawin"=>$r->Y_KAWIN,
                   "capil_ycerai"=>$r->Y_CERAI
                );
                return $output;
        }
         public function get_mobilitas()
        {
                $sql = "SELECT
                        (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.FROM_NO_PROP = ".$this->get_no_prop()." AND A.FROM_NO_KAB = ".$this->get_no_kab().") N_PINDAH_H,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.FROM_NO_PROP = ".$this->get_no_prop()." AND A.FROM_NO_KAB = ".$this->get_no_kab().") N_PINDAH_D,
                        (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab().") N_DATANG_H,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab().") N_DATANG_D,
                        (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.FROM_NO_PROP = ".$this->get_no_prop()." AND A.FROM_NO_KAB = ".$this->get_no_kab().") Y_PINDAH_H,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.FROM_NO_PROP = ".$this->get_no_prop()." AND A.FROM_NO_KAB = ".$this->get_no_kab().") Y_PINDAH_D,
                        (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab().") Y_DATANG_H,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab().") Y_DATANG_D
                        FROM DUAL";
                $q = $this->yzdb->query($sql);
                $r = $q->row(0);
                $output = array(
                   "pindah_hn"=>$r->N_PINDAH_H,
                   "pindah_dn"=>$r->N_PINDAH_D,
                   "datang_hn"=>$r->N_DATANG_H,
                   "datang_dn"=>$r->N_DATANG_D,
                   "pindah_hy"=>$r->Y_PINDAH_H,
                   "pindah_dy"=>$r->Y_PINDAH_D,
                   "datang_hy"=>$r->Y_DATANG_H,
                   "datang_dy"=>$r->Y_DATANG_D
                );
                return $output;
        }
        public function get_mobilitas_detail()
        {
                $sql = "SELECT
                        (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.FROM_NO_PROP = ".$this->get_no_prop()."  AND A.FROM_NO_KAB = ".$this->get_no_kab().") N_PINDAH_H_ANTARKAB,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.FROM_NO_PROP = ".$this->get_no_prop()."  AND A.FROM_NO_KAB = ".$this->get_no_kab().") N_PINDAH_D_ANTARKAB,
                            (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.FROM_NO_PROP = ".$this->get_no_prop()."  AND A.FROM_NO_KAB = ".$this->get_no_kab().") N_PINDAH_H_ANTARKEC,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.FROM_NO_PROP = ".$this->get_no_prop()."  AND A.FROM_NO_KAB = ".$this->get_no_kab().") N_PINDAH_D_ANTARKEC,
                            (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.FROM_NO_PROP = ".$this->get_no_prop()."  AND A.FROM_NO_KAB = ".$this->get_no_kab().") N_PINDAH_H_DALAMKEC,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.FROM_NO_PROP = ".$this->get_no_prop()."  AND A.FROM_NO_KAB = ".$this->get_no_kab().") N_PINDAH_D_DALAMKEC,
                        (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.NO_PROP = ".$this->get_no_prop()."  AND A.NO_KAB = ".$this->get_no_kab().") N_DATANG_H_ANTARKAB,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.NO_PROP = ".$this->get_no_prop()."  AND A.NO_KAB = ".$this->get_no_kab().") N_DATANG_D_ANTARKAB,
                            (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.NO_PROP = ".$this->get_no_prop()."  AND A.NO_KAB = ".$this->get_no_kab().") N_DATANG_H_ANTARKEC,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.NO_PROP = ".$this->get_no_prop()."  AND A.NO_KAB = ".$this->get_no_kab().") N_DATANG_D_ANTARKEC,
                            (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.NO_PROP = ".$this->get_no_prop()."  AND A.NO_KAB = ".$this->get_no_kab().") N_DATANG_H_DALAMKEC,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.NO_PROP = ".$this->get_no_prop()."  AND A.NO_KAB = ".$this->get_no_kab().") N_DATANG_D_DALAMKEC,
                        (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.FROM_NO_PROP = ".$this->get_no_prop()."  AND A.FROM_NO_KAB = ".$this->get_no_kab().") Y_PINDAH_H_ANTARKAB,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.FROM_NO_PROP = ".$this->get_no_prop()."  AND A.FROM_NO_KAB = ".$this->get_no_kab().") Y_PINDAH_D_ANTARKAB,
                            (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.FROM_NO_PROP = ".$this->get_no_prop()."  AND A.FROM_NO_KAB = ".$this->get_no_kab().") Y_PINDAH_H_ANTARKEC,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.FROM_NO_PROP = ".$this->get_no_prop()."  AND A.FROM_NO_KAB = ".$this->get_no_kab().") Y_PINDAH_D_ANTARKEC,
                            (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.FROM_NO_PROP = ".$this->get_no_prop()."  AND A.FROM_NO_KAB = ".$this->get_no_kab().") Y_PINDAH_H_DALAMKEC,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER".$this->get_siak_dblink()." A INNER JOIN PINDAH_DETAIL".$this->get_siak_dblink()." B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.FROM_NO_PROP = ".$this->get_no_prop()."  AND A.FROM_NO_KAB = ".$this->get_no_kab().") Y_PINDAH_D_DALAMKEC,
                        (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.NO_PROP = ".$this->get_no_prop()."  AND A.NO_KAB = ".$this->get_no_kab().") Y_DATANG_H_ANTARKAB,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.NO_PROP = ".$this->get_no_prop()."  AND A.NO_KAB = ".$this->get_no_kab().") Y_DATANG_D_ANTARKAB,
                            (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.NO_PROP = ".$this->get_no_prop()."  AND A.NO_KAB = ".$this->get_no_kab().") Y_DATANG_H_ANTARKEC,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.NO_PROP = ".$this->get_no_prop()."  AND A.NO_KAB = ".$this->get_no_kab().") Y_DATANG_D_ANTARKEC,
                            (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.NO_PROP = ".$this->get_no_prop()."  AND A.NO_KAB = ".$this->get_no_kab().") Y_DATANG_H_DALAMKEC,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER".$this->get_siak_dblink()." A INNER JOIN DATANG_DETAIL".$this->get_siak_dblink()." B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.NO_PROP = ".$this->get_no_prop()."  AND A.NO_KAB = ".$this->get_no_kab().") Y_DATANG_D_DALAMKEC
                        FROM DUAL";
                $q = $this->yzdb->query($sql);
                $r = $q->row(0);
                $output = array(
                   "pindah_hn_akab"=>$r->N_PINDAH_H_ANTARKAB,
                   "pindah_dn_akab"=>$r->N_PINDAH_D_ANTARKAB,
                   "pindah_hn_akec"=>$r->N_PINDAH_H_ANTARKEC,
                   "pindah_dn_akec"=>$r->N_PINDAH_D_ANTARKEC,
                   "pindah_hn_dkec"=>$r->N_PINDAH_H_DALAMKEC,
                   "pindah_dn_dkec"=>$r->N_PINDAH_D_DALAMKEC,
                   "datang_hn_akab"=>$r->N_DATANG_H_ANTARKAB,
                   "datang_dn_akab"=>$r->N_DATANG_D_ANTARKAB,
                   "datang_hn_akec"=>$r->N_DATANG_H_ANTARKEC,
                   "datang_dn_akec"=>$r->N_DATANG_D_ANTARKEC,
                   "datang_hn_dkec"=>$r->N_DATANG_H_DALAMKEC,
                   "datang_dn_dkec"=>$r->N_DATANG_D_DALAMKEC,
                   "pindah_hy_akab"=>$r->Y_PINDAH_H_ANTARKAB,
                   "pindah_dy_akab"=>$r->Y_PINDAH_D_ANTARKAB,
                   "pindah_hy_akec"=>$r->Y_PINDAH_H_ANTARKEC,
                   "pindah_dy_akec"=>$r->Y_PINDAH_D_ANTARKEC,
                   "pindah_hy_dkec"=>$r->Y_PINDAH_H_DALAMKEC,
                   "pindah_dy_dkec"=>$r->Y_PINDAH_D_DALAMKEC,
                   "datang_hy_akab"=>$r->Y_DATANG_H_ANTARKAB,
                   "datang_dy_akab"=>$r->Y_DATANG_D_ANTARKAB,
                   "datang_hy_akec"=>$r->Y_DATANG_H_ANTARKEC,
                   "datang_dy_akec"=>$r->Y_DATANG_D_ANTARKEC,
                   "datang_hy_dkec"=>$r->Y_DATANG_H_DALAMKEC,
                   "datang_dy_dkec"=>$r->Y_DATANG_D_DALAMKEC
                );
                return $output;
        }

        public function get_biodata()
        {
                $sql = "SELECT
                        (SELECT COUNT(DISTINCT(B.NO_KK)) AS JUMLAH from DATA_KELUARGA".$this->get_siak_dblink()." B INNER JOIN BIODATA_WNI".$this->get_siak_dblink()." A ON A.NO_KK = B.NO_KK WHERE 
                        A.FLAG_STATUS = 0 AND TO_CHAR(B.TGL_INSERTION,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'dd/MM/yyyy')) N_KK,
                        (select COUNT(1) AS COUNT from BIODATA_WNI".$this->get_siak_dblink()." WHERE TO_CHAR(TGL_ENTRI,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'dd/MM/yyyy')) N_BIO,
                        (select COUNT(DISTINCT(B.NO_KK)) AS JUMLAH from DATA_KELUARGA".$this->get_siak_dblink()." B INNER JOIN BIODATA_WNI".$this->get_siak_dblink()." A ON A.NO_KK = B.NO_KK WHERE 
                        A.FLAG_STATUS = 0 AND TO_CHAR(B.TGL_INSERTION,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'dd/MM/yyyy')) Y_KK,
                        (select COUNT(1) AS COUNT from BIODATA_WNI".$this->get_siak_dblink()." WHERE TO_CHAR(TGL_ENTRI,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'dd/MM/yyyy')) Y_BIO
                        FROM DUAL";
                $q = $this->yzdb->query($sql);
                $r = $q->row(0);
                $output = array(
                   "kk_n"=>$r->N_KK,
                   "bio_n"=>$r->N_BIO,
                   "kk_y"=>$r->Y_KK,
                   "bio_y"=>$r->Y_BIO
                  
                );
                return $output;
        }
        function get_no_prop()
        {
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_prop();
        }
        function get_no_kab()
        {
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_kab();
        }
        function get_siak_dblink()
        {
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_siak_dblink();
        }
        function get_rekam_dblink()
        {
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_rekam_dblink();
        }
        function get_cetak_dblink()
        {
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cetak_dblink();
        }
        function get_master_dblink()
        {
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_master_dblink();
        }
}