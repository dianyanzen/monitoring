<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Laporankk extends CI_Model {
		function __construct()
		{
			parent:: __construct();
		}

		public function get_data_kk($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						C.NO_KEC,
						C.NAMA_KEC,
						COUNT(1) JUMLAH
						FROM T5_SEQN_PRINT_KK B
						INNER JOIN SETUP_KEC C 
						ON C.NO_PROP = B.NO_PROP
						AND C.NO_KAB = B.NO_KAB
						AND C.NO_KEC = B.NO_KEC
						WHERE 
						B.PRINT_TYPE =0 AND
						B.PRINTED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND B.PRINTED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
						GROUP BY C.NO_KEC, C.NAMA_KEC ORDER BY C.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
						FROM SETUP_KEL A LEFT JOIN
						(SELECT 
							C.NO_KEL,
							C.NAMA_KEL,
							COUNT(1) JUMLAH
							FROM T5_SEQN_PRINT_KK B
							INNER JOIN SETUP_KEL C 
							ON C.NO_PROP = B.NO_PROP
							AND C.NO_KAB = B.NO_KAB
							AND C.NO_KEC = B.NO_KEC
							AND C.NO_KEL = B.NO_KEL
							WHERE 
							B.PRINT_TYPE =0 AND
							B.PRINTED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND B.PRINTED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." and  C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				
				$q = $this->db->query($sql);
               return $q->result();


		}

		public function get_jumlah_kk($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
							COUNT(1) JUMLAH
							FROM T5_SEQN_PRINT_KK A
							WHERE 
							A.PRINT_TYPE =0 AND
							A.PRINTED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.PRINTED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND A.NO_PROP = 32 AND A.NO_KAB = 73";	
			}else{
				$sql .="SELECT 
							COUNT(1) JUMLAH
							FROM T5_SEQN_PRINT_KK A
							WHERE 
							A.PRINT_TYPE =0 AND
							A.PRINTED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.PRINTED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel." ";
						}
				}
				$q = $this->db->query($sql);
               return $q->result();


		}
		function get_no_prop()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_prop();
    	}
    	function get_no_kab()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_kab();
    	}
    	function get_siak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_siak_dblink();
    	}
    	function get_rekam_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_rekam_dblink();
    	}
    	function get_cetak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cetak_dblink();
    	}
    	function get_master_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_master_dblink();
    	}
}