<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Android extends CI_Model {
		function __construct()
		{
			parent:: __construct();
			$this->db2 = $this->load->database('DB2', TRUE);
			$this->db221 = $this->load->database('DB221', TRUE);
			$this->yzdb = $this->load->database('YZDB', TRUE);
			$this->smsdb = $this->load->database('SMSDB', TRUE);
			$this->dbprov = $this->load->database('DBPROV', TRUE);
		}
		public function get_count_suket($nik){
			$sql = "SELECT COUNT(1) AS JML FROM BIODATA_WNI WHERE NIK = $nik";
			$q = $this->db->query($sql);
			return $q->row(0)->JML;
		}
		public function get_count_suket_more($nik){
			$sql = "SELECT COUNT(1) AS JML FROM BIODATA_WNI WHERE NIK in ($nik)";
			$q = $this->db->query($sql);
			return $q->row(0)->JML;
		}
		public function get_count_suket_prov($nik){
			$sql = "SELECT COUNT(1) AS JML FROM BIODATA_WNI WHERE NIK = $nik";
			$q = $this->dbprov->query($sql);
			return $q->row(0)->JML;
		}
		public function get_data_suket($nik){
			$sql = " SELECT 
					  B.NIK
					  , B.NO_KK
					  , B.NAMA_LGKP
					  , B.TMPT_LHR
					  , TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					  , TRUNC(MONTHS_BETWEEN(SYSDATE,B.TGL_LHR)/12) UMUR
					  , CASE WHEN B.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN
					  , D.ALAMAT
					  , LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT
					  , LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW
					  , F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					  , F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.AGAMA AND SECT =501) AS AGAMA
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.JENIS_PKRJN AND SECT =201) AS JENIS_PKRJN
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.STAT_HBKEL AND SECT =301) AS STAT_HBKEL
					  , CASE WHEN B.AKTA_LHR = 1 THEN 'TIDAK ADA' ELSE 'ADA' END AS AKTA_LHR
					  , CASE WHEN B.NO_AKTA_LHR IS NULL THEN '-' ELSE B.NO_AKTA_LHR END AS NO_AKTA_LHR
					  , CASE 
					    WHEN B.STAT_KWN = 2 AND B.AKTA_KWN = 1 THEN 'KAWIN TIDAK TERCATAT' 
					    WHEN B.STAT_KWN = 2 AND B.AKTA_KWN = 2 THEN 'KAWIN TERCATAT'
					    ELSE(SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.STAT_KWN AND SECT =601) END AS STAT_KWN
					  , CASE WHEN B.NO_AKTA_KWN IS NULL THEN '-' ELSE B.NO_AKTA_KWN END AS NO_AKTA_KWN
					  , CASE WHEN A.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE A.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					  , CASE WHEN F.PRINTED_BY IS NULL THEN '-' ELSE F.PRINTED_BY END AS SUKET_BY
					  , CASE WHEN TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') END AS SUKET_DT
					  , CASE WHEN  TO_CHAR(G.CREATED,'DD-MM-YYYY') IS NULL THEN '-' ELSE  TO_CHAR(G.CREATED,'DD-MM-YYYY') END AS KTP_DT
					  , CASE WHEN TO_CHAR(G.CREATED_USERNAME) IS NULL THEN '-' ELSE TO_CHAR(G.CREATED_USERNAME) END AS KTP_BY 
					  FROM BIODATA_WNI B LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK LEFT JOIN DEMOGRAPHICS_ALL A  ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM 
					  (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET WHERE TIPE = 1) WHERE RNK = 1) F ON F.NIK = B.NIK LEFT JOIN 
					  (SELECT NIK,CREATED,CREATED_USERNAME FROM (SELECT NIK,CREATED,CREATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY CREATED DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) G ON G.NIK =B.NIK WHERE B.NIK = $nik";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_data_suket_more($nik){
			$sql = " SELECT 
					  B.NIK
					  , B.NO_KK
					  , B.NAMA_LGKP
					  , B.TMPT_LHR
					  , TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					  , TRUNC(MONTHS_BETWEEN(SYSDATE,B.TGL_LHR)/12) UMUR
					  , CASE WHEN B.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN
					  , D.ALAMAT
					  , LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT
					  , LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW
					  , F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					  , F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.AGAMA AND SECT =501) AS AGAMA
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.JENIS_PKRJN AND SECT =201) AS JENIS_PKRJN
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.STAT_HBKEL AND SECT =301) AS STAT_HBKEL
					  , CASE WHEN B.AKTA_LHR = 1 THEN 'TIDAK ADA' ELSE 'ADA' END AS AKTA_LHR
					  , CASE WHEN B.NO_AKTA_LHR IS NULL THEN '-' ELSE B.NO_AKTA_LHR END AS NO_AKTA_LHR
					  , CASE 
					    WHEN B.STAT_KWN = 2 AND B.AKTA_KWN = 1 THEN 'KAWIN TIDAK TERCATAT' 
					    WHEN B.STAT_KWN = 2 AND B.AKTA_KWN = 2 THEN 'KAWIN TERCATAT'
					    ELSE(SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.STAT_KWN AND SECT =601) END AS STAT_KWN
					  , CASE WHEN B.NO_AKTA_KWN IS NULL THEN '-' ELSE B.NO_AKTA_KWN END AS NO_AKTA_KWN
					  , CASE WHEN A.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE A.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					  , CASE WHEN F.PRINTED_BY IS NULL THEN '-' ELSE F.PRINTED_BY END AS SUKET_BY
					  , CASE WHEN TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') END AS SUKET_DT
					  , CASE WHEN  TO_CHAR(G.CREATED,'DD-MM-YYYY') IS NULL THEN '-' ELSE  TO_CHAR(G.CREATED,'DD-MM-YYYY') END AS KTP_DT
					  , CASE WHEN TO_CHAR(G.CREATED_USERNAME) IS NULL THEN '-' ELSE TO_CHAR(G.CREATED_USERNAME) END AS KTP_BY 
					  FROM BIODATA_WNI B LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK LEFT JOIN DEMOGRAPHICS_ALL A  ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM 
					  (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET WHERE TIPE = 1) WHERE RNK = 1) F ON F.NIK = B.NIK LEFT JOIN 
					  (SELECT NIK,CREATED,CREATED_USERNAME FROM (SELECT NIK,CREATED,CREATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY CREATED DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) G ON G.NIK =B.NIK WHERE B.NIK IN ($nik)";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_data_suket_prov($nik){
			$sql = " SELECT 
					  B.NIK
					  , B.NO_KK
					  , B.NAMA_LGKP
					  , B.TMPT_LHR
					  , TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					  , TRUNC(MONTHS_BETWEEN(SYSDATE,B.TGL_LHR)/12) UMUR
					  , CASE WHEN B.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN
					  , D.ALAMAT
					  , LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT
					  , LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW
					  , F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					  , F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.AGAMA AND SECT =501) AS AGAMA
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.JENIS_PKRJN AND SECT =201) AS JENIS_PKRJN
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.STAT_HBKEL AND SECT =301) AS STAT_HBKEL
					  , CASE WHEN B.AKTA_LHR = 1 THEN 'TIDAK ADA' ELSE 'ADA' END AS AKTA_LHR
					  , CASE WHEN B.NO_AKTA_LHR IS NULL THEN '-' ELSE B.NO_AKTA_LHR END AS NO_AKTA_LHR
					  , CASE 
					    WHEN B.STAT_KWN = 2 AND B.AKTA_KWN = 1 THEN 'KAWIN TIDAK TERCATAT' 
					    WHEN B.STAT_KWN = 2 AND B.AKTA_KWN = 2 THEN 'KAWIN TERCATAT'
					    ELSE(SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.STAT_KWN AND SECT =601) END AS STAT_KWN
					  , CASE WHEN B.NO_AKTA_KWN IS NULL THEN '-' ELSE B.NO_AKTA_KWN END AS NO_AKTA_KWN
					  , CASE WHEN A.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE A.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					  , CASE WHEN F.PRINTED_BY IS NULL THEN '-' ELSE F.PRINTED_BY END AS SUKET_BY
					  , CASE WHEN TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') END AS SUKET_DT
					  , CASE WHEN  TO_CHAR(G.CREATED,'DD-MM-YYYY') IS NULL THEN '-' ELSE  TO_CHAR(G.CREATED,'DD-MM-YYYY') END AS KTP_DT
					  , CASE WHEN TO_CHAR(G.CREATED_USERNAME) IS NULL THEN '-' ELSE TO_CHAR(G.CREATED_USERNAME) END AS KTP_BY 
					  FROM BIODATA_WNI B LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK LEFT JOIN DEMOGRAPHICS A  ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM 
					  (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET WHERE TIPE = 1) WHERE RNK = 1) F ON F.NIK = B.NIK LEFT JOIN 
					  (SELECT NIK,CREATED,CREATED_USERNAME FROM (SELECT NIK,CREATED,CREATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY CREATED DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) G ON G.NIK =B.NIK WHERE B.NIK = $nik";
			$q = $this->dbprov->query($sql);
			return $q->result();
		}
		public function get_count_bio($nik){
			$sql = "SELECT COUNT(1) AS JML FROM BIODATA_WNI WHERE NIK = $nik";
			$q = $this->smsdb->query($sql);
			return $q->row(0)->JML;
		}
		public function get_data_bio($nik){
			$sql = "SELECT 
					  B.NIK
					  , B.NO_KK
					  , B.NAMA_LGKP
					  , B.TMPT_LHR
					  , TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					  , CASE WHEN B.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN
					  , D.ALAMAT
					  , LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT
					  , LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW
					  , F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					  , F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.AGAMA AND SECT =501) AS AGAMA
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.JENIS_PKRJN AND SECT =201) AS JENIS_PKRJN
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.STAT_HBKEL AND SECT =301) AS STAT_HBKEL
					  , CASE WHEN B.AKTA_LHR = 1 THEN 'TIDAK ADA' ELSE 'ADA' END AS AKTA_LHR
					  , CASE WHEN B.NO_AKTA_LHR IS NULL THEN '-' ELSE B.NO_AKTA_LHR END AS NO_AKTA_LHR
					  , CASE 
					    WHEN B.STAT_KWN = 2 AND B.AKTA_KWN = 1 THEN 'KAWIN TIDAK TERCATAT' 
					    WHEN B.STAT_KWN = 2 AND B.AKTA_KWN = 2 THEN 'KAWIN TERCATAT'
					    ELSE(SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.STAT_KWN AND SECT =601) END AS STAT_KWN
					  , CASE WHEN B.NO_AKTA_KWN IS NULL THEN '-' ELSE B.NO_AKTA_KWN END AS NO_AKTA_KWN
					  , CASE WHEN A.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE A.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					  FROM BIODATA_WNI B LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK LEFT JOIN DEMOGRAPHICS_ALL".$this->get_cetak_dblink()." A  ON A.NIK = B.NIK  WHERE B.NIK = $nik";
			$q = $this->smsdb->query($sql);
			return $q->result();
		}
		public function sent_sms($phone,$message,$modem){
			$sql = "INSERT INTO SMS_SIAP_KIRIM (TELEPON,ISI_PESAN,TANGGAL_SMS,SERVER) VALUES ('$phone','$message',(SELECT MAX(TANGGAL_SMS) TGL FROM SMS_ANTRIAN_KELUAR),'2')";
			$q = $this->smsdb->query($sql);
		}

		/*public function get_dashboard_m1(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT COUNT(1) JML FROM DEMOGRAPHICS A WHERE EXISTS(SELECT 1 FROM FACES B WHERE A.NIK = B.NIK) AND A.CREATED >= TO_DATE('$nowdate','DD/MM/yyyy') AND A.CREATED < TO_DATE('$nowdate','DD/MM/yyyy')+1
					";
			$q = $this->db221->query($sql);
			return $q->result();
		}
		public function get_dashboard_m2(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT COUNT(1) AS JML
                    FROM CARD_MANAGEMENT A 
                    WHERE CASE WHEN TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE,'DD/MM/YYYY') 
                    AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END  NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB WHERE IS_ACTIVE = 1)
					";
			$q = $this->db2->query($sql);
			return $q->result();
		}
		public function get_dashboard_m3(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
                    COUNT(1) AS JML
                    FROM DEMOGRAPHICS".$this->get_rekam_dblink()." A 
                    INNER JOIN BIODATA_WNI B ON A.NIK = B.NIK
                    LEFT JOIN DATA_KELUARGA C ON B.NO_KK = C.NO_KK
                    WHERE A.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD' 
                    AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
                    AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
                    AND B.NO_PROP = ".$this->get_no_prop()." AND B.NO_KAB =".$this->get_no_kab()."  
                    AND NOT EXISTS(SELECT 1 FROM CARD_MANAGEMENT".$this->get_cetak_dblink()." D WHERE A.NIK = D.NIK)
                    AND NOT EXISTS(SELECT 1 FROM DEMOGRAPHICS".$this->get_cetak_dblink()." D WHERE A.NIK = D.NIK AND D.CURRENT_STATUS_CODE LIKE '%CARD%')
					";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_dashboard_m4(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
                    COUNT(1) AS JML
                    FROM DEMOGRAPHICS".$this->get_rekam_dblink()." A 
                    INNER JOIN BIODATA_WNI B ON A.NIK = B.NIK
                    LEFT JOIN DATA_KELUARGA C ON B.NO_KK = C.NO_KK
                    WHERE A.CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT' 
                    AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
                    AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
                    AND B.NO_PROP = ".$this->get_no_prop()." AND B.NO_KAB =".$this->get_no_kab()."  
                    AND NOT EXISTS(SELECT 1 FROM CARD_MANAGEMENT".$this->get_cetak_dblink()." D WHERE A.NIK = D.NIK)
                    AND NOT EXISTS(SELECT 1 FROM DEMOGRAPHICS".$this->get_cetak_dblink()." D WHERE A.NIK = D.NIK AND D.CURRENT_STATUS_CODE LIKE '%CARD%')
					";
			$q = $this->db->query($sql);
			return $q->result();
		}*/
		public function get_dashboard_m1(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PEREKAMAN'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_m2(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
				CASE WHEN  (SELECT (D.AMBIL-D.RUSAK-D.PENGEMBALIAN) CETAK_LUDO FROM SIAK_MONEV_BLANGKO_LUDO D WHERE TRUNC(D.TANGGAL) = TRUNC(SYSDATE)) IS NOT NULL THEN 
				(CASE WHEN B.VAL IS NULL THEN 0 ELSE B.VAL END) + 
				(SELECT (D.AMBIL-D.RUSAK-D.PENGEMBALIAN) CETAK_LUDO FROM SIAK_MONEV_BLANGKO_LUDO D WHERE TRUNC(D.TANGGAL) = TRUNC(SYSDATE)) ELSE CASE WHEN B.VAL IS NULL THEN 0 ELSE B.VAL END END AS JML
				FROM VW_SIAK_DASHBOARD B WHERE ID = 'GET_PENCETAKAN_KTP'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_m3(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PRR'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_m4(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_SFE'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_m5(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_SISA_SUKET'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_m6(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_BLANGKO_OUT'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_m7(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_DUPLICATE'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}	
		public function get_dashboard_m8(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_BLANGKO_SISA'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}	
		public function dashboard_yesterday(){
			$sql = "SELECT 'PEREKAMAN' KETERANGAN,SUM( CASE WHEN TO_CHAR(A.CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS JML FROM DEMOGRAPHICS".$this->get_rekam_dblink()." A WHERE EXISTS(SELECT 1 FROM FACES".$this->get_rekam_dblink()." B WHERE A.NIK = B.NIK)
					UNION ALL
					SELECT 'PENCETAKAN',SUM( CASE WHEN TO_CHAR(A.PERSONALIZED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS JML FROM CARD_MANAGEMENT".$this->get_cetak_dblink()." A WHERE EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.NIK =B.NIK) AND (UPPER(A.CREATED_USERNAME) NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB WHERE IS_ACTIVE = 1) OR UPPER(A.LAST_UPDATED_USERNAME) NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB WHERE IS_ACTIVE = 1))
					UNION ALL
					SELECT 'PRR', COUNT(1) AS JML FROM DEMOGRAPHICS".$this->get_rekam_dblink()." A WHERE EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.NIK = B.NIK AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NO_PROP = ".$this->get_no_prop()." AND B.NO_KAB = ".$this->get_no_kab().") AND A.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD'
					UNION ALL
					SELECT 'SFE', COUNT(1) AS JML FROM DEMOGRAPHICS".$this->get_rekam_dblink()." A WHERE EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.NIK = B.NIK AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NO_PROP = ".$this->get_no_prop()." AND B.NO_KAB = ".$this->get_no_kab().") AND A.CURRENT_STATUS_CODE LIKE 'SENT_FOR_ENROLLMENT'
					UNION ALL
					SELECT 'SISA_SUKET' ,COUNT(1) JML FROM BIODATA_WNI A WHERE (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3))) AND EXISTS 
					(SELECT 1 FROM (SELECT NIK FROM (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET) WHERE RNK = 1) A 
					WHERE NOT EXISTS (SELECT 1 FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY PERSONALIZED_DATE DESC) RNK FROM CARD_MANAGEMENT".$this->get_cetak_dblink().") WHERE RNK = 1) B WHERE A.NIK = B.NIK) AND EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL".$this->get_cetak_dblink()." C WHERE A.NIK = C.NIK AND C.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND C.CURRENT_STATUS_CODE NOT LIKE '%DUPLICATE%')) B WHERE A.NIK = B.NIK)
					UNION ALL 
					SELECT 'SISA_BLANGKO',CASE WHEN (SELECT SUM(JUMLAH) AS JUMLAH FROM SIAK_BLANGKO)  -(SELECT SUM(AMBIL) - (SUM(PENGEMBALIAN))AS JUMLAH  FROM SIAK_MONEV_BLANGKO WHERE TANGGAL > (SELECT TANGGAL_MASUK FROM (SELECT TANGGAL_MASUK FROM (SELECT TANGGAL_MASUK, ROWNUM AS URUT FROM(SELECT TANGGAL_MASUK FROM SIAK_BLANGKO order by TANGGAL_MASUK)) WHERE URUT =1)) AND TANGGAL < TO_DATE(SYSDATE-1,'DD/MM/YYYY')+1)  IS NULL THEN 0 ELSE (SELECT SUM(JUMLAH) AS JUMLAH FROM SIAK_BLANGKO)  -(SELECT SUM(AMBIL) - (SUM(PENGEMBALIAN))AS JUMLAH  FROM SIAK_MONEV_BLANGKO WHERE TANGGAL > (SELECT TANGGAL_MASUK FROM (SELECT TANGGAL_MASUK FROM (SELECT TANGGAL_MASUK, ROWNUM AS URUT FROM(SELECT TANGGAL_MASUK FROM SIAK_BLANGKO order by TANGGAL_MASUK)) WHERE URUT =1)) AND TANGGAL < TO_DATE(SYSDATE-1,'DD/MM/YYYY')+1) END AS JML  FROM DUAL
					UNION ALL
					SELECT 'DUPLICATE', COUNT(1) AS JML FROM DEMOGRAPHICS".$this->get_rekam_dblink()." A WHERE EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.NIK = B.NIK AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NO_PROP = ".$this->get_no_prop()." AND B.NO_KAB = ".$this->get_no_kab().") AND A.CURRENT_STATUS_CODE LIKE 'DUPLICATE_RECORD'
          			UNION ALL
					SELECT 'FAILURE', COUNT(1) AS JML FROM DEMOGRAPHICS".$this->get_rekam_dblink()." A WHERE EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.NIK = B.NIK AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NO_PROP = ".$this->get_no_prop()." AND B.NO_KAB = ".$this->get_no_kab().") AND (A.CURRENT_STATUS_CODE LIKE '%FAILURE%' OR  A.CURRENT_STATUS_CODE LIKE '%ADJUDICATE%')
					UNION ALL
					SELECT 'BLANGKO_OUT', CASE WHEN SUM(A.AMBIL) - (SUM(A.PENGEMBALIAN)) IS NULL THEN 0 ELSE SUM(A.AMBIL) - (SUM(A.PENGEMBALIAN)) END AS JML
					FROM SIAK_MONEV_BLANGKO A WHERE  TO_CHAR(A.TANGGAL,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function sisa_suket(){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, B.NO_KEC
					, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, B.NO_KEL
					, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
          			, B.EKTP_CURRENT_STATUS_CODE
					FROM  BIODATA_WNI B 
					WHERE (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) AND EXISTS 
					(SELECT 1 FROM (SELECT NIK FROM (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET WHERE TIPE = 1) WHERE RNK = 1) X 
					WHERE NOT EXISTS (SELECT 1 FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY PERSONALIZED_DATE DESC) RNK FROM CARD_MANAGEMENT".$this->get_cetak_dblink().") WHERE RNK = 1) Y WHERE X.NIK = Y.NIK) AND EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL".$this->get_cetak_dblink()." Z WHERE X.NIK = Z.NIK AND Z.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND Z.CURRENT_STATUS_CODE NOT LIKE '%DUPLICATE%')) X WHERE X.NIK = B.NIK)
					";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function jumlah_sisa_suket(){
			$sql = "SELECT 
					COUNT(1) AS JML
					FROM  BIODATA_WNI B 
					WHERE (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) AND EXISTS 
					(SELECT 1 FROM (SELECT NIK FROM (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET WHERE TIPE = 1) WHERE RNK = 1) X 
					WHERE NOT EXISTS (SELECT 1 FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY PERSONALIZED_DATE DESC) RNK FROM CARD_MANAGEMENT".$this->get_cetak_dblink().") WHERE RNK = 1) Y WHERE X.NIK = Y.NIK) AND EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL".$this->get_cetak_dblink()." Z WHERE X.NIK = Z.NIK AND Z.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND Z.CURRENT_STATUS_CODE NOT LIKE '%DUPLICATE%')) X WHERE X.NIK = B.NIK)
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}		
		public function get_contact_kec(){
			$sql = "SELECT NIK,NAMA_LGKP,NAMA_KANTOR,TO_CHAR(TGL_LHR,'DD-MM-YYYY') AS TGL_LHR, TMPT_LHR,TELP, ALAMAT_RUMAH FROM SIAK_USER_PLUS  ORDER BY USER_LEVEL, TO_NUMBER(NO_KEC), NAMA_LGKP";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}	
		public function get_contact_help(){
			$sql = "SELECT NIK,NAMA_LGKP,NAMA_KANTOR,TO_CHAR(TGL_LHR,'DD-MM-YYYY') AS TGL_LHR, TMPT_LHR,TELP,ALAMAT_RUMAH FROM SIAK_USER_PLUS WHERE USER_LEVEL = 5 AND IS_MONITORING = 1 AND  IS_SHOW = 1 ORDER BY to_date(TGL_LHR,'DD-MM-YYYY') ";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}		
		public function get_kec($user_id){
			$sql = "SELECT NO_KEC FROM SIAK_USER_PLUS WHERE IS_GISA =1 AND USER_ID = '$user_id'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			$no_kec = $r[0]->NO_KEC;
			if ($no_kec > 0){
				$sql = "SELECT NO_KEC, NAMA_KEC FROM SETUP_KEC A WHERE A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab()." AND A.NO_KEC = $no_kec ORDER BY NO_KEC";
			}else{
				$sql = "SELECT NO_KEC, NAMA_KEC FROM SETUP_KEC A WHERE A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab()." ORDER BY NO_KEC";
			}
			$q = $this->yzdb->query($sql);
			return $q->result();
		}	
		public function get_kel($user_id,$no_kec){
			$sql = "SELECT NO_KEL, NO_KEC FROM SIAK_USER_PLUS WHERE IS_GISA =1 AND USER_ID = '$user_id'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			$no_kel = $r[0]->NO_KEL;
			if ($no_kel > 0){
				$sql = "SELECT NO_KEL, NAMA_KEL FROM SETUP_KEL A WHERE A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab()." AND A.NO_KEC = $no_kec AND A.NO_KEL = $no_kel ORDER BY NO_KEL";
			}else{
				$sql = "SELECT NO_KEL, NAMA_KEL FROM SETUP_KEL A WHERE A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab()." AND A.NO_KEC = $no_kec ORDER BY NO_KEL";
			}
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_rw($user_id,$no_kec,$no_kel){
			$sql = "SELECT NO_KEL, NO_KEC, NO_RW FROM SIAK_USER_PLUS WHERE IS_GISA =1 AND USER_ID = '$user_id'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			$no_rw = $r[0]->NO_RW;
			if ($no_rw > 0){
				$sql = "SELECT NO_KEC, NO_KEL, NO_RW, JML FROM (SELECT NO_KEC, NO_KEL,NO_RW, COUNT(1) JML FROM DATA_KELUARGA A WHERE A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab()." AND A.NO_KEC = $no_kec AND A.NO_KEL = $no_kel AND A.NO_RW = $no_rw AND NO_RW IS NOT NULL GROUP BY NO_KEC, NO_KEL, NO_RW ORDER BY NO_KEC, NO_KEL, NO_RW) WHERE JML > 50  ORDER BY NO_KEC, NO_KEL, NO_RW";

			}else{
				$sql = "SELECT NO_KEC, NO_KEL, NO_RW, JML FROM (SELECT NO_KEC, NO_KEL,NO_RW, COUNT(1) JML FROM DATA_KELUARGA A WHERE A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab()." AND A.NO_KEC = $no_kec AND A.NO_KEL = $no_kel AND NO_RW IS NOT NULL GROUP BY NO_KEC, NO_KEL, NO_RW ORDER BY NO_KEC, NO_KEL, NO_RW) WHERE JML > 50  ORDER BY NO_KEC, NO_KEL, NO_RW";
			}
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_rt($user_id,$no_kec,$no_kel,$no_rw){
			$sql = "SELECT NO_KEL, NO_KEC, NO_RW, NO_RT FROM SIAK_USER_PLUS WHERE IS_GISA =1 AND USER_ID = '$user_id'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			$no_rt = $r[0]->NO_RT;
			if ($no_rt > 0){
				$sql = "SELECT NO_KEC, NO_KEL, NO_RW, NO_RT, JML FROM (SELECT NO_KEC, NO_KEL,NO_RW,NO_RT, COUNT(1) JML FROM DATA_KELUARGA WHERE NO_PROP=".$this->get_no_prop()." AND NO_KAB= 73 AND NO_KEC = $no_kec AND NO_KEL = $no_kel AND NO_RW = $no_rw AND NO_RT = $no_rt AND NO_RW IS NOT NULL AND NO_RT IS NOT NULL GROUP BY NO_KEC, NO_KEL, NO_RW, NO_RT ORDER BY NO_KEC, NO_KEL, NO_RW, NO_RT) WHERE JML > 5 ORDER BY NO_KEC, NO_KEL, NO_RW, NO_RT";

			}else{
				$sql = "SELECT NO_KEC, NO_KEL, NO_RW,NO_RT, JML FROM (SELECT NO_KEC, NO_KEL,NO_RW,NO_RT, COUNT(1) JML FROM DATA_KELUARGA WHERE NO_PROP=".$this->get_no_prop()." AND NO_KAB= 73 AND NO_KEC = $no_kec AND NO_KEL = $no_kel AND NO_RW = $no_rw AND NO_RW IS NOT NULL AND NO_RT IS NOT NULL GROUP BY NO_KEC, NO_KEL, NO_RW, NO_RT ORDER BY NO_KEC, NO_KEL, NO_RW, NO_RT) WHERE JML > 5 ORDER BY NO_KEC, NO_KEL, NO_RW, NO_RT";
			}
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_no_kk($no_kec = 0,$no_kel =0,$no_rw = 0,$no_rt = 0){
			$sql = "SELECT B.NO_KK, B.NAMA_KEP, (SELECT COUNT(NIK) FROM BIODATA_WNI A WHERE A.NO_KK= B.NO_KK) AS JML_KELUARGA, B.ALAMAT, LPAD(TO_CHAR(B.NO_RT), 3, '0') AS RT,LPAD(TO_CHAR(B.NO_RW), 3, '0') AS RW, TO_CHAR(B.TGL_INSERTION,'DD-MM-YYYY') AS TGL_ENTRY, B.NO_KEC, B.NO_KEL, (SELECT C.NAMA_KEC FROM SETUP_KEC C WHERE C.NO_PROP = ".$this->get_no_prop()." AND C.NO_KAB =  73 AND B.NO_KEC = C.NO_KEC) AS NAMA_KEC, (SELECT C.NAMA_KEL FROM SETUP_KEL C WHERE C.NO_PROP = ".$this->get_no_prop()." AND C.NO_KAB =  73 AND B.NO_KEC = C.NO_KEC AND B.NO_KEL = C.NO_KEL) AS NAMA_KEL  FROM BIODATA_WNI A INNER JOIN DATA_KELUARGA B ON A.NO_KK = B.NO_KK WHERE A.STAT_HBKEL =1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3))) AND B.NO_KEC = $no_kec AND B.NO_KEL = $no_kel AND B.NO_RW = $no_rw AND B.NO_RT = $no_rt ORDER BY NAMA_KEP";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r;
		}
		public function get_dashboard_kk(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_CETAK_KK'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_kia(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_CETAK_KIA'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_nik(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_NIK_BARU'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}/*public function get_dashboard_kk(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
						COUNT(1) JML
						FROM T5_SEQN_PRINT_KK B
						WHERE 
						B.PRINT_TYPE =0 AND
						TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')
            			AND B.NO_PROP = ".$this->get_no_prop()." AND B.NO_KAB = ".$this->get_no_kab()."
					";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_dashboard_kia(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
						COUNT(1) JML
						FROM T5_SEQN_KIA_PRINT B
						WHERE 
						B.PRINT_TYPE =0 AND
						TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')
            			AND B.NO_PROP = ".$this->get_no_prop()." AND B.NO_KAB = ".$this->get_no_kab()."
					";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_dashboard_nik(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT
					    COUNT(1) AS JML from BIODATA_WNI A
					    WHERE 
					     A.FLAG_STATUS = 0 AND
					    TO_CHAR(A.TGL_ENTRI,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')
					    AND A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab()."
					";
			$q = $this->db->query($sql);
			return $q->result();
		}*/
		/*public function get_dashboard_akta_lu(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PINDAH_ANTAR_KAB'
					";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_dashboard_akta_lt(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
				    COUNT(DISTINCT(A.BAYI_NO)) JML
              		FROM CAPIL_LAHIR A 
                    WHERE 1=1  AND A.ADM_NO_PROV=".$this->get_no_prop()." AND A.ADM_NO_KAB=".$this->get_no_kab()." AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D'
                    AND A.ADM_AKTA_NO LIKE '%LT%'
                    AND TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')
					";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_dashboard_akta_mt(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
				    COUNT(DISTINCT(A.MATI_NO)) JML
              		FROM CAPIL_MATI A 
                    WHERE 1=1  AND A.ADM_NO_PROV=".$this->get_no_prop()." AND A.ADM_NO_KAB=".$this->get_no_kab()." AND A.MATI_LUAR_NEGERI = 'T' AND A.MATI_DOM_MATI = 'D'
                    AND TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')
					";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_dashboard_akta_kwn(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
				    COUNT(DISTINCT(A.KAWIN_NO)) JML
              		FROM CAPIL_KAWIN A 
                    WHERE 1=1  AND A.ADM_NO_PROV=".$this->get_no_prop()." AND A.ADM_NO_KAB=".$this->get_no_kab()." 
                    AND TO_CHAR(A.KAWIN_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')
					";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_dashboard_akta_cry(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
				    COUNT(DISTINCT(A.CERAI_NO)) JML
              		FROM CAPIL_CERAI A 
                    WHERE 1=1  AND A.ADM_NO_PROV=".$this->get_no_prop()." AND A.ADM_NO_KAB=".$this->get_no_kab()." 
                    AND TO_CHAR(A.CERAI_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')
					";
			$q = $this->db->query($sql);
			return $q->result();
		}*/
		public function get_dashboard_akta_lu(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_LHR_UM'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_akta_lt(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_LHR_LT'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_akta_mt(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_MT'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_akta_kwn(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_KWN'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_akta_cry(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_CRY'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_pdh_akab(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PINDAH_ANTAR_KAB'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_pdh_akec(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PINDAH_ANTAR_KEC'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_pdh_dkec(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PINDAH_DALAM_KEC'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_dtg_akab(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_DATANG_ANTAR_KAB'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_dtg_akec(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_DATANG_ANTAR_KEC'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dashboard_dtg_dkec(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_DATANG_DALAM_KEC'
					";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}/*public function get_dashboard_pdh_akab(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
					COUNT(DISTINCT(A.NO_PINDAH)) AS JML
              		FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH 
              		WHERE 1=1 
              		AND A.KLASIFIKASI_PINDAH >3
                  	AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')
                  	AND A.FROM_NO_PROP = ".$this->get_no_prop()." AND A.FROM_NO_KAB = ".$this->get_no_kab()." 
					";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_dashboard_pdh_akec(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
					COUNT(DISTINCT(A.NO_PINDAH)) AS JML
              		FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH 
              		WHERE 1=1 
              		AND A.KLASIFIKASI_PINDAH =3
                  	AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')
                  	AND A.FROM_NO_PROP = ".$this->get_no_prop()." AND A.FROM_NO_KAB = ".$this->get_no_kab()." 
					";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_dashboard_pdh_dkec(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
					COUNT(DISTINCT(A.NO_PINDAH)) AS JML
              		FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH 
              		WHERE 1=1 
              		AND A.KLASIFIKASI_PINDAH <3
                  	AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')
                  	AND A.FROM_NO_PROP = ".$this->get_no_prop()." AND A.FROM_NO_KAB = ".$this->get_no_kab()." 
					";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_dashboard_dtg_akab(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
					COUNT(DISTINCT(A.NO_DATANG)) AS JML
              		FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG 
					WHERE 1=1 
              		AND A.KLASIFIKASI_PINDAH >3
                	AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')
					AND A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab()."
					";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_dashboard_dtg_akec(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
					COUNT(DISTINCT(A.NO_DATANG)) AS JML
              		FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG 
					WHERE 1=1 
              		AND A.KLASIFIKASI_PINDAH =3
                	AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')
					AND A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab()."
					";
			$q = $this->db->query($sql);
			return $q->result();
		}
		public function get_dashboard_dtg_dkec(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT 
					COUNT(DISTINCT(A.NO_DATANG)) AS JML
              		FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG 
					WHERE 1=1 
              		AND A.KLASIFIKASI_PINDAH <3
                	AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')
					AND A.NO_PROP = ".$this->get_no_prop()." AND A.NO_KAB = ".$this->get_no_kab()."
					";
			$q = $this->db->query($sql);
			return $q->result();
		}*/
		function get_no_prop()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_prop();
    	}
    	function get_no_kab()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_kab();
    	}
    	function get_siak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_siak_dblink();
    	}
    	function get_rekam_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_rekam_dblink();
    	}
    	function get_cetak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cetak_dblink();
    	}
    	function get_master_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_master_dblink();
    	}
}