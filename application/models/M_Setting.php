<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Setting extends CI_Model {
  function __construct()
    {
      parent:: __construct();
      $this->yzdb = $this->load->database('YZDB', TRUE);
    }
    public function get_group(){
      $sql = "SELECT LEVEL_CODE, LEVEL_NAME FROM SIAK_USER_GROUP ORDER BY LEVEL_CODE";
      $q = $this->yzdb->query($sql);
      return $q->result();
    }
    public function get_group_user(){
      $sql = "SELECT USER_LEVEL, LEVEL_NAME FROM SIAK_USER_LEVEL ORDER BY GROUP_LEVEL, USER_LEVEL";
      $q = $this->yzdb->query($sql);
      return $q->result();
    }
    public function get_atasan(){
      $sql = "SELECT PEJABAT_ID, NIP, NAMA, JABATAN,IS_STRUKTURAL FROM SIAK_PEJABAT WHERE IS_STRUKTURAL =1 ORDER BY PEJABAT_ID";
      $q = $this->yzdb->query($sql);
      return $q->result();
    }
    public function get_group_lvl($lvl){
      $sql = "SELECT USER_LEVEL, LEVEL_NAME, GROUP_LEVEL FROM SIAK_USER_LEVEL WHERE GROUP_LEVEL = $lvl ORDER BY USER_LEVEL";
      $q = $this->yzdb->query($sql);
      return $q->result();
    }
    public function get_activity_data($activity_id){
      $sql = "SELECT A.ACTIVITY_ID, A.ACTIVITY, B.LEVEL_NAME FROM SIAK_ACTIVITY A INNER JOIN SIAK_USER_LEVEL B ON A.USER_LEVEL = B.USER_LEVEL WHERE A.ACTIVITY_ID = $activity_id";
      $q = $this->yzdb->query($sql);
      return $q->result();
    }
        public function get_level(){
            $sql = "SELECT USER_LEVEL, LEVEL_NAME FROM SIAK_USER_LEVEL ORDER BY USER_LEVEL";
            $q = $this->yzdb->query($sql);
            return $q->result();
        }
        public function get_head_group($user_level){
            $sql = "SELECT A.USER_LEVEL, A.LEVEL_NAME, B.LEVEL_NAME GROUP_NAME, B.LEVEL_CODE FROM SIAK_USER_LEVEL A INNER JOIN SIAK_USER_GROUP B ON A.GROUP_LEVEL = B.LEVEL_CODE WHERE A.USER_LEVEL = $user_level";
            $q = $this->yzdb->query($sql);
            return $q->result();
        }
        public function get_activity($lvl){
        $sql = "SELECT A.ACTIVITY_ID, A.ACTIVITY, B.LEVEL_NAME FROM SIAK_ACTIVITY A INNER JOIN SIAK_USER_LEVEL B ON A.USER_LEVEL = B.USER_LEVEL WHERE A.ACTIVITY_ID <> 0 AND A.USER_LEVEL = $lvl ORDER BY A.ACTIVITY_ID";
        $q = $this->yzdb->query($sql);
        return $q;
      }
        public function get_user_level($group_id){
            $sql = "SELECT A.USER_LEVEL, A.LEVEL_NAME, B.LEVEL_NAME GROUP_NAME, B.LEVEL_CODE  FROM SIAK_USER_LEVEL A INNER JOIN SIAK_USER_GROUP B ON A.GROUP_LEVEL = B.LEVEL_CODE WHERE A.GROUP_LEVEL = $group_id ORDER BY USER_LEVEL";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function get_user_helpdesk(){
            $sql = "SELECT A.HELPDESK_ID, A.HELPDESK_DESCRIPTION FROM SIAK_MASTER_HELPDESK A  ORDER BY A.HELPDESK_ID";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function get_user_list($lvl_id = 0,$user_id = '',$user_nm = ''){
            $sql = "";
            $sql .= "SELECT A.USER_ID,A.NAMA_LGKP, A.NIK,A.TMPT_LHR, A.TGL_LHR, CASE WHEN A.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END JENIS_KLMIN, A.NAMA_KANTOR, A.TELP, B.LEVEL_NAME, C.LEVEL_NAME GROUP_NAME FROM SIAK_USER_PLUS A INNER JOIN SIAK_USER_LEVEL B ON A.USER_LEVEL = B.USER_LEVEL INNER JOIN SIAK_USER_GROUP C ON B.GROUP_LEVEL = C.LEVEL_CODE WHERE 1=1 ";
            if ($lvl_id != 0){
                $sql .=" AND A.USER_LEVEL = $lvl_id";    
            }
            if ($user_id != ''){
                $sql .=" AND UPPER(A.USER_ID) LIKE UPPER('$user_id')";    
            }
            if ($user_nm != ''){
                $sql .=" AND UPPER(A.NAMA_LGKP) LIKE UPPER('$user_nm')";    
            }
                $sql .=" ORDER BY A.USER_LEVEL, A.NO_KEC, A.NO_KEL, A.NAMA_LGKP";   
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function get_pejabat_list($nama = '',$nip = ''){
            $sql = "";
            $sql .= "SELECT PEJABAT_ID, NIP, JABATAN, NAMA, CASE WHEN IS_STRUKTURAL = 1 THEN 'YA' ELSE '-' END STRUKTURAL FROM SIAK_PEJABAT WHERE 1=1 ";
            if ($nama != ''){
                $sql .=" AND UPPER(NAMA) LIKE UPPER('%$nama%')";    
            }
            if ($nip != ''){
                $sql .=" AND NIP LIKE '%$nip%'";    
            }
                $sql .=" ORDER BY IS_STRUKTURAL DESC, PEJABAT_ID";   
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function edit_pejabat_list($pejabat_id){
            $sql = "";
            $sql .= "SELECT PEJABAT_ID, NIP, JABATAN, NAMA, CASE WHEN IS_STRUKTURAL = 1 THEN 'YA' ELSE '-' END STRUKTURAL, IS_STRUKTURAL FROM SIAK_PEJABAT WHERE 1=1 AND PEJABAT_ID = $pejabat_id";
                $sql .=" ORDER BY IS_STRUKTURAL DESC, PEJABAT_ID";   
            $q = $this->yzdb->query($sql);
            return $q->result();
        }
        public function get_user_group($group_id){
            $sql = "SELECT USER_ID, NAMA_LGKP, NIK,TMPT_LHR, TO_CHAR(TGL_LHR,'DD-MM-YYYY') TGL_LHR,JENIS_KLMIN,NAMA_KANTOR,TELP,ALAMAT_RUMAH FROM SIAK_USER_PLUS WHERE USER_LEVEL = $group_id ORDER BY USER_LEVEL, TO_NUMBER(NO_KEC), NAMA_LGKP";
            $q = $this->yzdb->query($sql);
            return $q->result();
        } 
        public function check_lvl($user_level){
           $sql = "SELECT COUNT(1) JML, LEVEL_NAME FROM SIAK_USER_LEVEL WHERE USER_LEVEL = $user_level GROUP BY LEVEL_NAME";
            $q = $this->yzdb->query($sql);
            $r = $q->result();
            return $r;
        }   
        public function check_userid($user_id){
           $sql = "SELECT COUNT(1) JML, NAMA_LGKP FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id' GROUP BY NAMA_LGKP";
            $q = $this->yzdb->query($sql);
            $r = $q->result();
            return $r;
        }  
        public function check_jml_userid($user_id){
           $sql = "SELECT COUNT(1) JML  FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id'";
            $q = $this->yzdb->query($sql);
            $r = $q->result();
            return $r;
        }
        public function save_lvl($lvl_cd,$lvl_nm,$kdgroup,$user_id){
            $sql = "INSERT INTO SIAK_USER_LEVEL (USER_LEVEL,LEVEL_NAME,GROUP_LEVEL,CREATED_DATE,CREATED_BY) VALUES ($lvl_cd,'$lvl_nm',$kdgroup,SYSDATE,'$user_id')";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function max_pejabat(){
            $sql = "SELECT CASE WHEN MAX(PEJABAT_ID) +1 IS NULL THEN 1 ELSE MAX(PEJABAT_ID) +1 END JML FROM SIAK_PEJABAT";
            $q = $this->yzdb->query($sql);
            $r = $q->result();
            return $r[0]->JML;
            
            
        }
        public function save_pejabat($id,$nip,$nama,$jabatan,$struktural){
            $sql = "INSERT INTO SIAK_PEJABAT (PEJABAT_ID,NIP,JABATAN,NAMA,IS_STRUKTURAL) VALUES ($id,$nip,'$jabatan','$nama',$struktural)";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function edit_pejabat_id($pejabat_id,$nip,$nama,$jabatan,$struktural){
            $sql = "UPDATE SIAK_PEJABAT 
                       SET 
                       NIP = $nip
                       , JABATAN = UPPER('$jabatan')
                       , NAMA = UPPER('$nama')
                       , IS_STRUKTURAL = $struktural 
                     WHERE PEJABAT_ID = $pejabat_id";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function save_activity($activity_nm,$kdgroup,$user_id){
            $sql = "INSERT INTO SIAK_ACTIVITY (ACTIVITY_ID, USER_LEVEL, ACTIVITY, CREATED_DT, CREATED_BY) VALUES (ACTIVITY_ID.NEXTVAL, $kdgroup, UPPER('$activity_nm'), SYSDATE, '$user_id')";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_lvl($lvl_cd,$lvl_nm,$user_id){
            $sql = "UPDATE SIAK_USER_LEVEL 
                       SET LEVEL_NAME = UPPER('$lvl_nm'), MODIFIED_BY = '$user_id', MODIFIED_DATE = SYSDATE 
                     WHERE USER_LEVEL = $lvl_cd";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_activity($activity_cd,$activity_nm){
            $sql = "UPDATE SIAK_ACTIVITY 
                       SET ACTIVITY = UPPER('$activity_nm')
                     WHERE ACTIVITY_ID = $activity_cd";
            $q = $this->yzdb->query($sql);
            return $q;
        }

        public function update_userid_data($user_id,$nama_lgkp,$nama_dpn,$nik,$tmpt_lhr,$tgl_lhr,$jenis_klmin,$gol_drh,$nama_kantor,$alamat_kantor,$telp,$alamat_rumah,$kdgroup,$kdlvl,$kdprop,$kdkab,$kdkec,$kdkel,$no_rw,$no_rt,$user_siak,$user_bcard,$user_benrol,$is_monitoring,$is_gisa,$is_absen,$is_asn,$absensi_checking,$is_active,$ipaddress_check,$is_show,$changed_by){
            $sql = ""; 
            $sql .= "UPDATE SIAK_USER_PLUS 
                       SET 
                      NAMA_LGKP = '$nama_lgkp' 
                      , NIK = $nik
                      , TMPT_LHR = '$tmpt_lhr'
                      , TGL_LHR = TO_DATE('$tgl_lhr','DD-MM-YYYY')
                      , JENIS_KLMIN = $jenis_klmin
                      , GOL_DRH = $gol_drh
                      , NAMA_KANTOR = '$nama_kantor'
                      , ALAMAT_KANTOR = '$alamat_kantor'
                      , TELP = '$telp'
                      , ALAMAT_RUMAH = '$alamat_rumah'
                      , USER_LEVEL = $kdlvl
                      , NO_PROP = $kdprop
                      , NO_KAB = $kdkab
                      , NO_KEC = $kdkec
                      , NO_KEL = $kdkel
                      , NAMA_DPN = '$nama_dpn' ";
                      if ($user_siak == ''){
                        $sql .= ", USER_SIAK = NULL " ;
                      }else{
                        $sql .= ", USER_SIAK = '$user_siak' " ;
                      }
                      if ($user_bcard == ''){
                        $sql .= ", USER_BCARD = NULL " ;
                      }else{
                        $sql .= ", USER_BCARD = '$user_bcard' " ;
                      }
                      if ($user_benrol == ''){
                        $sql .= ", USER_BENROL = NULL " ;
                      }else{
                        $sql .= ", USER_BENROL = '$user_benrol' " ;
                      }
             $sql .= "
                      , IS_MONITORING = $is_monitoring
                      , IS_GISA = $is_gisa
                      , NO_RW = $no_rw
                      , NO_RT = $no_rt
                      , IS_ABSEN = $is_absen
                      , IS_ASN = $is_asn
                      , ABSENSI_CHECKING = $absensi_checking
                      , IS_ACTIVE = $is_active
                      , IPADDRESS_CHECK = $ipaddress_check
                      , IS_SHOW = $is_show
                      , CHANGED_BY = '$changed_by'
                      , CHANGED_DT =  SYSDATE
                     WHERE USER_ID = '$user_id'";
                   
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function insert_userid_data($user_id,$nama_lgkp,$nama_dpn,$nik,$tmpt_lhr,$tgl_lhr,$jenis_klmin,$gol_drh,$nama_kantor,$alamat_kantor,$telp,$alamat_rumah,$kdgroup,$kdlvl,$kdprop,$kdkab,$kdkec,$kdkel,$no_rw,$no_rt,$user_siak,$user_bcard,$user_benrol,$is_monitoring,$is_gisa,$is_absen,$is_asn,$absensi_checking,$is_active,$ipaddress_check,$is_show,$new_password,$created_by){
            $sql = ""; 
            $sql .= "INSERT INTO SIAK_USER_PLUS (USER_ID
                    , NAMA_LGKP
                    , NIK
                    , TMPT_LHR
                    , TGL_LHR
                    , JENIS_KLMIN
                    , GOL_DRH
                    , NAMA_KANTOR
                    , ALAMAT_KANTOR
                    , TELP
                    , ALAMAT_RUMAH
                    , USER_LEVEL
                    , NO_PROP
                    , NO_KAB
                    , NO_KEC
                    , NO_KEL
                    , NAMA_DPN ";
                    if ($user_siak != ''){
                        $sql .= ", USER_SIAK " ;
                    }
                    if ($user_bcard != ''){
                        $sql .= ", USER_BCARD " ;
                    }
                    if ($user_benrol != ''){
                        $sql .= ", USER_BENROL " ;
                    }
           $sql .= ", IS_MONITORING
                    , IS_GISA
                    , NO_RW
                    , NO_RT
                    , IS_ABSEN
                    , IS_ASN
                    , ABSENSI_CHECKING
                    , IS_ACTIVE
                    , IPADDRESS_CHECK
                    , IS_SHOW
                    , USER_PWD
                    , CREATED_BY
                    , CREATED_DT) VALUES (
                      '$user_id' 
                      , '$nama_lgkp' 
                      , $nik
                      , '$tmpt_lhr'
                      , TO_DATE('$tgl_lhr','DD-MM-YYYY')
                      , $jenis_klmin
                      , $gol_drh
                      , '$nama_kantor'
                      , '$alamat_kantor'
                      , '$telp'
                      , '$alamat_rumah'
                      , $kdlvl
                      , $kdprop
                      , $kdkab
                      , $kdkec
                      , $kdkel
                      , '$nama_dpn' ";
                    if ($user_siak != ''){
                        $sql .= ", '$user_siak' " ;
                    }
                    if ($user_bcard != ''){
                        $sql .= ", '$user_bcard' " ;
                    }
                    if ($user_benrol != ''){
                        $sql .= ", '$user_benrol' " ;
                    }
           $sql .= "  , $is_monitoring
                      , $is_gisa
                      , $no_rw
                      , $no_rt
                      , $is_absen
                      , $is_asn
                      , $absensi_checking
                      , $is_active
                      , $ipaddress_check
                      , $is_show
                      , '$new_password'
                      , '$created_by'
                      ,  SYSDATE)";
                   
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function def_akses($lvl_cd){
            $sql = "UPDATE SIAK_AKSES 
                       SET IS_ACTIVE = 0
                     WHERE USER_LEVEL = $lvl_cd";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function change_akses($lvl_cd,$menu_id){
            $sql = "UPDATE SIAK_AKSES C
                       SET IS_ACTIVE = 1
                     WHERE EXISTS (
                        SELECT 1 FROM SIAK_AKSES A INNER JOIN
                    (SELECT MENU_ID FROM SIAK_MENU WHERE MENU_ID = $menu_id OR MENU_ID IN 
                    (SELECT PARENT_ID FROM SIAK_MENU WHERE MENU_ID = $menu_id OR MENU_ID IN (SELECT PARENT_ID FROM SIAK_MENU WHERE MENU_ID = $menu_id))) 
                    B ON A.MENU_ID = B.MENU_ID WHERE A.USER_LEVEL = $lvl_cd AND A.MENU_ID = C.MENU_ID AND A.USER_LEVEL = C.USER_LEVEL)";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function save_akses($lvl_cd){
            $sql = "INSERT INTO SIAK_AKSES (USER_LEVEL,MENU_ID,IS_ACTIVE)
                    SELECT $lvl_cd USER_LEVEL, A.MENU_ID, 0 IS_ACTIVE FROM SIAK_MENU A LEFT JOIN SIAK_AKSES B ON A.MENU_ID = B.MENU_ID AND USER_LEVEL = $lvl_cd WHERE B.MENU_ID IS NULL ORDER BY A.MENU_ID";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function insert_atasan($user_id,$is_atasan_satu,$is_atasan_dua){
            $sql = "INSERT INTO SIAK_ATASAN (USER_ID,ATASAN_SATU,ATASAN_DUA) VALUES ('$user_id',$is_atasan_satu,$is_atasan_dua)";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function update_atasan($user_id,$is_atasan_satu,$is_atasan_dua){
            $sql = "UPDATE SIAK_ATASAN 
                       SET ATASAN_SATU = $is_atasan_satu, ATASAN_DUA = $is_atasan_dua
                     WHERE USER_ID = '$user_id'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function delete_lvl($lvl_cd){
            $sql = "DELETE FROM SIAK_USER_LEVEL WHERE USER_LEVEL = $lvl_cd";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function delete_pejabat($pejabat){
            $sql = "DELETE FROM SIAK_PEJABAT WHERE PEJABAT_ID = $pejabat";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function delete_akses($lvl_cd){
            $sql = "DELETE FROM SIAK_AKSES WHERE USER_LEVEL = $lvl_cd";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function delete_akun($lvl_cd){
            $sql = "DELETE FROM SIAK_USER_PLUS WHERE USER_LEVEL = $lvl_cd";
            $q = $this->yzdb->query($sql);
            return $q;
        }  
        public function get_theuser($user_id){
            $sql = "SELECT 
                  A.USER_ID
                  , A.NAMA_LGKP
                  , A.NAMA_DPN
                  , A.NIK
                  , A.TMPT_LHR
                  , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
                  , CASE WHEN A.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN
                  , A.JENIS_KLMIN KLMN
                  , UPPER(F5_GET_REF_WNI(A.GOL_DRH, 401)) GOL_DRH
                  , A.GOL_DRH GLDRH
                  , A.NAMA_KANTOR
                  , A.ALAMAT_KANTOR
                  , A.TELP
                  , A.ALAMAT_RUMAH
                  , B.USER_LEVEL
                  , B.LEVEL_NAME
                  , C.LEVEL_NAME GROUP_NAME
                  , C.LEVEL_CODE GROUP_CODE
                  , A.NO_PROP
                  , UPPER(F5_GET_NAMA_PROVINSI(A.NO_PROP)) NAMA_PROP
                  , A.NO_KAB
                  , UPPER(F5_GET_NAMA_KABUPATEN(A.NO_PROP,A.NO_KAB)) NAMA_KAB
                  , A.NO_KEC
                  , UPPER(F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC)) NAMA_KEC
                  , A.NO_KEL
                  , A.NO_RW
                  , A.NO_RT
                  , UPPER(F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL)) NAMA_KEL
                  , A.USER_SIAK
                  , A.USER_BCARD
                  , A.USER_BENROL
                  , CASE WHEN  A.IS_MONITORING = 1 THEN 'YA' ELSE 'TIDAK' END IS_MONITORING
                  , CASE WHEN  A.IS_GISA = 1 THEN 'YA' ELSE 'TIDAK' END  IS_GISA 
                  , CASE WHEN  A.IS_ABSEN = 1 THEN 'YA' ELSE 'TIDAK' END  IS_ABSEN 
                  , CASE WHEN  A.IS_ASN = 1 THEN 'YA' ELSE 'TIDAK' END  IS_ASN 
                  , CASE WHEN  A.ABSENSI_CHECKING = 1 THEN 'YA' ELSE 'TIDAK' END  ABSENSI_CHECKING 
                  , CASE WHEN  A.IS_ACTIVE = 1 THEN 'YA' ELSE 'TIDAK' END  IS_ACTIVE 
                  , CASE WHEN  A.IPADDRESS_CHECK = 1 THEN 'YA' ELSE 'TIDAK' END  IPADDRESS_CHECK 
                  , CASE WHEN  A.IS_SHOW = 1 THEN 'YA' ELSE 'TIDAK' END  IS_SHOW 
                  , CASE WHEN  E.PEJABAT_ID IS NULL THEN 0 ELSE E.PEJABAT_ID END  PEJABAT_SATU 
                  , CASE WHEN  E.NAMA IS NULL THEN '-' ELSE E.NAMA END  NAMA_SATU 
                  , CASE WHEN  F.PEJABAT_ID IS NULL THEN 0 ELSE F.PEJABAT_ID END  PEJABAT_DUA
                  , CASE WHEN  F.NAMA IS NULL THEN '-' ELSE F.NAMA END  NAMA_DUA 
                FROM SIAK_USER_PLUS A 
                INNER JOIN SIAK_USER_LEVEL B 
                ON A.USER_LEVEL = B.USER_LEVEL 
                INNER JOIN SIAK_USER_GROUP C 
                ON B.GROUP_LEVEL = C.LEVEL_CODE
                LEFT JOIN SIAK_ATASAN D 
                ON A.USER_ID = D.USER_ID
                LEFT JOIN SIAK_PEJABAT E 
                ON D.ATASAN_SATU = E.PEJABAT_ID
                LEFT JOIN SIAK_PEJABAT F 
                ON D.ATASAN_DUA = F.PEJABAT_ID
                 WHERE 1=1 AND A.USER_ID = '$user_id'";
            $q = $this->yzdb->query($sql);
            return $q;
        }

        public function del_user_backup($user_id){
            $sql = "DELETE FROM SIAK_USER_PLUS_HIST WHERE USER_ID = '$user_id'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function delete_user($user_id){
            $sql = "DELETE FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function delete_atasan($user_id){
            $sql = "DELETE FROM SIAK_ATASAN WHERE USER_ID = '$user_id'";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function backup_user(){
            $sql = "INSERT INTO SIAK_USER_PLUS_HIST (USER_ID, NAMA_LGKP, NIK, TMPT_LHR, TGL_LHR, JENIS_KLMIN, GOL_DRH, PENDIDIKAN, PEKERJAAN, NAMA_KANTOR, ALAMAT_KANTOR, TELP, ALAMAT_RUMAH, USER_LEVEL, USER_PWD, NO_PROP, NO_KAB, NO_KEC, NO_KEL, NAMA_DPN, USER_SIAK, USER_BCARD, USER_BENROL, IS_MONITORING, IS_GISA, NO_RW, NO_RT, IS_ABSEN, IS_ASN, ABSENSI_CHECKING, IS_ACTIVE, IPADDRESS_CHECK, IS_SHOW, CREATED_BY, CREATED_DT, CHANGED_BY, CHANGED_DT) (SELECT B.USER_ID, B.NAMA_LGKP, B.NIK, B.TMPT_LHR, B.TGL_LHR, B.JENIS_KLMIN, B.GOL_DRH, B.PENDIDIKAN, B.PEKERJAAN, B.NAMA_KANTOR, B.ALAMAT_KANTOR, B.TELP, B.ALAMAT_RUMAH, B.USER_LEVEL, B.USER_PWD, B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, B.NAMA_DPN, B.USER_SIAK, B.USER_BCARD, B.USER_BENROL, B.IS_MONITORING, B.IS_GISA, B.NO_RW, B.NO_RT, B.IS_ABSEN, B.IS_ASN, B.ABSENSI_CHECKING, B.IS_ACTIVE, B.IPADDRESS_CHECK, B.IS_SHOW, B.CREATED_BY, B.CREATED_DT, B.CHANGED_BY, B.CHANGED_DT FROM SIAK_USER_PLUS B WHERE NOT EXISTS (SELECT 1 FROM SIAK_USER_PLUS_HIST A WHERE A.USER_ID = B.USER_ID))";
            $q = $this->yzdb->query($sql);
            return $q;
        }
         public function delete_activity_id($activity_id){
            $sql = "DELETE FROM SIAK_ACTIVITY WHERE ACTIVITY_ID = $activity_id";
            $q = $this->yzdb->query($sql);
            return $q;
        }
         public function delete_daily($activity_id){
            $sql = "DELETE FROM SIAK_DAILY WHERE ACTIVITY_ID = $activity_id";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function backup_daily(){
            $sql = "INSERT INTO SIAK_DAILY_BCK (DAILY_ID, ACTIVITY_ID, USER_ID,START_ACTIVITY,END_ACTIVITY,DESCRIPTION,CREATED_BY,CREATED_DT,NUMBER_ACTIVITY)
                    SELECT DAILY_ID, ACTIVITY_ID, USER_ID,START_ACTIVITY,END_ACTIVITY,DESCRIPTION,CREATED_BY,CREATED_DT,NUMBER_ACTIVITY FROM SIAK_DAILY B WHERE NOT EXISTS(SELECT 1 FROM SIAK_DAILY_BCK C WHERE C.DAILY_ID = B.DAILY_ID)";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function menu_settting($group_id = 0){
            if ($group_id != 0){
              $sql = "SELECT A.MENU_ID, A.PARENT_ID,CASE WHEN (SELECT B.TITLE FROM SIAK_MENU B WHERE B.MENU_ID = A.PARENT_ID) IS NULL THEN 'Root' ELSE (SELECT B.TITLE FROM SIAK_MENU B WHERE B.MENU_ID = A.PARENT_ID) END PARENT_TITLE, A.TITLE, A.MENU_LEVEL,A.LEVEL_HEAD, A.LEVEL_SUB FROM SIAK_MENU A WHERE 1=1 AND A.MENU_LEVEL = $group_id ORDER BY A.MENU_LEVEL,A.LEVEL_HEAD, A.PARENT_ID,A.LEVEL_SUB";
            }else{
              $sql = "SELECT A.MENU_ID, A.PARENT_ID,CASE WHEN (SELECT B.TITLE FROM SIAK_MENU B WHERE B.MENU_ID = A.PARENT_ID) IS NULL THEN 'Root' ELSE (SELECT B.TITLE FROM SIAK_MENU B WHERE B.MENU_ID = A.PARENT_ID) END PARENT_TITLE, A.TITLE, A.MENU_LEVEL,A.LEVEL_HEAD, A.LEVEL_SUB FROM SIAK_MENU A WHERE 1=1  ORDER BY A.MENU_LEVEL,A.LEVEL_HEAD, A.PARENT_ID,A.LEVEL_SUB";
            }
            
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function detail_menu_settting($menu_id = 0){
            $sql = "SELECT A.MENU_ID, A.PARENT_ID,CASE WHEN (SELECT B.TITLE FROM SIAK_MENU B WHERE B.MENU_ID = A.PARENT_ID) IS NULL THEN 'Root' ELSE (SELECT B.TITLE FROM SIAK_MENU B WHERE B.MENU_ID = A.PARENT_ID) END PARENT_TITLE, A.TITLE, A.MENU_LEVEL,A.LEVEL_HEAD, A.LEVEL_SUB FROM SIAK_MENU A WHERE 1=1 AND A.MENU_ID = $menu_id ORDER BY A.MENU_LEVEL,A.LEVEL_HEAD, A.PARENT_ID,A.LEVEL_SUB";
            $q = $this->yzdb->query($sql);
            return $q->result();
        }
        public function get_parent($menu_level = 0){
          if ($menu_level == 1){
            $sql = "SELECT 0 MENU_ID, 'Root' TITLE FROM DUAL";
          }else if ($menu_level == 2){
             $sql = "SELECT MENU_ID, TITLE FROM SIAK_MENU WHERE IS_ACTIVE = 0 AND MENU_LEVEL =1 ORDER BY LEVEL_HEAD, LEVEL_SUB";
          }else{
            $sql = "SELECT MENU_ID, TITLE FROM SIAK_MENU WHERE IS_ACTIVE = 0 AND MENU_LEVEL =2 ORDER BY PARENT_ID,LEVEL_HEAD, LEVEL_SUB";
          }
            $q = $this->yzdb->query($sql);
            return $q->result();
        }
        public function get_child($menu_id = 0){
            $sql = "SELECT COUNT(1) JML FROM SIAK_MENU WHERE PARENT_ID = $menu_id";
            $q = $this->yzdb->query($sql);
            $r = $q->result();
            return $r[0]->JML;
          }
        public function do_edit_menu_id($menu_id,$title,$parent_id,$menu_level,$level_head,$level_sub){
            $sql = "UPDATE SIAK_MENU 
                       SET 
                       PARENT_ID = $parent_id
                       , TITLE = '$title'
                       , MENU_LEVEL = $menu_level
                       , LEVEL_HEAD = $level_head
                       , LEVEL_SUB = $level_sub
                     WHERE MENU_ID = $menu_id";
            $q = $this->yzdb->query($sql);
            return $q;
        }
        public function max_helpdesk(){
            $sql = "SELECT MAX(HELPDESK_ID) + 1 JML FROM SIAK_MASTER_HELPDESK";
            $q = $this->yzdb->query($sql);
            $r = $q->result();
            return $r[0]->JML;
        }

        public function save_helpdesk($helpdesk_cd,$helpdesk_nm,$user_id){
            $sql = "INSERT INTO SIAK_MASTER_HELPDESK (HELPDESK_ID,HELPDESK_DESCRIPTION,CREATED_DT,CREATED_BY) VALUES ($helpdesk_cd,'$helpdesk_nm',SYSDATE,'$user_id')";
            $q = $this->yzdb->query($sql);
            return $q;
        }

        public function get_helpdesk($helpdesk_id){
            $sql = "SELECT A.HELPDESK_ID, A.HELPDESK_DESCRIPTION FROM SIAK_MASTER_HELPDESK A  WHERE A.HELPDESK_ID = $helpdesk_id";
            $q = $this->yzdb->query($sql);
            return $q->result();
        }
        public function get_all_helpdesk(){
            $sql = "SELECT A.HELPDESK_ID, A.HELPDESK_DESCRIPTION FROM SIAK_MASTER_HELPDESK A";
            $q = $this->yzdb->query($sql);
            return $q->result();
        }

        public function edit_helpdesk_cd($helpdesk_cd,$helpdesk_nm){
            $sql = "UPDATE SIAK_MASTER_HELPDESK 
                       SET 
                       HELPDESK_DESCRIPTION = UPPER('$helpdesk_nm')
                     WHERE HELPDESK_ID = $helpdesk_cd";
            $q = $this->yzdb->query($sql);
            return $q;
        }

        public function delete_helpdesk($helpdesk_cd){
            $sql = "DELETE FROM SIAK_MASTER_HELPDESK WHERE HELPDESK_ID = $helpdesk_cd";
            $q = $this->yzdb->query($sql);
            return $q;
        }
    
    function get_no_prop()
      {
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_prop();
      }
      function get_no_kab()
      {
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_kab();
      }
      function get_siak_dblink()
      {
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_siak_dblink();
      }
      function get_rekam_dblink()
      {
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_rekam_dblink();
      }
      function get_cetak_dblink()
      {
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cetak_dblink();
      }
      function get_master_dblink()
      {
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_master_dblink();
      }
}