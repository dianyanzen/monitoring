 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Bulan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="tanggal" name = "tanggal" value="<?php echo date('m-Y');?>" placeholder="MM-YYYY" readonly>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th style="text-align: center;">No</th>
                                             <?php if (isset($data[0]->PRINTED_DATE)){ ?>
                                            <th style="text-align: center;">TANGGAL CETAK</th>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C1)){ ?>
                                                <?php if (!empty($nama_wil[0])){echo '<th style="text-align: center;">'.$nama_wil[0]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C2)){ ?>
                                                <?php if (!empty($nama_wil[1])){echo '<th style="text-align: center;">'.$nama_wil[1]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C3)){ ?>
                                                <?php if (!empty($nama_wil[2])){echo '<th style="text-align: center;">'.$nama_wil[2]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C4)){ ?>
                                                <?php if (!empty($nama_wil[3])){echo '<th style="text-align: center;">'.$nama_wil[3]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C5)){ ?>
                                                <?php if (!empty($nama_wil[4])){echo '<th style="text-align: center;">'.$nama_wil[4]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C6)){ ?>
                                                <?php if (!empty($nama_wil[5])){echo '<th style="text-align: center;">'.$nama_wil[5]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C7)){ ?>
                                                <?php if (!empty($nama_wil[6])){echo '<th style="text-align: center;">'.$nama_wil[6]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C8)){ ?>
                                                <?php if (!empty($nama_wil[7])){echo '<th style="text-align: center;">'.$nama_wil[7]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C9)){ ?>
                                                <?php if (!empty($nama_wil[8])){echo '<th style="text-align: center;">'.$nama_wil[8]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C10)){ ?>
                                                <?php if (!empty($nama_wil[9])){echo '<th style="text-align: center;">'.$nama_wil[9]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C11)){ ?>
                                                <?php if (!empty($nama_wil[10])){echo '<th style="text-align: center;">'.$nama_wil[10]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C12)){ ?>
                                                <?php if (!empty($nama_wil[11])){echo '<th style="text-align: center;">'.$nama_wil[11]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C13)){ ?>
                                                <?php if (!empty($nama_wil[12])){echo '<th style="text-align: center;">'.$nama_wil[12]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C14)){ ?>
                                                <?php if (!empty($nama_wil[13])){echo '<th style="text-align: center;">'.$nama_wil[13]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C15)){ ?>
                                                <?php if (!empty($nama_wil[14])){echo '<th style="text-align: center;">'.$nama_wil[14]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C16)){ ?>
                                                <?php if (!empty($nama_wil[15])){echo '<th style="text-align: center;">'.$nama_wil[15]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C17)){ ?>
                                                <?php if (!empty($nama_wil[16])){echo '<th style="text-align: center;">'.$nama_wil[16]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C18)){ ?>
                                                <?php if (!empty($nama_wil[17])){echo '<th style="text-align: center;">'.$nama_wil[17]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C19)){ ?>
                                                <?php if (!empty($nama_wil[18])){echo '<th style="text-align: center;">'.$nama_wil[18]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C20)){ ?>
                                                <?php if (!empty($nama_wil[19])){echo '<th style="text-align: center;">'.$nama_wil[19]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C21)){ ?>
                                                <?php if (!empty($nama_wil[20])){echo '<th style="text-align: center;">'.$nama_wil[20]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C22)){ ?>
                                                <?php if (!empty($nama_wil[21])){echo '<th style="text-align: center;">'.$nama_wil[21]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C23)){ ?>
                                                <?php if (!empty($nama_wil[22])){echo '<th style="text-align: center;">'.$nama_wil[22]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C24)){ ?>
                                                <?php if (!empty($nama_wil[23])){echo '<th style="text-align: center;">'.$nama_wil[23]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C25)){ ?>
                                                <?php if (!empty($nama_wil[24])){echo '<th style="text-align: center;">'.$nama_wil[24]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C26)){ ?>
                                                <?php if (!empty($nama_wil[25])){echo '<th style="text-align: center;">'.$nama_wil[25]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C27)){ ?>
                                                <?php if (!empty($nama_wil[26])){echo '<th style="text-align: center;">'.$nama_wil[26]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C28)){ ?>
                                                <?php if (!empty($nama_wil[27])){echo '<th style="text-align: center;">'.$nama_wil[27]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C29)){ ?>
                                                <?php if (!empty($nama_wil[28])){echo '<th style="text-align: center;">'.$nama_wil[28]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C30)){ ?>
                                                <?php if (!empty($nama_wil[29])){echo '<th style="text-align: center;">'.$nama_wil[29]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C31)){ ?>
                                                <?php if (!empty($nama_wil[30])){echo '<th style="text-align: center;">'.$nama_wil[30]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C32)){ ?>
                                                <?php if (!empty($nama_wil[31])){echo '<th style="text-align: center;">'.$nama_wil[31]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C33)){ ?>
                                                <?php if (!empty($nama_wil[32])){echo '<th style="text-align: center;">'.$nama_wil[32]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C34)){ ?>
                                                <?php if (!empty($nama_wil[33])){echo '<th style="text-align: center;">'.$nama_wil[33]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C35)){ ?>
                                                <?php if (!empty($nama_wil[34])){echo '<th style="text-align: center;">'.$nama_wil[34]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C36)){ ?>
                                                <?php if (!empty($nama_wil[35])){echo '<th style="text-align: center;">'.$nama_wil[35]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C37)){ ?>
                                                <?php if (!empty($nama_wil[36])){echo '<th style="text-align: center;">'.$nama_wil[36]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C38)){ ?>
                                                <?php if (!empty($nama_wil[37])){echo '<th style="text-align: center;">'.$nama_wil[37]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C39)){ ?>
                                                <?php if (!empty($nama_wil[38])){echo '<th style="text-align: center;">'.$nama_wil[38]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C40)){ ?>
                                                <?php if (!empty($nama_wil[39])){echo '<th style="text-align: center;">'.$nama_wil[39]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C41)){ ?>
                                                <?php if (!empty($nama_wil[40])){echo '<th style="text-align: center;">'.$nama_wil[40]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C42)){ ?>
                                                <?php if (!empty($nama_wil[41])){echo '<th style="text-align: center;">'.$nama_wil[41]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C43)){ ?>
                                                <?php if (!empty($nama_wil[42])){echo '<th style="text-align: center;">'.$nama_wil[42]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C44)){ ?>
                                                <?php if (!empty($nama_wil[43])){echo '<th style="text-align: center;">'.$nama_wil[43]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C45)){ ?>
                                                <?php if (!empty($nama_wil[44])){echo '<th style="text-align: center;">'.$nama_wil[44]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C46)){ ?>
                                                <?php if (!empty($nama_wil[45])){echo '<th style="text-align: center;">'.$nama_wil[45]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C47)){ ?>
                                                <?php if (!empty($nama_wil[46])){echo '<th style="text-align: center;">'.$nama_wil[46]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C48)){ ?>
                                                <?php if (!empty($nama_wil[47])){echo '<th style="text-align: center;">'.$nama_wil[47]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C49)){ ?>
                                                <?php if (!empty($nama_wil[48])){echo '<th style="text-align: center;">'.$nama_wil[48]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->C50)){ ?>
                                                <?php if (!empty($nama_wil[49])){echo '<th style="text-align: center;">'.$nama_wil[49]->NAMA_WIL.'</th>'; }?>
                                            <?php } ?>
                                            <th style="text-align: center;">JUMLAH</th>
                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       <?php
                                        if (!empty($data)){
                                            $i=0;
                                            $j1=0;
                                            $j2=0;
                                            $j3=0;
                                            $j4=0;
                                            $j5=0;
                                            $j6=0;
                                            $j7=0;
                                            $j8=0;
                                            $j9=0;
                                            $j10=0;
                                            $j11=0;
                                            $j12=0;
                                            $j13=0;
                                            $j14=0;
                                            $j15=0;
                                            $j16=0;
                                            $j17=0;
                                            $j18=0;
                                            $j19=0;
                                            $j20=0;
                                            $j21=0;
                                            $j22=0;
                                            $j23=0;
                                            $j24=0;
                                            $j25=0;
                                            $j26=0;
                                            $j27=0;
                                            $j28=0;
                                            $j29=0;
                                            $j30=0;
                                            $j31=0;
                                            $j32=0;
                                            $j33=0;
                                            $j34=0;
                                            $j35=0;
                                            $j36=0;
                                            $j37=0;
                                            $j38=0;
                                            $j39=0;
                                            $j40=0;
                                            $j41=0;
                                            $j42=0;
                                            $j43=0;
                                            $j44=0;
                                            $j45=0;
                                            $j46=0;
                                            $j47=0;
                                            $j48=0;
                                            $j49=0;
                                            $j50=0;
                                            $jum_all=0;
                                           foreach($data as $row){
                                            $i++;
                                            $jum = 0;
                                            ?>
                                             <tr>
                                                <td style="text-align: center;">
                                                <?php echo $i ;?>
                                                </td>

                                                <?php if (isset($row->PRINTED_DATE)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->PRINTED_DATE ;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->C1)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C1 ;?>
                                                <?php $jum = $jum + $row->C1;?>
                                                <?php $j1 = $j1 + $row->C1;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->C2)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C2 ;?>
                                                <?php $jum = $jum + $row->C2;?>
                                                <?php $j2 = $j2 + $row->C2;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C3)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C3 ;?>
                                                <?php $jum = $jum + $row->C3;?>
                                                <?php $j3 = $j3 + $row->C3;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C4)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C4 ;?>
                                                <?php $jum = $jum + $row->C4;?>
                                                <?php $j4 = $j4 + $row->C4;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C5)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C5 ;?>
                                                <?php $jum = $jum + $row->C5;?>
                                                <?php $j5 = $j5 + $row->C5;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C6)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C6 ;?>
                                                <?php $jum = $jum + $row->C6;?>
                                                <?php $j6 = $j6 + $row->C6;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C7)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C7 ;?>
                                                <?php $jum = $jum + $row->C7;?>
                                                <?php $j7 = $j7 + $row->C7;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C8)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C8 ;?>
                                                <?php $jum = $jum + $row->C8;?>
                                                <?php $j8 = $j8 + $row->C8;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C9)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C9 ;?>
                                                <?php $jum = $jum + $row->C9;?>
                                                <?php $j9 = $j9 + $row->C9;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C10)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C10 ;?>
                                                <?php $jum = $jum + $row->C10;?>
                                                <?php $j10 = $j10 + $row->C10;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->C11)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C11 ;?>
                                                <?php $jum = $jum + $row->C11;?>
                                                <?php $j11 = $j11 + $row->C11;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->C12)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C12 ;?>
                                                <?php $jum = $jum + $row->C12;?>
                                                <?php $j12 = $j12 + $row->C12;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C13)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C13 ;?>
                                                <?php $jum = $jum + $row->C13;?>
                                                <?php $j13 = $j13 + $row->C13;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C14)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C14 ;?>
                                                <?php $jum = $jum + $row->C14;?>
                                                <?php $j14 = $j14 + $row->C14;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C15)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C15 ;?>
                                                <?php $jum = $jum + $row->C15;?>
                                                <?php $j15 = $j15 + $row->C15;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C16)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C16 ;?>
                                                <?php $jum = $jum + $row->C16;?>
                                                <?php $j16 = $j16 + $row->C16;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C17)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C17 ;?>
                                                <?php $jum = $jum + $row->C17;?>
                                                <?php $j17 = $j17 + $row->C17;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C18)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C18 ;?>
                                                <?php $jum = $jum + $row->C18;?>
                                                <?php $j18 = $j18 + $row->C18;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C19)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C19 ;?>
                                                <?php $jum = $jum + $row->C19;?>
                                                <?php $j19 = $j19 + $row->C19;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C20)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C20 ;?>
                                                <?php $jum = $jum + $row->C20;?>
                                                <?php $j20 = $j20 + $row->C20;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->C21)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C21 ;?>
                                                <?php $jum = $jum + $row->C21;?>
                                                <?php $j21 = $j21 + $row->C21;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->C22)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C22 ;?>
                                                <?php $jum = $jum + $row->C22;?>
                                                <?php $j22 = $j22 + $row->C22;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C23)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C23 ;?>
                                                <?php $jum = $jum + $row->C23;?>
                                                <?php $j23 = $j23 + $row->C23;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C24)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C24 ;?>
                                                <?php $jum = $jum + $row->C24;?>
                                                <?php $j24 = $j24 + $row->C24;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C25)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C25 ;?>
                                                <?php $jum = $jum + $row->C25;?>
                                                <?php $j25 = $j25 + $row->C25;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C26)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C26 ;?>
                                                <?php $jum = $jum + $row->C26;?>
                                                <?php $j26 = $j26 + $row->C26;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C27)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C27 ;?>
                                                <?php $jum = $jum + $row->C27;?>
                                                <?php $j27 = $j27 + $row->C27;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C28)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C28 ;?>
                                                <?php $jum = $jum + $row->C28;?>
                                                <?php $j28 = $j28 + $row->C28;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C29)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C29 ;?>
                                                <?php $jum = $jum + $row->C29;?>
                                                <?php $j29 = $j29 + $row->C29;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C30)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C30 ;?>
                                                <?php $jum = $jum + $row->C30;?>
                                                <?php $j30 = $j30 + $row->C30;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->C31)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C31 ;?>
                                                <?php $jum = $jum + $row->C31;?>
                                                <?php $j31 = $j31 + $row->C31;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->C32)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C32 ;?>
                                                <?php $jum = $jum + $row->C32;?>
                                                <?php $j32 = $j32 + $row->C32;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C33)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C33 ;?>
                                                <?php $jum = $jum + $row->C33;?>
                                                <?php $j33 = $j33 + $row->C33;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C34)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C34 ;?>
                                                <?php $jum = $jum + $row->C34;?>
                                                <?php $j34 = $j34 + $row->C34;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C35)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C35 ;?>
                                                <?php $jum = $jum + $row->C35;?>
                                                <?php $j35 = $j35 + $row->C35;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C36)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C36 ;?>
                                                <?php $jum = $jum + $row->C36;?>
                                                <?php $j36 = $j36 + $row->C36;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C37)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C37 ;?>
                                                <?php $jum = $jum + $row->C37;?>
                                                <?php $j37 = $j37 + $row->C37;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C38)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C38 ;?>
                                                <?php $jum = $jum + $row->C38;?>
                                                <?php $j38 = $j38 + $row->C38;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C39)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C39 ;?>
                                                <?php $jum = $jum + $row->C39;?>
                                                <?php $j39 = $j39 + $row->C39;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C40)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C40 ;?>
                                                <?php $jum = $jum + $row->C40;?>
                                                <?php $j40 = $j40 + $row->C40;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->C41)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C41 ;?>
                                                <?php $jum = $jum + $row->C41;?>
                                                <?php $j41 = $j41 + $row->C41;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->C42)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C42 ;?>
                                                <?php $jum = $jum + $row->C42;?>
                                                <?php $j42 = $j42 + $row->C42;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C43)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C43 ;?>
                                                <?php $jum = $jum + $row->C43;?>
                                                <?php $j43 = $j43 + $row->C43;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C44)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C44 ;?>
                                                <?php $jum = $jum + $row->C44;?>
                                                <?php $j44 = $j44 + $row->C44;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C45)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C45 ;?>
                                                <?php $jum = $jum + $row->C45;?>
                                                <?php $j45 = $j45 + $row->C45;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C46)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C46 ;?>
                                                <?php $jum = $jum + $row->C46;?>
                                                <?php $j46 = $j46 + $row->C46;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C47)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C47 ;?>
                                                <?php $jum = $jum + $row->C47;?>
                                                <?php $j47 = $j47 + $row->C47;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C48)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C48 ;?>
                                                <?php $jum = $jum + $row->C48;?>
                                                <?php $j48 = $j48 + $row->C48;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C49)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C49 ;?>
                                                <?php $jum = $jum + $row->C49;?>
                                                <?php $j49 = $j49 + $row->C49;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->C50)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->C50 ;?>
                                                <?php $jum = $jum + $row->C50;?>
                                                <?php $j50 = $j50 + $row->C50;?>
                                                </td>
                                                <?php } ?>

                                                <td style="text-align: center;">
                                                <?php echo $jum ;?>
                                                <?php $jum_all = $jum_all + $jum;?>
                                                </td>
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($data)){ ?>
                                            <th width="80%" colspan="2" style="text-align: center;">Jumlah</th>

                                            <?php if (isset($data[0]->C1)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j1),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C2)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j2),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C3)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j3),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C4)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j4),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C5)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j5),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C6)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j6),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C7)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j7),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C8)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j8),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C9)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j9),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C10)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j10),0,',','.').'</th>'; }?>
                                            <?php } ?>
                                            <?php if (isset($data[0]->C11)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j11),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C12)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j12),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C13)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j13),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C14)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j14),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C15)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j15),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C16)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j16),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C17)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j17),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C18)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j18),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C19)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j19),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C20)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j20),0,',','.').'</th>'; }?>
                                            <?php } ?>
                                            <?php if (isset($data[0]->C21)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j21),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C22)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j22),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C23)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j23),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C24)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j24),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C25)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j25),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C26)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j26),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C27)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j27),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C28)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j28),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C29)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j29),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C30)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j30),0,',','.').'</th>'; }?>
                                            <?php } ?>
                                            <?php if (isset($data[0]->C31)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j31),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C32)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j32),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C33)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j33),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C34)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j34),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C35)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j35),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C36)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j36),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C37)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j37),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C38)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j38),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C39)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j39),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C40)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j40),0,',','.').'</th>'; }?>
                                            <?php } ?>
                                            <?php if (isset($data[0]->C41)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j41),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C42)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j42),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C43)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j43),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C44)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j44),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C45)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j45),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C46)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j46),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C47)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j47),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C48)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j48),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C49)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j49),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->C50)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j50),0,',','.').'</th>'; }?>
                                            <?php } ?>
                                                <th width="80%" colspan="1" style="text-align: center;"><?php echo $jum_all ;?></th>

                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       <?php $this->view('shared/footer_detail'); ?>