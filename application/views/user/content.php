 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-xs-6">
                                    <h2 class="font-medium m-t-0">List User  <small><span id="group_name"></span></small></h2>
                                </div>
                            </div>
                           
                            <div class="row" style="margin-top:  5px">
                                 <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b> User Id</b></h3>
                                        <input class="form-control" type="text" id="user_id" name="user_id"/>
                                    </div>
                                 </div>
                                 <div class="col-lg-8">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b> User Name</b></h3>
                                        <input class="form-control" type="text" id="user_nm" name="user_nm" style="text-transform:uppercase !important"/>
                                    </div>
                                 </div>
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Level</b></h3>
                                        <select class="form-control select2" name="kdLevel" id="kdLevel">
                                <option  value="0">-- Select Level --</option></select>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                                  <button type="button" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-search" onclick="on_select();">Lihat <i class="mdi  mdi-magnify fa-fw"></i></button>
                                   <button type="button" class="btn btn-primary waves-effect waves-light m-r-10 pull-right" id="btn-add" onclick="on_add();">Add <i class="mdi  mdi-plus fa-fw"></i></button>
                                  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="level-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="5%" style="text-align: center;">No</th>
                                            <th width="5%" style="text-align: center;">User Id</th>
                                            <th width="40%" style="text-align: center;">User Name</th>
                                            <th width="20%" style="text-align: center;">User Level</th>
                                            <th width="30%" style="text-align: center;">Operation</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
       <?php $this->view('shared/footer_detail'); ?>