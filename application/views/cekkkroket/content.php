 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> Kelurahan</label>
                                        </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                       <tr>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO KK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEP</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">STATUS KK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">REQUEST TTE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEC</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEL</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NOMOR RW</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NOMOR RT</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">ALAMAT</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">KODE POS</th>
                                            </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       <?php
                                        if (!empty($data)){
                                            $i=0;
                                           foreach($data as $row){
                                            $i++;
                                            ?>
                                            <tr style="border: 1px solid #e4e7ea;">
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $i ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;">
                                                   
                                                          <span style="color: #fff">,</span><?php echo $row->NO_KK ;?>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->NAMA_KEP ;?>
                                                     
                                                </td>
                                                 <?php if ($row->CERT_STATUS == '0' || $row->CERT_STATUS == '1'){ ?>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"> <a type="button" class="btn btn-danger btn-xs" style="margin-top: 5px;"> BELUM DISERTIVIKASI <i class="mdi  mdi-information fa-fw"></i></a><br />
                                                  <?php }else if ($row->CERT_STATUS == '2'){ ?>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"> <a type="button" class="btn btn-warning btn-xs" style="margin-top: 5px;"> PROSES PENERBITAN <i class="mdi  mdi-upload fa-fw"></i></a><br />
                                                <?php }else if  ($row->CERT_STATUS == '3'){ ?>
                                                    <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"> <a type="button" class="btn btn-info btn-xs" style="margin-top: 5px;"> BELUM DIPUBLISH <i class="mdi  mdi-rocket fa-fw"></i></a><br />
                                                <?php }else if  ($row->CERT_STATUS == '9'){ ?>
                                                    <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"> <a type="button" class="btn btn-info btn-xs" style="margin-top: 5px;"> BELUM DIVERIVIKASI <i class="mdi  mdi-information fa-fw"></i></a><br />  
                                                <?php }else if  ($row->CERT_STATUS == '-'){ ?>
                                                    <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"> -<br />
                                                <?php }else{ ?>
                                                    <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"> <a type="button" class="btn btn-success btn-xs" style="margin-top: 5px;"> SUDAH DIPUBLISH <i class="mdi  mdi-check-circle fa-fw"></i></a><br />
                                                <?php } ?>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                    <i class="mdi mdi-account-circle text-white" style="float: none; color: #41b3f9 !important;"></i><span style="float: none; color: #41b3f9 !important;"> Request Date : <?php echo $row->REQ_DATE ;?></span><br>
                                                </td>
                                        
                                                 <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->NAMA_KEC ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->NAMA_KEL ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->NO_RW ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->NO_RT ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->ALAMAT ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->KODE_POS ;?></td>
                                                
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($data)){ ?>
                                            <th width="80%" colspan="10" style="text-align: center;">Jumlah</th>
                                            <th width="20%"  style="text-align: left;"><?php echo number_format(htmlentities($i),0,',','.');?></th>
                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       <?php $this->view('shared/footer_detail'); ?>