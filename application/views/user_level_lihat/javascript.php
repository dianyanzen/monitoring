    <script src="<?php echo base_url()?>assets/js/gijgo.min.js"></script>
    <link href="<?php echo base_url()?>assets/css/gijgo.min.css" rel="stylesheet">
    
     <script>
        function on_back() {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Setting/UserLevel";
            var win = window.location.replace(url);
            win.focus();
        }
   
      $(document).ready(function () {
                var user_level = $('#code_wil').val();
                var tree = $('#user_tree').tree({
                    primaryKey: 'id',
                    uiLibrary: 'bootstrap4',
                    dataSource: BASE_URL+"Setting/get_group_tree_one?user_level="+<?php if (!empty($chead)){echo $chead[0]->USER_LEVEL; }?>,
                    checkboxes: true,
                    cascadeCheck: false
                });
                tree.on('dataBound', function() {
                    tree.expandAll();
                });
                $('#btnSave').on('click', function () {
                    var checkedIds = tree.getCheckedNodes();
                    $.ajax({ url: '/Locations/SaveCheckedNodes', data: { checkedIds: checkedIds }, method: 'POST' })
                        .fail(function () {
                            alert('Failed to save.');
                        });
                });
            });
      function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }

    </script>