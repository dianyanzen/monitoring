    <script src="<?php echo base_url()?>assets/js/gijgo.min.js"></script>
    <link href="<?php echo base_url()?>assets/css/gijgo.min.css" rel="stylesheet">
    
     <script>
        function on_back() {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Setting/ActivityId";
            var win = window.location.replace(url);
            win.focus();
        }
        function on_save() {
            if (validationdaily()){
                do_save();
            }
        }
        function validationdaily() {
        var activity_cd = $("#activity_cd");
            if (activity_cd.val().length == 0) {                
                  swal("Warning!", "Activity Code Cannot Be Empty !", "warning");  
                 return false;
            }
        var activity_nm = $("#activity_nm");
            if (activity_nm.val().length == 0) {                
                  swal("Warning!", "Activity Name Cannot Be Empty !", "warning");  
                 return false;
            }
            return true;
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/update_activity",
                    dataType: "json",
                    data: {
                        activity_cd : $("#activity_cd").val(),
                        activity_nm : $("#activity_nm").val()

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_back();
                    }
                });
        }
        function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>