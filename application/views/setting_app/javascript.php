    <script src="<?php echo base_url()?>assets/js/gijgo.min.js"></script>
    <link href="<?php echo base_url()?>assets/css/gijgo.min.css" rel="stylesheet">
    
     <script>
        $(document).ready(function() {
           

        });
       
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       jQuery(document).ready(function() {
        $('select[name="give_kec"]').empty();
            $('select[name="give_kec"]').append('<option value="1">YA</option>');
            $('select[name="give_kec"]').append('<option value="0">TIDAK</option>');
            $('select[name="give_kec"]').val("<?php if (!empty($data)){echo $data['give_kec']; }?>").trigger("change");
        $('select[name="give_kel"]').empty();
            $('select[name="give_kel"]').append('<option value="1">YA</option>');
            $('select[name="give_kel"]').append('<option value="0">TIDAK</option>');
            $('select[name="give_kel"]').val("<?php if (!empty($data)){echo $data['give_kel']; }?>").trigger("change");
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
       jQuery('#dkb_cutoff').datepicker({
            format: 'dd-mm-yyyy',               
            autoclose: true,
            todayHighlight: true
        });
        function on_change_master() {
            if (validationdaily()){
                do_edit();
            }
            
        }
        function validationdaily() {
            
        var no_prop = $("#no_prop");
            if (no_prop.val().length == 0) {                
                  swal("Warning!", "No Prop Tidak Boleh Kosong !", "warning");  
                  $("#no_prop").focus();
                 return false;
            }
        var no_kab = $("#no_kab");
            if (no_kab.val().length == 0) {                
                  swal("Warning!", "No Kab Tidak Boleh Kosong !", "warning");  
                  $("#no_kab").focus();
                 return false;
            }
        var nm_prop = $("#nm_prop");
            if (nm_prop.val().length == 0) {                
                  swal("Warning!", "Nama Prop Tidak Boleh Kosong !", "warning");  
                  $("#nm_prop").focus();
                 return false;
            }
        var nm_kab = $("#nm_kab");
            if (nm_kab.val().length == 0) {                
                  swal("Warning!", "Nama Kab Tidak Boleh Kosong !", "warning");  
                  $("#nm_kab").focus();
                 return false;
            }
        var siak_dblink = $("#siak_dblink");
            if (siak_dblink.val().length == 0) {                
                  swal("Warning!", "Siak Dblink Tidak Boleh Kosong !", "warning");  
                  $("#siak_dblink").focus();
                 return false;
            }
        var cetak_dblink = $("#cetak_dblink");
            if (cetak_dblink.val().length == 0) {                
                  swal("Warning!", "Cetak Dblink Tidak Boleh Kosong !", "warning");  
                  $("#cetak_dblink").focus();
                 return false;
            }
        var rekam_dblink = $("#rekam_dblink");
            if (rekam_dblink.val().length == 0) {                
                  swal("Warning!", "Rekam Dblink Tidak Boleh Kosong !", "warning");  
                  $("#rekam_dblink").focus();
                 return false;
            }
        var master_dblink = $("#master_dblink");
            if (master_dblink.val().length == 0) {                
                  swal("Warning!", "Master Dblink Tidak Boleh Kosong !", "warning");  
                   $("#master_dblink").focus();
                 return false;
            }
        var dkb_bio = $("#dkb_bio");
            if (dkb_bio.val().length == 0) {                
                  swal("Warning!", "Dkb Bio Tidak Boleh Kosong !", "warning"); 
                  $("#dkb_bio").focus(); 
                 return false;
            }
        var dkb_kk = $("#dkb_kk");
            if (dkb_kk.val().length == 0) {                
                  swal("Warning!", "Dkb KK Tidak Boleh Kosong !", "warning"); 
                  $("#dkb_kk").focus();   
                 return false;
            }
        var dkb_tahun = $("#dkb_tahun");
            if (dkb_tahun.val().length == 0) {                
                  swal("Warning!", "Dkb Tahun Tidak Boleh Kosong !", "warning");   
                  $("#dkb_tahun").focus(); 
                 return false;
            }
        var dkb_cutoff = $("#dkb_cutoff");
            if (dkb_cutoff.val().length == 0) {                
                  swal("Warning!", "Dkb Cutoff Tidak Boleh Kosong !", "warning");  
                  $("#dkb_cutoff").focus();  
                 return false;
            }
        var default_pass = $("#default_pass");
            if (default_pass.val().length == 0) {                
                  swal("Warning!", "Default Pass Tidak Boleh Kosong !", "warning");  
                  $("#default_pass").focus();  
                 return false;
            }
        var give_kec = $("#give_kec");
            if (give_kec.val().length == 0) {                
                  swal("Warning!", "Akses Semua Kecamatan Tidak Boleh Kosong !", "warning");  
                  $("#give_kec").focus();  
                 return false;
            }
        var give_kel = $("#give_kel");
            if (give_kel.val().length == 0) {                
                  swal("Warning!", "Akses Semua Kelurahan Tidak Boleh Kosong !", "warning");  
                  $("#give_kel").focus();  
                 return false;
            }

            return true;
        }
        function do_edit(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/edit_setting",
                    dataType: "json",
                    data: {
                        no_prop : $("#no_prop").val(),
                        no_kab : $("#no_kab").val(),
                        nm_prop : $("#nm_prop").val(),
                        nm_kab : $("#nm_kab").val(),
                        siak_dblink : $("#siak_dblink").val(),
                        cetak_dblink : $("#cetak_dblink").val(),
                        rekam_dblink : $("#rekam_dblink").val(),
                        master_dblink : $("#master_dblink").val(),
                        dkb_bio : $("#dkb_bio").val(),
                        dkb_kk : $("#dkb_kk").val(),
                        dkb_tahun : $("#dkb_tahun").val(),
                        dkb_cutoff : $("#dkb_cutoff").val(),
                        default_pass : $("#default_pass").val(),
                        give_kec : $("#give_kec").val(),
                        give_kel : $("#give_kel").val(),

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                    }
                });
        }
      function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }

    </script>