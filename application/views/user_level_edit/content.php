 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-xs-6">
                                    <h2 class="font-medium m-t-0">PREVILAGE ACCESS USER LEVEL</h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <h3 class="font-medium m-t-0">CODE : <span id="code_wil"><?php if (!empty($chead)){echo $chead[0]->USER_LEVEL; }?></span></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <h3 class="font-medium m-t-0">GROUP NAME : <span id="group_name"><?php if (!empty($chead)){echo $chead[0]->LEVEL_NAME; }?></span></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <h3 class="font-medium m-t-0">GROUP LEVEL : <span id="group_level"><?php if (!empty($chead)){echo $chead[0]->GROUP_NAME; }?></span></h3>
                                </div>
                            </div>
                            <div class="row">
                                  <button type="button" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btnSave">Save <i class="mdi  mdi-plus fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-back" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                  
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="tabs.html#hak_akses" aria-controls="hak_akses" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Hak Akses</span></a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="hak_akses">
                                    <div class="col-md-12">
                                        <div id="user_tree" style="background: #ffffff !important;"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
                

            </div>
       <?php $this->view('shared/footer_detail'); ?>