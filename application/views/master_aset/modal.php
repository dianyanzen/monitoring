<button class="btn btn-primary btn-lg" id="do_aset" data-toggle="modal" data-target="#aset-modal" style="display: none;"></button>

           <div class="modal fade" id="aset-modal" tabindex="-1" role="dialog" aria-labelledby="aset" aria-hidden="true" >
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" aria-hidden="true" onclick="do_close()">&times;</button>
                      <h4 class="modal-title">Master Aset</h4>
                    </div>
                    <div class="modal-body">
                     <div class="row">
                            <div class="col-md-12">
                              <form class="form-horizontal" role="form">
                                <fieldset>


                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Keterangan</label>
                                    <div class="col-sm-10">
                                    <input class="form-control" id="mdl_value_data" type="text"  style="text-transform:uppercase !important">
                                    <input class="form-control" id="mdl_no_data" type="hidden" >
                                    <input class="form-control" id="mdl_sect_data" type="hidden" >
                                    </div>
                                  </div>



                                  <!-- Text input-->
                                 
                                  
                                     <div class="col-sm-12" id="bantuan_text">
            
                                  </div>
                                  
                                </fieldset>
                              </form>
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->
                        </div>

                    <div class="modal-footer" style="margin-top: 0px !important;">
                      <button type="button" id="closemodal" class="btn btn-default" data-dismiss="modal" onclick="onClearModal()">Tutup</button>
                      <button type="button" id="go_send" class="btn btn-primary" onclick="do_save_mdl()">Kirim</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->