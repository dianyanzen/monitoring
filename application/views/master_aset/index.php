<!DOCTYPE html>
<html lang="en">
<?php $this->view('shared/head'); ?>
<body class="fix-header">
    <div id="wrapper">
            <?php $this->view('shared/nav_top'); ?>
            <?php $this->view('shared/nav_side'); ?>
            <?php $this->view('master_aset/content'); ?>
            <?php $this->view('master_aset/modal'); ?>
    </div>
     
    <?php $this->view('shared/footer'); ?>
    
    <?php $this->view('master_aset/javascript'); ?>
</body>
</html>