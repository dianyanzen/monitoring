    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script type="text/javascript">
    	 function on_update(){
        if (validationdaily()){
          // swal("Warning!", "Ganti Password Belum Bisa Hehe, Contact Admin Aplikasinya !", "warning");  
          do_save();
        }
    }
    function validationdaily() {
        var old_password = $("#old_password");
        var new_password = $("#new_password");
        var re_password = $("#re_password");
            if (old_password.val().length == 0) {                
                  swal("Warning!", "Old Password Cannot Be Empty !", "warning");  
                 return false;
            }
            if (new_password.val().length == 0) {                
                  swal("Warning!", "New Password Cannot Be Empty !", "warning");  
                 return false;
            }
            if (re_password.val().length == 0) {                
                  swal("Warning!", "Re Password Cannot Be Empty !", "warning");  
                 return false;
            }
            if (new_password.val().length <= 3) {                
                  swal("Warning!", "New Password Too Short, We Need More Complex !", "warning");  
                 return false;
            }
            if (re_password.val().length <= 3) {                
                  swal("Warning!", "Re Password Too Short, We Need More Complex !", "warning");  
                 return false;
            }
            if (re_password.val() != new_password.val()) {                
                  swal("Warning!", "New Password And Re Password Doesn't Match  !", "warning");  
                 return false;
            }

            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Activity/do_save",
                    dataType: "json",
                    data: {
                        old_password : $("#old_password").val(),
                        new_password : $("#new_password").val(),
                        user_name : '<?php $user_id = (!empty($user_id)) ? $user_id : '-'; echo $user_id;?>'
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
	                   		swal("Success!", data.message, "success");
	                   		$("#old_password").val('');
	                   		$("#new_password").val('');
	                   		$("#re_password").val('');
                    	}else{
                    		swal("Warning!", data.message, "warning");  
                    	}
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                    }
                });
        }
         function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>
   