<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg"> <img width="100%" alt="user" src="<?php echo base_url('assets/plugins/images/wallpaper2.jpg') ?>">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)"> <?php $filename = 'assets/upload/pp/'.$user_nik.'.jpg';
                                            if (file_exists($filename)) { ?>
                                            <img src='<?php echo base_url('assets/upload/pp/'.$user_nik.'.jpg') ?>'  class="img-circle thumb-lg" alt="user-img" >
                                            <?php } else {?>
                                            <img src="<?php echo base_url()?>assets/plugins/images/calming-cat.gif" class="img-circle thumb-lg" alt="user-img">
                                        <?php }?></a>
                                        <h4 class="text-white"><?php echo $user_nama_lgkp; ?></h4>
                                        <h5 class="text-white"><?php if($user_level == 1){echo 'KECAMATAN';}?> <?php echo $user_nama_kantor; ?></h5>  </div>
                                </div>
                            </div>
                        </div>
                        <?php if (!empty($data)){ ?>
                        <div class="white-box">
                            <h3><b>My User Control</b> <span class="pull-right"></span></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <address>
                                            <h3><b class="text-danger"><?php echo $user_nama_lgkp; ?></b></h3>
                                            <h4 class="font-bold">My Profile</h4>
                                            <p class="m-l-5">
                                                <?php if ($data[0]->USER_ID != '-'){ ?>
                                                    <b>Username : &nbsp;<?php echo $data[0]->USER_ID; ?></b><br/>
                                                <?php } ?>
                                                <?php if ($data[0]->NIK != '-'){ ?>
                                                    <b>NIK : &nbsp;<?php echo $data[0]->NIK; ?></b><br/>
                                                <?php } ?>
                                                <?php if ($data[0]->JENIS_KLMIN != '-'){ ?>
                                                    <b>Jenis Kelamin : &nbsp;<?php echo $data[0]->JENIS_KLMIN; ?></b><br/>
                                                <?php } ?>
                                                <?php if ($data[0]->TMPT_LHR != '-'){ ?>
                                                    <b>Tempat Lahir : &nbsp;<?php echo $data[0]->TMPT_LHR; ?></b><br/>
                                                <?php } ?>
                                                <?php if ($data[0]->TGL_LHR != '-'){ ?>
                                                    <b>Tanggal Lahir : &nbsp;<?php echo $data[0]->TGL_LHR; ?></b><br/>
                                                <?php } ?>
                                                <?php if ($data[0]->TELP != '-'){ ?>
                                                    <b>Telephone : &nbsp;<?php echo $data[0]->TELP; ?></b><br/>
                                                <?php } ?>
                                                <?php if ($data[0]->USER_SIAK != '-'){ ?>
                                                    <b>User Siak : &nbsp;<?php echo $data[0]->USER_SIAK; ?></b><br/>
                                                <?php } ?>
                                                <?php if ($data[0]->USER_BCARD != '-'){ ?>
                                                    <b>User Bcard : &nbsp;<?php echo $data[0]->USER_BCARD; ?></b><br/>
                                                <?php } ?>
                                                <?php if ($data[0]->USER_BENROL != '-'){ ?>
                                                    <b>User Benrol : &nbsp;<?php echo $data[0]->USER_BENROL; ?></b><br/>
                                                <?php } ?>
                                                
                                            </p>
                                        </address>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>

                    <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                            <ul class="nav nav-tabs tabs customtab">
                                <li class="active tab">
                                    <a href="#setting" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-gear"></i></span> <span class="hidden-xs">Setting</span> </a>
                                </li>
                               
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="setting">
                                        <form class="form-horizontal form-material">
                                        <div class="form-group">
                                            <label class="col-md-12">Old Password</label>
                                            <div class="col-md-12">
                                                <input type="password" value="" id="old_password" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">New Password</label>
                                            <div class="col-md-12">
                                                <input type="password" value="" id="new_password" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Re-Password</label>
                                            <div class="col-md-12">
                                                <input type="password" value="" id="re_password" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <a onclick="on_update();" class="btn btn-info">Update Password</a>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         <?php $this->view('shared/footer_detail'); ?>