<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                 <div class="row">
                    <form name ="get_form" id="get_form" action="<?php echo $my_url; ?>" method="post">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" maxlength="16" onkeypress="return isNumberKey(event)"/></div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_no_kk" type="checkbox" checked disabled="true">
                                            <label for="cb_no_kk"> No KK</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="no_kk" name="no_kk" maxlength="16" onkeypress="return isNumberKey(event)"/></div>
                                </div>
                                </div>
                            <div class="row">
                                <a class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="get_data();" >Search <i class="mdi  mdi-magnify fa-fw"></i></a>
                                  <a  class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <?php if (!empty($data)){ ?>
                         <div class="row" id="all_content">
                    <div class="col-lg-12" id="all_<?php echo $data[0]->NIK; ?>">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">Biodata Wni</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                    <div class="white-box">
                                        <?php if ($data[0]->PATH != '-'){ ?>
                                    <?php $filename = 'http://10.32.73.222:8080/Siak/'.$data[0]->PATH.'/'.$data[0]->NIK.'.jpg';
                                        if ($_SERVER['SERVER_NAME'] == '10.32.73.224') { ?>
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo $filename; ?>" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo $filename; ?>"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <?php }else{ ?>
                                                <?php if ($data[0]->JENIS_KLMIN == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 

                                            <?php } ?> 
                                        <?php }else{ ?>
                                            <?php if ($data[0]->JENIS_KLMIN == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 
                                        <?php } ?> 
                                    
                                    </div>
                                    </tr>
                                    <?php if ($data[0]->KET == 'TIDAK AKTIF DI SIAK'){ ?>
                                          <?php if ($data[0]->CURRENT_STATUS_CODE != 'DUPLICATE_RECORD'){ ?> 

                                            <?php if (empty($mati) && empty($pindah)){ ?>
                                            <tr>
                                                <td>
                                                    <button class="btn btn-block btn-success btn-rounded" onclick="restore_biodata('<?php echo $data[0]->NIK; ?>','<?php echo $data[0]->NO_KK; ?>','<?php echo $data[0]->STAT_HBKEL; ?>');" id="restore_biodata"><i class="mdi mdi-database-plus"></i> RESTORE BIODATA </button>
                                                </td>
                                            </tr>
                                            
                                            <?php } else if (!empty($pindah)){ ?>
                                                <?php if ($pindah[0]->KLASIFIKASI_PINDAH == 'ANTAR KECAMATAN' || $pindah[0]->KLASIFIKASI_PINDAH == 'ANTAR DESA/KELURAHAN' || $pindah[0]->KLASIFIKASI_PINDAH == 'DALAM SATU DESA/KELURAHAN'){ ?>
                                            <tr>
                                                <td>
                                                    <button class="btn btn-block btn-success btn-rounded" onclick="restore_pindah_biodata('<?php echo $data[0]->NIK; ?>','<?php echo $data[0]->NO_KK; ?>','<?php echo $data[0]->STAT_HBKEL; ?>');" id="restore_pindah_biodata"><i class="mdi mdi-database-plus"></i> RESTORE BIODATA PINDAH </button>
                                                </td>
                                            </tr>
                                            <?php } ?> 
                                            <?php } ?> 
                                             <?php if ($data[0]->STAT_HBKEL != 'KEPALA KELUARGA' && empty($mati) && empty($pindah)){ ?> 
                                            <tr>
                                                <td>
                                                    <button class="btn btn-block btn-info btn-rounded" data-toggle="modal" data-target="#new_kk_modal"><i class="mdi mdi-google-photos"></i> RUBAH NOMOR KK</button>
                                                </td>
                                            </tr>
                                            <div id="new_kk_modal" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h4 class="modal-title">Ubah Nomer Kartu Keluarga</h4> </div>
                                                    <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="recipient-name" class="control-label">Nomor KK Tujuan:</label>
                                                                <div class="input-group">
                                                               
                                                                <input  class="form-control" type="text" id="no_kk_baru" name="no_kk_baru" maxlength="16" onkeypress="return isNumberKey(event)"/> <span class="input-group-btn">
                                                                <button type="button" class="btn waves-effect waves-light btn-info" onclick="cek_no_kk_baru('<?php echo $data[0]->NO_KK; ?>');"><i class="fa fa-search"></i></button>
                                                                </span>
                                                              </div>
                                                              </div>
                                                            
                                                        <div class="row">
                                                        <div id="form_kk_lama" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                         <h4>Data Keluarga Asal</h4>
                                                        <p>
                                                            <li>Nomor KK : <b><span id="txt_no_kk_lama"><?php echo $data[0]->NO_KK; ?></span></b></li>
                                                            <li>Kepala Keluarga: <b><span id="txt_kep_kel_lama"><?php echo $data[0]->NAMA_KEP; ?></span></b></li>
                                                            <li>Nomor Kecamatan :  <b><span id="txt_no_kec_lama"><?php echo $data[0]->NO_KEC; ?></span></b></li>
                                                            <li>Nomor Kelurahan :  <b><span id="txt_no_kel_lama"><?php echo $data[0]->NO_KEL; ?></span></b></li>
                                                            <li>Nomor RW :  <b><span id="txt_rw_lama"><?php echo $data[0]->RW; ?></span></b></li>
                                                            <li>Nomor RT :  <b><span id="txt_rt_lama"><?php echo $data[0]->RT; ?></span></b></li>
                                                            <li>ALAMAT :  <b><span id="txt_alamat_lama"><?php echo $data[0]->ALAMAT; ?></span></b></li>
                                                        </p>
                                                        </div>
                                                        <div id="form_kk_baru" class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="display: none;">
                                                         <h4>Data Keluarga Baru</h4>
                                                        <p>
                                                            <li>Nomor KK : <b><span id="txt_no_kk_baru">-</span></b></li>
                                                            <li>Kepala Keluarga: <b><span id="txt_nama_kep_baru">-</span></b></li>
                                                            <li>Nomor Kecamatan :  <b><span id="txt_no_kec_baru">-</span></b></li>
                                                            <li>Nomor Kelurahan :  <b><span id="txt_no_kel_baru">-</span></b></li>
                                                            <li>Nomor RW :  <b><span id="txt_rw_baru">-</span></b></li>
                                                            <li>Nomor RT :  <b><span id="txt_rt_baru">-</span></b></li>
                                                            <li>Alamat :  <b><span id="txt_alamat_baru">-</span></b></li>
                                                        </p>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                                                        <button type="button" id="ubah_kk" class="btn btn-success waves-effect waves-light" disabled="true" onclick="change_no_kk('<?php echo $data[0]->NIK; ?>');">Ubah Nomor KK</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } else if (!empty($pindah)) { ?>
                                                <?php if ($pindah[0]->KLASIFIKASI_PINDAH == 'ANTAR KECAMATAN' || $pindah[0]->KLASIFIKASI_PINDAH == 'ANTAR DESA/KELURAHAN' || $pindah[0]->KLASIFIKASI_PINDAH == 'DALAM SATU DESA/KELURAHAN'){ ?>
                                                    <tr>
                                                <td>
                                                    <button class="btn btn-block btn-info btn-rounded" data-toggle="modal" data-target="#new_kk_modal"><i class="mdi mdi-google-photos"></i> RUBAH NOMOR KK</button>
                                                </td>
                                            </tr>
                                            <div id="new_kk_modal" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h4 class="modal-title">Ubah Nomer Kartu Keluarga</h4> </div>
                                                    <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="recipient-name" class="control-label">Nomor KK Tujuan:</label>
                                                                <div class="input-group">
                                                               
                                                                <input  class="form-control" type="text" id="no_kk_baru" name="no_kk_baru" maxlength="16" onkeypress="return isNumberKey(event)"/> <span class="input-group-btn">
                                                                <button type="button" class="btn waves-effect waves-light btn-info" onclick="cek_no_kk_baru('<?php echo $data[0]->NO_KK; ?>');"><i class="fa fa-search"></i></button>
                                                                </span>
                                                              </div>
                                                              </div>
                                                            
                                                        <div class="row">
                                                        <div id="form_kk_lama" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                         <h4>Data Keluarga Asal</h4>
                                                        <p>
                                                            <li>Nomor KK : <b><span id="txt_no_kk_lama"><?php echo $data[0]->NO_KK; ?></span></b></li>
                                                            <li>Kepala Keluarga: <b><span id="txt_kep_kel_lama"><?php echo $data[0]->NAMA_KEP; ?></span></b></li>
                                                            <li>Nomor Kecamatan :  <b><span id="txt_no_kec_lama"><?php echo $data[0]->NO_KEC; ?></span></b></li>
                                                            <li>Nomor Kelurahan :  <b><span id="txt_no_kel_lama"><?php echo $data[0]->NO_KEL; ?></span></b></li>
                                                            <li>Nomor RW :  <b><span id="txt_rw_lama"><?php echo $data[0]->RW; ?></span></b></li>
                                                            <li>Nomor RT :  <b><span id="txt_rt_lama"><?php echo $data[0]->RT; ?></span></b></li>
                                                            <li>ALAMAT :  <b><span id="txt_alamat_lama"><?php echo $data[0]->ALAMAT; ?></span></b></li>
                                                        </p>
                                                        </div>
                                                        <div id="form_kk_baru" class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="display: none;">
                                                         <h4>Data Keluarga Baru</h4>
                                                        <p>
                                                            <li>Nomor KK : <b><span id="txt_no_kk_baru">-</span></b></li>
                                                            <li>Kepala Keluarga: <b><span id="txt_nama_kep_baru">-</span></b></li>
                                                            <li>Nomor Kecamatan :  <b><span id="txt_no_kec_baru">-</span></b></li>
                                                            <li>Nomor Kelurahan :  <b><span id="txt_no_kel_baru">-</span></b></li>
                                                            <li>Nomor RW :  <b><span id="txt_rw_baru">-</span></b></li>
                                                            <li>Nomor RT :  <b><span id="txt_rt_baru">-</span></b></li>
                                                            <li>Alamat :  <b><span id="txt_alamat_baru">-</span></b></li>
                                                        </p>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                                                        <button type="button" id="ubah_kk" class="btn btn-success waves-effect waves-light" disabled="true" onclick="change_no_kk('<?php echo $data[0]->NIK; ?>');">Ubah Nomor KK</button>
                                                    </div>
                                                </div>
                                            </div>
                                          <?php } ?>
                                          <?php } ?>
                                         <?php } ?> 
                                    <?php }else{ ?>
                                        <?php if ($data[0]->CURRENT_STATUS_CODE == 'DUPLICATE_RECORD' || $data[0]->CURRENT_STATUS_CODE == 'BELUM REKAM'){ ?> 
                                        <tr>
                                                <td>
                                                    <button class="btn btn-block btn-danger btn-rounded" onclick="delete_biodata('<?php echo $data[0]->NIK; ?>','<?php echo $data[0]->NO_KK; ?>','<?php echo $data[0]->STAT_HBKEL; ?>');" id="del_biodata"><i class="mdi mdi-database-minus"></i> DELETE BIODATA </button>
                                                </td>
                                            </tr>
                                        <?php } ?> 
                                    <?php } ?> 
                                    </tbody>
                                    </table>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4><?php echo $data[0]->NAMA_LGKP; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4><?php echo $data[0]->NIK; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4><?php echo $data[0]->NO_KK; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STAT HUBKEL</h4></td>
                                                        <td> <h4><?php echo $data[0]->STAT_HBKEL; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4><?php echo $data[0]->TMPT_LHR; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4><?php echo $data[0]->TGL_LHR; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4><?php echo $data[0]->JENIS_KLMIN; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KECAMATAN</h4></td>
                                                        <td> <h4><?php echo $data[0]->NAMA_KEC; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KELURAHAN</h4></td>
                                                        <td> <h4><?php echo $data[0]->NAMA_KEL; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        <?php if ($data[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_REGIONAL' || $data[0]->CURRENT_STATUS_CODE == 'PACKET_RETRY' || $data[0]->CURRENT_STATUS_CODE == 'RECEIVED_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'PROCESSING' || $data[0]->CURRENT_STATUS_CODE == 'SENT_FOR_ENROLLMENT' || $data[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'SEARCH_FAILURE_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_RECORD' || $data[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_IN_PROCESS' || $data[0]->CURRENT_STATUS_CODE == 'SENT_FOR_DEDUP' || $data[0]->CURRENT_STATUS_CODE == 'DUPLICATE_RECORD' || $data[0]->CURRENT_STATUS_CODE == 'BELUM REKAM'){ ?>
                                                        <td> <h4 class="text-danger"><b><?php echo $data[0]->CURRENT_STATUS_CODE; ?></b></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><b><?php echo $data[0]->CURRENT_STATUS_CODE; ?></b></h4> </td>
                                                    <?php } ?>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KETERANGAN </h4></td>
                                                    <?php if ($data[0]->KET == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <td> <h4 class="text-danger"><b><?php echo $data[0]->KET; ?></b></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><b><?php echo $data[0]->KET; ?></b></h4> </td>
                                                    <?php } ?>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <?php if ($data[0]->KET == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <?php if (!empty($pindah)){ ?>
                                                        <tr>
                                                            <td><h4>HISTORY PINDAH</h4></td>
                                                            <?php if ($pindah[0]->KLASIFIKASI_PINDAH == 'ANTAR KECAMATAN' || $pindah[0]->KLASIFIKASI_PINDAH == 'ANTAR DESA/KELURAHAN' || $pindah[0]->KLASIFIKASI_PINDAH == 'DALAM SATU DESA/KELURAHAN'){ ?>
                                                                <td> <h4 class="text-success"><?php echo $pindah[0]->NO_PINDAH; ?></h4> </td>
                                                            <td> <h4 class="text-success"><?php echo $pindah[0]->KLASIFIKASI_PINDAH; ?></h4> </
                                                               <?php }else{ ?> 
                                                                <td> <h4 class="text-danger"><?php echo $pindah[0]->NO_PINDAH; ?></h4> </td>
                                                            <td> <h4 class="text-danger"><?php echo $pindah[0]->KLASIFIKASI_PINDAH; ?></h4> </
                                                               <?php } ?> 
                                                            td>

                                                        </tr>
                                                        <tr>
                                                            <td><h4>DARI :</h4></td>
                                                            <td> <h4><?php echo $pindah[0]->DARI_NAMA_PROVINSI; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->DARI_NAMA_KABUPATEN; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4></h4></td>
                                                            <td> <h4><?php echo $pindah[0]->DARI_NAMA_KECAMATAN; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->DARI_NAMA_KELURAHAN; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4>KE :</h4></td>
                                                            <td> <h4><?php echo $pindah[0]->TUJUAN_NAMA_PROVINSI; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->TUJUAN_NAMA_KABUPATEN; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4></h4></td>
                                                            <td> <h4><?php echo $pindah[0]->TUJUAN_NAMA_KECAMATAN; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->TUJUAN_NAMA_KELURAHAN; ?></h4> </td>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if ($data[0]->KET == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <?php if (!empty($mati)){ ?>
                                                        <tr>
                                                            <td><h4>HISTORY KEMATIAN</h4></td>
                                                            <td colspan="2"> <h4 class="text-danger"><?php echo $mati[0]->KET; ?></h4> </td>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                    <?php if (!empty($data_kk)){ ?>
                        <div class="col-lg-12">
                            <div class="white-box">
                                <div class="table-responsive">
                                    <h2 class="m-b-0 m-t-0">Data Keluarga</h2>
                                <hr>
                                    <table class="table color-bordered-table info-bordered-table">
                                        <thead>
                                            <tr>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NO</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NIK</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NO KK</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NAMA LENGKAP</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">STAT HUBKEL</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">TANGGAL LAHIR</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">KECAMATAN</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">KELURAHAN</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">STATUS KTP</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">FLAG STATUS</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">KETERANGAN</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">ACTION</h6></th>
                                            </tr>
                                        </thead>
                                        <tbody id="table_kk_<?php echo $data_kk[0]->NO_KK; ?>">
                                            <?php 
                                                $i=0;
                                                foreach($data_kk as $row){ 
                                                $i++;    
                                            ?>
                                            <?php if($row->NIK == $data[0]->NIK){ ?>
                                            <tr>
                                                        <td><h6 class="text-info" style="text-align: center;"><?php echo $i ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center;"><?php echo $row->NIK ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center;"><?php echo $row->NO_KK ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center;"><?php echo $row->NAMA_LGKP ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center;"><?php echo $row->STAT_HUBKEL ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center;"><?php echo $row->TGL_LHR ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center;"><?php echo $row->NAMA_KECAMATAN ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center;"><?php echo $row->NAMA_KELURAHAN ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center;"><?php echo $row->CURRENT_STATUS_CODE ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center;"><?php echo $row->FLAG_STATUS ;?></h6></td>
                                                        <?php if ($row->DELETED_BY == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <td><h6 style="text-align: center;" class="text-danger"><?php echo $row->DELETED_BY ;?></h6></td>
                                                        <?php }else{ ?>
                                                        <td><h6 style="text-align: center;" class="text-success"><?php echo $row->DELETED_BY ;?></h6></td>
                                                        <?php } ?>
                                                        <td><h6 class="text-info" style="text-align: center;"><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/calming.gif" onclick="on_fast_search('<?php echo $row->NIK; ?>');"><i class="icon-magnifier"></i></a></h6></td>
                                            </tr>
                                            <?php }else{ ?>
                                                <tr>
                                                        <td><h6 style="text-align: center;"><?php echo $i ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->NIK ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->NO_KK ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->NAMA_LGKP ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->STAT_HUBKEL ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->TGL_LHR ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->NAMA_KECAMATAN ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->NAMA_KELURAHAN ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->CURRENT_STATUS_CODE ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->FLAG_STATUS ;?></h6></td>
                                                        <?php if ($row->DELETED_BY == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <td><h6 style="text-align: center;" class="text-danger"><?php echo $row->DELETED_BY ;?></h6></td>
                                                        <?php }else{ ?>
                                                        <td><h6 style="text-align: center;" class="text-success"><?php echo $row->DELETED_BY ;?></h6></td>
                                                        <?php } ?>
                                                        <td><h6 class="text-info" style="text-align: center;"><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/calming.gif" onclick="on_fast_search('<?php echo $row->NIK; ?>');"><i class="icon-magnifier"></i></a></h6></td>
                                            </tr>
                                            <?php } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    <?php } ?>        
                </div>
                <?php }else if (!empty($data_kk)){ ?>
                     <div class="col-lg-12">
                            <div class="white-box">
                                <div class="table-responsive">
                                    <h2 class="m-b-0 m-t-0">Data Keluarga</h2>
                                <hr>
                                    <table class="table color-bordered-table info-bordered-table">
                                        <thead>
                                            <tr>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NO</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NIK</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NO KK</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NAMA LENGKAP</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">STAT HUBKEL</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">TANGGAL LAHIR</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">KECAMATAN</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">KELURAHAN</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">STATUS KTP</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">FLAG STATUS</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">KETERANGAN</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">ACTION</h6></th>
                                            </tr>
                                        </thead>
                                        <tbody id="table_kk_<?php echo $data_kk[0]->NO_KK; ?>">
                                            <?php 
                                                $i=0;
                                                foreach($data_kk as $row){ 
                                                $i++;    
                                            ?>
                                                <tr>
                                                        <td><h6 style="text-align: center;"><?php echo $i ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->NIK ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->NO_KK ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->NAMA_LGKP ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->STAT_HUBKEL ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->TGL_LHR ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->NAMA_KECAMATAN ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->NAMA_KELURAHAN ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->CURRENT_STATUS_CODE ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->FLAG_STATUS ;?></h6></td>
                                                        <?php if ($row->DELETED_BY == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <td><h6 style="text-align: center;" class="text-danger"><?php echo $row->DELETED_BY ;?></h6></td>
                                                        <?php }else{ ?>
                                                        <td><h6 style="text-align: center;" class="text-success"><?php echo $row->DELETED_BY ;?></h6></td>
                                                        <?php } ?>
                                                        <td><h6 class="text-info" style="text-align: center;"><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/calming.gif" onclick="on_fast_search('<?php echo $row->NIK; ?>');"><i class="icon-magnifier"></i></a></h6></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                        </div>
                <?php }else{ ?>
                <?php if (!empty($is_ada)){ ?>
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">Biodata Wni Tidak Ditemukan</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="white-box">
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/calming-cat.gif" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/calming-cat.gif"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KETERANGAN</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
            <!-- /.container-fluid -->
         <?php $this->view('shared/footer_detail'); ?>