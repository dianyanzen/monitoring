 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-xs-6">
                                    <h2 class="font-medium m-t-0">List User Menu <small><span id="group_name"></span></small></h2>
                                </div>
                            </div>
                           
                            <div class="row" style="margin-top:  5px">
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Group</b></h3>
                                        <select class="form-control select2" name="kdgroup" id="kdgroup">
                                            <option  value="0">-- Select Group --</option>
                                            <option  value="1">Level 1</option>
                                            <option  value="2">Level 2</option>
                                            <option  value="3">Level 3</option>
                                        </select>

                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="group-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="5%" style="text-align: center;">No</th>
                                            <th width="5%" style="text-align: center;">Code</th>
                                            <th width="10%" style="text-align: center;">Parent Code</th>
                                            <th width="15%" style="text-align: center;">Parent Title</th>
                                            <th width="15%" style="text-align: center;">Title</th>
                                            <th width="10%" style="text-align: center;">Menu Level</th>
                                            <th width="10%" style="text-align: center;">Level Head</th>
                                            <th width="10%" style="text-align: center;">Level Sub</th>
                                            <th width="20%" style="text-align: center;">Operation</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
       <?php $this->view('shared/footer_detail'); ?>