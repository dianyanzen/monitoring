 <style type="text/css">
     textarea {
  resize: none;
  height: 155px !important;
}

 </style>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <!-- <form name ="get_form" action="<?php echo $my_url; ?>" method="post"> -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <div class="row">
                               
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Input</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal"/> </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Jenis Barang</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                          <select class="form-control select2" name="kd_brg" id="kd_brg">
                                            <option  value="0">-- Pilih Barang --</option></select></div>
                                </div>
                            <div class="row">
                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                           <div class="row">
                            <div class="col-md-12">
                                    <div class="white-box">
                                        <h3 class="box-title"><?php echo $mtitle; ?></h3>
                                        <div class="scrollable">
                                            <div class="table-responsive">
                                                <table id="list_barang" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                               <thead>
                                                
                                                    <tr>
                                                        <th width="5%" style="text-align: center;">No</th>
                                                        <th width="25%" style="text-align: center;">Jenis Barang</th>
                                                        <th width="20%" style="text-align: center;">Jumlah</th>
                                                        <th width="20%" style="text-align: center;">Harga</th>
                                                        <th width="20%" style="text-align: center;">Tanggal</th>
                                                        <th width="20%" style="text-align: center;">Penerima</th>

                                                    </tr>
                                                   
                                                </thead>
                                                <tbody id="show_data">
                                                   
                                                    
                                                </tbody>
                                                
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div>
                        </div>
                    </div>
                    <!-- </form> -->
                </div>
               
                 </div>
       <?php $this->view('shared/footer_detail'); ?>