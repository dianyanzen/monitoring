 <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
                <div class="user-profile">
     
                </div>
                <ul class="nav" id="side-menu">
                    <?php if (!empty($menu)){ ?>
                    <?php foreach($menu as $mn){?>
                    <li><a <?php if($mn->URL != '#') { ?> href="<?php echo base_url()?><?php echo $mn->URL ?>" <?php } else { ?> href="<?php echo $mn->URL ?>" <?php } ?> <?php if($mn->IS_ACTIVE != 0) { ?> onclick="on_menu();" <?php }else{ ?> class="waves-effect" <?php } ?>><i class="<?php echo $mn->ICON ?>"></i> <span class="hide-menu"><?php echo $mn->TITLE ?><?php if($mn->IS_ACTIVE == 0) { ?><span class="fa arrow"></span></span><?php } ?></a>
                        
                        <?php if (!empty($mn->SUB_MENU)){ ?>
                        <ul class="nav nav-second-level">
                            <?php foreach($mn->SUB_MENU as $s_mn){ ?>
                            <li><a <?php if($s_mn->URL != '#') { ?> href="<?php echo base_url()?><?php echo $s_mn->URL ?>" <?php } else { ?> href="<?php echo $s_mn->URL ?>" <?php } ?> <?php if($s_mn->IS_ACTIVE != 0) { ?> onclick="on_menu();" <?php }else{ ?> class="waves-effect" <?php } ?>><i class="<?php echo $s_mn->ICON ?>"></i> <span class="hide-menu"><?php echo $s_mn->TITLE ?><?php if($s_mn->IS_ACTIVE == 0) { ?><span class="fa arrow"></span></span><?php } ?></a>
                                <?php if (!empty($s_mn->SUB_MENU)){ ?>
                                <ul class="nav nav-third-level">
                                <?php foreach($s_mn->SUB_MENU as $ss_mn){ ?>
                                    <li><a <?php if($ss_mn->URL != '#') { ?> href="<?php echo base_url()?><?php echo $ss_mn->URL ?>" <?php } else { ?> href="<?php echo $ss_mn->URL ?>" <?php } ?> <?php if($ss_mn->IS_ACTIVE != 0) { ?> onclick="on_menu();" <?php }else{ ?> class="waves-effect" <?php } ?>><i class="<?php echo $ss_mn->ICON ?>"></i> <span class="hide-menu"><?php echo $ss_mn->TITLE ?><?php if($ss_mn->IS_ACTIVE == 0) { ?><span class="fa arrow"></span></span><?php } ?></a>
                                    </li>
                                <?php } ?>
                                </ul>
                                <?php } ?>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>

                    </li>
                    <?php } ?>
                    <?php } ?>
                    <?php if( $this->session->userdata(S_IS_ASN) == 0 && $_SERVER['SERVER_NAME'] == '10.32.73.7') { ?>
                         <li><a href="http://10.32.73.7:8080/absensi/pages/op/index.php?user=<?php echo base64_encode($this->session->userdata(S_USER_ID)) ?>" target="_blank" class="waves-effect"><i class="mdi mdi-clock-fast fa-fw"></i> <span class="hide-menu">Absensi</span></a>
                        </li>
                    <?php } ?>

                </ul>
            </div>
        </div>