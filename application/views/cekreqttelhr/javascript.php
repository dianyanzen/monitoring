     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    $(document).ready(function($) {
        $('#cb_nik').change(function() {
            if ($('#cb_nik').is(':checked')) {
                $('#nik').attr("disabled",false);
                $('#nik').val("");
            }else{
                $('#nik').attr("disabled",true);
                $('#nik').val("");
            }
            
        });
        $('#nik').on('paste', function() {
          var $el = $(this);
          setTimeout(function() {
            $el.val(function(i, val) {
              return val.replace(/[^0-9,]/g, '')
            })
          })
        });
    });
    $(document).ready(function() {
    
    console.log(init_kec);
     $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_user_cpltte",
                    dataType: "json",
                    data: {
                        is_user : init_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="is_user"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="is_user"]').empty();
                       $('select[name="is_user"]').append('<option value="0">-- Pilih User --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="is_user"]').append('<option value="'+ value.REQ_BY +'">'+ value.REQ_BY +'</option>');
                        });
                        
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="is_user"]').attr("disabled",false);
                    }
                });

    });
    function on_clear() {
        
        $('select[name="is_user"]').val(init_kec).trigger("change");
        $('#cb_nik').attr('checked', false);
        $('#nik').attr("disabled",true);
        $('#nik').val("");
        $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
    }
    function on_serach(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-inverse',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
           $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
        });
    </script>
   