 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> Kelurahan</label>
                                        </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Request</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal" /></div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                       <!--  <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox">
                                            <label for="cb_nik"> Nik</label>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <!-- <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" disabled="true" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div> -->
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                     
                                        <tr>
                                            <th width="10%" style="text-align: center;">No Kec</th>
                                            <th width="15%" style="text-align: center;">Nik</th>
                                            <th width="20%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="15%" style="text-align: center;">Status E-KTP</th>
                                            <th width="15%" style="text-align: center;">Agama</th>
                                            <th width="10%" style="text-align: center;">Req Date</th>
                                            <th width="10%" style="text-align: center;">Req By</th>
                                            <th width="10%" style="text-align: center;">Print Date</th>
                                            <th width="10%" style="text-align: center;">Print By</th>
                                             <?php if ($my_url == 'uktp'){ ?>
                                            <th width="10%" style="text-align: center;">Status Pengajuan</th>
                                            <th width="10%" style="text-align: center;">Keterangan Pengajuan</th>
                                             <?php } ?>
                                             <th width="10%" style="text-align: center;">Alamat</th>
                                             <th width="10%" style="text-align: center;">Rw</th>
                                             <th width="10%" style="text-align: center;">Rt</th>
                                             <th width="10%" style="text-align: center;">Kode Pos</th>
                                             <th width="10%" style="text-align: center;">Status Kawin</th>
                                            <th width="10%" style="text-align: center;">Jenis Pekerjaan</th>
                                            <th width="10%" style="text-align: center;">Nama Kecamatan</th>
                                            <th width="10%" style="text-align: center;">Nama Kelurahan</th>
                                            
                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       <?php
                                        if (!empty($data)){
                                            $i=0;
                                           foreach($data as $row){
                                            $i++;
                                            ?>
                                            
                                             <tr>
                                                 <td width="10%" style="text-align: center;"><?php echo $row->NO_KEC ;?></td>
                                                <td width="15%" style="text-align: center;"><span style="color: #fff">,</span><?php echo $row->NIK ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->NAMA_LENGKAP ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->CURRENT_STATUS_CODE ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->AGAMA ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->REQ_DATE ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->REQ_BY ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->PRINTED_DATE ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->PRINTED_BY ;?></td>
                                                <?php if ($my_url == 'uktp'){ ?>
                                                <td width="10%" style="text-align: center;"><?php echo $row->STATUS_PENGAJUAN ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->KETERANGAN_STATUS ;?></td>
                                                <?php } ?>
                                                <td width="10%" style="text-align: center;"><?php echo $row->ALAMAT ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->RW ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->RT ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->KODE_POS ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->STAT_KWN ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->JENIS_PKRJN ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->NAMA_KEC ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->NAMA_KEL ;?></td>
                                                
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($data)){ ?>
                                            <?php if ($my_url == 'uktp'){ ?>
                                            <th width="80%" colspan="18" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($i),0,',','.');?>
                                            <?php }else{ ?>
                                            <th width="80%" colspan="15" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($i),0,',','.');?></th>
                                            <?php } ?>
                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       <?php $this->view('shared/footer_detail'); ?>