<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url()?>">Laporan</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <input type="text" name="get_data" id="get_data" value="1" style="display: none;">
                                <button type="submit" id="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            
                                            <th width="10%" style="text-align: center;">No Kec</th>
                                            <th width="20%" style="text-align: center;">Kecamatan</th>
                                            <th width="20%" style="text-align: center;">No Kel</th>
                                            <th width="20%" style="text-align: center;">Kelurahan</th>
                                            <th width="20%" style="text-align: center;">NIK</th>
                                            <th width="20%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="20%" style="text-align: center;">Tanggal Lahir</th>
                                            <th width="20%" style="text-align: center;">Status Ektp</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: center;"><?php echo $row->NO_KEC ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->NAMA_KEC ;?></td>
                                                <th width="20%" style="text-align: center;"><?php echo $row->NO_KEL ;?></th>
                                                <th width="20%" style="text-align: center;"><?php echo $row->NAMA_KEL ;?></th>
                                                <th width="20%" style="text-align: center;"><span style="color: #fff">,</span><?php echo $row->NIK ;?></th>
                                                <th width="20%" style="text-align: center;"><?php echo $row->NAMA_LGKP ;?></th>
                                                <th width="20%" style="text-align: center;"><?php echo $row->TGL_LHR ;?></th>
                                                <th width="20%" style="text-align: center;"><?php echo $row->EKTP_CURRENT_STATUS_CODE ;?></th>
                                                
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="30%" colspan="7" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->JML),0,',','.');?></th>
                                            
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
           <?php $this->view('shared/footer_detail'); ?>
        </div>