<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url()?>">Absensi</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form >
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Tanggal Daily</b></h3>
                                        <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal"/> </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                  <button type="button" class="btn btn-primary waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_back();">Back <i class="mdi mdi-arrow-left-box fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $stitle; ?></h3>
                                <div class="row">
                                <div class="col-md-4">
                                <div class="button-box">
                              
                                        <button type="submit" onclick="do_pdf();" class="btn btn-info waves-effect waves-light"> <i class="mdi mdi-file-excel-box m-r-5"></i> <span>Pdf</span></button>
                           
                                    <form target="_blank" id="pdf_data" name ="get_form" action="<?php echo $my_url; ?>/Pdf" method="post">
                                        <input type="hidden" id="pdf_tanggal" name="pdf_tanggal" value="">
                                    </form>
                                </div>
                                </div>
                                </div>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="demo-foo-row-toggler" class="table m-t-30 table-hover contact-list color-table info-table" data-page-size="100" style="margin-left: 1px; margin-right: 1px;">
                                        <thead>
                                            <tr>
                                                <th width="5%" style="text-align: center;">No</th>
                                                <th width="10%" style="text-align: center;">User Id</th>
                                                <th width="10%" style="text-align: center;">Tanggal</th>
                                                <th width="40%" style="text-align: center;">Description</th>
                                                <th width="25%" style="text-align: center;">Jumlah Aktivitas</th>
                                            </tr>
                                        </thead>
                                        <tbody id='my_data' style="border: 1px solid #e4e7ea;">
                                           
                                        </tbody>
                                        <tfoot>
                                            <tr style="border: 0px solid black;">
                                                
                                                <td colspan="5" style="border: 0px solid black;">
                                                    <div class="text-left">
                                                        <ul class="pagination"> </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tfoot id="my_foot">
                                                <tr>
                                                <th colspan="5" style="text-align: left;">Total : <span id ='jumdata'>0</span> Data</th>
                                                </tr>
                                            </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
       <?php $this->view('shared/footer_detail'); ?>