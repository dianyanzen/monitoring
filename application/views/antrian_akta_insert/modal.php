<button class="btn btn-primary btn-lg" id="do_akta_lhr" data-toggle="modal" data-target="#akta_lhr-modal" style="display: none;"></button>

           <div class="modal fade" id="akta_lhr-modal" tabindex="-1" role="dialog" aria-labelledby="akta_lhr" aria-hidden="true" >
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" aria-hidden="true" onclick="do_close()">&times;</button>
                      <h4 class="modal-title">Tambah No Akta</h4>
                    </div>
                    <div class="modal-body">
                     <div class="row">
                            <div class="col-md-12">
                              <form class="form-horizontal" role="form">
                                <fieldset>

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Nik</label>
                                    <div class="col-sm-10">
                                               <input class="form-control" id="mdl_nik" placeholder="" type="text" value="" readonly>
                                
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Akta Lahir</label>
                                    <div class="col-sm-10">
                                      <select class="form-control" name="mdl_ststus_akta" id="mdl_ststus_akta" value="">
                                       <option value='1'>1. TIDAK ADA AKTA</option>
                                       <option value='2'>2. ADA AKTA</option>
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">No Akta</label>
                                    <div class="col-sm-10">
                                      <input type="text" placeholder="No Akta"  name="mdl_no_akta_lhr" id="mdl_no_akta_lhr"  class="form-control input-modal" >
                                    </div>
                                  </div>



                                  <!-- Text input-->
                                 
                                  
                                     <div class="col-sm-12" id="bantuan_text">
            
                                  </div>
                                  
                                </fieldset>
                              </form>
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->
                        </div>

                    <div class="modal-footer" style="margin-top: 0px !important;">
                      <button type="button" id="closemodal" class="btn btn-default" data-dismiss="modal" onclick="onClearModal()">Tutup</button>
                      <button type="button" id="go_send" class="btn btn-primary" onclick="do_save()">Kirim</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->