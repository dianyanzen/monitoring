<!DOCTYPE html>
<html lang="en">
<?php $this->view('shared/head'); ?>
<body class="fix-header">
    <div id="wrapper">
            <?php $this->view('shared/nav_top'); ?>
            <?php $this->view('shared/nav_side'); ?>
            <?php $this->view('Antrian_akta_insert/content'); ?>
            <?php $this->view('Antrian_akta_insert/modal'); ?>
    </div>
     
    <?php $this->view('shared/footer'); ?>
    
    <?php $this->view('Antrian_akta_insert/javascript'); ?>
</body>
</html>