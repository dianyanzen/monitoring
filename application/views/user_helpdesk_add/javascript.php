     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function on_add_level() {
			if (validationdaily()){
                do_save();
            }
           
        }
        function on_back() {
            block_screen();
            var url = "<?php echo base_url()?>";
            url += "Setting/Helpdesk";
            var win = window.location.replace(url);
            win.focus();
        }
        function validationdaily() {
        var helpdesk_cd = $("#helpdesk_cd");
            if (helpdesk_cd.val().length == 0) {                
                  swal("Warning!", "Level Code Cannot Be Empty !", "warning");  
                 return false;
            }
        var helpdesk_nm = $("#helpdesk_nm");
            if (helpdesk_nm.val().length == 0) {                
                  swal("Warning!", "Level Name Cannot Be Empty !", "warning");  
                 return false;
            }

            return true;
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/save_helpdesk",
                    dataType: "json",
                    data: {
                        helpdesk_cd : $("#helpdesk_cd").val(),
                        helpdesk_nm : $("#helpdesk_nm").val(),

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }

        $(document).ready(function() {
             get_max();
        });
    
    function get_max(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/max_helpdesk",
                    dataType: "json",
                    data: {
                    },
                    beforeSend:
                    function () {
                       
                    },
                    success: function (data) {
                      $('#helpdesk_cd').val(data.jml);
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {

                    }
                });   
    }
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    function on_clear() {
        get_max();
        $('#helpdesk_nm').val("");
        $('#helpdesk_nm').focus();
    }
    </script>