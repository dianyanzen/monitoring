    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script>
         $(document).ready(function () {
        //ktpel section
        var chart_ktpel = new CanvasJS.Chart("chart_ktpel", {
            title: {
                text: "Detail Status Rekam Ktp-El"
            },
            axisY: {
                title: "",
                suffix: ""
            },
            data: [{
                type: "column", 
                yValueFormatString: "#,### ",
                indexLabel: "{y}",
                dataPoints: [
                    { label: "Perekaman Ktp-El", y: 0 },
                    { label: "Pencetakan KTP-EL", y: 0 },
                    { label: "Print Ready Record", y: 0 },
                    { label: "Sent For Enrollment", y: 0 },
                    { label: "PR Suket", y: 0 },
                    { label: "Pengeluaran Blangko", y: 0 },
                    { label: "Sisa Blangko E-Ktp", y: 0 },
                    { label: "Duplicate Record", y: 0 },
                    
                ]
            }]
        });

        function updateChart_ktpel(data,jumlah,title_data) {
            var boilerColor, deltaY, yVal;
            var dps = chart_ktpel.options.data[0].dataPoints;
            dps[data] = {label: title_data, y: jumlah, color: '#41b3f9'}; 
            chart_ktpel.options.data[0].dataPoints = dps; 
            chart_ktpel.render();
        };

       
        function get_data_ktpel(){
             $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m1",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                    updateChart_ktpel(0,parseInt(data.perekaman_today),"Perekaman Ktp-El");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m2",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(1,parseInt(data.pencetakan_today),"Pencetakan KTP-EL");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                  
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m3",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(2,parseInt(data.sisa_prr),"Print Ready Record");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                  
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m4",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(3,parseInt(data.sisa_sfe),"Sent For Enrollment");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                  
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m5",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(4,parseInt(data.sisa_suket),"PR Suket");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                  
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m6",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(5,parseInt(data.blangko_out),"Sisa Blangko E-Ktp");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                  
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m7",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(6,parseInt(data.duplicate),"Duplicate Record");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                  
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_m8",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(7,parseInt(data.sisa_blangko),"Pengeluaran Blangko");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                  
                }
            });

        }
       
        get_data_ktpel();
        //dafduk section
         var chart_dafduk = new CanvasJS.Chart("chart_dafduk", {
            title: {
                text: "Detail Pelayanan Pendaftaran Penduduk"
            },
            axisY: {
                title: "",
                suffix: ""
            },
            data: [{
                type: "column", 
                yValueFormatString: "#,### ",
                indexLabel: "{y}",
                dataPoints: [
                    { label: "Pencetakan KK", y: 0 },
                    { label: "Pencetakan Kia", y: 0 },
                    { label: "Kepindahan Antar Kab", y: 0 },
                    { label: "Kepindahan Antar Kec", y: 0 },
                    { label: "Kepindahan Dalam Kec", y: 0 },
                    { label: "Kedatangan Antar Kab", y: 0 },
                    { label: "Kedatangan Antar Kec", y: 0 },
                    { label: "Kedatangan Dalam Kec", y: 0 },
                    { label: "Nik Baru", y: 0 },
                    
                ]
            }]
        });
            function updateChart_dafduk(data,jumlah,title_data) {
            var boilerColor, deltaY, yVal;
            var dps = chart_dafduk.options.data[0].dataPoints;
            dps[data] = {label: title_data, y: jumlah, color: '#41b3f9'}; 
            chart_dafduk.options.data[0].dataPoints = dps; 
            chart_dafduk.render();
        };
        function get_data_dafduk(){
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_kk",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(0,parseInt(data.pen_kk),"Pencetakan KK");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_kia",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(1,parseInt(data.pen_kia),"Pencetakan Kia");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_pdh_akab",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(2,parseInt(data.pen_pindah_akab),"Kepindahan Antar Kab");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_pdh_akec",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(3,parseInt(data.pen_pindah_akec),"Kepindahan Antar Kec");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_pdh_dkec",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(4,parseInt(data.pen_pindah_dkec),"Kepindahan Dalam Kec");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_dtg_akab",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(5,parseInt(data.pen_datang_akab),"Kedatangan Antar Kab");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_dtg_akec",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(6,parseInt(data.pen_datang_akec),"Kedatangan Antar Kec");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_dtg_dkec",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(7,parseInt(data.pen_datang_dkec),"Kedatangan Dalam Kec");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_nik",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(8,parseInt(data.pen_nik_baru),"Nik Baru");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
        }
         get_data_dafduk();
         //capil section
         var chart_capil = new CanvasJS.Chart("chart_capil", {
            title: {
                text: "Detail Pelayanan Pencatatan Sipil"
            },
            axisY: {
                title: "",
                suffix: ""
            },
            data: [{
                type: "column", 
                yValueFormatString: "#,### ",
                indexLabel: "{y}",
                dataPoints: [
                    { label: "Akta Kelahiran Umum", y: 0 },
                    { label: "Akta Kelahiran Terlambat", y: 0 },
                    { label: "Akta Kematian", y: 0 },
                    { label: "Akta Perkawinan", y: 0 },
                    { label: "Akta Perceraian", y: 0 },
                    
                    
                ]
            }]
        });
            function updateChart_capil(data,jumlah,title_data) {
            var boilerColor, deltaY, yVal;
            var dps = chart_capil.options.data[0].dataPoints;
            dps[data] = {label: title_data, y: jumlah, color: '#41b3f9'}; 
            chart_capil.options.data[0].dataPoints = dps; 
            chart_capil.render();
        };
        function get_data_capil(){
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_akta_lu",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_capil(0,parseInt(data.pen_lahir_lu),"Akta Kelahiran Umum");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_akta_lt",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_capil(1,parseInt(data.pen_lahir_lt),"Akta Kelahiran Terlambat");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_akta_mt",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_capil(2,parseInt(data.pen_mati),"KepindahanAkta Kematian");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_akta_kwn",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_capil(3,parseInt(data.pen_kawin),"Akta Perkawinan");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard/get_dashboard_akta_cry",
                data: {},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_capil(4,parseInt(data.pen_cerai),"Akta Perceraian");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
        }
         get_data_capil();

        });
        $(document).ready(function () {
           
        });
        </script>
    <script type="text/javascript">
    $(document).ready(function () {
    var interval = setInterval(function() {
        var momentNow = moment();
        $('#date-part').html(momentNow.format('DD-MM-YYYY'));
        $('#time-part').html(momentNow.format('HH:mm:ss'));
    }, 100);
       
    });
   function do_ktpl(){
            window.location.href = "<?php echo site_url('Dashboard_ktpel'); ?>";
    }
    function do_bio(){
            window.location.href = "<?php echo site_url('Dashboard_biodata'); ?>";
    }
    function do_mobi(){
            window.location.href = "<?php echo site_url('Dashboard_mobilitas'); ?>";
    }
    function do_capil(){
            window.location.href = "<?php echo site_url('Dashboard_capil'); ?>";
    }
    function get_data_m1() {
       
        }
    </script>   
    