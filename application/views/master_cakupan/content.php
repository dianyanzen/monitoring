<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url()?>">Laporan</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Jenis Laporan</b></h3>
                                        <select class="form-control select2" name="mlap" id="mlap">
                                <option  value="0">-- Pilih Menu --</option>
                            </select>
                            
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <h3 class="box-title m-b-0"><b>Kecamatan</b></h3>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                <option  value="0">-- Pilih Kecamatan --</option>
                            </select>
                            
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <h3 class="box-title m-b-0"><b>Kelurahan</b></h3>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <?php if (!empty($jenis_lap)){ ?>
                <?php if ($jenis_lap == 1 ){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box" id="my_box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk</th>
                                            <th width="20%" style="text-align: right;">Memiliki Nik</th>
                                            <th width="20%" style="text-align: right;">Tidak Memiliki Nik</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan Nik</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <!-- <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 2){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Keluarga</th>
                                            <th width="20%" style="text-align: right;">Memiliki KK</th>
                                            <th width="20%" style="text-align: right;">Tidak Memiliki KK</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan KK</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                   <!--  <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 3){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk Usia 0 - 17</th>
                                            <th width="20%" style="text-align: right;">Memiliki KIA</th>
                                            <th width="20%" style="text-align: right;">Tidak Memiliki KIA</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan KIA</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <!-- <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 4){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk Wajib KTP</th>
                                            <th width="20%" style="text-align: right;">Sudah Perekaman</th>
                                            <th width="20%" style="text-align: right;">Belum Perekaman</th>
                                            <th width="20%" style="text-align: right;">Ratio Perekaman KTP-El</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                   <!--  <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 5){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk Wajib KTP</th>
                                            <th width="20%" style="text-align: right;">Sudah Cetak</th>
                                            <th width="20%" style="text-align: right;">Belum Cetak</th>
                                            <th width="20%" style="text-align: right;">Ratio Pencetakan KTP-El</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                   <!--  <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 6){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk Usia 0 - 18</th>
                                            <th width="20%" style="text-align: right;">Sudah Memiliki Akta</th>
                                            <th width="20%" style="text-align: right;">Belum Memiliki Akta</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan Akta 0-18</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                   <!--  <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 7){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk Usia 18 Keatas</th>
                                            <th width="20%" style="text-align: right;">Sudah Memiliki Akta</th>
                                            <th width="20%" style="text-align: right;">Belum Memiliki Akta</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan Akta 18 Keatas</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                   <!--  <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 8){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk</th>
                                            <th width="20%" style="text-align: right;">Sudah Memiliki Akta</th>
                                            <th width="20%" style="text-align: right;">Belum Memiliki Akta</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan Akta Keseluruhan</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <!-- <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 9){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk Status Kawin</th>
                                            <th width="20%" style="text-align: right;">Sudah Memiliki Akta Perkawinan</th>
                                            <th width="20%" style="text-align: right;">Belum Memiliki Akta Perkawinan</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan Akta Keseluruhan Perkawinan</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <!-- <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 10){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk Status Kawin Non Muslim</th>
                                            <th width="20%" style="text-align: right;">Sudah Memiliki Akta Perkawinan</th>
                                            <th width="20%" style="text-align: right;">Belum Memiliki Akta Perkawinan</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan Akta Perkawinan Non Muslim</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <!-- <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 11){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk Status Kawin Muslim</th>
                                            <th width="20%" style="text-align: right;">Sudah Memiliki Akta Perkawinan</th>
                                            <th width="20%" style="text-align: right;">Belum Memiliki Akta Perkawinan</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan Akta Perkawinan Muslim</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <!-- <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 12){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk Status Cerai</th>
                                            <th width="20%" style="text-align: right;">Sudah Memiliki Akta Perceraian</th>
                                            <th width="20%" style="text-align: right;">Belum Memiliki Akta Perceraian</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan Akta Keseluruhan Perceraian</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <!-- <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 13){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk Status Cerai Non Muslim</th>
                                            <th width="20%" style="text-align: right;">Sudah Memiliki Akta Perceraian</th>
                                            <th width="20%" style="text-align: right;">Belum Memiliki Akta Perceraian</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan Akta Perceraian< Non Muslim</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <!-- <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 14){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil <?php echo ucwords(strtolower($this->session->userdata(S_NM_KAB))); ?></p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk Status Cerai Muslim</th>
                                            <th width="20%" style="text-align: right;">Sudah Memiliki Akta Perceraian</th>
                                            <th width="20%" style="text-align: right;">Belum Memiliki Akta Perceraian</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan Akta Perceraian Muslim</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->NO_WIL ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->NAMA_WIL ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->R3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->PRS ;?></td>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <!-- <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo $jumlah[0]->PRS; ?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
            </div>
            <!-- /.container-fluid -->
           <?php $this->view('shared/footer_detail'); ?>
        </div>