<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                 <div class="row">
                    <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" maxlength="16" onkeypress="return isNumberKey(event)"/></div>
                                </div>
                                </div>
                            <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">                
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_option" type="checkbox" checked disabled="true">
                                            <label for="cb_option"> Option</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="biometric_option" id="biometric_option">
                                         <option  value="0">-- Pilih Jenis Perbaikan Biometric --</option>
                                         <option  value="2">Hapus Biometric Pencetakan</option>
                                         <option  value="1">Hapus Biometric Perekaman</option>
                                         <!-- <option  value="3">Inject Biometric Pencetakan Dari Perekaman</option> -->
                                        </select>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <?php if (!empty($data)){ ?>
                    <?php if ($option == 0){ ?>
                         <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">Data Biometric</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="white-box">
                                        <?php if (!empty($face)){ ?>
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="data:image/jpeg;base64,<?php echo base64_encode($face[0]->FACE->load()); ?>" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="data:image/jpeg;base64,<?php echo base64_encode($face[0]->FACE->load()); ?>"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php }else{ ?>
                                            <?php if ($data[0]->JENIS_KLMIN == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 
                                        <?php } ?> 
                                    </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4><?php echo $data[0]->NAMA_LGKP; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4><?php echo $data[0]->NIK; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4><?php echo $data[0]->NO_KK; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4><?php echo $data[0]->TMPT_LHR; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4><?php echo $data[0]->TGL_LHR; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4><?php echo $data[0]->JENIS_KLMIN; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        <?php if ($data[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_REGIONAL' || $data[0]->CURRENT_STATUS_CODE == 'PACKET_RETRY' || $data[0]->CURRENT_STATUS_CODE == 'RECEIVED_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'PROCESSING' || $data[0]->CURRENT_STATUS_CODE == 'SENT_FOR_ENROLLMENT' || $data[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'SEARCH_FAILURE_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_RECORD' || $data[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_IN_PROCESS' || $data[0]->CURRENT_STATUS_CODE == 'SENT_FOR_DEDUP' || $data[0]->CURRENT_STATUS_CODE == 'DUPLICATE_RECORD'){ ?>
                                                        <td> <h4 class="text-danger"><?php echo $data[0]->CURRENT_STATUS_CODE; ?></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><?php echo $data[0]->CURRENT_STATUS_CODE; ?></h4> </td>
                                                    <?php } ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                </div>
                    <?php }else if ($option == 1){ ?>
                         <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">Data Biometric Database Perekaman Local</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="white-box">
                                        <?php if (!empty($face)){ ?>
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img id="face_holder" src="data:image/jpeg;base64,<?php echo base64_encode($face[0]->FACE->load()); ?>" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" id="face_holder_href" href="data:image/jpeg;base64,<?php echo base64_encode($face[0]->FACE->load()); ?>"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php }else{ ?>
                                            <?php if ($data[0]->JENIS_KLMIN == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img id="face_holder" src="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" id="face_holder_href" href="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img id="face_holder" src="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" id="face_holder_href" href="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 
                                        <?php } ?> 
                                    </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4 id="txt_nama_lgkp"><?php echo $data[0]->NAMA_LGKP; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4 id="txt_nik"><?php echo $data[0]->NIK; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4 id="txt_no_kk"><?php echo $data[0]->NO_KK; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4 id="txt_tmpt_lhr"><?php echo $data[0]->TMPT_LHR; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4 id="txt_tgl_lhr"><?php echo $data[0]->TGL_LHR; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4 id="txt_jenis_klmin"><?php echo $data[0]->JENIS_KLMIN; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        <?php if ($data[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_REGIONAL' || $data[0]->CURRENT_STATUS_CODE == 'PACKET_RETRY' || $data[0]->CURRENT_STATUS_CODE == 'RECEIVED_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'PROCESSING' || $data[0]->CURRENT_STATUS_CODE == 'SENT_FOR_ENROLLMENT' || $data[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'SEARCH_FAILURE_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_RECORD' || $data[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_IN_PROCESS' || $data[0]->CURRENT_STATUS_CODE == 'SENT_FOR_DEDUP' || $data[0]->CURRENT_STATUS_CODE == 'DUPLICATE_RECORD'){ ?>
                                                        <td> <h4 class="text-danger" id="txt_status_ktp"><?php echo $data[0]->CURRENT_STATUS_CODE; ?></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success" id="txt_status_ktp"><?php echo $data[0]->CURRENT_STATUS_CODE; ?></h4> </td>
                                                    <?php } ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    <button class="btn btn-block btn-danger btn-rounded" onclick="do_delete_biometric_rekam();" id="delete_biometric_rekam"><i class="ti-trash"></i> BERSIHKAN BIOMETRIC DATABASE PEREKAMAN LOCAL </button>
                                    </div>
                                    <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <h3 class="box-title m-t-40">Database Perekaman</h3>
                                        <div class="table-responsive">
                                            <table class="table" id="table_rekam">
                                                <tbody>
                                                    <tr>
                                                        <td width="170"></td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <h3 class="box-title m-t-40">Database Pencetakan</h3>
                                        <div class="table-responsive">
                                            <table class="table" id="table_cetak">
                                                <tbody>
                                                    <tr>
                                                        <td width="170"></td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <?php }else if ($option == 2){ ?>
                         <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">Data Biometric Database Pencetakan Local</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="white-box">
                                        <?php if (!empty($face)){ ?>
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img id="face_holder" src="data:image/jpeg;base64,<?php echo base64_encode($face[0]->FACE->load()); ?>" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" id="face_holder_href" href="data:image/jpeg;base64,<?php echo base64_encode($face[0]->FACE->load()); ?>"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php }else{ ?>
                                            <?php if ($data[0]->JENIS_KLMIN == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img id="face_holder" src="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" id="face_holder_href" href="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img id="face_holder" src="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" id="face_holder_href" href="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 
                                        <?php } ?> 
                                    </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4 id="txt_nama_lgkp"><?php echo $data[0]->NAMA_LGKP; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4 id="txt_nik"><?php echo $data[0]->NIK; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4 id="txt_no_kk"><?php echo $data[0]->NO_KK; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4 id="txt_tmpt_lhr"><?php echo $data[0]->TMPT_LHR; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4 id="txt_tgl_lhr"><?php echo $data[0]->TGL_LHR; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4 id="txt_jenis_klmin"><?php echo $data[0]->JENIS_KLMIN; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        <?php if ($data[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_REGIONAL' || $data[0]->CURRENT_STATUS_CODE == 'PACKET_RETRY' || $data[0]->CURRENT_STATUS_CODE == 'RECEIVED_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'PROCESSING' || $data[0]->CURRENT_STATUS_CODE == 'SENT_FOR_ENROLLMENT' || $data[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'SEARCH_FAILURE_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_RECORD' || $data[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_IN_PROCESS' || $data[0]->CURRENT_STATUS_CODE == 'SENT_FOR_DEDUP' || $data[0]->CURRENT_STATUS_CODE == 'DUPLICATE_RECORD'){ ?>
                                                        <td> <h4 class="text-danger" id="txt_status_ktp"><?php echo $data[0]->CURRENT_STATUS_CODE; ?></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success" id="txt_status_ktp"><?php echo $data[0]->CURRENT_STATUS_CODE; ?></h4> </td>
                                                    <?php } ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    <button class="btn btn-block btn-danger btn-rounded" onclick="do_delete_biometric_cetak();" id="delete_biometric_cetak"><i class="ti-trash"></i> BERSIHKAN BIOMETRIC DATABASE PENCETAKAN LOCAL </button>
                                    </div>
                                    <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h3 class="box-title m-t-40">Database Pencetakan</h3>
                                        <div class="table-responsive">
                                            <table class="table" id="table_cetak">
                                                <tbody>
                                                    <tr>
                                                        <td width="340"></td>
                                                        <td> </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }else if ($option == 3){ ?>
                         <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">Inject Data Biometric Database Perekaman Local</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="white-box">
                                        <?php if (!empty($face)){ ?>
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="data:image/jpeg;base64,<?php echo base64_encode($face[0]->FACE->load()); ?>" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="data:image/jpeg;base64,<?php echo base64_encode($face[0]->FACE->load()); ?>"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php }else{ ?>
                                            <?php if ($data[0]->JENIS_KLMIN == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 
                                        <?php } ?> 
                                    </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4><?php echo $data[0]->NAMA_LGKP; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4><?php echo $data[0]->NIK; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4><?php echo $data[0]->NO_KK; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4><?php echo $data[0]->TMPT_LHR; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4><?php echo $data[0]->TGL_LHR; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4><?php echo $data[0]->JENIS_KLMIN; ?></h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        <?php if ($data[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_REGIONAL' || $data[0]->CURRENT_STATUS_CODE == 'PACKET_RETRY' || $data[0]->CURRENT_STATUS_CODE == 'RECEIVED_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'PROCESSING' || $data[0]->CURRENT_STATUS_CODE == 'SENT_FOR_ENROLLMENT' || $data[0]->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'SEARCH_FAILURE_AT_CENTRAL' || $data[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_RECORD' || $data[0]->CURRENT_STATUS_CODE == 'ADJUDICATE_IN_PROCESS' || $data[0]->CURRENT_STATUS_CODE == 'SENT_FOR_DEDUP' || $data[0]->CURRENT_STATUS_CODE == 'DUPLICATE_RECORD'){ ?>
                                                        <td> <h4 class="text-danger"><?php echo $data[0]->CURRENT_STATUS_CODE; ?></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><?php echo $data[0]->CURRENT_STATUS_CODE; ?></h4> </td>
                                                    <?php } ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    <button class="btn btn-block btn-info btn-rounded" id="inject_biometric_rekam"><i class="ti-trash"></i> INJECT BIOMETRIC DATABASE PEREKAMAN LOCAL </button>
                                    </div>
                                    <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h3 class="box-title m-t-40">Database Perekaman</h3>
                                        <div class="table-responsive">
                                            <table class="table" id="table_cetak">
                                                <tbody>
                                                    <tr>
                                                        <td width="340">DEMOGRAPHICS</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>FACES</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>FINGERS</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>IRIS</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>SIGNATURES</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>FACE_TEMPLATES</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>FINGER_TEMPLATES</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>IRIS_TEMPLATES</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>SIGNATURE_TEMPLATES</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>BIOMETRICS_LOSSLESS</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>BIOMETRIC_EXCEPTIONS</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>BIOMETRIC_DIAGNOSTICS</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>AUDITS</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>DUPLICATE_RESULTS</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>CARD_ISSUANCE_EVIDENCE</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>MIDDLEWARE_DIAGNOSTICS</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>REPROCESS_FAILED_NIKS</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                    <tr>
                                                        <td>MANUAL_DEDUP_DIAGNOSTICS</td>
                                                        <td> ADA</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php }else{ ?>
                <?php if (!empty($is_ada)){ ?>
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">Data Biometric Tidak Ditemukan</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="white-box">
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url()?>assets/plugins/images/calming-cat.gif" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url()?>assets/plugins/images/calming-cat.gif"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
            <!-- /.container-fluid -->
         <?php $this->view('shared/footer_detail'); ?>