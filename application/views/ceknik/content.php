 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                    <?php if (!empty($_GET['nik'])){ ?>
                <form name ="get_form" action="<?php echo $my_url; ?>?nik=<?php echo $_GET['nik']; ?>" method="post">
                    <?php }else{ ?>
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    <?php } ?>
                    <div class="col-sm-12">
                        <div class="white-box">
                            
                                <?php if (!empty($_GET['nik'])){ ?>
                                <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" onkeypress="return isNumberKey(event)" onchange="onlyNum()" value="<?php echo $_GET['nik']; ?>" readonly/></div>
                                </div>
                                </div>
                                <?php }else{ ?>
                                <?php if ($my_url != 'Push'){ ?>
                                    <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> Kelurahan</label>
                                        </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                                <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> No KK</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="no_kk" name="no_kk" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nama Lengkap</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nama_lgkp" name="nama_lgkp" /></div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl_lhr" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl_lhr"> Tanggal Lahir</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">         
                                    <div class="form-group">
                                         <input type="text" name="tgl_lhr" class="form-control" id="tgl_lhr" value="" placeholder="DD-MM-YYYY" readonly></div>
                                </div>
                                </div>
                                <?php } ?>
                                <?php } ?>
                            
                           <?php if ($my_url == 'Push'){ ?>
                            <?php if (!empty($data)){ ?>
                            <div class="row">
                                  <button type="button" class="btn btn-primary waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="openTab('<?php echo base_url()?>Check/Ktpel');">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                  
                            </div>
                            <?php }else{ ?>
                            <div class="row">
                                <button type="submit" name="do_search" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Push <i class="mdi  mdi-near-me fa-fw"></i></button>
                                   <button type="button" class="btn btn-primary waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="openTab('<?php echo base_url()?>Check/Ktpel');">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                  
                            </div>
                            <?php } ?> 
                           <?php }else{ ?>
                           <div class="row">
                                <button type="submit" name="do_search" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                                  
                            </div>
                           <?php } ?>
                            
                        </div>
                    </div>
                    </form>
                </div>
                 <?php if ($my_url == 'Push'){ ?>
                            <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Status Yang Dapat Di Push Ulang Oleh Monitoring</div>
                            <div class="panel-wrapper collapse in">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Status Code</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center">1</td>
                                            <td>ENROLL_FAILURE_AT_REGIONAL</td>
                                        </tr>
                                        <tr>
                                            <td align="center">2</td>
                                            <td>PACKET_RETRY</td>
                                        </tr>
                                        <tr>
                                            <td align="center">3</td>
                                            <td>RECEIVED_AT_CENTRAL</td>
                                        </tr>
                                         <tr>
                                            <td align="center">4</td>
                                            <td><b style="font-weight:bold !important; color:red;">PROCESSING *</b></td>
                                        </tr>
                                         <tr>
                                            <td align="center">5</td>
                                            <td><b style="font-weight:bold !important; color:red;">SENT_FOR_ENROLLMENT *</b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Apabila Status E-Ktp Kembali Seperti Awal, Coba Lagi Next Time</div>
                            <div class="panel-wrapper collapse in">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Status Code</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center">6</td>
                                            <td><b style="font-weight:bold !important; color:red;">ENROLL_FAILURE_AT_CENTRAL *</b></td>
                                        </tr>
                                        <tr>
                                            <td align="center">7</td>
                                            <td>SEARCH_FAILURE_AT_CENTRAL</td>
                                        </tr>
                                        <tr>
                                            <td align="center">8</td>
                                            <td><b style="font-weight:bold !important; color:red;">ADJUDICATE_RECORD *</b></td>
                                        </tr>
                                         <tr>
                                            <td align="center">9</td>
                                            <td>ADJUDICATE_IN_PROCESS</td>
                                        </tr>
                                         <tr>
                                            <td align="center">10</td>
                                            <td>SENT_FOR_DEDUP</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                            </div>
                            <?php } ?>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $stitle; ?></h3>
                            <?php if (!empty($data)){ 
                                $niks = array();
                                foreach($data as $row){
                                    $niks[] = $row->NIK;
                                    }
                                ?>
                                <div class="row">
                                <div class="col-md-4">
                                <div class="button-box">
                                        <button type="submit" onclick="do_export();" class="btn btn-info waves-effect waves-light"> <i class="mdi mdi-file-excel-box m-r-5"></i> <span>Export</span></button>
                                    
                                        <button type="submit" onclick="do_pdf();" class="btn btn-info waves-effect waves-light"> <i class="mdi mdi-file-excel-box m-r-5"></i> <span>Pdf</span></button>
                                    
                                    <form target="_blank" id="export_data" name ="get_form" action="<?php echo $my_url; ?>/Export" method="post">
                                        <input type="hidden" name="niks" id="export_niks" value="<?php echo implode(',',$niks); ?>">
                                    </form>
                                    <form target="_blank" id="pdf_data" name ="get_form" action="<?php echo $my_url; ?>/Pdf" method="post">
                                        <input type="hidden" id="pdf_niks" name="niks" value="<?php echo implode(',',$niks); ?>">
                                    </form>
                                </div>
                                </div>
                                </div>
                            <?php } ?>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="demo-foo-row-toggler" class="table m-t-30 table-hover contact-list color-table info-table" data-page-size="100" style="margin-left: 1px; margin-right: 1px;">
                                        <thead>
                                            <tr>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA </th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NIK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">LAHIR</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">STATUS KTP</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">CETAK KTP</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">CETAK SUKET</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">PENGAJUAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NOMOR KK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEC</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEL</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">ALAMAT</th>
                                            </tr>
                                        </thead>
                                        <tbody id='my_data' style="border: 1px solid #e4e7ea;">
                                             <?php
                                        if (!empty($data)){
                                            $i = 0;
                                           foreach($data as $row){
                                            $i++;
                                            ?>
                                            <tr style="border: 1px solid #e4e7ea;">
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $i ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                   
                                                            <img src='http://10.32.73.222:8080/Siak/<?php echo $row->PATH ;?>/<?php echo $row->NIK ;?>.jpg'  alt="" width="36" class="img-circle">
                                                          <?php echo $row->NAMA_LGKP ;?>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->NIK ;?>
                                                     
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->TMPT_LHR ;?><br><?php echo $row->TGL_LHR ;?>
                                                     
                                                </td>
                                                
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->CURRENT_STATUS_CODE ;?><br />
                                                <?php if ($row->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_REGIONAL' || $row->CURRENT_STATUS_CODE == 'PACKET_RETRY' || $row->CURRENT_STATUS_CODE == 'RECEIVED_AT_CENTRAL' || $row->CURRENT_STATUS_CODE == 'PROCESSING' || $row->CURRENT_STATUS_CODE == 'SENT_FOR_ENROLLMENT' || $row->CURRENT_STATUS_CODE == 'ENROLL_FAILURE_AT_CENTRAL' || $row->CURRENT_STATUS_CODE == 'SEARCH_FAILURE_AT_CENTRAL' || $row->CURRENT_STATUS_CODE == 'ADJUDICATE_RECORD' || $row->CURRENT_STATUS_CODE == 'ADJUDICATE_IN_PROCESS' || $row->CURRENT_STATUS_CODE == 'SENT_FOR_DEDUP'){ ?>
                                                <?php if ($my_url != 'Push'){ ?>
                                                <?php if (!empty($data)){ ?>
                                                <button type="button" class="btn btn-warning btn-xs" id="btn-repush" onclick="openInNewTab('<?php echo base_url()?>Check/Ktpel/Push?nik=<?php echo $row->NIK ;?>');" style="margin-top: 5px;">Re-Push <i class="mdi  mdi-near-me fa-fw"></i></button>
                                                <?php } ?>
                                                <?php } ?>
                                                <?php } ?>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                    <i class="mdi mdi-account-circle text-white" style="float: none; color: #41b3f9 !important;"></i><span style="float: none; color: #41b3f9 !important;"> <?php echo $row->KTP_BY ;?></span><br>
                                                    <i class="mdi mdi-label text-white" style="float: none; color: #41b3f9 !important;"></i><span style="float: none; color: #41b3f9 !important;"> <?php echo $row->KTP_DT ;?></span><br>
                                                    <?php if ($row->JUMLAH_CETAK != 0){ ?>
                                                    <i class="mdi mdi-printer-alert text-white" style="float: none; color: #41b3f9 !important;"></i><span style="float: none; color: #41b3f9 !important;"> Dicetak Sebanyak <?php echo $row->JUMLAH_CETAK ;?> Kali</span>
                                                    <?php } ?>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                    <i class="mdi mdi-account-circle text-white" style="float: none; color: #41b3f9 !important;"></i><span style="float: none; color: #41b3f9 !important;"> <?php echo $row->SUKET_BY ;?></span><br>
                                                    <i class="mdi mdi-label text-white" style="float: none; color: #41b3f9 !important;"></i><span style="float: none; color: #41b3f9 !important;"> <?php echo $row->SUKET_DT ;?></span>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                    <i class="mdi mdi-account-circle text-white" style="float: none; color: #41b3f9 !important;"></i><span style="float: none; color: #41b3f9 !important;"> <?php echo $row->REQ_BY ;?></span><br>
                                                    <i class="mdi mdi-label text-white" style="float: none; color: #41b3f9 !important;"></i><span style="float: none; color: #41b3f9 !important;"> <?php echo $row->REQ_DATE ;?></span><br>
                                                    <i class="mdi mdi-verified text-white" style="float: none; color: #41b3f9 !important;"></i><span style="float: none; color: #41b3f9 !important;"> <?php echo $row->PROC_BY ;?></span><br>
                                                    <i class="mdi mdi-spotify text-white" style="float: none; color: #41b3f9 !important;"></i><span style="float: none; color: #41b3f9 !important;"> <?php echo $row->STEP_PROC ;?></span>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->NO_KK ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->NAMA_KEC ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->NAMA_KEL ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->ALAMAT ;?><br>RT. <?php echo $row->RT ;?> RW. <?php echo $row->RW ;?><br>KODE POS : <?php echo $row->KODE_POS ;?></td>
                                            </tr>

                                            <?php } }else{ ?>
                                            
                                                <tr>
                                                <td colspan="10" style="text-align: center;">No data available</td>
                                            </tr>
                                            <?php }?>

                                        </tbody>
                                        <tfoot>
                                            <tr style="border: 0px solid black;">
                                                
                                                <td colspan="10" style="border: 0px solid black;">
                                                    <div class="text-left">
                                                        <ul class="pagination"> </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tfoot id="my_foot">
                                            
                                                <?php if (!empty($data)){ ?>
                                                <tr>
                                                <th colspan="10" style="text-align: left;">Total : <?php echo $i ;?> Data</th>
                                                </tr>
                                                <?php }else{ ?>
                                                <tr>
                                                <th colspan="10" style="text-align: left;">Total : 0 Data</th>
                                                </tr>
                                                <?php } ?>
                                            </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       <?php $this->view('shared/footer_detail'); ?>