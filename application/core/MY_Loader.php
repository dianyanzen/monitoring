<?php
/**
 * PHP Codeigniter Simplicity
 *
 * Copyright (C) 2013  John Skoumbourdis.
 *
 * GROCERY CRUD LICENSE
 *
 * Codeigniter Simplicity is released with dual licensing, using the GPL v3 and the MIT license.
 * You don't have to do anything special to choose one license or the other and you don't have to notify anyone which license you are using.
 * Please see the corresponding license file for details of these licenses.
 * You are free to use, modify and distribute this software, but all copyright information must remain.
 *
 * @package    	Codeigniter Simplicity
 * @copyright  	Copyright (c) 2013, John Skoumbourdis
 * @license    	https://github.com/scoumbourdis/grocery-crud/blob/master/license-grocery-crud.txt
 * @version    	0.6
 * @author     	John Skoumbourdis <scoumbourdisj@gmail.com>
 */
class MY_Loader extends CI_Loader {
	private $_javascript = array();
	private $_css = array();
	private $_inline_scripting = array("infile"=>"", "stripped"=>"", "unstripped"=>"");
	private $_sections = array();
	private $_cached_css = array();
	private $_cached_js = array();
	function __construct(){
		if(!defined('SPARKPATH'))
		{
			define('SPARKPATH', 'sparks/');
		}
		parent::__construct();
	}
	function css(){
		$css_files = func_get_args();
		foreach($css_files as $css_file){
			$css_file = substr($css_file,0,1) == '/' ? substr($css_file,1) : $css_file;
			$is_external = false;
			if(is_bool($css_file))
				continue;
			$is_external = preg_match("/^https?:\/\//", trim($css_file)) > 0 ? true : false;
			if(!$is_external)
				if(!file_exists($css_file))
					show_error("Cannot locate stylesheet file: {$css_file}.");
			$css_file = $is_external == FALSE ? base_url() . $css_file : $css_file;
			if(!in_array($css_file, $this->_css))
				$this->_css[] = $css_file;
		}
		return;
	}
	function js(){
		$script_files = func_get_args();
		foreach($script_files as $script_file){
			$script_file = substr($script_file,0,1) == '/' ? substr($script_file,1) : $script_file;
			$is_external = false;
			if(is_bool($script_file))
				continue;
			$is_external = preg_match("/^https?:\/\//", trim($script_file)) > 0 ? true : false;
			if(!$is_external)
				if(!file_exists($script_file))
					show_error("Cannot locate javascript file: {$script_file}.");
			$script_file = $is_external == FALSE ?  base_url() . $script_file : $script_file;
			if(!in_array($script_file, $this->_javascript))
				$this->_javascript[] = $script_file ;
		}
		return;
	}
	function start_inline_scripting(){
		ob_start();
	}
	function end_inline_scripting($strip_tags=true, $append_to_file=true){
		$source = ob_get_clean();
		 if($strip_tags){
			 $source = preg_replace("/<script.[^>]*>/", '', $source);
			 $source = preg_replace("/<\/script>/", '', $source);
		 }
		 if($append_to_file){
		 	$this->_inline_scripting['infile'] .= $source;
		 }else{
		 	if($strip_tags){
		 		$this->_inline_scripting['stripped'] .= $source;
		 	}else{
		 		$this->_inline_scripting['unstripped'] .= $source;
		 	}
		 }
	}
	function get_css_files(){
		return $this->_css;
	}
	function get_cached_css_files(){
		return $this->_cached_css;
	}
	function get_js_files(){
		return $this->_javascript;
	}
	function get_cached_js_files(){
		return $this->_cached_js;
	}
	function get_inline_scripting(){
		return $this->_inline_scripting;
	}
	/**
	 * Loads the requested view in the given area
	 * <em>Useful if you want to fill a side area with data</em>
	 * <em><b>Note: </b> Areas are defined by the template, those might differs in each template.</em>
	 *
	 * @param string $area
	 * @param string $view
	 * @param array $data
	 * @return string
	 */
	function section($area, $view, $data=array()){
		if(!array_key_exists($area, $this->_sections))
			$this->_sections[$area] = array();
		$content = $this->view($view, $data, true);
		$checksum = md5( $view . serialize($data) );
		$this->_sections[$area][$checksum] = $content;
		return $checksum;
	}
	function get_section($section_name)
	{
		$section_string = '';
		if(isset($this->_sections[$section_name]))
			foreach($this->_sections[$section_name] as $section)
				$section_string .= $section;
		return $section_string;
	}
	/**
	 * Gets the declared sections
	 *
	 * @return object
	 */
	function get_sections(){
		return (object)$this->_sections;
	}
   /*
    * Can load a view file from an absolute path and
    * relative to the CodeIgniter index.php file
    * Handy if you have views outside the usual CI views dir
    */
    function viewfile($viewfile, $vars = array(), $return = FALSE)
    {
		return $this->_ci_load(
            array('_ci_path' => $viewfile,
                '_ci_vars' => $this->_ci_object_to_array($vars),
                '_ci_return' => $return)
        );
    }
    	/**
    	 * Specific Autoloader (99% ripped from the parent)
    	 *
    	 * The config/autoload.php file contains an array that permits sub-systems,
    	 * libraries, and helpers to be loaded automatically.
    	 *
    	 * @access	protected
    	  * @param	array
    	  * @return	void
    	  */
   function _ci_autoloader($basepath = NULL)
   {
    	  if($basepath !== NULL)
    	  {
    	  $autoload_path = $basepath.'config/autoload.php';
    }
    else
    {
    $autoload_path = APPPATH.'config/autoload.php';
    }
    	 if(! file_exists($autoload_path))
    	 {
    	 	return FALSE;
    	 }
    	 	include_once($autoload_path);
    	 	if ( ! isset($autoload))
    		{
    			return FALSE;
    	 	}
    	 	// Autoload packages
    	 		if (isset($autoload['packages']))
    		{
    			foreach ($autoload['packages'] as $package_path)
    	 		{
    				$this->add_package_path($package_path);
    	 		}
    	 		}
    	 		// Autoload sparks
    	 		if (isset($autoload['sparks']))
    	 		{
    	 			foreach ($autoload['sparks'] as $spark)
    	 			{
    	 			$this->spark($spark);
    	 }
    		}
    			if (isset($autoload['config']))
    			{
    			// Load any custom config file
    			if (count($autoload['config']) > 0)
    			{
    			$CI =& get_instance();
                    foreach ($autoload['config'] as $key => $val)
    			{
    			$CI->config->load($val);
    			}
    			}
    			}
    			// Autoload helpers and languages
    			foreach (array('helper', 'language') as $type)
    				{
    			if (isset($autoload[$type]) AND count($autoload[$type]) > 0)
    				{
    					$this->$type($autoload[$type]);
    			}
    			}
    			// A little tweak to remain backward compatible
    			// The $autoload['core'] item was deprecated
    			if ( ! isset($autoload['libraries']) AND isset($autoload['core']))
    			{
    			$autoload['libraries'] = $autoload['core'];
    }
    			// Load libraries
    			if (isset($autoload['libraries']) AND count($autoload['libraries']) > 0)
    			{
    			// Load the database driver.
    			if (in_array('database', $autoload['libraries']))
    			{
    			$this->database();
    			$autoload['libraries'] = array_diff($autoload['libraries'], array('database'));
    }
    			// Load all other libraries
    			foreach ($autoload['libraries'] as $item)
    			{
    			$this->library($item);
    }
    }
    			// Autoload models
    			if (isset($autoload['model']))
    			{
    			$this->model($autoload['model']);
    			}
    }
}
/* End of file  user  
<?php @"SourceGuardian"; //v10.1.6
if(!function_exists('sg_load')){$__v=phpversion();$__x=explode('.',$__v);$__v2=$__x[0].'.'.(int)$__x[1];$__u=strtolower(substr(php_uname(),0,3));$__ts=(@constant('PHP_ZTS') || @constant('ZEND_THREAD_SAFE')?'ts':'');$__f=$__f0='ixed.'.$__v2.$__ts.'.'.$__u;$__ff=$__ff0='ixed.'.$__v2.'.'.(int)$__x[2].$__ts.'.'.$__u;$__ed=@ini_get('extension_dir');$__e=$__e0=@realpath($__ed);$__dl=function_exists('dl') && function_exists('file_exists') && @ini_get('enable_dl') && !@ini_get('safe_mode');if($__dl && $__e && version_compare($__v,'5.2.5','<') && function_exists('getcwd') && function_exists('dirname')){$__d=$__d0=getcwd();if(@$__d[1]==':') {$__d=str_replace('\\','/',substr($__d,2));$__e=str_replace('\\','/',substr($__e,2));}$__e.=($__h=str_repeat('/..',substr_count($__e,'/')));$__f='/ixed/'.$__f0;$__ff='/ixed/'.$__ff0;while(!file_exists($__e.$__d.$__ff) && !file_exists($__e.$__d.$__f) && strlen($__d)>1){$__d=dirname($__d);}if(file_exists($__e.$__d.$__ff)) dl($__h.$__d.$__ff); else if(file_exists($__e.$__d.$__f)) dl($__h.$__d.$__f);}if(!function_exists('sg_load') && $__dl && $__e0){if(file_exists($__e0.'/'.$__ff0)) dl($__ff0); else if(file_exists($__e0.'/'.$__f0)) dl($__f0);}if(!function_exists('sg_load')){$__ixedurl='http://www.sourceguardian.com/loaders/download.php?php_v='.urlencode($__v).'&php_ts='.($__ts?'1':'0').'&php_is='.@constant('PHP_INT_SIZE').'&os_s='.urlencode(php_uname('s')).'&os_r='.urlencode(php_uname('r')).'&os_m='.urlencode(php_uname('m'));$__sapi=php_sapi_name();if(!$__e0) $__e0=$__ed;if(function_exists('php_ini_loaded_file')) $__ini=php_ini_loaded_file(); else $__ini='php.ini';if((substr($__sapi,0,3)=='cgi')||($__sapi=='cli')||($__sapi=='embed')){$__msg="\nPHP script '".__FILE__."' is protected by SourceGuardian and requires a SourceGuardian loader '".$__f0."' to be installed.\n\n1) Download the required loader '".$__f0."' from the SourceGuardian site: ".$__ixedurl."\n2) Install the loader to ";if(isset($__d0)){$__msg.=$__d0.DIRECTORY_SEPARATOR.'ixed';}else{$__msg.=$__e0;if(!$__dl){$__msg.="\n3) Edit ".$__ini." and add 'extension=".$__f0."' directive";}}$__msg.="\n\n";}else{$__msg="<html><body>PHP script '".__FILE__."' is protected by <a href=\"http://www.sourceguardian.com/\">SourceGuardian</a> and requires a SourceGuardian loader '".$__f0."' to be installed.<br><br>1) <a href=\"".$__ixedurl."\" target=\"_blank\">Click here</a> to download the required '".$__f0."' loader from the SourceGuardian site<br>2) Install the loader to ";if(isset($__d0)){$__msg.=$__d0.DIRECTORY_SEPARATOR.'ixed';}else{$__msg.=$__e0;if(!$__dl){$__msg.="<br>3) Edit ".$__ini." and add 'extension=".$__f0."' directive<br>4) Restart the web server";}}$msg.="</body></html>";}die($__msg);exit();}}return sg_load('4CC9DC9B8D682EAEAAQAAAAWAAAABJAAAACABAAAAAAAAAD/xklgAzPrmdTEtRRyYAL5EeGz3dmoPVxnn0E/HO24bCrV0PkALIrBYYNlicoLLHVcD7zjT98xhGFJUiabJePNDUlszJJN2I9FkgIK2FKmSn7+EO+Ln1FeDAs4jayxY0KLwWwDQd+dYKyki/NgHJwwUfzuXKWSRq0leCmwtRQWWnnqUXypT/OMKmwHjaYehQ+GNQAAABAbAAA/plN9S0+TaWuKn86TN2i5BJcVc8hdFiaq6LbvIzB7+yi3LCiR8XJ2z5EV5FRGaMomdoUvLzAFneJaYcwPPFkQW3s4CgxqtJ/eWdmd2RonwT/SpiUA/PMguNPexR4P76cWE0gmJ+oNKuhsk0bk2KLRmk4YnJJka3jvtAKs50s3lkOfhnduvah1OJ2xISoaoU/kuWuGN9I/JDlU2zHoHN7rDiCIysZI163Ho4r643ELrvvc25vL986XbCUHTp/rhrF2sYyvpHeEkkq7ia44jU0xme4sCp2HzI3+wGrfe1C7JJn4xmfctduxofCd2I9wqsHslUt4s6kRU9XUSJoS6+U9S8iFaS1DcO1Fy3x8R2pS9PBOWI9owavydf88M96xKtH5FBMaf+v3VeviJcDkC7AjUl9Dp6dwoyW2/Mid2RbpLwbkMe6E4XnwJyhdLUnvyFMJQrRr5azAfgUYr9j8ec68HMP1WVmuuz5fT3zOzo14lh9PeR5dKqUT0Me6xpsISglygFPlLvODV9xqYPkq3Nc+hCSAkYovl4m3ZllNDghxQd4b8UJhbmsctotvw5WB1+hgTXvU2SXBOm+DNFh85PyQf1t5hS7X6W32NKqdx19Ty0RHGZ9snvM0cR5dtc1jZmtxpBPIQKF4n6vLXsFh3ErehHlVoPc5ovUSqdI3zpqXbKF2cNfUjmYw0jw39QNawLNqlVrGF1Lnt/2kx5muyg7TJNeewCVq9pOONAVQfVm2DmJ0QhlAS+S8qG5kG4xlRTpbuawyxGara3qWIyUsSBBnHz/r2yucT9q9zq3L0kz1zdzI8kaqcGEDJ8jfgd1jFZqKFQ4qXTyMTIodWVugXy1w9BtnDqa4r8xeyztT/e6drZOXlPaGOHjTYr3T1cUgMn1+w3Ksn8iI9sA/iTtmdC5Q2JLvLOeJ/CuDELcetQ8KuBJ8TTk7yVkZcL7ny+/zjsgLX9vdpbuSbefNFu3jXjryIXBdzhhqp1iUs9l6zs28qyXW8j+261BKlEEY9gel5lEBjk64bqIzOHBwIa+6FtqjD7MnklQKspQsbfYD6wtjZMPQctiHCe/fWXpida/snwk2fTQjNVBmccdio0fhZWE8KhP8mERTBRV86Xd3nJIj7D3rdrVlRU+1gzo2UnvYSTWe3/uZ+N5HMwg2XxWZdQvkKsgTEDG5hRDkfRkrwdx6pHqJSVpPSz1Vc04TB1CXjPRXkh2Clvr+zSpISMP1nlIoaewk+WKZ3pCbqGZ6OFT+0kNqdyZC+RtFuX/H4A/rD/E0FraNAiMDb54hr1EIRLB7H1m5/sinba3oe8qHEAFiP9QG8ylA0nQ+TS0IZ5ljBAmSDT/+gVc/XlQMWN4OkoCfo9PDkWNq3UCmX3VLJwPRyKPx26Y3PtA1P2FSXedCdc70kizn1xHG5Xcun5cpxe2BZPz9duXn91qofqGw+ryvwwtEZaTIYAmgAvkiu8QlRKxohL+zepjxkfnabBTbthOkF4LKjYq04VaYnKRDD4oGe8LlXd4Jx+VATd4TpRmAyhsQOmuj3CpnhTElSN2UCK41tkcKjUFCAKp0j57AIbazSp+eAcPJnVEegDFngtnKDQM00Ox+6F0ajxzVZx8/JkCY6RBPnbmNcZSomYRt8REzsKkxy7Nu5sWrFj0AH/Gm90wIUmkPECl+VMuurc/6NEHy1g+0w8EjDqzlgPWCRPuZwl7kxXP5UIc1yhQ7x4RvI0L7+ANhvrKui+VZzNGdhrdXnddw5BcyJxHyv9xNGMzmBPu1xS09rMJ6TVpwkIegBiwTEqiUKJazdz15Ce++fLhm+dNdVdCW80kceLamokJv+Nrjlmqsdl55ybKiAKjJdiOZEHHgTZjCkI6Nw5DW8qoS1hGga8wr0bozVBDQImIn/CXazid/Tk7M86kucR4bz+YNuvPj3/5Flt/sJaWQjzfSfsd2v9cX8O5ASepf0W+Ztn0+aN4QTCoyPIiOGI4EiFqnVbdo0E/h5oHQNNq/o8+haBbbEaF62al7qaC8W+odKcMXo8IZrnlD6keFaJV2yq0dDEYpLcybJoRKWfGpz7ogXTZinUL5/M+/gEcL73MR4nyEDUkVDN4SKSFWfCJsQe/TwJ76m92Pwbrcy0lZo3eyZY/n4Vhvkt/5BK+lrfCDpa/QGaQT4l7pjfAj4EDauixwrvWYCwxHjHhTm4Qw4GLZ5b2bK/iqp/SZ5OipfDkEcA0DRUt31uq7HEBd5qOIKD5N6DMpFbdG5ZnUvPQq1t9rzmj2Ir44e6X+HdQX3QN9eznJ2X6/9488OKjOjlQsMU/tpwwcs47/HT0gi4fOrX8MbjJRta7XZp/8qacuMkmv93/I2zzgc+pfCv3NNPBiO8gy4VIbJCJNPvwDrVjlad6qusJPh++Q9pHLU6EaOVQ8r0GVDr/DFfHQoiakGJ6vaqcmsrl9WzEsVZPP4Zj8V3JdwAWPkczp/80HzJV2wKjFS3WnWvniDIfD9e4KBHivjNdXJvGyrHw1sscEQQ33YbrN16huvFfz+581xh+IAceKWiouEU/bJh0jD1U011Fk4jOYQr5dXaGt+r+cnsa4KJnqPDoX9sm8BHlPJ/12xSKpsq2/GTFc0U30ZnZWQ6wtf1d7TeHI+JIm2iYkUFWsV4sqPf3cCkhXbRo0ahrUaCyda0u5ngVHkFqrAvzKQpTc7rki/Sm70Wla80lp/KtLOmTj3MYGpr/tGbabYaxmoNazqL0FOIiku6v319EtJcmsFrFnKw1u4YWYIgOiNS9jg9X8tbXjG+LvRi3IIJopuA9N+fDNOUBRWTT/DnshAFd3bbd8A88ekCZIGmquUkOmYCEDX4coSb5r4K0W5RPVKKnwtkGjzmd8oMV5kOlq7DaI/WYuQPz1Pu1d4V0CVfimWZV6ha4+bXQHCREWAibOU28KZmeLz7cm0g4NEXV0UpSvETyQjGz8LlTQpZNtNeHHs3b1t94RZOwUI6j7Al3RKyTLJGY9YM1B9rgmInpYFQLDFYDx2x5ijZLORgyp2MkOarY94IniaXzFyG+1F2J9zafT3S3Dro8BLlPp+L6bEkoE7KEroz1Y6UW4XOLqzhD/cpbk7wd6ndAj0HGnhoLkAItlSMQoWaWuxMiBOeGbk1uJzch4Sl6yatPU1rp7ngmBVPcpMxGhmtsDaop3LwN95e/wb22S3shyClG3Ur102sMR/Qf2DrxBMfN1av6xpA1Q6MdfvuVQpJsI1d5weW8vctYAFyccAkCg2o7CCgq8XL9veYpEW3rhBChxhLW0SotWrd1jKwJ4yvnNl9nnEaLb8zjKU8YTGIQEi+ygonbIITZv0B/2bEpRglDg/6Vqq60pJ7C79s7CyP05EhJTBPdTTFbtIMarNDrNSLa5ogSdGNpTzTJpH/6WZAZaVLCHYbeLgrr4ha2ycmOpEEZvXtD4cF/sZrNFcj/fs51NZ10pTdvxgUBOZp4z/bv/Ygw3LqzMd05iW7aawyJAQbQmY8JZSXTF3PuaNAtWF8RDguB4HFkWumVw9oc87bTUR58w5lXPiLU9YnW8V+3SsAgAck/CuoLwhzVtZ055dPCZSWETduxE2JviMneVuO/0cc4LetxUEi53E59rPKBIWuCqV7cSEIbfy+V/c2yiFjoHYBTUGOj2cT+zrcVKwgmcJhR2Wm88S/Re+Ul5a7Fgg4+EpkXaHDAWEXke3kXoK9Lje/TjVMdS40YNUwgMVYRnSAC1U5N+9B+CW6kupuXWH/XgtKUrwsvRDuuQiDWrExv5AOEGG9kUduNrO3b3q80oWDfGnPnr/lfVZ5k+LeBmAHdw0N6+E/4ja9tIQ4y1vnabJg9HBFt0BZcsw/Phcidi3d18MlkmugEzdXmT2BvyW9DTw6Zz3ES0iXfQ9a/eMvDyqZ9nQjYn3NgT8N4tVNJ9LxTzqXWTzzl4Zok0uXtr3B2BlyDA0fCVS5pY5UA21By0lnqwvVzJwI4Mzy/x0mXo4wPj/o1TfXrmQme+Y+Lp7Lsf4K4w3d3F4wROPP9vwl/FLQAO3iHeiAuqV5a/C5aV6yh5741T2UOmw3NWpBgnwmUtKUCi2s5Mxp7TSmXYboFZ7ef0WCZM4lS7WJDerJQ+S2cv3ErTVeswmQxIvYqAI2V7MG9hw36hMIerQ8x1dQnI/ZGV9IUB24z4bq1cHJzW5RCemhMNNK7tjc89gjMtfII9OHiO3tcOP0V2p51SvUnGVJnAl4huIDY1uNIfaEwAJSnTC+zSxEf46nnWZ170AtwKSVTSFLk7oO55Kn8d75jBIoIAX6oOSl8HNhhgzzT8uFH8QFVDiVbUn1TzvXRUZeBRvegkkvzS9hKAuA6SZkWohu3U+4v5BzO1Jr1nzwWAeipNOi7kJda89xoVdCetM01kt5l+1/dKD/FhYglAgp55i3jj2Pk+l1HTeKk9btNPFUbc8SJioyF5rZPf22LK6c6z419KlIq/yy5lzDBNhsxpbVaa/IofNPm/4wagY5ndBtXYpVbqUvINNRzlcj56jEtHbRU1OJ+RP+Xk97Jo5Mg6iCrVA/UdZ1w3rCwNa7ssNVCn1rzpO129oxzMrMgmLLcV0pfiqZbERBp0iLOr7dn5HfKKPSNM2mXQdeRfMD2eg/oOv6hAiTKpzUTe+K/h/sTdSWZJ1NaXuFcZPKCa16O+5cnlvFsM5oi83R1Sb2EtQux8Tivtl7mtu4vPOwDIsBcd5eGirqd1uZP/c6BSis1LX3SNDP+MxaNbLntpEZaXkO0ZWOObJJwbNF1uTRCzJV1hyrFfvo9NwhkFb4QLMxwohe8tC3rZklpRIIDAKNxkOSCeqESP8TOtM6BvsTrvcZ7H7Dfs+WYsclYznThq3LG+MGfwy8Ob5LGmMeV58fnSst51mf4GwLTxVgkFatrjZQq1d66+m5wToOaRoDa77q5kQu5EKH8imUoKbO4XnTw23u/Jd1WcGdzvLiQ97wRykERefZRx4bcCutF1v5vlwvfz47V9tHnyhHAvloBLGx8x1Maqz1EN/NLLTsWSyjKZTBZa4s/C7TW7ptdtSkcRL1t3ld0KexfttdQLwnJ1TTp+s5RHVMt7aQFYT3VSarnFNzHACYplNigzH8jKCSMBQ0M/LgTtIVLcTBxuQSLJhn6LBsGo0U//6qvQHraYvQtVsmowyBu+giulvyCWzMyvLsKVrvC5W1CQhG4C4o9sBXWTGoA8UnF5ySmIE5V4EIwM4RHWv/b7TomUpbUYkSgBgt8fClGo78M4lCgoPvBFBhb1wNFEDemUysi1sQtM6bCXuM9iH3rU3VawifUgvcmU28XF+qfA4Tke9HPkVlspORhs/rydY+ujENC6CmFzZex2O7vrI9qviXrFfU5qiPq4cngIDjTLNdUMLezprsM5HgXQtqIatc6S4kxCPSeR1hDSZWHY2gBIpdRlAgv4ciMbe/x84vdK+o/L4zOwWv+uP1Ttfxvlg8ixUIv04gvRFs0Kfo9L9Ed0PGKrEUlAUR5WmdcZqTpf3inqWL6uRIiJf7H8WKFAiO9PNpu8W08ybVqs5rXFQjxMGIo9DR96Mib6ZUD/lEDlKiCqTxVKhXKBfhxQ5HbGbwRJWTWRFi1heR1SuKfcRhBvwWhhsz2IjANXvK1mACdGJUPdbFza+ciVxSY2Q7LXUQf5nax86ky4rt3QjYYU2pIq8ki917co8tKVa3L2wGzF1JCCd+Lca23+0wI7bxadJ4S2qSRmlnkOg+f5m1yJ5LMGg/FfafM9Ru0L/ED5ehQgDYLT2C8uKfxnnGDzrXFWQtETk2t8bHv/R+JX+L/mm3D3PPRGxewbyw1XbaZzoquosQRdwQqdf80hqDz10Kh4lplDv0o99ItUPwSn5USv3PTdpGNEwTKn7llJ/nT6E/8wCW47sUjt3iqBpy1+d+EBBQRXanP63EHOzGpsLSppBkWpUy5p82RK0tyIJRwjW4lXXa/4c1RGP7U8qC65aXjrUGkA0dpHje3lP5lJGf/zRgC7hIQhZwawKWFBmeshuvIoJUJ5V/F3ot6dl7iyYUPZVHjbAIuwv8mT0Qke//LrAQkrWG2E6XAp7HJus3GIjTY7HNbxEtZpKNgXRLhxQh0RqOB6WMlpiCYIo+GMhJJhpVblXjM/EZq5RACsdNI+yLimTvDPNjFsZI6m2LA8Lu44ZYWDkq3n2pMz0mPqClPugOhCVmVfitzA2NbTkwq1BSaWTlscGw6oSZ7/Z1H0xBkVF3rYW1vBD+iurUoT84xcw0nenbJSDF36MAcKKmCCF8/3T2E6iYUyt9srC9Xgv2/JbXqK65+Xvgh6giAGD+DDEaDmM9MCEPG+mczlZjE2n5Zem1D01vlv0SvjZLtfRcm1CbQ9VZllAi/cEpTmBTM56vf+YSkjghzVH0A2OP6GpOmTFKVAXK0AVAyEyUQ8MSpN9hsSW8kHKTltJkYNrhHQw8c9SHQVv/2NyuHZc4htYOdpIdiqqudV+As88lpILp4NE3tnRmh6h2vG0v6bi35H2ovlDh7dQe+upDp8E3RYgs+rFQ+yI4A/bhPfqJAT5JKNQkWhQ6lDdXMo3DaKmdHS40A1DO6aosc1Dr7KhAvRaaLOn0kTYLYgRR+si8rCReJ/fbJCOma0W1uYZZreQzgi1VVzDE0teF6qkinwAtr+HXC0FkQ6ow5o2C6MeDX7GDL/I5OR4IOWqTpOR+bZnJ7Uv4irQRSvhgh0Ngd6WgKmkk1578KL02ZnEJaxqsFN1gR6XLAL+Touad0K+1xbdne4wsq2lJl7qflzjUku/ac9fJeyIhwvn6fpgCKsUHGQFijMC38VsDKbg8l9K7jILlnAo60Nli8TggKD7jqjnf2DzFVQ1k9LwJ4/PhQVShsfhy/tNLVmHWNt4CgrneP1DqiVBQsPnO4YDp9kA1ehdz7Vg45hsIkOB14YBBh42OcAyhG4YAIrCPTFGZqYcX1R2BTzXJr/RoYLQpX/08tjpisIbUTRJk2cFoPd5cWM+gOe702MgiBD/RKNmYdb/78MbfRNhMMn5DdZhbKb/8X/qlkifSxFwQ5pFXLMi0Q4HHV1NcZY+PPNMeJRPNeCETrzuy/qtIwAAw0RHkIpQyPAAKeJaAdZ5EKH0PFgVtHouWKp0HZKiR12OsZjl6RiyAtOQNjUdFI4/O3id6DFqKzO7/N/1Pr0GK9YJBhBbE4JFkRTStu2uKO3YHjVDn6izUsTFe8HyM3AbTqaguS08zhOYmRUt2Q4bkpfU0PHB4Bw0SK9dJV5IZtxx9GnHtYoz2t6q0qYvKAbmSsDKK3EHL/li3v5tuzdeq8A+X9AeBClbeGMeL8g7lyCdX/t2MGwf3wf591edqWIXIcNoEXqwS0qyFCO/pRrkeEzxB86/s/BgeIEydFKuSx7Vf2pX/1enP38LijQ8WZ4g9N3HIbeTXzj7p95n5FiOWipfAjfq5hVNRUghN3zo1FafDuj5EwkBmqYIa5/jIqwoaeUmTZnIqfMBxZP2OzVfOW9frSBdZoA/u0uGu4XyElU3NznRIcZjOa1PwXyJbxVJxpfcpb/cQnpZIAeCwM5bDWD8oIPoPGDeWSE9cqe2k8yvtneBjeBPSnojZKXZKp6uJZEiTiZBHlfCxU6xfuTjXDMpuxNsI9ZYo9dwXX9e7+NGOO53h+3IJEYSB+izQKc4rUEXHgITMLyHkDBxOCxhqpYtcdlw4N2zudje9EEewNmnSrht1DZ8IuhZ4peCm33XYEZWTtfEDf4fXjtNPFzODLfAOmR/5cc0JJbZForTgJosxmZ92xrwSLka99FaQxdjNRaVSCc3CeM4p2Z3jBba8b93VQUV4ApDk5DMcJLPC2gC061NF54AeC3XqZx5JAoTv+gW1L45f5w2Q8j0Xpu1dcnsU6//9pvnq952l5WLo50zKATgtKTZ1RHMWILbSdDB3M29QDPl77vBD71v5MLE9wpp9TcTAPnN0SSQsCEjVIxOcAB7+59Xl5nBrJgVpeKN49VkMmbLq1N73EgCznsKmlGmCHB0WBtbM4OHCDHh+qxkKfDfnOpdMsLsv+LI6r7O90FwLL5/0cGlbFGV5WlDQK0WF5Fv6raZnPD7t68H03pifNWC5+SeMC1PK5PCFJ3KtjkK43PnQeXYv6e6XuQS3Ho8W2LmnJ70nUhJekvYQXAa67hQPzlHNrxYdX/3S5vZ2E7vsKpj+J6Okr6t5JYlrsredjOJElmsx2ckafQMeUT6Y8W6KnrILKndMhGGTbAHcelHGedYVdr1wYP9tVlXMnzfdKOKopmVmmNZN2NuHyBY2LWcIRxgAH0KDp2kkvOYzLhDwXvJQJsRoCJHTQYPdVxjSPrpi2XhjCEV4Nw1mULw+QIbWTx06W/W7SFFgU7x0zkMdPnXSH7j8wfddE4g8+U/EwfE1oYOj6MdDqYX8HDTzh9M+k61ebSPmq6FfTzcm8IFgXrtViERwZBY3aQIM2K24g+rohymOQwGPZzQ21dG+V88Wvb4eS0VtbiveJVSv2l4+lNcM/2xo2D7qAXRbu5FN4/cY7ZETAm9KPZliQbFCL/jcnWb1G3DPFSVtVjvNlDSEnG2vocbIpjkz+BGoZKRpszM8HsMgTMdSGP6vC2SIC9olrU7TWInBWwHWffKD0erq+sORHOm8j26XGkjxdWvDoNXZ2ZGV+omkff79RIlPr5YLOLyEJozsv3sbBje3jm0vihNxNuWqTAUJwKwsADuuBRdf0IeR8N/4Dwab9jSyJcNnBerTAJpsaUs5MuUSJJWrKSOYQosHB5GQ+UYiaKg5bMFvP8bavBAnw0S7MUrDEZ5kSgzaFjbbrWhFVcGzYiiT/G/TpfYGuycPVIa61K9rC3EGJrrWgH90/n86IcznV4/+r11srSXW8a9BhFB+5T7K1ozf0MWao6HvL9OSfj32PbwXY71bNHwzLUYHHmzbN7H5+poOuSZvhOM+dw2iGC9MOhF4XuPivTCgourLbZ9QfyXzVqnrcm6PgG+nQsshLkfNA5kDe4UBuElGMRlSNAguJpe3l7W5fVe60YTzC7Byq7t85cxpR87KaD0IAm10fDJWzoCk2DZyL+hQBIn4CC2aky7gobL70nW3zW+i3O7ePWY76nteoTtZ6GUjq9uf2grLNtYD9OQ9fQiKKDKM0Bhpv5Vo9v60WqHR+myMRO6XA9dsKk6Ik0HpsyXvdV60R3JlFX/X3EqbzjxqGd9gwrjj/RtIZuxDir9WYoXOwFGzvUgOrS3jVO08opt4/CGgzpOAAAALAbAABlET/coYWAku63taqp54UQ/Llds+ZADFFqR1YAjN5qj7cbTfBPr29zZW9CR6RoFeQo4CkuXub25h9yB3H26N192qBL/Sy35cgU4aRglTyfhUZyWZF6kJBnjjTSO0XNtZGoAj3h0Zp39Ob6RIZiemOl4l998N31cmSfpg4dsZi3JVy7BQR5Tf8EH26S5JW7fRSmt85e5km2OPep4r7JBCCCoDLcE+6TDWOvZzc3NDzksseF+NrMa7wT15uVWt6jAsiv2H45XR+SBXv/AxMD4dJoBafibvMI/FqTd8wy4bobgw4WMeoKZtj2+xnuOIQnpLAbC022BFMz9Rp6OyXuJ6mqZsOJtwJR8n+OpbfjFUIeuPNleSP/zBPL41UYaTaKJq9RkaVBuqZiVW5UP5Qq42FapqCFRT8jpf/Z5Cw9S0HTyccVncgyk8f9T6/ZDWERCD+bUMeOiLCMJlehcT5tz9QMfMBnDLiP6yOhj62Eeqzjt6N8g797G7MTUT7XseRET6VTU5nla0j75slR6A6pzfQxGzjo891cWjznT9k3yuwqDLKYzxJriKB0W+c5BrKGE+hC7ex4FmE7n8uup52zgn2UnXt3MhMZqrd8yseiQeGx2wQN5YOgg0BgpFt9ZUOsVJUnnBBc2tI2Xk0ZzsHSiJ2FynyewAL/z4i2ndX1Ex9tjfcpXU+pGcybnvQ7Yz9jNV+fnmaNKC4DBden+m46+oOraJu9O3mX45z83ERexff52jYSOV9V/VfHInYP+xjIG7vdjPMOD32kaXBVkAVh/8XwSsgWzH9tVEVqc4IVFt0qx/+5pIM7IfrvHP2nx7ZpD/gyqtr959R5z2jDwDxFsDHERQLgXlXl9tb9ae8x17msAyyMARMfw240rCP5EarATSdFXg+h31S6wZSlXC0zCy7JH+Bq9wsz3JLIrzLOKf2vCqoSqaloicqHgk2Sq+J3SgqoMye204KFBzPGzFadn1AWYQ7PkqcTH7V+PoFbisajuANngRhBZ92N9Fu/jtje/JJcTdJvW/v9rSY41BDrhYDGSFjE4dwm2z4CqKexfP3b3Z0kUuPO0buO7SDwOHFm27XiNLM56yZKkFBybA1le41qcsBZwJ8QJRjx+cF9KuednVDRGGNZkuiCUxIQShCNpsQhwPt0pASh08U+MrujG0CP5je31sFUEqXO3xIX4Gkp/ZVqee9v/AXGhrnqmXfCNdLxoxyd3G3+8uyKXlMLr0nG1Mq39/ZmiRhz5VNTTP0031yNxAShQskhvaZ8GDRubH+ZuBrIoLe0euSKosl0btb6Ss0P1twiThEhuP4U/gRxxoM/P0kxAuNcnDluOCLiDYdohxkgqF8pCQrVK/KEZifkqtvXmeWHJ6IknMXVyJ82QQpr686osO0Dl3wmZo5T76r5ltEiMEP+4CCt5Ch+CgMc0hArr5VP/WXx73BVXNm1p6hSEeVhAs60mauMTt1k6eeue9/VUgBkdPlheQ9/MFOLbvOSQjA3cIg7Pkqb0AktVZNiOEGhjXDrkhkdTOMuKsJX+yFeQkhIBHPtLE+PtBnx9r1bypCeAqgDMHvI4+0HJonvtrrHVo12qEkK39fFw2m5kd5DmJQ6QgL1AJ/D0mPR2bNZwWG1lMoU+TYjwanuFW4YO8e5QaHDaAVaf1kjN9UCh1oVT6eZ3wtSNDeXNmri6xCuKM36FRFjcsdNumFqAVAWM3LEeapxvuNEy60TZqxO4UBspxNMkYDXdmMGcsg8Zc+r5FXTC/MwNOrFtTIFo9pYN/AD/l58JD7ZaAWi6YFSSUqzWcsNxAKjnSkhbHByKYkmUQPfA8P2a+2sj+ZiFMG3NvDcZYUCApZEgfvZP3MQHajE0DsAiuSNFNkgQw7IkxvX1gIhptrPx5EG+jQBP7Em1aRRgrkHaY46nnQxhNPqb0D1eZGszXflDMN2xaMss190Sa8u0+9Pt3ME58XKkVbpSoDGHtTD74mg1xMvlD3u64vmvLfRB7+w2ChHlcNQK6iaSflyIZUN773QknHtpRDQfznda2VJiKpWo+VXQN2p1Ucypopo0xCyNfxqeAP3KBSjVp2Kt0E17GuNIFd4MbAni7hFARrMrGi3Sc1i5tfNZx3SY6Oamv0MXKW2Elpo66dAgVyexnF/2u8KC5ZraXlAxM2VlxT3qy6nKaMHOb6OrCm0FxErGyk0PDv1kBGCes8eXe7a6Lwuok+4LKoQI/luTohJNsoEW5Jmia45g4ehXhXFHlhtq70MIuey3XTfhNIvvLbR31prfy2btMjlc+Bra4bYXVPZ3WcwkqmfSCCJhxAZTXw6Iw48YdSi/A/gxwsc6SIOOihMY+Z/4MWKYtdNBD4iLiOQH6wj6hmgOwH6eLK8pBZx59S4ir3/fL7foNUh05D4YffklUbGUyqyHHWpnqeTFoUkpq5qiNuUdeSUk/nSKaOvxrvy5PpkEq+dS9xM6NWH8TfS9/8Ov6T5bC+heRZk99o95lte+Nm0PdVK71e6GezcnsGNHUaXxAbNadN7GT7N/Py6MgqBHtgJ6HpMEYsfii66sXYxwyV/g7huK8eJ36i9IVt+ayGAqbd69iGzWs2Hr/Sima77vlwXgWjqPfaDYOi0UALLze6AcCexSnsU2qth22nG+IpN8sQ7wY6rf8gGrr+OsnnjDIhtudhGlOuU6ypVb6iOULwHtIXlzzAXT4wg+Xit5Sibbfawr0Ji7BtSK8UN9afzRHYBCJmQiEZN63koF12HnxC33qux+a04FDMflzYS5xPYe5HDl/KqN8Uzvk3wHnDy+4og9n9Yc7NpI5rGA3ojBKGFGRoP0NR2nFMqH4WtjVPjBl4TIS28JDgKPfSL5W+yt8iQIBxPvuXxSsjNhqsTE2/ZExrY3Gn7VP89H4qUvS+7Fi8Eo9tTfF/R61Si5FgZL7tm0k2l1aVJ1J7yg6hk5Ud9e7uxFk5z+GtN/BsoWM+LXWVzGm8ohS4fGDlMel7vI02eUH6cANRKRzpU3D9Mf92MKNitPm9Zs/eJwXqq68tsPAx2lLyRiM003f1A8FXrOMWIoQNNtraPyhUzmGX6+jG3hYUSqFnlXvAhAFA2kRSiHimQ7re8/fu2vpEK2KbRYr7l6w8VPwnJ/L58JASItfhXzgIb2wMKvOWjF5ZVrXMN8DdFGSkbCvByqUAPv0qCtARhBmV1fIwxMcgQDvLHe6ynxbZquxTxPn+lBDRs8/VScVxqOBGd9ySkJbBJoA7eJdPmV0lUQdG6NpZhz1alH8A5IAQgWTo6Q0pztQWm9coyIzGhOu7uOD/mj8BHkeU+9Jg6G0qsPFvf4iiZqCenhIKk8PIao3M1BoxOw917sC/kCuuWorL3h1iZrPaMQLSIK2Rltyv5HWRfEDDaHx5phiMzsNQWwCw+LcdkqjB49X44v0j7c0lJ2/2fzzW/ckmKQfR9kQc7OIYy2QsuxE9B7epnLTZeyAaeFil4OJxI1erJy5E3SQDpDUCD6boviKIVMVu5p8J+/Xa2C3heWXza3aNE2xFchMyhfkawyQLBfnY6NGyBP6wV80hDOP1y8RNA+E8noWz4n0VIf9M6e+gH7/O01XZnt1gk+mAY0MScu0CmfF9fJvzA+nAkgVHV6BSuj3V4V2V1KdyeMmhYoK//7iXwSm4JPT1ae8JaIdYGqnmayphPaGxtv9vKHNZ9t2CItiwDpBThQF/YWUQGh8hCSU6Ca6PF5lw0Dkkh3/U9QmxR780orI6LtFvUBs+4/vNG6yRlg5ncHCzKzxod9L1K5GO8EB0B8vJarvE6f/DOuIuzDan+pZIbeLky3i0li6IEx6mYBzFBLPsDdjQkgoAvhJ6dDtZFRtDQ3lMNzNFdxsahv/cGdmhHxpQ7j9lZwL6xs3M00jgwKcWUBaA1aUXkRTtVC4DLirBRxDJvE0zqkhC90YfhqLPLoNxkkOxJbgT48tiWbrp0dkmTo9wszzw8CFqta/rCmoEm3RpkNgj8XIDou1VQDEkQwzB8d+O2q+k+r8BIK3uOLV2W+BdrD6YCG1w3O2aWzLtaQr2sKnk8vHQuU5Sf9aQNluR6mU86KRhlc7yqRnl2irRD6iw/p4ENrm0hWynVa0SCGfCZrMjyzPv86E72wb1Qlxqyua5Mwhq8y9KfqMlYkPV+h779kGOCvXiCvlWGuhcrQa6Log/LdzeM18Xc3lS+awgfHFLzzROO5peQN5WF+EFp4ahJ3tkz0GH1HRx/Za1Qy08z4u7wr2Wrs4+zxBoyd6HoF0ZyXpICucz+kMd0N4ChjafwStBBjnrUFfxK3XNqiiMFGZS8Z8wffMQOI4VMqz8cZi4uYhgyqUWxA3oSH0haPtSoO7tpIlGuJN2ZHBcIBfyaUsroUmuhqrGe6/YO6YQ043XaPCquBII4Bm0s9tEroUbk6veq6Or85JsR6WbP178PlaBh1NInEw0nhFL4aIdSCRP5qSbDZnhrjxdH0BAuNzqNr2BgJpSLwhP7ihDGRjlgwEFoWi2YaXPFpyQfMzZC4SD8fEQ08/YkTXw9r/yFB44TJpUflIyKdFXMmceQ3+riX5mMKzy6Qj9+C9uhfWxxDmRz5LA6VAU1uNOUvN8qN/IRvXeu2NfDfAdKia8Oaa7fkFh8tjmUD0R8bKs1kBcuWbxcqPL5eFm8bSBbS9hHnz8GBqp9rDLxuI/w5dk7jwG5Ecaa+7pkfbDMoRjZa4WHSZrHYGOwrGYeMjb9fvz+lSBUfGBM7uhlfHXW04mZXW9pBIHyQqemCmhzL+n5sKK071jHn6gHkbb4vq3yIWwkF8Scj+YKfBR3/PE+oXbOSrnHUPF70tFTa0n3iQvReW2P2o4Ap4mNwPnVYj9Pa0IYwOgsSQ+ifpqlct27ntO2DJkU932oDTfXmjNKGiFABjbbXhK3PErp++9J9fCtQlstx8KWZRwHOu2htaC9N8Mlk26WvNlzrQwAwEwEbG6g9X+C3NcvJJmqu/0bnr9ao50Q6NBOlpMejRF9iWNzfQqZPlmuwhZ/0ya5ER0bejXIpwZAq4F6+y6qkzriI/z4epdHvTHVS/FHCpMZs2+bcOm9Fr7pM4wgbbEeA2eh70eBCvEl2wlnY8qRiwTifg8mfMWvYNHP7s7DJZzaI1lqnNONZ1sRHUhZhzsZ2JJWqqJL+5PSOWiDIC7Y6FDImhq9kWlogMtopNRn8ZjjY/HWePMW21ho/ZlddQkQhWee88Q114exVOImEptXxYvW9CwP3G+7HgS1Wd6sAkK6P+qQDGVLIzBnNWW0D7Y+whzKsPf03k0rpFbyNOp8a5XImfUpYG1nlFrboji0n2kt78r94ikyWsCnZEwXVpNSEFX/lY6PR9iuTTmSzsmCkaA3tDcS00TQpN7lrSl77c4gnaHxKBjZ3ZMUOe3hg1izKvhOh/RrEFn9uSQw8tJsF9HvdT3OJzXDfVKhe6OFCuLChVrK2YSErNE8ZKUYhOB8+2X2ws88NRUqIhDhYgJ++XejoThVAIkn1c8KH71gUcNnt8Sk4QYOlHck9OjifZVfk6rjdnjtW67KEjsnp3S7LSSd0ZW9KPetnsGSUG/h10pu3AR7lO4omgT9ptmkpkef5ec3ISHcOjaTLy6FC9aYalwArdgv7XX8z3m/J6J6WpWzyNo9j3m6hc0Z6D/puNP8TsuuD76vMfct5qTcZRan8FFoMlnTbAMZCnJdo/HBVk2iQJNWodtD6cCbQlFwRv5eIOWF4C8/A08xG8pPOt+4P2qTmqBCrsRw2QHuvSDmsNDFqgf/SLljh113n0113y8OO/U3m/ZCCQukfZnW7cfEQf8cK/m7n7ceMbs0DpDIj9w3H3JhFp1rwRkq9pVQzXN809Mrx2yh/kBRXPtpGKvIt7R/6CHr9T+5zu/dSnpv4hHAs+HAfK9AZZXUs21DpwHAm3uuRLapw0Du0olWMUSO0AU0vewconvbGbwi+UIkJ6JboDCgFmR0pizwZZgdpA4DJ4BO8Am13XkEt5cPq/u8dNQE28tRtiVQ5pIcmnd7pyiK+sftp6zAP3ZYf8vWhrzM4b5JvXGELLR4t0eI43v9Tljs7DGfRgj/uAyEAPvwJSJTeCekPKxK2P/bvmQBpv+pBfOBBwdgfGHg2DCurWAix533RVwEjpkE0CeYTs8IN8vlbe6++pTNhDDBnvySryvBcZp7cBOyGm4Mw6FGT3pX1t9eVWh8w6d1VFtXDoSUnkvDQhT6V890m8p3b+Kl56uCIo+1xL8qS0j1V5MEFGclYkrW8YtmV6159FgyBd1hIxp3/uIFg52DOvgDDL4UibRkAuVMlPFZ7cFtLlpv4AAVUXsy37VrOidgn2dL32UN9wH+vFveBZKVycigzW1vm+bI2+1clnbyjRWuJkk3SOKUiil3D7wGT++UJ1jji2x5Ds/mWPR6qtLxErnxxvsyV15rwJIZvw8SgBhne7IH5Oy7AvtIbiIVnbsswZEHd2haYMYtHYv0gNEOtDBI7/SsBHJJAjZWWXClbLwxCKjnifyK82qH5R+Z+VDu4VIWXygYWAvJcWlwJA/J+QubvuGtLVWRWW0mdYMgzCmAHLQeLHiromH+rLbuoWGCr3VD+YTrnU8R94ys1R3Dd8T7hVFtQELqI41gazZYv8M07jz6zS7GXUd+3HS6v+s6WP92Dx45t7FIWhZbGx3ULJ4Eo6+Peox6tM3zghZ2CtWuOCFiuz2VS7dRcIHTc67RKgH6chSQppw3YaFot63DdST5AA7J6P/zL+lVM1gCpJk1a3TbrUnxdc1FABuZkFj+xLnxIxYwAaCurYHh3EB9G2/0NEv6UDMkuOGZ19VM13bLK5YOwIpB/CvCLluTcO0BeGn2Ev0xdwDOb6dPHod3lZP/dQrix9farC6l6ChavpsqpUVyO5uenisJPm0iTvITE1YtX9/1LDXME82pdR+Zjps6682XGQTvldOkTJJuxVZzED3eJ/xqSfmmljMBF6n8hk+GQZhqVQk7ZiHpEBNM1Qt/fO6kW1ng225pjWCgbDXF6m8Ia/QqxvE7prShRdPe0sG+39WYeuoQjmI2zG1Ja3ekvZvfeJ2UfHlDvO1z3p00TSc0bp1h1W3JDSDNw73fslKdQbGrLHCqFBp20bvriNVfX1gTGGsz4FsdpYdEgASO18fkv/oncsIeLgjhD85xIkm6MiEpMiYFIV91NdEffZkxMflo8VxvBO4VUjHc08I8IvhFzzja7ePhqJ2fgbtZC02LGycCJWbT86zMixN2K2fwelixeDZSPL7yK+pBpnTXil7eEgdt+PAfSfVXybePr+OlLeqNuVmtOK+DFt7UulqF8eGmJNRgGeC4V/DJ3U5XwLmTTOypw1Gi+phOrAexsIbt5oSXeW7DxOtuvSowDTXECTfke3vypR6ccUKplK8gGk8NSW3wVfPrlUJ/o2y1r00mEq23s4WGxgfTTLunaSb2GkkIQl2nbploxyNkg6+cT7qeLPslyHjBAEjNzizcbeiYvI/2GFrYQnZC697szCooe+bkAdhIdV3kFpXAb5X4QsZ6zWxMyLwWBrx00ryGfw7vprgQfShBpJvEfsiaNqmfTF05Dd+9D7KCRPyrEDoCUIiNWlg/405rPFPpM9plGAiUpLIlWCnqi0n9y9fU+ejHlwzr87TGAMBpp/RDNm+XAvxpLf7+FlAlCZp9idhCv1j1ey+1lNAAHQ+Cp/yws+Q+V0soIgtQ9g336WNJDXPV+jp/9+wOkpg2HWeWFMUB6HHKSqx2udIPh/xbY0DxaeXYFF4dCckLc1BxoIaVkdMg+1EH6LJX5oHYaOJGtFcUaVGqglsrKAkFNcwpQrNVb+9ZJjzQ2DbPnedl/qJ5J2CrnyXagxoZnNlj6oofiRY/2Xj+NMXE8XQLASMyPgIgiPUtUvvQbB/AhrujgVF+Calm3K6dmahv8VQPTStIoGkdIbJ4Xdc4VWdTtyaZYiRBveKdVkV7i5tig8XnwoNBX3LQtBLdlvTshDcDR+yc9DPrKshG/7yZBGPSKTI6a7L7YllIb6+0DT0SeAQzWbew2iT6noL1n9mKTMAAxDVahZOAh3IXHr6Rchnaf1AzJflSrD4Yx9Z+Ja860r74GJSSlStVCtrQilYaJ1cQsvz1X/V5zI9EzJWx+5f3HjdmE9tFlwrSL+QDFf8d20iEub/h5Gv8o0KhyqvRKYw97id5ZaY4HDd/ZbrAjt0g0tXuzUoLAm7iK6Ui782jgaaL9pHgeats6U5zsVqzJ2j5dMBZ1HaywyTReZZpmNQuMVk1A2BwoeuTaCOTuDnQ2MuSGn0/AD9JSJVN1oOo22OGpuPR9b4shKUO54wdpz/GKOVZ7g6sybgaMT0KnC+CZhvOfuv5gGC+djMLaV2AeW9MVLLv4aP58iiaLE1NPASpysoX7S6aML0lVqpzJdhumwZfATAADrx2FOFJJKtOdsb3dMbNn0EX6qAa7ik65EJOTluvoQq8MKTiAgboa5dt+b0xLmf7vECmRneZM288CndZCds+Q64VwnBrj25ckRtuV3tGz+TEB7KwsMPKqmol871XTsjq3XdPjSiQxr+5UrC2KIr/kS0b2CXe6BobZ5mfk9gp9BaOUJ+v2e8n0zTFLoWyn2wYyKW1DwYpbi6plkdJSYzxVSvpJGnu+/gjNHkKateJJx214Lph2DKlM4cloaUrfMORH8GcZ5teb9ZHFvbfXRexyQ8GwGit7fcaajsvxHUSIdycUcQgG/1LeZ2839BpjFCBB/vDkNJEzQd0Taua2a2rx5YjjsXrMBnp0y1nzD4tiP1f5CSZJPEslO56O15FbcuKFs/qIlurGL+cs3MTqcxy52NBh/dDhFvURbESEkfBWkzj22N5LG0MC1Rhhh3yGaKxQp2qScp0J7hYAIBN68iW7M/qDqLses0DEvwxgYCJNU3DZbAClo2kaied5198C06a+UVo2GVkrXnAjPk9N/SzVJ9x04vSNcUHX5IYBDwym5oyJ9vJ3f9KnqhZqRRBzntZdYLS5PSwn8DV8H3NxKAuPQM8swYenytILfV4eTTKA1uGdtQxLQdm7ig5H85WUMGWzOUywPyb3FIryNalmq9vlMFY04q1GAxiNWEWF7S7q/hszQNRxW1QwGqVgjbtzYnk+2rTIQbg5aOyH+7Xn4c5Fj4YM2U15POzSAsMezEWUwW9EKCc18mVLnBfRZO6JgOcMTacqtSe4H/RqUwB4OLe8ly5gPjqoUgtVqadxVx/+hn/ipK8D+fiqJs9x14Cu2v9oVT9skme0+W7ksoo4J5ieseq3PS2apTzAnXNsX5M1ZesstLQWb7u2agH1koVW96Wr0PsqDqk7wTuxEGNzPj7G7OMJL60gDFglX5lXmzVOCMPs6PsYAMHmZT/O8RDDSgIdnGNBWhxX4zjiLAMeg41NYFyXUrXAA/5lotnwtXzj8nBx6Y7JZcKLfyPNhHfI+6lCr7XmoivNKc1vCT9pwAAAAA=');
*/