<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

defined('APP_NAME') OR define('APP_NAME', 'Monitoring');
defined('S_USER_ID') OR define('S_USER_ID', 's_user_id');
defined('S_SESSION_ID') OR define('S_SESSION_ID', 's_session_id');
defined('S_IP_ADDRESS') OR define('S_IP_ADDRESS', 's_ip_address');
defined('S_USER_AGENT') OR define('S_USER_AGENT', 's_user_agent');
defined('S_NAMA_LGKP') OR define('S_NAMA_LGKP', 's_nama_lgkp');
defined('S_NIK') OR define('S_NIK', 's_nik');
defined('S_TGL_LHR') OR define('S_TGL_LHR', 's_tgl_lhr');
defined('S_JENIS_KLMIN') OR define('S_JENIS_KLMIN', 's_jenis_klmin');
defined('S_NAMA_KANTOR') OR define('S_NAMA_KANTOR', 's_nama_kantor');
defined('S_ALAMAT_KANTOR') OR define('S_ALAMAT_KANTOR', 's_alamat_kantor');
defined('S_TELP') OR define('S_TELP', 's_telp');
defined('S_ALAMAT_RUMAH') OR define('S_ALAMAT_RUMAH', 's_alamat_rumah');
defined('S_USER_LEVEL') OR define('S_USER_LEVEL', 's_user_level');
defined('S_NO_PROP') OR define('S_NO_PROP', 's_no_prop');
defined('S_NO_KAB') OR define('S_NO_KAB', 's_no_kab');
defined('S_NO_KEC') OR define('S_NO_KEC', 's_no_kec');
defined('S_NO_KEL') OR define('S_NO_KEL', 's_no_kel');
defined('S_NO_RW') OR define('S_NO_RW', 's_no_rw');
defined('S_NO_RT') OR define('S_NO_RT', 's_no_rt');
defined('S_NAMA_DPN') OR define('S_NAMA_DPN', 's_nama_dpn');
defined('S_NM_KAB') OR define('S_NM_KAB', 's_nm_kab');
defined('S_IS_ASN') OR define('S_IS_ASN', 's_is_asn');
