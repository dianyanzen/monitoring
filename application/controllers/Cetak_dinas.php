<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak_dinas extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Search','scr');
		$this->load->model('M_Blangko','blk');	
		$this->load->model('M_Shared','shr');
		if ($this->session->userdata(S_SESSION_ID) == null) 
	    {
	      redirect('/','refresh');
	    } else {
	      $is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
	      if ($is_log == 0){
	        if ($this->session->userdata(S_SESSION_ID) != null) {
	        $this->shr->stop_activity($this->session->userdata(S_USER_ID));
	        }
	        $this->session->sess_destroy();
	        redirect('/','refresh');
	      }
	    }		
	}
	public function index()
	{
           redirect('/','refresh');
	}
	public function input_cetak() 
	{
			$menu_id = 101;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Cetak Dinas',
		 		"mtitle"=>'Kendali Cetak Dinas',
		 		"my_url"=>'input_cetak',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('cetak_dinas/index',$data);
	}
	
	public function cek_cetak_dinas() 
	{
			$menu_id = 102;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('nik') != null OR $this->input->post('nama') != null){
			$nik = $this->input->post('nik');
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			$nama = $this->input->post('nama');
			$r = $this->blk->cek_rekap_cetak_dinas($nik,$nama);
			$j = $this->blk->cek_count_cetak_dinas($nik,$nama);
			$data = array(
		 		"stitle"=>'Pencarian Cetak Dinas',
		 		"mtitle"=>'Pencarian Cetak Dinas',
		 		"my_url"=>'cek_cetak_dinas',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Pencarian Cetak Dinas',
		 		"mtitle"=>'Pencarian Cetak Dinas',
		 		"my_url"=>'cek_cetak_dinas',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('cekcetakdinas/index',$data);
	}
	public function rekap_cetak_dinas() 
	{
			$menu_id = 103;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
	        $isakses_kec = $this->shr->get_give_kec();
	        $isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('tanggal') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->blk->rekap_cetak_dinas($tgl_start,$tgl_end);
			$j = $this->blk->count_cetak_dinas($tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Rekap Cetak Dinas',
		 		"mtitle"=>'Rekap Cetak Dinas',
		 		"my_url"=>'rekap_cetak_dinas',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekap Cetak Dinas',
		 		"mtitle"=>'Rekap Cetak Dinas',
		 		"my_url"=>'rekap_cetak_dinas',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('rekapcetakdinas/index',$data);
	}
	public function get_nik() 
	{
		if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			$j = $this->blk->get_nik($nik);
			if($j > 0){
				$r = $this->blk->get_data_nik($nik);
				$data["success"] = TRUE;
				$data["is_exists"] = 1;
        		$data["nama"] = $r[0]->NAMA_LGKP;
        		$data["no_kec"] = $r[0]->NO_KEC;
        		$data["nama_kec"] = $r[0]->NAMA_KEC;
        		echo json_encode($data);
			}else{
				$data["success"] = TRUE;
				$data["is_exists"] = 0;
        		$data["nama"] = '';
        		$data["no_kec"] = 0;
        		$data["nama_kec"] = 'LUAR DOMISILI';
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function do_save_cetak() 
	{
		if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			$nama = str_replace('\'', '',$this->input->post('nama'));
			$no_kec = $this->input->post('no_kec');
			$nama_kec = $this->input->post('nama_kec');
			$request_by = $this->input->post('request_by');
			$receive = $this->input->post('receive');
			$tanggal = $this->input->post('tanggal');
			$this->blk->save_cetak($nik,$nama,$no_kec,$nama_kec,$request_by,$receive,$tanggal,$this->session->userdata(S_USER_ID));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
}