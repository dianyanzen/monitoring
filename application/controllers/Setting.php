<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Shared','shr');	
		$this->load->model('M_Setting','sett');	
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}
	}
	public function index()
	{
		redirect('/','refresh');
	}
	public function user() 
	{
			$menu_id = 135;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Setting User',
		 		"mtitle"=>'Setting User',
		 		"my_url"=>'User',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User/index',$data);
	}
	public function user_level() 
	{
			$menu_id = 134;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Setting User Level',
		 		"mtitle"=>'Setting User Level',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_level/index',$data);
	}
	public function set_helpdesk() 
	{
			$menu_id = 178;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Setting Helpdesk',
		 		"mtitle"=>'Setting Helpdesk',
		 		"my_url"=>'Helpdesk',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_helpdesk/index',$data);
	}
	public function set_helpdesk_add() 
	{
			$menu_id = 178;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Setting Helpdesk',
		 		"mtitle"=>'Setting Helpdesk',
		 		"my_url"=>'Helpdesk',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_helpdesk_add/index',$data);
	}
		
	public function set_helpdesk_edit($id) 
	{
			$menu_id = 178;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    $helpdesk_id = $id;
		    $c_head = $this->sett->get_helpdesk($helpdesk_id);
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Setting Helpdesk',
		 		"mtitle"=>'Setting Helpdesk',
		 		"my_url"=>'Helpdesk',
		 		"type_tgl"=>'Tanggal',
		 		"chead"=>$c_head,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_helpdesk_edit/index',$data);
	}
		
		
	public function set_helpdesk_delete($id) 
	{
			$menu_id = 178;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    $helpdesk_id = $id;
		    $c_head = $this->sett->get_helpdesk($helpdesk_id);
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Setting Helpdesk',
		 		"mtitle"=>'Setting Helpdesk',
		 		"my_url"=>'Helpdesk',
		 		"type_tgl"=>'Tanggal',
		 		"chead"=>$c_head,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_helpdesk_delete/index',$data);
	}
	public function set_helpdesk_see($id) 
	{
			$menu_id = 178;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    $helpdesk_id = $id;
		    $c_head = $this->sett->get_helpdesk($helpdesk_id);
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Setting Helpdesk',
		 		"mtitle"=>'Setting Helpdesk',
		 		"my_url"=>'Helpdesk',
		 		"type_tgl"=>'Tanggal',
		 		"chead"=>$c_head,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_helpdesk_lihat/index',$data);
	}


	public function menu_user() 
	{
			$menu_id = 168;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Setting User Menu',
		 		"mtitle"=>'Setting User Menu',
		 		"my_url"=>'MenuUser',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_menu/index',$data);
	}

	public function see_menu_user($id) 
	{
			$menu_id = 168;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    if($id != null){
		    $menu_id = $id;
		    $c_head = $this->sett->detail_menu_settting($menu_id);
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Select Menu',
		 		"mtitle"=>'Select Menu',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('user_menu_lihat/index',$data);
			}else{
				redirect('/','refresh');
			}
	}
	public function edit_menu_user($id) 
	{
			$menu_id = 168;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    if($id != null){
		    $menu_id = $id;
		    $c_head = $this->sett->detail_menu_settting($menu_id);
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Edit Menu',
		 		"mtitle"=>'Edit Menu',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('user_menu_edit/index',$data);
			}else{
				redirect('/','refresh');
			}
	}
	public function user_activity() 
	{
			$menu_id = 162;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Setting Activity',
		 		"mtitle"=>'Setting Activity',
		 		"my_url"=>'activity_id',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_activity/index',$data);
	}

	public function activity_add() 
	{
			$menu_id = 162;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Tambah Activity',
		 		"mtitle"=>'Tambah Activity',
		 		"my_url"=>'activity_add',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('user_activity_add/index',$data);
	}

	public function edit_activity($id) 
	{
			$menu_id = 162;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    if($id != null){
		    $activity_id = $id;
		    $c_head = $this->sett->get_activity_data($activity_id);
		    // print_r($c_head);
		    // die;
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Edit Activity',
		 		"mtitle"=>'Edit Activity',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('user_activity_edit/index',$data);
			}else{
				redirect('/','refresh');
			}
	}
	public function user_add() 
	{
			$menu_id = 134;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Tambah User Level',
		 		"mtitle"=>'Tambah User Level',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_level_add/index',$data);
	}
	public function userid_add() 
	{
			$menu_id = 135;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			    
				$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
				$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
				$no_prop = $this->shr->get_no_prop();
				$no_kab = $this->shr->get_no_kab();
				$data = array(
			 		"stitle"=>'Add User Id',
			 		"mtitle"=>'Add User Id',
			 		"my_url"=>'Add',
			 		"type_tgl"=>'Tanggal',
			 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
			 		"no_prop"=>$no_prop,
			 		"no_kab"=>$no_kab,
			 		"user_id"=>$this->session->userdata(S_USER_ID),
			 		"user_nik"=>$this->session->userdata(S_NIK),
			 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
			 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
			 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
			 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
			 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
	    		);
				$this->load->view('Userid_add/index',$data);
	}
	public function seting_app() 
	{
			$menu_id = 135;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			    
				$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
				$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
				$no_prop = $this->shr->get_no_prop();
				$no_kab = $this->shr->get_no_kab();
				$data_master = [];
				$data_master['no_prop'] =$no_prop;
				$data_master['no_kab'] =$no_kab;
				$data_master['nm_prop'] =$this->shr->get_nm_prop();
				$data_master['nm_kab'] =$this->shr->get_nm_kab();
				$data_master['siak_dblink'] =$this->shr->get_siak_dblink();
				$data_master['cetak_dblink'] =$this->shr->get_cetak_dblink();
				$data_master['rekam_dblink'] =$this->shr->get_rekam_dblink();
				$data_master['master_dblink'] =$this->shr->get_master_dblink();
				$data_master['default_pass'] =$this->shr->get_default_pass();
				$data_master['dkb_bio'] =$this->shr->get_dkb_bio();
				$data_master['dkb_kk'] =$this->shr->get_dkb_keluarga();
				$data_master['dkb_cutoff'] =$this->shr->get_cut_off_date();
				$data_master['dkb_tahun'] =$this->shr->get_dkb();
				$data_master['give_kec'] =$this->shr->get_give_kec();
				$data_master['give_kel'] =$this->shr->get_give_kel();
				$data = array(
			 		"stitle"=>'Add User Id',
			 		"mtitle"=>'Add User Id',
			 		"my_url"=>'Add',
			 		"type_tgl"=>'Tanggal',
			 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
			 		"data"=>$data_master,
			 		"no_prop"=>$no_prop,
			 		"no_kab"=>$no_kab,
			 		"user_id"=>$this->session->userdata(S_USER_ID),
			 		"user_nik"=>$this->session->userdata(S_NIK),
			 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
			 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
			 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
			 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
			 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
	    		);
				$this->load->view('Setting_app/index',$data);
	}
	public function seting_pejabat() 
	{
			$menu_id = 164;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			    
				$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
				$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
				$data = array(
			 		"stitle"=>'List Pejabat',
			 		"mtitle"=>'List Pejabat',
			 		"my_url"=>'Add',
			 		"type_tgl"=>'Tanggal',
			 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
			 		"user_id"=>$this->session->userdata(S_USER_ID),
			 		"user_nik"=>$this->session->userdata(S_NIK),
			 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
			 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
			 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
			 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
			 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
	    		);
				$this->load->view('User_pejabat/index',$data);
	}

	public function add_pejabat() 
	{
			$menu_id = 164;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Tambah Pejabat',
		 		"mtitle"=>'Tambah Pejabat',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_pejabat_add/index',$data);

	}public function edit_pejabat($id) 
	{
			$menu_id = 164;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$datpeg = $this->sett->edit_pejabat_list($id);
			$data = array(
		 		"stitle"=>'Edit Pejabat',
		 		"mtitle"=>'Edit Pejabat',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$datpeg,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('user_pejabat_edit/index',$data);
	}
	public function menu_settting()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $group_id = $this->input->post('group_id');
          $rekap_menu = $this->sett->menu_settting($group_id);
          $data = array();
          $i = 0;
          foreach($rekap_menu->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->MENU_ID,
                    $r->PARENT_ID,
                    $r->PARENT_TITLE,
                    $r->TITLE,
                    $r->MENU_LEVEL,
                    $r->LEVEL_HEAD,
                    $r->LEVEL_SUB,
                    '<button type="button" class="btn btn-info btn-xs" id="btn-lihat" onclick="lihat('.$r->MENU_ID.');" style="margin-top: 5px;">Select <i class="mdi  mdi-magnify fa-fw"></i></button> &nbsp <button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->MENU_ID.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button>'
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $rekap_menu->num_rows(),
                 "recordsFiltered" => $rekap_menu->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
      }
	public function get_level()
	{
		header("Content-Type: application/json", true);
		$r = $this->sett->get_level();
		echo json_encode($r);
	}
	public function get_group()
	{
		header("Content-Type: application/json", true);
		$r = $this->sett->get_group();
		echo json_encode($r);
	}
	public function get_group_user()
	{
		header("Content-Type: application/json", true);
		$r = $this->sett->get_group_user();
		echo json_encode($r);
	}
	public function get_atasan()
	{
		header("Content-Type: application/json", true);
		$r = $this->sett->get_atasan();
		echo json_encode($r);
	}
	public function get_group_lvl()
	{
		header("Content-Type: application/json", true);
		$lvl = $this->input->post('lvl');
		$r = $this->sett->get_group_lvl($lvl);
		echo json_encode($r);
	}
	public function get_parent()
	{
		header("Content-Type: application/json", true);
		$menu_level = $this->input->post('menu_level');
		$r = $this->sett->get_parent($menu_level);
		echo json_encode($r);
	}
	public function get_child()
	{
		header("Content-Type: application/json", true);
		$menu_id = $this->input->post('menu_id');
		$r = $this->sett->get_child($menu_id);
		$data["child"] = $r;
		echo json_encode($data);
	}
	public function check_lvl() 
	{
		if($this->input->post('lvl_cd') != null){
			$lvl_cd = $this->input->post('lvl_cd');
			$j = $this->sett->check_lvl($lvl_cd);
			if($j[0]->JML > 0){
				$data["success"] = TRUE;
				$data["is_exists"] = 1;
				$data["lvl_name"] = ucfirst(strtolower($j[0]->LEVEL_NAME));
        		echo json_encode($data);
			}else{
				$data["success"] = TRUE;
				$data["is_exists"] = 0;
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function check_userid() 
	{
		if($this->input->post('user_id') != null){
			$user_id = $this->input->post('user_id');
			$j = $this->sett->check_jml_userid($user_id);
			if($j[0]->JML > 0){
				$j = $this->sett->check_userid($user_id);
				$data["success"] = TRUE;
				$data["is_exists"] = 1;
				$data["nama_lgkp"] = ucfirst(strtolower($j[0]->NAMA_LGKP));
        		echo json_encode($data);
			}else{
				$data["success"] = TRUE;
				$data["is_exists"] = 0;
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function save_pejabat() 
	{
		if($this->input->post('nip') != null){
			$nip = $this->input->post('nip');
			$nama = strtoupper($this->input->post('nama'));
			$jabatan = strtoupper($this->input->post('jabatan'));
			$struktural = $this->input->post('struktural');
			$j = $this->sett->max_pejabat();
			$this->sett->save_pejabat($j,$nip,$nama,$jabatan,$struktural);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function edit_pejabat_id() 
	{
		if($this->input->post('pejabat_id') != null){
			$pejabat_id = $this->input->post('pejabat_id');
			$nip = $this->input->post('nip');
			$nama = strtoupper($this->input->post('nama'));
			$jabatan = strtoupper($this->input->post('jabatan'));
			$struktural = $this->input->post('struktural');
			$this->sett->edit_pejabat_id($pejabat_id,$nip,$nama,$jabatan,$struktural);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Update";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function do_edit_menu_id() 
	{
		if($this->input->post('menu_id') != null){
			$menu_id = $this->input->post('menu_id');
			$title = $this->input->post('title');
			$parent_id = $this->input->post('parent_id');
			$menu_level = $this->input->post('menu_level');
			$level_head = $this->input->post('level_head');
			$level_sub = $this->input->post('level_sub');
			$this->sett->do_edit_menu_id($menu_id,$title,$parent_id,$menu_level,$level_head,$level_sub);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Update";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}

	public function edit_menu_id() 
	{
		if($this->input->post('menu_id') != null){
			$menu_id = $this->input->post('menu_id');
			$title = $this->input->post('title');
			$parent_id = $this->input->post('parent_id');
			$parent_old = $this->input->post('parent_old');
			$menu_level = $this->input->post('menu_level');
			$level_head = $this->input->post('level_head');
			$level_sub = $this->input->post('level_sub');
			$this->sett->edit_parent_id($menu_id,$parent_id,$parent_old,$menu_level);
			$this->sett->edit_menu_id($menu_id,$title,$parent_id,$menu_level,$level_head,$level_sub);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Update";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function save_lvl() 
	{
		if($this->input->post('lvl_cd') != null){
			$lvl_cd = $this->input->post('lvl_cd');
			$lvl_nm = strtoupper($this->input->post('lvl_nm'));
			$kdgroup = $this->input->post('kdgroup');
			$this->sett->save_lvl($lvl_cd,$lvl_nm,$kdgroup,$this->session->userdata(S_USER_ID));
			$this->sett->save_akses($lvl_cd);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function save_activity() 
	{
		if($this->input->post('activity_nm') != null){
			$activity_nm = strtoupper($this->input->post('activity_nm'));
			$kdgroup = $this->input->post('kdgroup');
			$this->sett->save_activity($activity_nm,$kdgroup,$this->session->userdata(S_USER_ID));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function update_lvl() 
	{
		if($this->input->post('lvl_cd') != null){
			$lvl_cd = $this->input->post('lvl_cd');
			$lvl_nm = strtoupper($this->input->post('lvl_nm'));
			$this->sett->update_lvl($lvl_cd,$lvl_nm,$this->session->userdata(S_USER_ID));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function update_activity() 
	{
		if($this->input->post('activity_cd') != null){
			$activity_cd = $this->input->post('activity_cd');
			$activity_nm = strtoupper($this->input->post('activity_nm'));
			$this->sett->update_activity($activity_cd,$activity_nm);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function edit_userid() 
	{
		if($this->input->post('user_id') != null){
			$user_id = $this->input->post('user_id');
			$nama_lgkp = strtoupper($this->input->post('nama_lgkp'));
			$nama_dpn = strtoupper($this->input->post('nama_dpn'));
			$nik = $this->input->post('nik');
			$tmpt_lhr = strtoupper($this->input->post('tmpt_lhr'));
			$tgl_lhr = $this->input->post('tgl_lhr');
			$jenis_klmin = $this->input->post('jenis_klmin');
			$gol_drh = $this->input->post('gol_drh');
			$nama_kantor = strtoupper($this->input->post('nama_kantor'));
			$alamat_kantor = strtoupper($this->input->post('alamat_kantor'));
			$telp = $this->input->post('telp');
			$alamat_rumah = strtoupper($this->input->post('alamat_rumah'));
			$kdgroup = $this->input->post('kdgroup');
			$kdlvl = $this->input->post('kdlvl');
			$kdprop = $this->input->post('kdprop');
			$kdkab = $this->input->post('kdkab');
			$kdkec = $this->input->post('kdkec');
			$kdkel = $this->input->post('kdkel');
			$no_rw = $this->input->post('no_rw');
			$no_rt = $this->input->post('no_rt');
			$user_siak = $this->input->post('user_siak');
			$user_bcard = $this->input->post('user_bcard');
			$user_benrol = $this->input->post('user_benrol');
			$is_monitoring = $this->input->post('is_monitoring');
			$is_gisa = $this->input->post('is_gisa');
			$is_absen = $this->input->post('is_absen');
			$is_asn = $this->input->post('is_asn');
			$absensi_checking = $this->input->post('absensi_checking');
			$is_active = $this->input->post('is_active');
			$ipaddress_check = $this->input->post('ipaddress_check');
			$is_show = $this->input->post('is_show');
			$is_atasan_satu = $this->input->post('is_atasan_satu');
			$is_atasan_dua = $this->input->post('is_atasan_dua');
			$this->sett->update_userid_data($user_id,$nama_lgkp,$nama_dpn,$nik,$tmpt_lhr,$tgl_lhr,$jenis_klmin,$gol_drh,$nama_kantor,$alamat_kantor,$telp,$alamat_rumah,$kdgroup,$kdlvl,$kdprop,$kdkab,$kdkec,$kdkel,$no_rw,$no_rt,$user_siak,$user_bcard,$user_benrol,$is_monitoring,$is_gisa,$is_absen,$is_asn,$absensi_checking,$is_active,$ipaddress_check,$is_show,$this->session->userdata(S_USER_ID));
			$this->sett->update_atasan($user_id,$is_atasan_satu,$is_atasan_dua);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function edit_setting() 
	{
		if($this->input->post('no_prop') != null){
			$no_prop = $this->input->post('no_prop');
			$this->shr->update_no_prop($no_prop,$this->session->userdata(S_USER_ID));

			$no_kab = $this->input->post('no_kab');
			$this->shr->update_no_kab($no_kab,$this->session->userdata(S_USER_ID));

			$nm_prop = $this->input->post('nm_prop');
			$this->shr->update_nm_prop($nm_prop,$this->session->userdata(S_USER_ID));

			$nm_kab = $this->input->post('nm_kab');
			$this->shr->update_nm_kab($nm_kab,$this->session->userdata(S_USER_ID));

			$siak_dblink = $this->input->post('siak_dblink');
			$this->shr->update_siak_dblink($siak_dblink,$this->session->userdata(S_USER_ID));

			$cetak_dblink = $this->input->post('cetak_dblink');
			$this->shr->update_cetak_dblink($cetak_dblink,$this->session->userdata(S_USER_ID));

			$rekam_dblink = $this->input->post('rekam_dblink');
			$this->shr->update_rekam_dblink($rekam_dblink,$this->session->userdata(S_USER_ID));

			$master_dblink = $this->input->post('master_dblink');
			$this->shr->update_master_dblink($master_dblink,$this->session->userdata(S_USER_ID));

			$dkb_bio = $this->input->post('dkb_bio');
			$this->shr->update_dkb_bio($dkb_bio,$this->session->userdata(S_USER_ID));

			$dkb_kk = $this->input->post('dkb_kk');
			$this->shr->update_dkb_kk($dkb_kk,$this->session->userdata(S_USER_ID));

			$dkb_tahun = $this->input->post('dkb_tahun');
			$this->shr->update_dkb_tahun($dkb_tahun,$this->session->userdata(S_USER_ID));

			$dkb_cutoff = $this->input->post('dkb_cutoff');
			$this->shr->update_dkb_cutoff($dkb_cutoff,$this->session->userdata(S_USER_ID));

			$default_pass = $this->input->post('default_pass');
			$this->shr->update_default_pass($default_pass,$this->session->userdata(S_USER_ID));

			$give_kec = $this->input->post('give_kec');
			$this->shr->update_give_kec($give_kec,$this->session->userdata(S_USER_ID));

			$give_kel = $this->input->post('give_kel');
			$this->shr->update_give_kel($give_kel,$this->session->userdata(S_USER_ID));
			
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function add_userid() 
	{
		if($this->input->post('user_id') != null){
			$user_id = $this->input->post('user_id');
			$nama_lgkp = strtoupper($this->input->post('nama_lgkp'));
			$nama_dpn = strtoupper($this->input->post('nama_dpn'));
			$nik = $this->input->post('nik');
			$tmpt_lhr = strtoupper($this->input->post('tmpt_lhr'));
			$tgl_lhr = $this->input->post('tgl_lhr');
			$jenis_klmin = $this->input->post('jenis_klmin');
			$gol_drh = $this->input->post('gol_drh');
			$nama_kantor = strtoupper($this->input->post('nama_kantor'));
			$alamat_kantor = strtoupper($this->input->post('alamat_kantor'));
			$telp = $this->input->post('telp');
			$alamat_rumah = strtoupper($this->input->post('alamat_rumah'));
			$kdgroup = $this->input->post('kdgroup');
			$kdlvl = $this->input->post('kdlvl');
			$kdprop = $this->input->post('kdprop');
			$kdkab = $this->input->post('kdkab');
			$kdkec = $this->input->post('kdkec');
			$kdkel = $this->input->post('kdkel');
			$no_rw = $this->input->post('no_rw');
			$no_rt = $this->input->post('no_rt');
			$user_siak = $this->input->post('user_siak');
			$user_bcard = $this->input->post('user_bcard');
			$user_benrol = $this->input->post('user_benrol');
			$is_monitoring = $this->input->post('is_monitoring');
			$is_gisa = $this->input->post('is_gisa');
			$is_absen = $this->input->post('is_absen');
			$is_asn = $this->input->post('is_asn');
			$absensi_checking = $this->input->post('absensi_checking');
			$is_active = $this->input->post('is_active');
			$ipaddress_check = $this->input->post('ipaddress_check');
			$is_show = $this->input->post('is_show');
			$new_password = md5($this->input->post('new_password'));
			$is_atasan_satu = $this->input->post('is_atasan_satu');
			$is_atasan_dua = $this->input->post('is_atasan_dua');
			$this->sett->insert_userid_data($user_id,$nama_lgkp,$nama_dpn,$nik,$tmpt_lhr,$tgl_lhr,$jenis_klmin,$gol_drh,$nama_kantor,$alamat_kantor,$telp,$alamat_rumah,$kdgroup,$kdlvl,$kdprop,$kdkab,$kdkec,$kdkel,$no_rw,$no_rt,$user_siak,$user_bcard,$user_benrol,$is_monitoring,$is_gisa,$is_absen,$is_asn,$absensi_checking,$is_active,$ipaddress_check,$is_show,$new_password,$this->session->userdata(S_USER_ID));
			$this->sett->insert_atasan($user_id,$is_atasan_satu,$is_atasan_dua);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function do_akses() 
	{
		if($this->input->post('lvl_cd') != null){
			$lvl_cd = $this->input->post('lvl_cd');
			$menu_id = array();
			$menu_id = $this->input->post('checkedIds');
			$this->sett->def_akses($lvl_cd);
			foreach( $menu_id  as $r)
			{
				$this->sett->change_akses($lvl_cd,$r);
			}
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function delete_lvl() 
	{
		if($this->input->post('lvl_cd') != null){
			$lvl_cd = $this->input->post('lvl_cd');
			$this->sett->delete_lvl($lvl_cd);
			$this->sett->delete_akses($lvl_cd);
			$this->sett->backup_user();
			$this->sett->delete_akun($lvl_cd);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function delete_pejabat() 
	{
		if($this->input->post('pejabat') != null){
			$pejabat = $this->input->post('pejabat');
			$this->sett->delete_pejabat($pejabat);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function delete_activity_id() 
	{
		if($this->input->post('activity_id') != null){
			$activity_id = $this->input->post('activity_id');
			$this->sett->delete_activity_id($activity_id);
			$this->sett->backup_daily();
			$this->sett->delete_daily($activity_id);
			$data["success"] = TRUE;
			$data["message"] = "Activity Berhasil Di Hapus";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function delete_user_id(){
		header('Content-type: application/json');
		if ($this->input->post('user_id') != null){
			$user_id = $this->input->post('user_id');
			$this->sett->del_user_backup($user_id);
			$this->sett->backup_user();
			$this->sett->delete_user($user_id);
			$this->sett->delete_atasan($user_id);
			$output = array(
	    			"message_type"=>1,
	    			"message"=>"User Ini Telah Dihapus !"
	    	);
			echo json_encode($output);	
		}
	}
	public function get_user_level()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $group_id = $this->input->post('group_id');

          $user_lvl = $this->sett->get_user_level($group_id);

          $data = array();
          $i = 0;
          foreach($user_lvl->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->USER_LEVEL,
                    $r->LEVEL_NAME,
                    $r->GROUP_NAME,
                    '<button type="button" class="btn btn-info btn-xs" id="btn-lihat" onclick="lihat('.$r->USER_LEVEL.');" style="margin-top: 5px;">Select <i class="mdi  mdi-magnify fa-fw"></i></button> &nbsp <button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->USER_LEVEL.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-warning btn-xs" id="btn-ubah" onclick="akses('.$r->USER_LEVEL.');" style="margin-top: 5px;">Privilege Access <i class="mdi  mdi-apps fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->USER_LEVEL.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $user_lvl->num_rows(),
                 "recordsFiltered" => $user_lvl->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}

	public function get_user_helpdesk()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $group_id = $this->input->post('group_id');

          $helpdesk = $this->sett->get_user_helpdesk();

          $data = array();
          $i = 0;
          foreach($helpdesk->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->HELPDESK_ID,
                    $r->HELPDESK_DESCRIPTION,
                    '<button type="button" class="btn btn-info btn-xs" id="btn-lihat" onclick="lihat('.$r->HELPDESK_ID.');" style="margin-top: 5px;">Select <i class="mdi  mdi-magnify fa-fw"></i></button> &nbsp <button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->HELPDESK_ID.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->HELPDESK_ID.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $helpdesk->num_rows(),
                 "recordsFiltered" => $helpdesk->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}
	public function get_user_activity()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $group_id = $this->input->post('group_id');

          $user_lvl = $this->sett->get_activity($group_id);

          $data = array();
          $i = 0;
          foreach($user_lvl->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->ACTIVITY,
                    $r->LEVEL_NAME,
                    '<button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->ACTIVITY_ID.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->ACTIVITY_ID.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $user_lvl->num_rows(),
                 "recordsFiltered" => $user_lvl->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}
	public function get_user_list()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $level_id = $this->input->post('level_id');
          $user_id = $this->input->post('user_id');
          $user_nm = $this->input->post('user_nm');

          $user_data = $this->sett->get_user_list($level_id,$user_id,$user_nm);

          $data = array();
          $i = 0;
          foreach($user_data->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->USER_ID,
                    $r->NAMA_LGKP,
                    $r->LEVEL_NAME,
                    '<button type="button" class="btn btn-info btn-xs" id="btn-lihat" onclick="lihat(\''.$r->USER_ID.'\');" style="margin-top: 5px;">Select <i class="mdi  mdi-magnify fa-fw"></i></button> &nbsp <button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit(\''.$r->USER_ID.'\');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-warning btn-xs" id="btn-ubah" onclick="reset(\''.$r->USER_ID.'\');" style="margin-top: 5px;">Reset Password <i class="mdi  mdi-sync fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus(\''.$r->USER_ID.'\');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $user_data->num_rows(),
                 "recordsFiltered" => $user_data->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}
	public function get_pejabat_list()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $nama = $this->input->post('nama');
          $nip = $this->input->post('nip');

          $user_data = $this->sett->get_pejabat_list($nama,$nip);

          $data = array();
          $i = 0;
          foreach($user_data->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
               		$r->NIP,
                    $r->NAMA,
                    $r->JABATAN,
                    $r->STRUKTURAL,
                    '<button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->PEJABAT_ID.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->PEJABAT_ID.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $user_data->num_rows(),
                 "recordsFiltered" => $user_data->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}

	public function see_user($id) 
	{
			$menu_id = 134;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    if($id != null){
		    $user_level = $id;
		    $c_head = $this->sett->get_head_group($user_level);
		    $theuser = $this->sett->get_user_group($user_level);
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Setting User Level',
		 		"mtitle"=>'Setting User Level',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"theuser"=>$theuser,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_level_lihat/index',$data);
			}else{
				redirect('/','refresh');
			}
	}
	public function see_user_id($user_id) 
	{
			$menu_id = 135;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    if($user_id != null){
			    $theuser = $this->sett->get_theuser($user_id);
				$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
				$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
				$data = array(
			 		"stitle"=>'Setting User Id',
			 		"mtitle"=>'Setting User Id',
			 		"my_url"=>'UserLevel',
			 		"type_tgl"=>'Tanggal',
			 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
			 		"theuser"=>$theuser->result(),
			 		"user_id"=>$this->session->userdata(S_USER_ID),
			 		"user_nik"=>$this->session->userdata(S_NIK),
			 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
			 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
			 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
			 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
			 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
	    		);
				$this->load->view('Userid_lihat/index',$data);
			}else{
				redirect('/','refresh');
			}
	}
	public function edit_user_id($user_id) 
	{
			$menu_id = 135;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    if($user_id != null){
			    $theuser = $this->sett->get_theuser($user_id);
				$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
				$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
				$data = array(
			 		"stitle"=>'Setting User Id',
			 		"mtitle"=>'Setting User Id',
			 		"my_url"=>'UserLevel',
			 		"type_tgl"=>'Tanggal',
			 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
			 		"theuser"=>$theuser->result(),
			 		"user_id"=>$this->session->userdata(S_USER_ID),
			 		"user_nik"=>$this->session->userdata(S_NIK),
			 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
			 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
			 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
			 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
			 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
	    		);
				$this->load->view('Userid_edit/index',$data);
			}else{
				redirect('/','refresh');
			}
	}
	public function change_user($id) 
	{
			$menu_id = 134;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    if($id != null){
		    $user_level = $id;
		    $c_head = $this->sett->get_head_group($user_level);
		    $theuser = $this->sett->get_user_group($user_level);
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Setting User Level',
		 		"mtitle"=>'Setting User Level',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"theuser"=>$theuser,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_level_change/index',$data);
			}else{
				redirect('/','refresh');
			}
	}
	public function edit_user($id) 
	{
			$menu_id = 134;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    if($id != null){
		    $user_level = $id;
		    $c_head = $this->sett->get_head_group($user_level);
		    $theuser = $this->sett->get_user_group($user_level);
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Setting User Level',
		 		"mtitle"=>'Setting User Level',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"theuser"=>$theuser,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_level_edit/index',$data);
			}else{
				redirect('/','refresh');
			}
	}
	public function delete_user($id) 
	{
			$menu_id = 134;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    if($id != null){
		    $user_level = $id;
		    $c_head = $this->sett->get_head_group($user_level);
		    $theuser = $this->sett->get_user_group($user_level);
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Delete User Level',
		 		"mtitle"=>'Delete User Level',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"theuser"=>$theuser,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('User_level_delete/index',$data);
			}else{
				redirect('/','refresh');
			}
	}

	public function get_group_tree_one() 
	{
		$user_level = $this->input->get('user_level');
		$data = $this->shr->get_menu_all($user_level);
		$menu = array();
		$i=0;
		foreach( $data as $r )
		{
		    $menu[$i]['text'] = $r->TITLE;
			$menu[$i]['id'] = $r->MENU_ID;
			$menu[$i]['disabled'] = true;
			$menu[$i]['checked'] = ($r->ACTIVE_MENU != 0) ? true : false;
			$menu[$i]['children'] = $this->get_group_tree_two($r->MENU_ID,$user_level);
			$i++;
		}
		echo json_encode($menu);
	}
	public function get_group_tree_two($menu_id,$user_level) 
	{
		$lvl = $user_level;
		$data = $this->shr->get_sub_menu_all($menu_id,$lvl);
		$sub_menu = array();
		$i=0;
		foreach( $data as $r )
		{
		    $sub_menu[$i]['text'] = $r->TITLE;
			$sub_menu[$i]['id'] = $r->MENU_ID;
			$sub_menu[$i]['disabled'] = true;
			$sub_menu[$i]['checked'] = ($r->ACTIVE_MENU != 0) ? true : false;
			$sub_menu[$i]['children'] = $this->get_group_tree_three($r->MENU_ID,$lvl);
			$i++;
		}
		return $sub_menu;
	}
	public function get_group_tree_three($menu_id,$user_level) 
	{
		$lvl = $user_level;
		$data = $this->shr->get_sub_menu_all($menu_id,$lvl);
		$sub_menu = array();
		$i=0;
		foreach( $data as $r )
		{
		    $sub_menu[$i]['text'] = $r->TITLE;
			$sub_menu[$i]['id'] = $r->MENU_ID;
			$sub_menu[$i]['disabled'] = true;
			$sub_menu[$i]['checked'] = ($r->ACTIVE_MENU != 0) ? true : false;
			$sub_menu[$i]['children'] = array();
			$i++;
		}
		return $sub_menu;
	}
	public function edit_tree_one() 
	{
		$user_level = $this->input->get('user_level');
		$data = $this->shr->get_menu_all($user_level);
		$menu = array();
		$i=0;
		foreach( $data as $r )
		{
		    $menu[$i]['text'] = $r->TITLE;
			$menu[$i]['id'] = $r->MENU_ID;
			$menu[$i]['checked'] = ($r->ACTIVE_MENU != 0) ? true : false;
			$menu[$i]['children'] = $this->edit_tree_two($r->MENU_ID,$user_level);
			$i++;
		}
		echo json_encode($menu);
	}
	public function edit_tree_two($menu_id,$user_level) 
	{
		$lvl = $user_level;
		$data = $this->shr->get_sub_menu_all($menu_id,$lvl);
		$sub_menu = array();
		$i=0;
		foreach( $data as $r )
		{
		    $sub_menu[$i]['text'] = $r->TITLE;
			$sub_menu[$i]['id'] = $r->MENU_ID;
			$sub_menu[$i]['checked'] = ($r->ACTIVE_MENU != 0) ? true : false;
			$sub_menu[$i]['children'] = $this->edit_tree_three($r->MENU_ID,$lvl);
			$i++;
		}
		return $sub_menu;
	}
	public function edit_tree_three($menu_id,$user_level) 
	{
		$lvl = $user_level;
		$data = $this->shr->get_sub_menu_all($menu_id,$lvl);
		$sub_menu = array();
		$i=0;
		foreach( $data as $r )
		{
		    $sub_menu[$i]['text'] = $r->TITLE;
			$sub_menu[$i]['id'] = $r->MENU_ID;
			$sub_menu[$i]['checked'] = ($r->ACTIVE_MENU != 0) ? true : false;
			$sub_menu[$i]['children'] = array();
			$i++;
		}
		return $sub_menu;
	}
	public function max_helpdesk() 
	{
			$j = $this->sett->max_helpdesk();
			$data["jml"] = $j;
       		echo json_encode($data);
	}

	public function save_helpdesk() 
	{
		if($this->input->post('helpdesk_cd') != null){
			$helpdesk_cd = $this->input->post('helpdesk_cd');
			$helpdesk_nm = strtoupper($this->input->post('helpdesk_nm'));
			$this->sett->save_helpdesk($helpdesk_cd,$helpdesk_nm,$this->session->userdata(S_USER_ID));
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}

	public function update_helpdesk() 
	{
		if($this->input->post('helpdesk_id') != null){
			$helpdesk_id = $this->input->post('helpdesk_id');
			$helpdesk_nm = $this->input->post('helpdesk_nm');
			$this->sett->edit_helpdesk_cd($helpdesk_id,$helpdesk_nm);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Update";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}

	public function delete_helpdesk() 
	{
		if($this->input->post('helpdesk_id') != null){
			$helpdesk_id = $this->input->post('helpdesk_id');
			$this->sett->delete_helpdesk($helpdesk_id);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function get_helpdesk_option()
	{
		header("Content-Type: application/json", true);
		$r = $this->sett->get_all_helpdesk();
		echo json_encode($r);
	}
	
}
