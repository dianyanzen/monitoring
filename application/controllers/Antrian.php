<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Shared','shr');
		$this->load->model('M_antrian','ant');
    if ($this->session->userdata(S_SESSION_ID) == null) 
    {
      redirect('/','refresh');
    } else {
      $is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
      if ($is_log == 0){
        if ($this->session->userdata(S_SESSION_ID) != null) {
        $this->shr->stop_activity($this->session->userdata(S_USER_ID));
        }
        $this->session->sess_destroy();
        redirect('/','refresh');
      }
    }
	}
	public function index()
	{
        redirect('/','refresh');
	}

	public function list_antrian()
	{
      $menu_id = 106;
      $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
      if ($is_akses == 0){
        redirect('404Notfound','refresh');
      }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
      $isakses_kec = $this->shr->get_give_kec();
      $isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Antrian SMS',
		 		"mtitle"=>'Antrian SMS',
		 		"my_url"=>'list_antrian',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
            "akses_kec"=>$isakses_kec,
            "akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Antrian/index',$data);
	}
	public function cek_antrian()
	{
      $menu_id = 107;
      $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
      if ($is_akses == 0){
        redirect('404Notfound','refresh');
      }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
      $isakses_kec = $this->shr->get_give_kec();
      $isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Cek Antrian SMS',
		 		"mtitle"=>'Cek Antrian SMS',
		 		"my_url"=>'cek_antrian',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
            "akses_kec"=>$isakses_kec,
            "akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Antrian_cek/index',$data);
	}
	public function cek_antrian_log()
	{
      $menu_id = 108;
      $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
      if ($is_akses == 0){
        redirect('404Notfound','refresh');
      }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
      $isakses_kec = $this->shr->get_give_kec();
      $isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Cek Log Antrian SMS',
		 		"mtitle"=>'Cek Log Antrian SMS',
		 		"my_url"=>'cek_antrian_log',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
            "akses_kec"=>$isakses_kec,
            "akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Antrian_ceklog/index',$data);
	}
  public function cek_nohp()
  {
      $menu_id = 109;
      $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
      if ($is_akses == 0){
        redirect('404Notfound','refresh');
      }
      $menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
      $isakses_kec = $this->shr->get_give_kec();
      $isakses_kel = $this->shr->get_give_kel();
      $data = array(
        "stitle"=>'Cek Nomer HP',
        "mtitle"=>'Cek Nomer HP',
        "my_url"=>'cek_nohp',
        "type_tgl"=>'Tanggal',
        "menu"=>$menu,
            "akses_kec"=>$isakses_kec,
            "akses_kel"=>$isakses_kel,
        "user_id"=>$this->session->userdata(S_USER_ID),
        "user_nik"=>$this->session->userdata(S_NIK),
        "user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
        "user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
        "user_level"=>$this->session->userdata(S_USER_LEVEL),
        "user_no_kec"=>$this->session->userdata(S_NO_KEC),
        "user_level"=>$this->session->userdata(S_USER_LEVEL)
        );
      $this->load->view('Antrian_nohp/index',$data);
  }
	public function cek_kepakta()
	{
      $menu_id = 110;
      $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
      if ($is_akses == 0){
        redirect('404Notfound','refresh');
      }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
      $isakses_kec = $this->shr->get_give_kec();
      $isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Cek Nomor Akta Kelahiran Invalid',
		 		"mtitle"=>'Cek Nomor Akta Kelahiran Invalid',
		 		"my_url"=>'cek_kepakta',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
            "akses_kec"=>$isakses_kec,
            "akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Antrian_akta/index',$data);
	}
  public function insert_akta()
  {
      $menu_id = 179;
      $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
      if ($is_akses == 0){
        redirect('404Notfound','refresh');
      }
      $menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
      $isakses_kec = $this->shr->get_give_kec();
      $isakses_kel = $this->shr->get_give_kel();
      $data = array(
        "stitle"=>'Insert Akta Kelahiran Smsdb',
        "mtitle"=>'Insert Akta Kelahiran Smsdb',
        "my_url"=>'insert_akta',
        "type_tgl"=>'Tanggal',
        "menu"=>$menu,
            "akses_kec"=>$isakses_kec,
            "akses_kel"=>$isakses_kel,
        "user_id"=>$this->session->userdata(S_USER_ID),
        "user_nik"=>$this->session->userdata(S_NIK),
        "user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
        "user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
        "user_level"=>$this->session->userdata(S_USER_LEVEL),
        "user_no_kec"=>$this->session->userdata(S_NO_KEC),
        "user_level"=>$this->session->userdata(S_USER_LEVEL)
        );
      $this->load->view('Antrian_akta_insert/index',$data);
  }
  public function insert_biodata()
  {
      $menu_id = 180;
      $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
      if ($is_akses == 0){
        redirect('404Notfound','refresh');
      }
      $menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
      $isakses_kec = $this->shr->get_give_kec();
      $isakses_kel = $this->shr->get_give_kel();
      $data = array(
        "stitle"=>'Insert Biodata Smsdb',
        "mtitle"=>'Insert Biodata Smsdb',
        "my_url"=>'insert_biodata',
        "type_tgl"=>'Tanggal',
        "menu"=>$menu,
            "akses_kec"=>$isakses_kec,
            "akses_kel"=>$isakses_kel,
        "user_id"=>$this->session->userdata(S_USER_ID),
        "user_nik"=>$this->session->userdata(S_NIK),
        "user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
        "user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
        "user_level"=>$this->session->userdata(S_USER_LEVEL),
        "user_no_kec"=>$this->session->userdata(S_NO_KEC),
        "user_level"=>$this->session->userdata(S_USER_LEVEL)
        );
      $this->load->view('Antrian_biodata/index',$data);
  }
	public function get_loket()
	{
		header("Content-Type: application/json", true);
		$r = $this->ant->get_loket();
		echo json_encode($r);
	}

	public function get_antrian_today()
	{
		header("Content-Type: application/json", true);
		$loket = $this->input->post('loket');
		$r = $this->ant->get_antrian_today($loket);
		$response["jumlah"] = $r[0]->JML;
		echo json_encode($response);
	}

	public function get_antrian_tomorrow()
	{
		header("Content-Type: application/json", true);
		$loket = $this->input->post('loket');
		$r = $this->ant->get_antrian_tomorrow($loket);
		$response["jumlah"] = $r[0]->JML;
		echo json_encode($response);
	}
	public function get_antrian()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $loket = $this->input->post('loket');

          $antrian = $this->ant->get_antrian($loket);

          $data = array();
          $i = 0;
          foreach($antrian->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    '<span style="color: #fff">,</span>'.$r->NIK,
                    $r->NAMA,
                    $r->KODE_PELAYANAN,
                    $r->URUTAN,
                    $r->TANGGAL,
                    $r->JAM,
                    $r->HARI,
                    $r->TELEPON
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $antrian->num_rows(),
                 "recordsFiltered" => $antrian->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
     }
     public function get_cek_antrian()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $nik = $this->input->post('nik');
          $tlp = $this->input->post('tlp');

          $antrian = $this->ant->get_cek_antrian($nik,$tlp);

          $data = array();
          foreach($antrian->result() as $r) {
               $data[] = array(
                    '<span style="color: #fff">,</span>'.$r->NIK,
                    $r->NAMA,
                    $r->KODE_PELAYANAN,
                    $r->URUTAN,
                    $r->TANGGAL,
                    $r->JAM,
                    $r->HARI,
                    $r->TELEPON,
                    '<button type="button" class="btn btn-danger waves-effect waves-light m-r-10" id="btn-delete" onclick="on_delete('.$r->NIK.');">Hapus Nomer Antrian
                     <i class="mdi  mdi-delete fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $antrian->num_rows(),
                 "recordsFiltered" => $antrian->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
     }
     public function get_cek_antrianakta()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          if (!empty($this->input->post('nik'))){
            $nik = (!empty($this->input->post('nik'))) ? $this->input->post('nik') : '';
            $antrian = $this->ant->get_cek_antrianakta($nik);
            $data = array();
            foreach($antrian->result() as $r) {
               $data[] = array(
                    '<span style="color: #fff">,</span>'.$r->NIK,
                    $r->NAMA_LGKP,
                    $r->TMPT_LHR,
                    $r->TGL_LHR,
                    $r->STATUS_AKTA,
                    $r->NO_AKTA_LHR,
                    '<button type="button" class="btn btn-danger waves-effect waves-light m-r-10" id="btn-delete" onclick="on_delete('.$r->NIK.');">Hapus Nomer Akta Invalid
                     <i class="mdi  mdi-delete fa-fw"></i></button>'
               );
          }
          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $antrian->num_rows(),
                 "recordsFiltered" => $antrian->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
          }else{
            $data = array();
            $output = array(
               "draw" => $draw,
                 "recordsTotal" => 0,
                 "recordsFiltered" => 0,
                 "data" => $data
            );
          echo json_encode($output);
          exit();
          }
          
     }
     public function get_cek_antrian_biodata()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          if (!empty($this->input->post('nik'))){
            $nik = (!empty($this->input->post('nik'))) ? $this->input->post('nik') : '';
            $antrian = $this->ant->get_cek_antrian_biodata($nik);
            $data = array();
            foreach($antrian->result() as $r) {
               $data[] = array(
                    '<span style="color: #fff">,</span>'.$r->NIK,
                    $r->NAMA_LGKP,
                    $r->TMPT_LHR,
                    $r->TGL_LHR,
                    $r->STATUS_AKTA,
                    $r->NO_AKTA_LHR,
                    ($r->STATUS_DATA == 1) ? '<button type="button" class="btn btn-info waves-effect waves-light m-r-10" id="btn-delete" >Biodata Sudah Ada DI Smsdb
                     <i class="mdi  mdi-sync-alert fa-fw"></i></button>' : '<button type="button" class="btn btn-success waves-effect waves-light m-r-10" id="btn-delete" onclick="on_insert('.$r->NIK.');">Tambah Biodata Wni
                     <i class="mdi  mdi-plus-circle fa-fw"></i></button>',
                   
                    
               );
          }
          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $antrian->num_rows(),
                 "recordsFiltered" => $antrian->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
          }else{
            $data = array();
            $output = array(
               "draw" => $draw,
                 "recordsTotal" => 0,
                 "recordsFiltered" => 0,
                 "data" => $data
            );
          echo json_encode($output);
          exit();
          }
          
     }
     public function get_cek_antrianlog()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $nik = $this->input->post('nik');
          $tlp = $this->input->post('tlp');

          $antrian = $this->ant->get_cek_antrianlog($nik,$tlp);

          $data = array();
          $i = 0;
          foreach($antrian->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    '<span style="color: #fff">,</span>'.$r->NIK,
                    $r->NAMA,
                    $r->KODE_PELAYANAN,
                    $r->URUTAN,
                    $r->TANGGAL,
                    $r->JAM,
                    $r->HARI,
                    $r->TELEPON
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $antrian->num_rows(),
                 "recordsFiltered" => $antrian->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
     }
     public function get_cek_nohp()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $tlp = $this->input->post('tlp');

          $antrian = $this->ant->get_cek_nohp($tlp);

          $data = array();
          $i = 0;
          foreach($antrian->result() as $r) {
              $i ++;
               $data[] = array(
                  $i,
                    $r->TELEPON,
                    $r->JUMLAH_SMS,
                    $r->STATUS_BLOKIR,
                    $r->TANGGAL_BLOKIR,
                    '<button type="button" class="btn btn-danger waves-effect waves-light m-r-10" id="btn-delete" onclick="on_delete('.$r->TELEPON.');">Hapus
                     <i class="mdi  mdi-delete fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $antrian->num_rows(),
                 "recordsFiltered" => $antrian->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
     }
     public function do_delete_phone() 
      {
        if($this->input->post('tlp') != null){
          $tlp = '+'.$this->input->post('tlp');
          $antrian = $this->ant->get_cek_nohp_block($tlp);
          if($antrian->num_rows() > 0){
            $j = $this->ant->check_calo($tlp);
            if($antrian->num_rows() > 0){
            $data["success"] = TRUE;
            $data["is_save"] = 1;
            $data["message"] = "Nomor : ".$tlp." Nomor Telepon Sudah Di Block Permanen Tidak Dapat Dihapus, Contact Admin E-Spasi";
            echo json_encode($data);  
            }else{
            $this->ant->delete_nohp($tlp);
            $data["success"] = TRUE;
            $data["is_save"] = 0;
            $data["message"] = "Nomor : ".$tlp." Berhasil Di Reset Dari Daftar Blokir";
            echo json_encode($data);  
            }
          }else{
            $data["success"] = TRUE;
            $data["is_save"] = 1;
            $data["message"] = "Nomor : ".$tlp." Tidak Di Temukan Mohon Cek Kembali";
            echo json_encode($data);
          }
        }else{
          redirect('/','refresh');
        }
      }
       public function do_delete_antrian() 
      {
        if($this->input->post('nik') != null){
          $nik = $this->input->post('nik');
          $antrian = $this->ant->get_cek_antrian_invalid($nik);
          if($antrian->num_rows() > 0){
            $this->ant->delete_antrian($nik);
            $data["success"] = TRUE;
            $data["is_save"] = 0;
            $data["message"] = "Antrian Dari Nik : ".$nik." Berhasil Di Hapus";
            echo json_encode($data);
          }else{
            $data["success"] = TRUE;
            $data["is_save"] = 1;
            $data["message"] = "Antrian Dari Nik : ".$nik." Tidak Dapat Di Hapus";
            echo json_encode($data);
          }
        }else{
          redirect('/','refresh');
        }
      }
      public function do_delete_akta() 
      {
        if($this->input->post('nik') != null){
          $nik = $this->input->post('nik');
          $antrian = $this->ant->get_cek_antrianakta_invalid($nik);
          if($antrian->num_rows() > 0){
            $nik_akta = $antrian->row(0)->NIK;
            $akta_lhr = $antrian->row(0)->AKTA_LHR;
            $no_akta_lhr = $antrian->row(0)->NO_AKTA_LHR;
            $this->ant->delete_akta($nik_akta,$akta_lhr,$no_akta_lhr,$this->session->userdata(S_USER_ID),$this->input->ip_address());
            // $this->ant->delete_akta($nik);
            $data["success"] = TRUE;
            $data["is_save"] = 0;
            $data["message"] = "No Akta Lahir Dari Nik : ".$nik." Berhasil Di Ubah";
            echo json_encode($data);
          }else{
            $data["success"] = TRUE;
            $data["is_save"] = 1;
            $data["message"] = "Mohon Maaf Nik : ".$nik." Tidak Memiliki Nomor Akta Invalid, Silahkan Cek SIAK";
            echo json_encode($data);
          }
        }else{
          redirect('/','refresh');
        }
      }
      public function do_insert_biodata() 
      {
        if($this->input->post('nik') != null){
          $nik = $this->input->post('nik');
          $antrian = $this->ant->get_cek_insert_biodata($nik);
          if($antrian->num_rows() > 0){
            $nik_bio = $antrian->row(0)->NIK;
            $this->ant->insert_nik_biodata($nik_bio);
            // $this->ant->delete_akta($nik);
            $data["success"] = TRUE;
            $data["is_save"] = 0;
            $data["message"] = "Nik : ".$nik." Berhasil Di Masukan Ke Smsdb";
            echo json_encode($data);
          }else{
            $data["success"] = TRUE;
            $data["is_save"] = 1;
            $data["message"] = "Mohon Maaf Nik : ".$nik." Sudah Ada Di Smsdb";
            echo json_encode($data);
          }
        }else{
          redirect('/','refresh');
        }
      }
      public function edit_akta_sms() 
      {
        if($this->input->post('nik') != null){
          $nik = $this->input->post('nik');
          $akta_lhr = $this->input->post('akta_lhr');
          $no_akta_lhr = $this->input->post('no_akta_lhr');
          $antrian = $this->ant->get_cek_antrianakta_not_invalid($nik);
          if($antrian->num_rows() > 0){
            $nik_akta = $antrian->row(0)->NIK;
            $this->ant->update_akta($nik_akta,$akta_lhr,$no_akta_lhr,$this->session->userdata(S_USER_ID),$this->input->ip_address());
            // $this->ant->delete_akta($nik);
            $data["success"] = TRUE;
            $data["is_save"] = 0;
            $data["message"] = "No Akta Lahir Dari Nik : ".$nik." Berhasil Di Ubah";
            echo json_encode($data);
          }else{
            $data["success"] = TRUE;
            $data["is_save"] = 1;
            $data["message"] = "Mohon Maaf Nik : ".$nik." Tidak Memiliki Nomor Akta Invalid, Silahkan Cek SIAK";
            echo json_encode($data);
          }
        }else{
          redirect('/','refresh');
        }
      }
     public function get_edit_antrianakta()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          if (!empty($this->input->post('nik'))){
            $nik = (!empty($this->input->post('nik'))) ? $this->input->post('nik') : '';
            $antrian = $this->ant->get_cek_antrianakta($nik);
            $data = array();
            foreach($antrian->result() as $r) {
               $data[] = array(
                    '<span style="color: #fff">,</span>'.$r->NIK,
                    $r->NAMA_LGKP,
                    $r->TMPT_LHR,
                    $r->TGL_LHR,
                    $r->STATUS_AKTA,
                    $r->NO_AKTA_LHR,
                     ($r->STATUS_AKTA == 'ADA') ? '<button type="button" class="btn btn-info waves-effect waves-light m-r-10" id="btn-delete" >No Akta Sudah Ada
                     <i class="mdi  mdi-sync-alert fa-fw"></i></button>' : '<button type="button" class="btn btn-success waves-effect waves-light m-r-10" id="btn-delete" onclick="on_update('.$r->NIK.');">Tambah No Akta Lahirs
                     <i class="mdi  mdi-plus-circle fa-fw"></i></button>'
               );
          }
          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $antrian->num_rows(),
                 "recordsFiltered" => $antrian->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
          }else{
            $data = array();
            $output = array(
               "draw" => $draw,
                 "recordsTotal" => 0,
                 "recordsFiltered" => 0,
                 "data" => $data
            );
          echo json_encode($output);
          exit();
          }
          
     }

}