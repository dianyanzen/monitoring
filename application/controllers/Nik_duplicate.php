<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nik_duplicate extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Search','scr');
		$this->load->model('M_Shared','shr');	
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}	
	}
	public function index()
	{
			$menu_id = 77;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    $isakses_kec = $this->shr->get_give_kec();
		    $isakses_kel = $this->shr->get_give_kel();
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			// $r = [];
			// $j = [];
			// $nik = $this->input->post('nik');
			$j = $this->scr->cek_count_duplicate($nik);
			$r = $this->scr->cek_data_duplicate($nik); 	
			
			$data = array(
		 		"stitle"=>'Ktp-el Cek Duplicate Monitoring',
		 		"mtitle"=>'Ktp-el Cek Duplicate Monitoring',
		 		"my_url"=>'Duplicate',
		 		"type_tgl"=>'Rekam',
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Ktp-el Cek Duplicate Monitoring',
		 		"mtitle"=>'Ktp-el Cek Duplicate Monitoring',
		 		"my_url"=>'Duplicate',
		 		"type_tgl"=>'Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('cekduplicate/index',$data);
		
		
	}
	public function export() {
		// create file name
		if($this->input->post('niks') != null){
			$nik = $this->input->post('niks');
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
        $fileName = strtoupper($this->session->userdata(S_USER_ID)).'_DUPLICATE_RECORD_'.time().'.xlsx';  
		// load excel library
        $this->load->library('excel');
        $data_rekam =  $this->scr->cek_data_duplicate($nik );
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $style_aligment = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        )
    	);
    	$style_color = array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '01B0F1')
	        )
    	);
		
    	$objPHPExcel->getActiveSheet()->getStyle("A1:G5")->applyFromArray($style_aligment);
    	$objPHPExcel->getActiveSheet()->getStyle("A9:S9")->applyFromArray($style_color);
        $objPHPExcel->getActiveSheet()->setTitle('Data Duplicate Record');
        // set Header
        $objPHPExcel->getActiveSheet()->mergeCells('B1:G1');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', "DAFTAR DUPLICATE RECORD EKTP ".strtoupper($this->session->userdata(S_USER_ID)));

		$objPHPExcel->getActiveSheet()->mergeCells('B2:G2');
		$objPHPExcel->getActiveSheet()->setCellValue('B2', "DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL");

		$objPHPExcel->getActiveSheet()->mergeCells('B3:G3');
		$objPHPExcel->getActiveSheet()->setCellValue('B3', "".ucwords(strtolower($this->session->userdata(S_NM_KAB)))."");

		$objPHPExcel->getActiveSheet()->mergeCells('B4:G4');
		$objPHPExcel->getActiveSheet()->setCellValue('B4', "DONT BE DUPLICATE RECORD");

		$objPHPExcel->getActiveSheet()->getStyle('B1:G5')->getFont()->setName('Arial');
		$objPHPExcel->getActiveSheet()->getStyle('B1:G5')->getFont()->setSize(14);
		$objPHPExcel->getActiveSheet()->getStyle('B1:G5')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A9:S9')->getFont()->setBold(true);

		// untuk sub judul
		$objPHPExcel->getActiveSheet()->setCellValue('A6', "Kab/Kota : ".ucwords(strtolower($this->session->userdata(S_NM_KAB))));
		$objPHPExcel->getActiveSheet()->setCellValue('A7', "Provinsi : Jawa Barat");
	
		$objPHPExcel->getActiveSheet()->getStyle('A6:I7')->getFont()->setName('Arial');
		$objPHPExcel->getActiveSheet()->getStyle('A6:I7')->getFont()->setSize(9);
		
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo_disduk');
		$objDrawing->setDescription('Logo_disduk');
		// $logo = base_path() . '/images/logo.png';
		$objDrawing->setPath('assets/plugins/images/pemkot.png');
		$objDrawing->setCoordinates('A1');
		$objDrawing->setOffsetX(30); 
		$objDrawing->setOffsetY(10); 
		$objDrawing->setHeight(100);
		$objDrawing->setWidth(100);
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);


        $objPHPExcel->getActiveSheet()->SetCellValue('A9', 'NIK DUPLICATE');
        $objPHPExcel->getActiveSheet()->SetCellValue('B9', 'NAMA LENGKAP DUPLICATE');
        $objPHPExcel->getActiveSheet()->SetCellValue('C9', 'ALAMAT DUPLICATE');
        $objPHPExcel->getActiveSheet()->SetCellValue('D9', 'STATUS KTP DUPLICATE');
        $objPHPExcel->getActiveSheet()->SetCellValue('E9', 'PROVINSI DUPLICATE');       
        $objPHPExcel->getActiveSheet()->SetCellValue('F9', 'KABUPATEN DUPLICATE');       
        $objPHPExcel->getActiveSheet()->SetCellValue('G9', 'KECAMATAN DUPLICATE');       
        $objPHPExcel->getActiveSheet()->SetCellValue('H9', 'KELURAHAN DUPLICATE');       
        $objPHPExcel->getActiveSheet()->SetCellValue('I9', 'TANGGAL REKAM DUPLICATE');       
        $objPHPExcel->getActiveSheet()->SetCellValue('J9', 'NIK SINGLE / MULTIPLE NIK\'S');       
        $objPHPExcel->getActiveSheet()->SetCellValue('K9', 'NAMA LENGKAP SINGLE');       
        $objPHPExcel->getActiveSheet()->SetCellValue('L9', 'ALAMAT SINGLE');       
        $objPHPExcel->getActiveSheet()->SetCellValue('M9', 'STATUS KTP SINGLE');       
        $objPHPExcel->getActiveSheet()->SetCellValue('N9', 'PROVINSI SINGLE');       
        $objPHPExcel->getActiveSheet()->SetCellValue('O9', 'KABUPATEN SINGLE');       
        $objPHPExcel->getActiveSheet()->SetCellValue('P9', 'KECAMATAN SINGLE');       
        $objPHPExcel->getActiveSheet()->SetCellValue('Q9', 'KELURAHAN SINGLE');       
        $objPHPExcel->getActiveSheet()->SetCellValue('R9', 'TANGGAL REKAM SINGLE');       
        $objPHPExcel->getActiveSheet()->SetCellValue('S9', 'SUMBER DATA');       
        // set Row
        $rowCount = 10;
        foreach ($data_rekam as $val) 
        {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $val->NIK);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $val->NAMA_LGKP);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $val->ALAMAT);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $val->CURRENT_STATUS_CODE);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $val->NAMA_PROP);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $val->NAMA_KAB);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $val->NAMA_KEC);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $val->NAMA_KEL);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $val->TGL_REKAM);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $val->NIK_SINGLE);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $val->NAMA_LGKP_SINGLE);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $val->ALAMAT_SINGLE);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $val->CURRENT_STATUS_CODE_SINGLE);
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $val->NAMA_PROP_SINGLE);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $val->NAMA_KAB_SINGLE);
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $val->NAMA_KEC_SINGLE);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $val->NAMA_KEL_SINGLE);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $val->TGL_REKAM_SINGLE);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, 'MONITORING');
            $rowCount++;
        }
        $endrow = $rowCount-1;
        $detailrow = $rowCount+1;
        $objPHPExcel->getActiveSheet()->mergeCells('A'.$detailrow.':G'.$detailrow);
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$detailrow, "Sumber : Data Pelayanan Tanggal ".date('d-m-Y')." Dari Aplikasi Monitoring Dinas Kependudukan Dan Pencatatan Sipil ".ucwords(strtolower($this->session->userdata(S_NM_KAB))).", Copyright © ".date('Y')." DianYanzen (PIAK)");
 		$styleborder = array(
		      'borders' => array(
		          'allborders' => array(
		              'style' => PHPExcel_Style_Border::BORDER_THIN
		          )
		      )
		  );
		$objPHPExcel->getActiveSheet()->getStyle("A9:S". $endrow)->applyFromArray($styleborder);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save('excel/'.$fileName);
		// download file
        header("Content-Type: application/vnd.ms-excel");
         redirect(site_url().'excel/'.$fileName); 
         } 
        else 
        {
           redirect('/','refresh');
        }             
    }
}