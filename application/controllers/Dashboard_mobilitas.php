<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_mobilitas extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Dashboard','dsb');
		$this->load->model('M_Shared','shr');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}
	}
	public function index()
	{
			$menu_id = 7;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Dashboard Mobilitas',
		 		"back_title"=>'Dashboard',
		 		"backurl"=>'Dashboard',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Dashboard_mobilitas/index',$data);
		
	}
	public function get_data()
	{
		header('Content-type: application/json');
		 $mobilitas = $this->dsb->get_mobilitas();
		 $output = array_merge($mobilitas);
        echo json_encode($output);
	}
	public function get_data_detail()
	{
		header('Content-type: application/json');
		 $mobilitas = $this->dsb->get_mobilitas_detail();
		 $output = array_merge($mobilitas);
        echo json_encode($output);
	}
}
