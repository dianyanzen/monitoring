<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Detail','dtl');
		$this->load->model('M_Shared','shr');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}
		if ($this->session->userdata(S_USER_LEVEL) < 5) 
		{
			redirect('/','refresh');
		}
	}
	public function index()
	{
		 	redirect('/','refresh');
	}
	// Detail DATA EKTP //
	public function gdert(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$pk_col = "NIK";
			$check_exists_bio = 1;
			$is_today = 1;
			$is_database = "221";
			$is_dblink = "@DB222";
			$is_coklit = 0;
			$r = $this->dtl->get_detail_rek_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink,'','',$is_coklit);
			$j = $this->dtl->get_jumlah_rek_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink,'','',$is_coklit);
			if($is_coklit == 1){
				$data = array(
				"function_id"=>'gdert',
		 		"status_tmp"=>0,
		 		"stitle"=>'Detail Perekaman Ktp-El',
		 		"stabletitle"=>'Detail Perekaman Ktp-El Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    			);
			}else{
				$data = array(
				"function_id"=>'gdert',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Perekaman Ktp-El',
		 		"stabletitle"=>'Detail Perekaman Ktp-El Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    			);
			}
			
			$this->load->view('Temporary/index',$data);
        
	}

	public function gdery(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$pk_col = "NIK";
			$check_exists_bio = 1;
			$is_today = 2;
			$is_database = "221";
			$is_dblink = "@DB222";
			$is_coklit = 0;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_rek_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink,'','',$is_coklit);
			$j = $this->dtl->get_jumlah_rek_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink,'','',$is_coklit);
			if($is_coklit == 1){
			$data = array(
				"function_id"=>'gdery',
		 		"status_tmp"=>0,
		 		"stitle"=>'Detail Perekaman Ktp-El',
		 		"stabletitle"=>'Detail Perekaman Ktp-El Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    			);
			}else{
    		$data = array(
    			"function_id"=>'gdery',
		 		"status_tmp"=>0,
		 		"stitle"=>'Detail Perekaman Ktp-El',
		 		"stabletitle"=>'Detail Perekaman Ktp-El Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    			);
    		}
			$this->load->view('Temporary/index',$data);
        
	}
	public function gdect(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "CARD_MANAGEMENT";
			$date_col = "PERSONALIZED_DATE";
			$pk_col = "NIK";
			$check_exists_bio = 1;
			$is_today = 1;
			$is_database = "2";
			$is_dblink = "@DB222";
			$r = $this->dtl->get_detail_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink);
			$j = $this->dtl->get_jumlah_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink);
			$data = array(
				"function_id"=>'gdect',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pencetakan Ktp-El',
		 		"stabletitle"=>'Detail Pencetakan Ktp-El Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Temporary/index',$data);
        
	}

	public function gdecy(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "CARD_MANAGEMENT";
			$date_col = "PERSONALIZED_DATE";
			$pk_col = "NIK";
			$check_exists_bio = 1;
			$is_today = 2;
			$is_database = "2";
			$is_dblink = "@DB222";
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink);
			$j = $this->dtl->get_jumlah_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink);
			$data = array(
				"function_id"=>'gdecy',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pencetakan Ktp-El',
		 		"stabletitle"=>'Detail Pencetakan Ktp-El Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Temporary/index',$data);
        
	}
	public function gdest(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "T7_HIST_SUKET";
			$date_col = "PRINTED_DATE";
			$pk_col = "NIK";
			$check_exists_bio = 1;
			$is_today = 1;
			$is_database = "222";
			$r = $this->dtl->get_detail_suket($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$j = $this->dtl->get_jumlah_suket($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$data = array(
				"function_id"=>'gdest',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pencetakan Suket',
		 		"stabletitle"=>'Detail Pencetakan Suket Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Temporary/index',$data);
        
	}

	public function gdesy(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "T7_HIST_SUKET";
			$date_col = "PRINTED_DATE";
			$pk_col = "NIK";
			$check_exists_bio = 1;
			$is_today = 2;
			$is_database = "222";
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$j = $this->dtl->get_jumlah_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$data = array(
				"function_id"=>'gdesy',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pencetakan Suket',
		 		"stabletitle"=>'Detail Pencetakan Suket Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Temporary/index',$data);
        
	}
	// Detail DATA BIODATA //
	public function gdbkt(){
			$menu_id = 6;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DATA_KELUARGA";
			$date_col = "TGL_INSERTION";
			$pk_col = "NO_KK";
			$check_exists_bio = 0;
			$is_today = 1;
			$is_database = "222";
			$r = $this->dtl->get_detail_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$j = $this->dtl->get_jumlah_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$data = array(
				"function_id"=>'gdbkt',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pendaftaran KK',
		 		"stabletitle"=>'Detail Pendaftaran KK Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Biodata',
		 		"backurl"=>'Dashboard_biodata',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdbky(){
			$menu_id = 6;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DATA_KELUARGA";
			$date_col = "TGL_INSERTION";
			$pk_col = "NO_KK";
			$check_exists_bio = 0;
			$is_today = 2;
			$is_database = "222";
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$j = $this->dtl->get_jumlah_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$data = array(
				"function_id"=>'gdbky',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pendaftaran KK',
		 		"stabletitle"=>'Detail Pendaftaran KK Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Biodata',
		 		"backurl"=>'Dashboard_biodata',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Temporary/index',$data);
        
	}
	public function gdbbt(){
			$menu_id = 6;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "BIODATA_WNI";
			$date_col = "TGL_ENTRI";
			$pk_col = "NIK";
			$check_exists_bio = 0;
			$is_today = 1;
			$is_database = "222";
			$r = $this->dtl->get_detail_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$j = $this->dtl->get_jumlah_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$data = array(
				"function_id"=>'gdbbt',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pendaftaran Biodata',
		 		"stabletitle"=>'Detail Pendaftaran Biodata Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Biodata',
		 		"backurl"=>'Dashboard_biodata',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdbby(){
			$menu_id = 6;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "BIODATA_WNI";
			$date_col = "TGL_ENTRI";
			$pk_col = "NIK";
			$check_exists_bio = 0;
			$is_today = 2;
			$is_database = "222";
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$j = $this->dtl->get_jumlah_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$data = array(
				"function_id"=>'gdbby',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pendaftaran Biodata',
		 		"stabletitle"=>'Detail Pendaftaran Biodata Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Biodata',
		 		"backurl"=>'Dashboard_biodata',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Temporary/index',$data);
        
	}

	// Detail DATA Mobilitas //
	public function gdmdt(){
			$menu_id = 7;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DATANG_HEADER A INNER JOIN DATANG_DETAIL B";
			$date_col = "CREATED_DATE";
			$pk_col = "NO_DATANG";
			$check_is_pindah = 0;
			$is_today = 1;
			$r = $this->dtl->get_detail_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today);
			$j = $this->dtl->get_jumlah_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today);
			$data = array(
				"function_id"=>'gdmdt',
		 		"status_tmp"=>2,
		 		"stitle"=>'Detail Kedatangan',
		 		"stabletitle"=>'Detail Pendaftaran Kedatangan Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Mobilitas',
		 		"backurl"=>'Dashboard_mobilitas',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdmdy(){
			$menu_id = 7;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DATANG_HEADER A INNER JOIN DATANG_DETAIL B";
			$date_col = "CREATED_DATE";
			$pk_col = "NO_DATANG";
			$check_is_pindah = 0;
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today);
			$j = $this->dtl->get_jumlah_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today);
			$data = array(
				"function_id"=>'gdmdy',
		 		"status_tmp"=>2,
		 		"stitle"=>'Detail Kedatangan',
		 		"stabletitle"=>'Detail Pendaftaran Kedatangan Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Mobilitas',
		 		"backurl"=>'Dashboard_mobilitas',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Temporary/index',$data);
        
	}
	public function gdmpt(){
			$menu_id = 7;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B";
			$date_col = "CREATED_DATE";
			$pk_col = "NO_PINDAH";
			$check_is_pindah = 1;
			$is_today = 1;
			$r = $this->dtl->get_detail_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today);
			$j = $this->dtl->get_jumlah_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today);
			$data = array(
				"function_id"=>'gdmpt',
		 		"status_tmp"=>2,
		 		"stitle"=>'Detail Kepindahan',
		 		"stabletitle"=>'Detail Pendaftaran Kepindahan Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Mobilitas',
		 		"backurl"=>'Dashboard_mobilitas',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdmpy(){
			$menu_id = 7;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B";
			$date_col = "CREATED_DATE";
			$pk_col = "NO_PINDAH";
			$check_is_pindah = 1;
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today);
			$j = $this->dtl->get_jumlah_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today);
			$data = array(
				"function_id"=>'gdmpy',
		 		"status_tmp"=>2,
		 		"stitle"=>'Detail Kepindahan',
		 		"stabletitle"=>'Detail Pendaftaran Kepindahan Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Mobilitas',
		 		"backurl"=>'Dashboard_mobilitas',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Temporary/index',$data);
        
	}

	// Detail DATA CAPIL //
	public function gdclut(){
			$menu_id = 8;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "CAPIL_LAHIR";
			$date_col = "PLPR_TGL_LAPOR";
			$pk_col = "BAYI_NIK";
			$check_exists_bio = 1;
			$is_today = 1;
			$is_database = "222";
			$is_where = " AND A.ADM_AKTA_NO LIKE '%LU%' AND A.ADM_NO_PROV= 32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D' ";
			$r = $this->dtl->get_detail_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where);
			$j = $this->dtl->get_jumlah_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where);
			$data = array(
				"function_id"=>'gdclut',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Kelahiran Umum',
		 		"stabletitle"=>'Detail Kelahiran Umum Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard_capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdcluy(){
			$menu_id = 8;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "CAPIL_LAHIR";
			$date_col = "PLPR_TGL_LAPOR";
			$pk_col = "BAYI_NIK";
			$check_exists_bio = 1;
			$is_today = 2;
			$is_database = "222";
			$is_where = " AND A.ADM_AKTA_NO LIKE '%LU%' AND A.ADM_NO_PROV= 32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D' ";
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where);
			$j = $this->dtl->get_jumlah_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where);
			$data = array(
				"function_id"=>'gdcluy',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Kelahiran Umum',
		 		"stabletitle"=>'Detail Kelahiran Umum Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard_capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Temporary/index',$data);
        
	}
	public function gdcltt(){
			$menu_id = 8;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "CAPIL_LAHIR";
			$date_col = "PLPR_TGL_LAPOR";
			$pk_col = "BAYI_NIK";
			$check_exists_bio = 1;
			$is_today = 1;
			$is_database = "222";
			$is_where = " AND A.ADM_AKTA_NO LIKE '%LT%' AND A.ADM_NO_PROV= 32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D' ";
			$r = $this->dtl->get_detail_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where);
			$j = $this->dtl->get_jumlah_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where);
			$data = array(
				"function_id"=>'gdcltt',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Kelahiran Terlambat',
		 		"stabletitle"=>'Detail Kelahiran Terlambat Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard_capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdclty(){
			$menu_id = 8;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "CAPIL_LAHIR";
			$date_col = "PLPR_TGL_LAPOR";
			$pk_col = "BAYI_NIK";
			$check_exists_bio = 1;
			$is_today = 2;
			$is_database = "222";
			$is_where = " AND A.ADM_AKTA_NO LIKE '%LT%' AND A.ADM_NO_PROV= 32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D' ";
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where);
			$j = $this->dtl->get_jumlah_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where);
			$data = array(
				"function_id"=>'gdclty',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Kelahiran Terlambat',
		 		"stabletitle"=>'Detail Kelahiran Terlambat Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard_capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Temporary/index',$data);
        
	}
	public function gdckt(){
			$menu_id = 8;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "CAPIL_KAWIN";
			$date_col = "KAWIN_TGL_LAPOR";
			$get_col = " A.KAWIN_NO AS CAPIL_1
    	 				, A.KAWIN_TGL_KAWIN AS CAPIL_2
    	 				, A.SUAMI_NAMA_LGKP AS CAPIL_3
    	 				, A.ISTRI_NAMA_LGKP AS CAPIL_4
    	 				, A.KAWIN_TGL_LAPOR AS CAPIL_5 ";
			$is_today = 1;
			$r = $this->dtl->get_detail_capil($tbl_name,$date_col,$is_today,$get_col);
			$data = array(
				"function_id"=>'gdckt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perkawinan',
		 		"stabletitle"=>'Detail Pencatatan Perkawinan Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard_capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"No Kawin",
		 		"col2"=>"Tanggal Kawin",
		 		"col3"=>"Nama Suami",
		 		"col4"=>"Nama Istri",
		 		"col5"=>"Tanggal Lapor Kawin"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdcky(){
			$menu_id = 8;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "CAPIL_KAWIN";
			$date_col = "KAWIN_TGL_LAPOR";
			$get_col = " A.KAWIN_NO AS CAPIL_1
    	 				, A.KAWIN_TGL_KAWIN AS CAPIL_2
    	 				, A.SUAMI_NAMA_LGKP AS CAPIL_3
    	 				, A.ISTRI_NAMA_LGKP AS CAPIL_4
    	 				, A.KAWIN_TGL_LAPOR AS CAPIL_5 ";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_capil($tbl_name,$date_col,$is_today,$get_col);
			$data = array(
				"function_id"=>'gdcky',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perkawinan',
		 		"stabletitle"=>'Detail Pencatatan Perkawinan Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard_capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"No Kawin",
		 		"col2"=>"Tanggal Kawin",
		 		"col3"=>"Nama Suami",
		 		"col4"=>"Nama Istri",
		 		"col5"=>"Tanggal Lapor Kawin"
    		);
			$this->load->view('Temporary/index',$data);
        
	}
	public function gdcct(){
			$menu_id = 8;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "CAPIL_CERAI";
			$date_col = "CERAI_TGL_LAPOR";
			$get_col = " A.CERAI_NO AS CAPIL_1
    	 				, A.CERAI_TGL_AKTA_KAWIN AS CAPIL_2
    	 				, A.SUAMI_NAMA_LGKP AS CAPIL_3
    	 				, A.ISTRI_NAMA_LGKP AS CAPIL_4
    	 				, A.CERAI_TGL_LAPOR AS CAPIL_5 ";
			$is_today = 1;
			$r = $this->dtl->get_detail_capil($tbl_name,$date_col,$is_today,$get_col);
			$data = array(
				"function_id"=>'gdcct',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perceraian',
		 		"stabletitle"=>'Detail Pencatatan Perceraian Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard_capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"No Cerai",
		 		"col2"=>"Tanggal Kawin",
		 		"col3"=>"Nama Suami",
		 		"col4"=>"Nama Istri",
		 		"col5"=>"Tanggal Lapor Cerai"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdccy(){
			$menu_id = 8;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "CAPIL_CERAI";
			$date_col = "CERAI_TGL_LAPOR";
			$get_col = " A.CERAI_NO AS CAPIL_1
    	 				, A.CERAI_TGL_AKTA_KAWIN AS CAPIL_2
    	 				, A.SUAMI_NAMA_LGKP AS CAPIL_3
    	 				, A.ISTRI_NAMA_LGKP AS CAPIL_4
    	 				, A.CERAI_TGL_LAPOR AS CAPIL_5 ";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_capil($tbl_name,$date_col,$is_today,$get_col);
			$data = array(
				"function_id"=>'gdccy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perceraian',
		 		"stabletitle"=>'Detail Pencatatan Perceraian Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard_capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"No Cerai",
		 		"col2"=>"Tanggal Kawin",
		 		"col3"=>"Nama Suami",
		 		"col4"=>"Nama Istri",
		 		"col5"=>"Tanggal Lapor Cerai"
    		);
			$this->load->view('Temporary/index',$data);
        
	}
	public function gdcmt(){
			$menu_id = 8;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "CAPIL_MATI";
			$date_col = "PLPR_TGL_LAPOR";
			$get_col = " A.MATI_NO AS CAPIL_1
    	 				, A.MATI_NAMA_LGKP AS CAPIL_2
    	 				, A.MATI_TGL_LAHIR AS CAPIL_3
    	 				, A.MATI_TGL_MATI AS CAPIL_4
    	 				, A.PLPR_TGL_LAPOR AS CAPIL_5 ";
			$is_today = 1;
			$r = $this->dtl->get_detail_capil($tbl_name,$date_col,$is_today,$get_col);
			$data = array(
				"function_id"=>'gdcmt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Kematian',
		 		"stabletitle"=>'Detail Pencatatan Kematian Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard_capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"No Mati",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Tanggal Lahir",
		 		"col4"=>"Tanggal Mati",
		 		"col5"=>"Tanggal Lapor Mati"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdcmy(){
			$menu_id = 8;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "CAPIL_MATI";
			$date_col = "PLPR_TGL_LAPOR";
			$get_col = " A.MATI_NO AS CAPIL_1
    	 				, A.MATI_NAMA_LGKP AS CAPIL_2
    	 				, A.MATI_TGL_LAHIR AS CAPIL_3
    	 				, A.MATI_TGL_MATI AS CAPIL_4
    	 				, A.PLPR_TGL_LAPOR AS CAPIL_5 ";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_capil($tbl_name,$date_col,$is_today,$get_col);
			$data = array(
				"function_id"=>'gdcmy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Kematian',
		 		"stabletitle"=>'Detail Pencatatan Kematian Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard_capil', 
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"No Mati",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Tanggal Lahir",
		 		"col4"=>"Tanggal Mati",
		 		"col5"=>"Tanggal Lapor Mati"
    		);
			$this->load->view('Temporary/index',$data);
        
	}

	public function gdsjrt(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "T5_REQ_CETAK_KTP";
			$date_col = "REQ_DATE";
			$get_col = " TO_CHAR(A.REQ_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LENGKAP AS CAPIL_4
    	 				, A.REQ_BY AS CAPIL_5 ";
    	 	$is_where = '';
			$is_today = 1;
			$r = $this->dtl->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjrt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Suket',
		 		"stabletitle"=>'Detail Jumlah Request Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Tanggal Request",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"User Request"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdsjry(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "T5_REQ_CETAK_KTP";
			$date_col = "REQ_DATE";
			$get_col = " TO_CHAR(A.REQ_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LENGKAP AS CAPIL_4
    	 				, A.REQ_BY AS CAPIL_5 ";
			$is_where = '';
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjry',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Suket',
		 		"stabletitle"=>'Detail Jumlah Request Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Tanggal Request",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"User Request"
    		);
			$this->load->view('Temporary/index',$data);
        
	}

	public function gdsjnrt(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "T7_HIST_SUKET";
			$date_col = "PRINTED_DATE";
			$get_col = " TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LGKP AS CAPIL_4
    	 				, (SELECT NAMA_KEC FROM SETUP_KEC C WHERE C.NO_PROP = 32 AND NO_KAB =73 AND C.NO_KEC = A.NO_KEC) AS CAPIL_5 ";
    	 	$is_where = " AND NOT EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AND NOT EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE,'DD/MM/YYYY'))";
			$is_today = 1;
			$r = $this->dtl->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjnrt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Cetak Non Request',
		 		"stabletitle"=>'Detail Jumlah Cetak Non Request Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Tanggal Cetak",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"Nama Kecamatan"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdsjnry(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "T7_HIST_SUKET";
			$date_col = "PRINTED_DATE";
			$get_col = " TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LGKP AS CAPIL_4
    	 				, (SELECT NAMA_KEC FROM SETUP_KEC C WHERE C.NO_PROP = 32 AND NO_KAB =73 AND C.NO_KEC = A.NO_KEC) AS CAPIL_5 ";
    	 	$is_where = " AND NOT EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) AND NOT EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjnry',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Cetak Non Request',
		 		"stabletitle"=>'Detail Jumlah Cetak Non Request Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Tanggal Cetak",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"Nama Kecamatan"
    		);
			$this->load->view('Temporary/index',$data);
        
	}

	public function gdsjdrt(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "T7_HIST_SUKET";
			$date_col = "PRINTED_DATE";
			$get_col = " TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LGKP AS CAPIL_4
    	 				, (SELECT NAMA_KEC FROM SETUP_KEC C WHERE C.NO_PROP = 32 AND NO_KAB =73 AND C.NO_KEC = A.NO_KEC) AS CAPIL_5 ";
    	 	$is_where = " AND NOT EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AND EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE,'DD/MM/YYYY'))";
			$is_today = 1;
			$r = $this->dtl->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjdrt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Cetak Non Request',
		 		"stabletitle"=>'Detail Jumlah Cetak Non Request Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Tanggal Cetak",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"Nama Kecamatan"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdsjdry(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "T7_HIST_SUKET";
			$date_col = "PRINTED_DATE";
			$get_col = " TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LGKP AS CAPIL_4
    	 				, (SELECT NAMA_KEC FROM SETUP_KEC C WHERE C.NO_PROP = 32 AND NO_KAB =73 AND C.NO_KEC = A.NO_KEC) AS CAPIL_5 ";
    	 	$is_where = " AND NOT EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AND EXISTS(SELECT 1 FROM T5_REQ_CETAK_KTP B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE,'DD/MM/YYYY'))";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjdry',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Cetak Non Request',
		 		"stabletitle"=>'Detail Jumlah Cetak Non Request Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Tanggal Cetak",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"Nama Kecamatan"
    		);
			$this->load->view('Temporary/index',$data);
        
	}

	public function gdsjct(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "T5_REQ_CETAK_KTP";
			$date_col = "REQ_DATE";
			$get_col = " TO_CHAR(A.REQ_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LENGKAP AS CAPIL_4
    	 				, A.REQ_BY AS CAPIL_5 ";
    	 	$is_where = 'AND EXISTS(SELECT 1 FROM T7_HIST_SUKET B WHERE A.NIK = B.NIK AND (B.PRINTED_DATE > SYSDATE-7 AND B.PRINTED_DATE > SYSDATE-7))';
			$is_today = 1;
			$r = $this->dtl->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjct',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Suket',
		 		"stabletitle"=>'Detail Jumlah Request Sudah Tercetak Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Tanggal Request",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"User Request"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdsjcy(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "T5_REQ_CETAK_KTP";
			$date_col = "REQ_DATE";
			$get_col = " TO_CHAR(A.REQ_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, CASE WHEN A.STEP_PROC = 'C' THEN 'STATUS CETAK SUKET BELUM PRINT' WHEN A.STEP_PROC = 'T' THEN 'STATUS DITOLAK' ELSE 'MASIH TAHAP PENGAJUAN' END  AS CAPIL_3
    	 				, A.NAMA_LENGKAP AS CAPIL_4
    	 				, A.REQ_BY AS CAPIL_5 ";
			$is_where = " AND NOT EXISTS(SELECT 1 FROM T7_HIST_SUKET B WHERE A.NIK = B.NIK AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY'))";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjcy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Suket',
		 		"stabletitle"=>'Detail Jumlah Request Sudah Tercetak Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Tanggal Request",
		 		"col2"=>"NIK",
		 		"col3"=>"Status Request",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"User Request"
    		);
			$this->load->view('Temporary/index',$data);
        
	}

	public function gdsjbt(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "T5_REQ_CETAK_KTP";
			$date_col = "REQ_DATE";
			$get_col = " TO_CHAR(A.REQ_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, CASE WHEN A.STEP_PROC = 'C' THEN 'STATUS CETAK SUKET BELUM PRINT' WHEN A.STEP_PROC = 'T' THEN 'STATUS DITOLAK' ELSE 'MASIH TAHAP PENGAJUAN' END  AS CAPIL_3
    	 				, A.NAMA_LENGKAP AS CAPIL_4
    	 				, A.REQ_BY AS CAPIL_5 ";
    	 	$is_where = " AND NOT EXISTS(SELECT 1 FROM T7_HIST_SUKET B WHERE A.NIK = B.NIK AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY'))";
			$is_today = 1;
			$r = $this->dtl->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjbt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Suket',
		 		"stabletitle"=>'Detail Jumlah Request Belum Tercetak Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Tanggal Request",
		 		"col2"=>"NIK",
		 		"col3"=>"Status Request",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"User Request"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdsjby(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "T5_REQ_CETAK_KTP";
			$date_col = "REQ_DATE";
			$get_col = " TO_CHAR(A.REQ_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LENGKAP AS CAPIL_4
    	 				, A.REQ_BY AS CAPIL_5 ";
			$is_where = 'AND NOT EXISTS(SELECT 1 FROM T7_HIST_SUKET B WHERE A.NIK = B.NIK AND (B.PRINTED_DATE > SYSDATE-7 AND B.PRINTED_DATE > SYSDATE-7))';
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjby',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Suket',
		 		"stabletitle"=>'Detail Jumlah Request Belum Tercetak Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Tanggal Request",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"User Request"
    		);
			$this->load->view('Temporary/index',$data);
        
	}

	public function gdrsbiot(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'BIO_CAPTURED'";
			$is_today = 1;
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsbiot',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Bio Captured Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdrsbioy(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'BIO_CAPTURED'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsbioy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Bio Captured Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			$this->load->view('Temporary/index',$data);
        
	}

	public function gdrsproct(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'PROCESSING'";
			$is_today = 1;
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsproct',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Processing Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdrsprocy(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'PROCESSING'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsprocy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Processing Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			$this->load->view('Temporary/index',$data);
        
	}

	public function gdrsadjt(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'ADJUDICATE_RECORD'";
			$is_today = 1;
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsadjt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Adjudicate Record Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdrsadjy(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'ADJUDICATE_RECORD'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsadjy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Adjudicate Record Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			$this->load->view('Temporary/index',$data);
        
	}

	public function gdrsefact(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'ENROLL_FAILURE_AT_CENTRAL'";
			$is_today = 1;
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsefact',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Enroll Failure At Central Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdrsefacy(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'ENROLL_FAILURE_AT_CENTRAL'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsefacy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Enroll Failure At Central Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			$this->load->view('Temporary/index',$data);
        
	}
	public function gdrssfet(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT'";
			$is_today = 1;
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrssfet',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Sent For Enrollment Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdrssfey(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrssfey',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Sent For Enrollment Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			$this->load->view('Temporary/index',$data);
        
	}
	public function gdrsprrt(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD'";
			$is_today = 1;
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsprrt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Print Ready Record Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdrsprry(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsprry',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Print Ready Record Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			$this->load->view('Temporary/index',$data);
        
	}
	public function gdrscapt(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, 'CARD_PRINTED' AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE <> 'SENT_FOR_ENROLLMENT' AND A.CURRENT_STATUS_CODE <> 'BIO_CAPTURED' AND A.CURRENT_STATUS_CODE <> 'DUPLICATE_RECORD' AND A.CURRENT_STATUS_CODE <> 'PRINT_READY_RECORD' AND A.CURRENT_STATUS_CODE <> 'PROCESSING' AND A.CURRENT_STATUS_CODE <> 'ADJUDICATE_RECORD' AND A.CURRENT_STATUS_CODE <> 'ENROLL_FAILURE_AT_CENTRAL' ";
			$is_today = 1;
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrscapt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Card Printed Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdrscapy(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, 'CARD_PRINTED' AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE <> 'SENT_FOR_ENROLLMENT' AND A.CURRENT_STATUS_CODE <> 'BIO_CAPTURED' AND A.CURRENT_STATUS_CODE <> 'DUPLICATE_RECORD' AND A.CURRENT_STATUS_CODE <> 'PRINT_READY_RECORD' AND A.CURRENT_STATUS_CODE <> 'PROCESSING' AND A.CURRENT_STATUS_CODE <> 'ADJUDICATE_RECORD' AND A.CURRENT_STATUS_CODE <> 'ENROLL_FAILURE_AT_CENTRAL' ";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrscapy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Card Printed Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			$this->load->view('Temporary/index',$data);
        
	}
	public function gdrsdurect(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'DUPLICATE_RECORD'";
			$is_today = 1;
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsdurect',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Duplicate Record Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);

			$this->load->view('Temporary/index',$data);
        
	}

	public function gdrsdurecy(){
			$menu_id = 5;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, A.CREATED AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'DUPLICATE_RECORD'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->dtl->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsdurecy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Duplicate Record Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard_ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			$this->load->view('Temporary/index',$data);
        
	}
	
}
