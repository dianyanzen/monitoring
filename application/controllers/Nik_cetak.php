<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nik_cetak extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Search','scr');
		$this->load->model('M_Shared','shr');		
    if ($this->session->userdata(S_SESSION_ID) == null) 
    {
      redirect('/','refresh');
    } else {
      $is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
      if ($is_log == 0){
        if ($this->session->userdata(S_SESSION_ID) != null) {
        $this->shr->stop_activity($this->session->userdata(S_USER_ID));
        }
        $this->session->sess_destroy();
        redirect('/','refresh');
      }
    }
	}
	public function index()
	{
		  $menu_id = 75;
        $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
        if ($is_akses == 0){
          redirect('404Notfound','refresh');
        }
      $isakses_kec = $this->shr->get_give_kec();
      $isakses_kel = $this->shr->get_give_kel();
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('nik') != null || $this->input->post('no_kk') != null || $this->input->post('nama_lgkp') != null || $this->input->post('tgl_lhr') != null){
			$nik = $this->input->post('nik');
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
      if (substr($nik, -1, 1) === ','){
        $nik = rtrim($nik, ',');
      }
			$no_kk = $this->input->post('no_kk');
			if (substr($no_kk, 0, 1) === ','){
				$no_kk = ltrim($no_kk, ',');
			}
      if (substr($no_kk, -1, 1) === ','){
        $no_kk = rtrim($no_kk, ',');
      }
      
			$nama_lgkp = $this->input->post('nama_lgkp');
      $tgl_lhr = $this->input->post('tgl_lhr');
      $no_kec = $this->input->post('no_kec');
      $no_kel = $this->input->post('no_kel');
			$r = $this->scr->cek_data_rekam($nik,$no_kk,$nama_lgkp,$tgl_lhr,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Ktp-el Cek Status',
		 		"mtitle"=>'Ktp-el Cek Status',
		 		"my_url"=>'Ktpel',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>0,
		 		"menu"=>$menu,
            "akses_kec"=>$isakses_kec,
            "akses_kel"=>$isakses_kel,
        "akses_kec"=>$isakses_kec,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Ktp-el Cek Status',
		 		"mtitle"=>'Ktp-el Cek Status',
		 		"my_url"=>'Ktpel',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
            "akses_kec"=>$isakses_kec,
            "akses_kel"=>$isakses_kel,
        "akses_kec"=>$isakses_kec,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('ceknik/index',$data);
	}
	public function export() {
		// create file name
		if($this->input->post('niks') != null){
			$nik = $this->input->post('niks');
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
        $fileName = strtoupper($this->session->userdata(S_USER_ID)).'_DATA_REKAM_'.time().'.xlsx';  
		// load excel library
        $this->load->library('excel');
        $data_rekam =  $this->scr->export_data_rekam($nik );
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $style_aligment = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        )
    	);
    	$style_color = array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '01B0F1')
	        )
    	);
		
    	$objPHPExcel->getActiveSheet()->getStyle("A1:G5")->applyFromArray($style_aligment);
    	$objPHPExcel->getActiveSheet()->getStyle("A9:S9")->applyFromArray($style_color);
        $objPHPExcel->getActiveSheet()->setTitle('Data Status Ektp');
        // set Header
        $objPHPExcel->getActiveSheet()->mergeCells('B1:G1');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', "DAFTAR STATUS EKTP ".strtoupper($this->session->userdata(S_USER_ID)));

		$objPHPExcel->getActiveSheet()->mergeCells('B2:G2');
		$objPHPExcel->getActiveSheet()->setCellValue('B2', "DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL");

		$objPHPExcel->getActiveSheet()->mergeCells('B3:G3');
		$objPHPExcel->getActiveSheet()->setCellValue('B3', "".ucwords(strtolower($this->session->userdata(S_NM_KAB)))."");

		$objPHPExcel->getActiveSheet()->mergeCells('B4:G4');
		$objPHPExcel->getActiveSheet()->setCellValue('B4', "-YZ-");

		$objPHPExcel->getActiveSheet()->getStyle('B1:G5')->getFont()->setName('Arial');
		$objPHPExcel->getActiveSheet()->getStyle('B1:G5')->getFont()->setSize(14);
		$objPHPExcel->getActiveSheet()->getStyle('B1:G5')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A9:S9')->getFont()->setBold(true);

		// untuk sub judul
		$objPHPExcel->getActiveSheet()->setCellValue('A6', "Kab/Kota : ".ucwords(strtolower($this->session->userdata(S_NM_KAB))));
		$objPHPExcel->getActiveSheet()->setCellValue('A7', "Provinsi : Jawa Barat");
	
		$objPHPExcel->getActiveSheet()->getStyle('A6:I7')->getFont()->setName('Arial');
		$objPHPExcel->getActiveSheet()->getStyle('A6:I7')->getFont()->setSize(9);
		
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo_disduk');
		$objDrawing->setDescription('Logo_disduk');
		// $logo = base_path() . '/images/logo.png';
		$objDrawing->setPath('assets/plugins/images/pemkot.png');
		$objDrawing->setCoordinates('A1');
		$objDrawing->setOffsetX(30); 
		$objDrawing->setOffsetY(10); 
		$objDrawing->setHeight(100);
		$objDrawing->setWidth(100);
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);


        $objPHPExcel->getActiveSheet()->SetCellValue('A9', 'NIK');
        $objPHPExcel->getActiveSheet()->SetCellValue('B9', 'NO KK');
        $objPHPExcel->getActiveSheet()->SetCellValue('C9', 'NAMA LENGKAP');
        $objPHPExcel->getActiveSheet()->SetCellValue('D9', 'STATUS KTP');
        $objPHPExcel->getActiveSheet()->SetCellValue('E9', 'PETUGAS CETAK KTP');       
        $objPHPExcel->getActiveSheet()->SetCellValue('F9', 'TANGGAL CETAK KTP');       
        $objPHPExcel->getActiveSheet()->SetCellValue('G9', 'PETUGAS CETAK SUKET');       
        $objPHPExcel->getActiveSheet()->SetCellValue('H9', 'TANGGAL CETAK SUKET');       
        $objPHPExcel->getActiveSheet()->SetCellValue('I9', 'TEMPAT LAHIR');       
        $objPHPExcel->getActiveSheet()->SetCellValue('J9', 'TANGGAL LAHIR');       
        $objPHPExcel->getActiveSheet()->SetCellValue('K9', 'JENIS KELAMIN');       
        $objPHPExcel->getActiveSheet()->SetCellValue('L9', 'ALAMAT');       
        $objPHPExcel->getActiveSheet()->SetCellValue('M9', 'NO RT');       
        $objPHPExcel->getActiveSheet()->SetCellValue('N9', 'NO RW');       
        $objPHPExcel->getActiveSheet()->SetCellValue('O9', 'NAMA KECAMATAN');       
        $objPHPExcel->getActiveSheet()->SetCellValue('P9', 'NAMA KELURAHAN');       
        $objPHPExcel->getActiveSheet()->SetCellValue('Q9', 'AGAMA');       
        $objPHPExcel->getActiveSheet()->SetCellValue('R9', 'JENIS PEKERJAAN');       
        $objPHPExcel->getActiveSheet()->SetCellValue('S9', 'STATUS KAWIN');       
        // set Row
        $rowCount = 10;
        foreach ($data_rekam as $val) 
        {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, ','.$val->NIK);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, ','.$val->NO_KK);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $val->NAMA_LGKP);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $val->CURRENT_STATUS_CODE);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $val->KTP_BY);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $val->KTP_DT);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $val->SUKET_BY);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $val->SUKET_DT);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $val->TMPT_LHR);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $val->TGL_LHR);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $val->JENIS_KLMIN);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $val->ALAMAT);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $val->RT);
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $val->RW);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $val->NAMA_KEC);
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $val->NAMA_KEL);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $val->AGAMA);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $val->JENIS_PKRJN);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $val->STAT_KWN);
            $rowCount++;
        }
        $endrow = $rowCount-1;
        $detailrow = $rowCount+1;
        $objPHPExcel->getActiveSheet()->mergeCells('A'.$detailrow.':G'.$detailrow);
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$detailrow, "Sumber : Data Pelayanan Tanggal ".date('d-m-Y')." Dari Aplikasi Monitoring Dinas Kependudukan Dan Pencatatan Sipil ".ucwords(strtolower($this->session->userdata(S_NM_KAB))).", Copyright © ".date('Y')." DianYanzen (PIAK)");
 		$styleborder = array(
		      'borders' => array(
		          'allborders' => array(
		              'style' => PHPExcel_Style_Border::BORDER_THIN
		          )
		      )
		  );
		$objPHPExcel->getActiveSheet()->getStyle("A9:S". $endrow)->applyFromArray($styleborder);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save('excel/'.$fileName);
		// download file
        header("Content-Type: application/vnd.ms-excel");
         redirect(site_url().'excel/'.$fileName); 
         } 
        else 
        {
           redirect('/','refresh');
        }             
    }
    
	public function pdf() {
		if($this->input->post('niks') != null){
			$nik = $this->input->post('niks');
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
        $fileName = strtoupper($this->session->userdata(S_USER_ID)).'_DATA_REKAM_'.time().'.xlsx';  
		$this->load->library('pdf');
		$data_rekam =  $this->scr->export_data_rekam($nik );
        // set document information
        $this->pdf->SetCreator('Dian Yanzen');
		$this->pdf->SetAuthor('Dian Yanzen');
		$this->pdf->SetTitle('PDF Monitoring');
        $this->pdf->SetSubject('YZ PDF');
        $this->pdf->SetKeywords('YZ, PDF, Monitoring, DisdukCapil, Kota Bandung');
		// set auto page breaks
		$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		

// ---------------------------------------------------------

// set font

        // set default header data
        $this->pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
		$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // set font
        $this->pdf->SetFont('times', '', 12);
        
        // add a page
        foreach ($data_rekam as $val) 
        {
        $this->pdf->AddPage();
        // new style
$style = array('border' => 0, 'vpadding' => 'auto', 'hpadding' => 'auto', 'fgcolor' => array(0, 0, 0), 'bgcolor' => false, 'module_width' => 1, 'module_height' => 1);
// QRCODE,H : QR-CODE Best error correction

        
        // print a line using Cell()
        // $this->pdf->Cell(0, 12, strtoupper($this->session->userdata(S_USER_ID)).'_DATA_REKAM_'.time(), 0, 1, 'C');
        $html = '
<!-- EXAMPLE OF CSS STYLE -->
<style>

    table.first {
    	padding: 0px;    
    	margin: 0px;    
        font-family: helvetica;
        font-size: 8pt;
        
    }
</style>
<table class="first" cellpadding="0" cellspacing="5" border="0">
 <tr>
  <td width="50" align="left">Nomor</td>
  <td width="20" align="center">:</td>
  <td width="140" align="left">'.time().'-Disdukcapil</td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="220" align="left">Bandung,'.date('d-m-Y').'</td>
 </tr>
 <tr>
  <td width="50" align="left">Lampiran</td>
  <td width="20" align="center">:</td>
  <td width="140" align="left">-</td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="220" align="left">Kepada</td>
 </tr>
 <tr>
  <td width="50" align="left">Perihal</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left">Resi Pengajuan Ktp-El</td>
  <td width="10" align="center"></td>
  <td width="60" align="right">Yth.</td>
  <td width="20" align="center">:</td>
  <td width="220" align="left"><B>'.$val->NAMA_LGKP.'</B></td>
 </tr>
</table>
<br>
<br>
<br>
<table class="first" cellpadding="0" cellspacing="5" border="0">
 <tr>
  <td colspan = "7" align="center"><h2>RESI PENGAJUAN PENCETAKAN E-KTP</h2></td>
 </tr>
 <tr>
  <td colspan = "7" align="center"><br></td>
 </tr>
 <tr>
  <td width="120" align="left">NIK</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->NIK.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center" colspan ="3"></td>
  
 </tr>
 <tr>
  <td width="120" align="left">Nama Lengkap</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->NAMA_LGKP.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">Tempat/Tanggal Lahir</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->TMPT_LHR.', '.$val->TGL_LHR.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">Jenis Kelamin</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->JENIS_KLMIN.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">Alamat</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->ALAMAT.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">&#160;&#160;&#160;&#160;&#160;&#160;RT/RW</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->RT.'/'.$val->RW.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">&#160;&#160;&#160;&#160;&#160;&#160;Kelurahan</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->NAMA_KEL.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">&#160;&#160;&#160;&#160;&#160;&#160;Kecamatan</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->NAMA_KEC.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">Status E-Ktp</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->CURRENT_STATUS_CODE.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">Tanggal Pengajuan E-Ktp</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->REQ_DATE.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">Diajukan E-Ktp Oleh</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->REQ_BY.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
</table>
<br>
<br>
<br>
<table class="first" cellpadding="0" cellspacing="5" border="0">
 <tr>
  <td colspan = "7" align="left"><p>Keterangan Pengajuan Pencetakan E-ktp Ini Dikeluarkan Oleh '.$this->session->userdata(S_NAMA_LGKP).' Selaku Operator Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung Di-'.$this->session->userdata(S_NAMA_KANTOR).', Melalui Aplikasi Monitoring Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p><br><p>Resi Ini Kami Buat Sebagai Bukti Bahwa Telah Mengajukan Pencetakan KTP-el, Dan <b>Tidak Dapat</b> Dipergunakan Untuk Kepentingan Pemilu, Pemilukada, Pilkades, Perbankan, Imigrasi, Kepolisian, Asuransi, BPJS, Pernikahan, dan lain-lain. Kepada yang berkepentingan agar menjadi maklum.</p></td>
 </tr>
</table>
<table class="first" cellpadding="0" cellspacing="5" border="0">
	<tr>
			<td colspan="3" height="33"></td>
	</tr>
        <tr>
            <td width="30%"></td>	
            <td width="25%"></td>											
            <td width="45%" align="center">KOTA BANDUNG, '.date('d-m-Y').'</td>
		</tr>
		<tr>
          <td></td>
          <td></td>
          <td align="center">Dikeluarkan Oleh : </td>
        </tr>
        <tr>

          <td></td>
          
			  <td rowspan="5" align="center" valign="bottom">
			   
			          
		</td>
          <td align="center">a.n. KEPALA DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL</td>
        </tr>
        <tr>
          <td height="60" valign="top" align="center">Pemohon</td>
          <td height="60" valign="top" align="center">Kepala Bidang Pelayanan Pendaftaran Penduduk</td>
        </tr>        
        <tr>
            <td height="20"></td>
            <td height="20"></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="center"><u>'.$val->NAMA_LGKP.'</u></td>
            <td align="center"><u>Hj. SITTI WAHYUNI, SH.</u></td>
        </tr>
        <tr style="line-height:7px">
            <td align="center">NIK. '.$val->NIK.'</td>
            <td></td>
            <td align="center">NIP. 196205261986032006</td>
        </tr>
    </table>
';

// output the HTML content
$filename = 'assets/upload/pp/'.$this->session->userdata(S_NIK).'.jpg';
if (file_exists($filename)) {
$mask = $this->pdf->Image(K_PATH_IMAGES.'example.png', 50, 140, 100, '', '', '', '', false, 300, '', true);
$image_file_bg = K_PATH_IMAGES.'example.png';
$this->pdf->Image($image_file_bg, 20, 50, 160, '', '', '', '', false, false, '', false, $mask);
} else {
$mask = $this->pdf->Image(K_PATH_IMAGES.'example.png', 50, 140, 100, '', '', '', '', false, 300, '', true);
$image_file_bg = K_PATH_IMAGES.'example.png';
$this->pdf->Image($image_file_bg, 20, 50, 160, '', '', '', '', false, false, '', false, $mask);
}	
$this->pdf->writeHTML($html, true, false, true, false, '');
$this->pdf->write2DBarcode($val->NIK, 'QRCODE,Q',150 ,80,25,25, $style, 'N');


// embed image, masked with previously embedded mask

// $this->pdf->Image(base_url('assets/upload/pp/'.$this->session->userdata(S_NIK).'.jpg'), 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);

// $this->pdf->Image($image_file_bg, 10, 120, 180, '', '', '', '', false, 300, '', false, 300);
}
        
        //Close and output PDF document
        $this->pdf->Output(strtoupper($this->session->userdata(S_USER_ID)).'_DATA_REKAM_'.time().'.pdf', 'I');
         } 
        else 
        {
           redirect('/','refresh');
        }
    }

	
	public function do_push()
	{
		  $isakses_kec = $this->shr->get_give_kec();
      $isakses_kel = $this->shr->get_give_kel();
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			$this->scr->push_demogh221($nik);
			$this->scr->push_demogh2($nik);
			$this->scr->push_demoghall($nik);
			$r = $this->scr->cek_data_rekam($nik);
			$j = $this->scr->cek_count_rekam($nik);
			$data = array(
		 		"stitle"=>'Re-Push Status Ktp-el',
		 		"mtitle"=>'Re-Push Status Ktp-el',
		 		"my_url"=>'Push',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
            "akses_kec"=>$isakses_kec,
            "akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Re-Push Status Ktp-el',
		 		"mtitle"=>'Re-Push Status Ktp-el',
		 		"my_url"=>'Push',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
            "akses_kec"=>$isakses_kec,
            "akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('ceknik/index',$data);
	}
}