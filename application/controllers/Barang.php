<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Search','scr');
		$this->load->model('M_Shared','shr');
		$this->load->model('M_Barang','brg');
		if ($this->session->userdata(S_SESSION_ID) == null) 
	    {
	      redirect('/','refresh');
	    } else {
	      $is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
	      if ($is_log == 0){
	        if ($this->session->userdata(S_SESSION_ID) != null) {
	        $this->shr->stop_activity($this->session->userdata(S_USER_ID));
	        }
	        $this->session->sess_destroy();
	        redirect('/','refresh');
	      }
	    }		
	}
	public function index()
	{
           redirect('/','refresh');
	}
	public function master_barang() 
	{
			$menu_id = 167;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Master Barang',
		 		"mtitle"=>'Master Barang',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Master_barang/index',$data);
	}
	public function master_aset() 
	{
			$menu_id = 181;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Master Aset',
		 		"mtitle"=>'Master Aset',
		 		"my_url"=>'MasterAset',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Master_aset/index',$data);
	}
	public function Master_aset_in() 
	{
			$menu_id = 152;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Input Aset',
		 		"mtitle"=>'Input Aset',
		 		"my_url"=>'InputAset',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Master_aset_in/index',$data);
	}
	public function Status_barang() 
	{
			$menu_id = 150;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Status Barang',
		 		"mtitle"=>'Status Barang',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Master_barang_status/index',$data);
	}
	public function Laporan_barang() 
	{
			$menu_id = 150;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Laporan Barang',
		 		"mtitle"=>'Laporan Barang',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Master_barang_laporan/index',$data);
	}
	public function edit_master_barang($id) 
	{
			$menu_id = 167;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$barang_id = $id;
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
		    $databarang = $this->brg->get_data_barang($barang_id);
			$data = array(
		 		"stitle"=>'Master Barang',
		 		"mtitle"=>'Master Barang',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$databarang,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Master_barang_edit/index',$data);
	}
	public function edit_master_barang_in($id) 
	{
			$menu_id = 167;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$barang_id = $id;
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
		    $databarang = $this->brg->get_data_barang_in($barang_id);
			$data = array(
		 		"stitle"=>'Barang Masuk',
		 		"mtitle"=>'Barang Masuk',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$databarang,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Master_barang_edit_in/index',$data);
	}
	public function edit_master_barang_out($id) 
	{
			$menu_id = 167;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$barang_id = $id;
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
		    $databarang = $this->brg->get_data_barang_out($barang_id);
			$data = array(
		 		"stitle"=>'Barang Keluar',
		 		"mtitle"=>'Barang Keluar',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$databarang,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Master_barang_edit_out/index',$data);
	}
	public function get_master_barang()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $user_data = $this->brg->get_master_barang();

          $data = array();
          $i = 0;
          foreach($user_data->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
               		$r->JENIS_BARANG,
                    $r->SATUAN,
                    'Rp.'.number_format($r->HARGA),
                    '<button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->BARANG_ID.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->BARANG_ID.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $user_data->num_rows(),
                 "recordsFiltered" => $user_data->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
	}
	public function get_master_aset()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $kategori = $this->input->post('kategori');
          $user_data = $this->brg->get_master_aset($kategori);

          $data = array();
          $i = 0;
          foreach($user_data->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->DESCRIP,
                    '<button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit(\''.$r->NO.'\',\''.$r->SECT.'\');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus(\''.$r->NO.'\',\''.$r->SECT.'\');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $user_data->num_rows(),
                 "recordsFiltered" => $user_data->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
	}
	public function get_master_barang_laporan()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $barang = $this->input->post('barang');
          $tgl = $this->input->post('tanggal');
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $user_data = $this->brg->get_master_barang_laporan($tgl_start,$tgl_end,$barang);

          $data = array();
          $i = 0;
          foreach($user_data->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
               		$r->JENIS_BARANG,
                    $r->JUMLAH,
                    'Rp.'.number_format($r->HARGA),
                    $r->TANGGAL,
                    $r->RECEIVED,
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $user_data->num_rows(),
                 "recordsFiltered" => $user_data->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
	}
	public function get_master_barang_status()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $user_data = $this->brg->get_master_barang_status();

          $data = array();
          $i = 0;
          foreach($user_data->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
               		$r->JENIS_BARANG,
                    $r->SATUAN,
                    'Rp.'.number_format($r->HARGA),
                    $r->JML_MASUK,
                    $r->JML_KELUAR,
                    $r->SALDO,
                    'Rp.'.number_format($r->HARGA_ASET)
                    
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $user_data->num_rows(),
                 "recordsFiltered" => $user_data->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
	}
	public function get_master_barang_in()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $user_data = $this->brg->get_master_barang_in();

          $data = array();
          $i = 0;
          foreach($user_data->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
               		$r->JENIS_BARANG,
                    $r->JUMLAH,
                    'Rp.'.number_format($r->HARGA),
                    $r->TANGGAL,
                    $r->RECEIVED,
                    '<button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->SEQ_ID.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->SEQ_ID.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $user_data->num_rows(),
                 "recordsFiltered" => $user_data->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
	}
	public function get_master_barang_out()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $user_data = $this->brg->get_master_barang_out();

          $data = array();
          $i = 0;
          foreach($user_data->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
               		$r->JENIS_BARANG,
                    $r->JUMLAH,
                    'Rp.'.number_format($r->HARGA),
                    $r->TANGGAL,
                    $r->RECEIVED,
                    '<button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->SEQ_ID.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->SEQ_ID.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $user_data->num_rows(),
                 "recordsFiltered" => $user_data->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
	}
	public function insert_master_aset() 
	{
		if($this->input->post('kd_kategori') != null){
			$kd_kategori = $this->input->post('kd_kategori');
			$value_data = strtoupper($this->input->post('value_data'));
			$j = $this->brg->max_aset($kd_kategori);
			$this->brg->save_aset($j,$kd_kategori,$value_data);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function get_modal_data() 
	{
		if($this->input->post('aset_no') != null){
			$aset_no = $this->input->post('aset_no');
			$aset_sect = $this->input->post('aset_sect');
			$r = $this->brg->aset_select_data($aset_no,$aset_sect);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
			$data["value_data"] = $r;
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function insert_master_barang() 
	{
		if($this->input->post('jenis_barang') != null){
			$jenis_barang = $this->input->post('jenis_barang');
			$satuan = $this->input->post('satuan');
			$harga_satuan = $this->input->post('harga_satuan');
			$keterangan = $this->input->post('keterangan');
			$j = $this->brg->max_barang();
			$this->brg->save_barang($j,$jenis_barang,$satuan,$harga_satuan,$keterangan,$this->session->userdata(S_USER_ID));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function insert_master_in_barang() 
	{
		if($this->input->post('kd_brg') != null){
			$kd_brg = $this->input->post('kd_brg');
			$received = $this->input->post('received');
			$jumlah = $this->input->post('jumlah');
			$keterangan = $this->input->post('keterangan');
			$j = $this->brg->max_barang_in();
			$this->brg->save_barang_in($j,$kd_brg,$received,$jumlah,$keterangan,$this->session->userdata(S_USER_ID));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function insert_master_out_barang() 
	{
		if($this->input->post('kd_brg') != null){
			$kd_brg = $this->input->post('kd_brg');
			$received = $this->input->post('received');
			$jumlah = $this->input->post('jumlah');
			$keterangan = $this->input->post('keterangan');
			$j = $this->brg->max_barang_out();
			$this->brg->save_barang_out($j,$kd_brg,$received,$jumlah,$keterangan,$this->session->userdata(S_USER_ID));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function do_edit_master_aset() 
	{
		if($this->input->post('mdl_no_data') != null){
			$mdl_no_data = $this->input->post('mdl_no_data');
			$mdl_value_data = strtoupper($this->input->post('mdl_value_data'));
			$mdl_sect_data = $this->input->post('mdl_sect_data');
			$this->brg->edit_aset($mdl_no_data,$mdl_value_data,$mdl_sect_data);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Ubah";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function do_edit_master_barang() 
	{
		if($this->input->post('kode_barang') != null){
			$kode_barang = $this->input->post('kode_barang');
			$jenis_barang = $this->input->post('jenis_barang');
			$satuan = $this->input->post('satuan');
			$harga_satuan = $this->input->post('harga_satuan');
			$keterangan = $this->input->post('keterangan');
			$this->brg->edit_barang($kode_barang,$jenis_barang,$satuan,$harga_satuan,$keterangan,$this->session->userdata(S_USER_ID));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Ubah";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function do_edit_master_barang_in() 
	{
		if($this->input->post('kode_barang') != null){
			$kode_barang = $this->input->post('kode_barang');
			$kd_brg = $this->input->post('kd_brg');
			$received = $this->input->post('received');
			$jumlah = $this->input->post('jumlah');
			$keterangan = $this->input->post('keterangan');
			$this->brg->edit_barang_in($kode_barang,$kd_brg,$received,$jumlah,$keterangan,$this->session->userdata(S_USER_ID));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Ubah";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function do_edit_master_barang_out() 
	{
		if($this->input->post('kode_barang') != null){
			$kode_barang = $this->input->post('kode_barang');
			$kd_brg = $this->input->post('kd_brg');
			$received = $this->input->post('received');
			$jumlah = $this->input->post('jumlah');
			$keterangan = $this->input->post('keterangan');
			$this->brg->edit_barang_out($kode_barang,$kd_brg,$received,$jumlah,$keterangan,$this->session->userdata(S_USER_ID));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Ubah";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function delete_master_barang() 
	{
		if($this->input->post('barang') != null){
			$barang = $this->input->post('barang');
			$this->brg->delete_master_barang($barang);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function delete_master_aset() 
	{
		if($this->input->post('aset_no') != null){
			$aset_no = $this->input->post('aset_no');
			$aset_sect = $this->input->post('aset_sect');
			$this->brg->delete_master_aset($aset_no,$aset_sect);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function delete_master_in_barang() 
	{
		if($this->input->post('barang') != null){
			$barang = $this->input->post('barang');
			$this->brg->delete_master_in_barang($barang);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function delete_master_out_barang() 
	{
		if($this->input->post('barang') != null){
			$barang = $this->input->post('barang');
			$this->brg->delete_master_out_barang($barang);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function get_barang()
	{
		header("Content-Type: application/json", true);
		$r = $this->brg->get_master_barang()->result();
		echo json_encode($r);
	}
	public function master_barang_in() 
	{
			$menu_id = 149;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Input Barang Masuk',
		 		"mtitle"=>'Input Barang Masuk',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('master_barang_in/index',$data);
	}
	public function master_barang_out() 
	{
			$menu_id = 165;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Input Barang Keluar',
		 		"mtitle"=>'Input Barang Keluar',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('master_barang_out/index',$data);
	}
}