<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blangko extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Search','scr');
		$this->load->model('M_Shared','shr');
		$this->load->model('M_Blangko','blk');	
		if ($this->session->userdata(S_SESSION_ID) == null) 
	    {
	      redirect('/','refresh');
	    } else {
	      $is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
	      if ($is_log == 0){
	        if ($this->session->userdata(S_SESSION_ID) != null) {
	        $this->shr->stop_activity($this->session->userdata(S_USER_ID));
	        }
	        $this->session->sess_destroy();
	        redirect('/','refresh');
	      }
	    }		
	}
	public function index()
	{
           redirect('/','refresh');
	}
	public function blangko_out() 
	{
			$menu_id = 98;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Blangko KTP Keluar',
		 		"mtitle"=>'Kendali Blangko KTP Keluar',
		 		"my_url"=>'blangko_out',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Blangko_out/index',$data);
	}
	public function blangko_in() 
	{
			$menu_id = 97;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Blangko KTP Masuk',
		 		"mtitle"=>'Kendali Blangko KTP Masuk',
		 		"my_url"=>'blangko_in',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Blangko_in/index',$data);
	}
	public function blangko_rekap() 
	{
			$menu_id = 99;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			if($this->input->post('tanggal') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->blk->cek_rekap_blangko($tgl_start,$tgl_end);
			$j = $this->blk->cek_rekap_count_blangko($tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Rekap Blangko KTP Keluar',
		 		"mtitle"=>'Rekap Blangko KTP Keluar',
		 		"my_url"=>'blangko_rekap',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekap Blangko KTP Keluar',
		 		"mtitle"=>'Rekap Blangko KTP Keluar',
		 		"my_url"=>'blangko_rekap',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('Blangko_rekap/index',$data);
	}
	public function blangko_kk_out() 
	{
			$menu_id = 0;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Blangko KK Keluar',
		 		"mtitle"=>'Kendali Blangko KK Keluar',
		 		"my_url"=>'blangko_out',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Blangko_kk_out/index',$data);
	}
	public function blangko_kk_in() 
	{
			$menu_id = 0;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Blangko KK Masuk',
		 		"mtitle"=>'Kendali Blangko KK Masuk',
		 		"my_url"=>'blangko_in',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Blangko_kk_in/index',$data);
	}

	public function do_save() 
	{
		if($this->input->post('no_kec') != null){
			$no_kec = (int)$this->input->post('no_kec');
			$tgl = $this->input->post('tanggal');
			$pengambilan = (int)$this->input->post('pengambilan');
			$rusak = (int)$this->input->post('rusak');
			$pengembalian = (int)$this->input->post('pengembalian');
			$j = $this->blk->get_count_blangko($no_kec,$tgl);
			if($j > 0){
				$this->blk->update_blangko($no_kec,$tgl,$pengambilan,$rusak,$pengembalian);
				$data["success"] = TRUE;
				$data["is_save"] = 0;
        		$data["message"] = "Data Berhasil Di Update";
        		echo json_encode($data);
			}else{
				$this->blk->save_blangko($no_kec,$tgl,$pengambilan,$rusak,$pengembalian);
				$data["success"] = TRUE;
				$data["is_save"] = 1;
        		$data["message"] = "Data Berhasil Di Simpan";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}

	public function do_save_kk() 
	{
		if($this->input->post('no_kec') != null){
			$no_kec = (int)$this->input->post('no_kec');
			$tgl = $this->input->post('tanggal');
			$pengambilan = (int)$this->input->post('pengambilan');
			$rusak = (int)$this->input->post('rusak');
			$user_id = $this->session->userdata(S_USER_ID);
			$j = $this->blk->get_count_blangko_kk($no_kec,$tgl);
			if($j > 0){
				$this->blk->update_blangko_kk($no_kec,$tgl,$pengambilan,$rusak,$user_id);
				$data["success"] = TRUE;
				$data["is_save"] = 0;
        		$data["message"] = "Data Berhasil Di Update";
        		echo json_encode($data);
			}else{
				$this->blk->save_blangko_kk($no_kec,$tgl,$pengambilan,$rusak,$user_id);
				$data["success"] = TRUE;
				$data["is_save"] = 1;
        		$data["message"] = "Data Berhasil Di Simpan";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function doin_save() 
	{
		if($this->input->post('tanggal') != null){
			$blangko_masuk = (int)$this->input->post('blangko_masuk');
			$tgl = $this->input->post('tanggal');
			$keterangan = $this->input->post('keterangan');
			$user_name = $this->input->post('user_name');
			$j = $this->blk->get_count_inblangko($tgl);
			if($j > 0){
				$this->blk->update_inblangko($blangko_masuk,$tgl,$keterangan,$user_name);
				$data["success"] = TRUE;
				$data["is_save"] = 0;
        		$data["message"] = "Data Berhasil Di Update";
        		echo json_encode($data);
			}else{
				$this->blk->save_inblangko($blangko_masuk,$tgl,$keterangan,$user_name);
				$data["success"] = TRUE;
				$data["is_save"] = 1;
        		$data["message"] = "Data Berhasil Di Simpan";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function doin_save_kk() 
	{
		if($this->input->post('tanggal') != null){
			$blangko_masuk = (int)$this->input->post('blangko_masuk');
			$tgl = $this->input->post('tanggal');
			$keterangan = $this->input->post('keterangan');
			$user_name = $this->input->post('user_name');
			$j = $this->blk->get_count_inblangko_kk($tgl);
			if($j > 0){
				$this->blk->update_inblangko_kk($blangko_masuk,$tgl,$keterangan,$user_name);
				$data["success"] = TRUE;
				$data["is_save"] = 0;
        		$data["message"] = "Data Berhasil Di Update";
        		echo json_encode($data);
			}else{
				$this->blk->save_inblangko_kk($blangko_masuk,$tgl,$keterangan,$user_name);
				$data["success"] = TRUE;
				$data["is_save"] = 1;
        		$data["message"] = "Data Berhasil Di Simpan";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function get_data() 
	{
		if($this->input->post('no_kec') != null){
			$no_kec = (int)$this->input->post('no_kec');
			$tgl = $this->input->post('tanggal');
			$j = $this->blk->get_count_blangko($no_kec,$tgl);
			if($j > 0){
				$r = $this->blk->get_blangko($no_kec,$tgl);
				$data["pengambilan"] = $r[0]->AMBIL;
				$data["rusak"] = $r[0]->RUSAK;
        		$data["pengembalian"] = $r[0]->PENGEMBALIAN;
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}else{
				$data["pengambilan"] = 0;
				$data["rusak"] = 0;
        		$data["pengembalian"] = 0;
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function get_kk_data() 
	{
		if($this->input->post('no_kec') != null){
			$no_kec = (int)$this->input->post('no_kec');
			$tgl = $this->input->post('tanggal');
			$j = $this->blk->get_count_blangko_kk($no_kec,$tgl);
			if($j > 0){
				$r = $this->blk->get_blangko_kk($no_kec,$tgl);
				$data["pengambilan"] = $r[0]->AMBIL;
				$data["rusak"] = $r[0]->RUSAK;
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}else{
				$data["pengambilan"] = 0;
				$data["rusak"] = 0;
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function get_all_data() 
	{
		if($this->input->post('tanggal') != null){
			$tgl = $this->input->post('tanggal');
			$j = $this->blk->get_count_all_blangko($tgl);
			if($j > 0){
				$r = $this->blk->get_all_blangko($tgl);
				$data["pengambilan"] = $r[0]->AMBIL;
				$data["rusak"] = $r[0]->RUSAK;
        		$data["pengembalian"] = $r[0]->PENGEMBALIAN;
        		$data["total"] = $r[0]->TOTAL;
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}else{
				$data["pengambilan"] = 0;
				$data["rusak"] = 0;
        		$data["pengembalian"] = 0;
        		$data["total"] = 0;
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function get_all_kk_data() 
	{
		if($this->input->post('tanggal') != null){
			$tgl = $this->input->post('tanggal');
			$no_kec = (int)$this->input->post('no_kec');
			$j = $this->blk->get_count_all_blangko_kk($no_kec,$tgl);
			if($j > 0){
				$r = $this->blk->get_all_blangko_kk($no_kec,$tgl);
				$data["pengambilan"] = $r[0]->AMBIL;
				$data["rusak"] = $r[0]->RUSAK;
        		$data["total"] = $r[0]->TOTAL;
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}else{
				$data["pengambilan"] = 0;
				$data["rusak"] = 0;
        		$data["total"] = 0;
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function get_all_kk_saldo() 
	{
		if($this->input->post('no_kec') != null){
			$no_kec = (int)$this->input->post('no_kec');
			$j = $this->blk->get_count_saldo_blangko_kk($no_kec);
			if($j > 0){
				$r = $this->blk->get_saldo_blangko_kk($no_kec);
				$data["saldo"] = $r[0]->SALDO;
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}else{
        		$data["saldo"] = 0;
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
		
	}
	public function get_allin_data() 
	{
		if($this->input->post('tanggal') != null){
			$tgl = $this->input->post('tanggal');
				$r = $this->blk->get_allin_blangko();
				$data["masuk"] = $r[0]->MASUK;
				$data["keluar"] = $r[0]->KELUAR;
        		$data["rusak"] = $r[0]->RUSAK;
        		$data["sisa"] = $r[0]->SISA;
        		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
	public function get_allin_kk_data() 
	{
		if($this->input->post('tanggal') != null){
			$tgl = $this->input->post('tanggal');
				$r = $this->blk->get_allin_kk_blangko();
				$data["masuk"] = $r[0]->MASUK;
				$data["keluar"] = $r[0]->KELUAR;
        		$data["rusak"] = $r[0]->RUSAK;
        		$data["sisa"] = $r[0]->SISA;
        		echo json_encode($data);
		}else{
			redirect('/','refresh');
		}
	}
}