<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nik_kk extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Search','scr');
		$this->load->model('M_Shared','shr');		
    if ($this->session->userdata(S_SESSION_ID) == null) 
    {
      redirect('/','refresh');
    } else {
      $is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
      if ($is_log == 0){
        if ($this->session->userdata(S_SESSION_ID) != null) {
        $this->shr->stop_activity($this->session->userdata(S_USER_ID));
        }
        $this->session->sess_destroy();
        redirect('/','refresh');
      }
    }
	}
	public function index()
	{
			$menu_id = 129;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    $isakses_kec = $this->shr->get_give_kec();
		    $isakses_kel = $this->shr->get_give_kel();
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('no_kk') != null){
			$no_kk = $this->input->post('no_kk');
			if (substr($no_kk, 0, 1) === ','){
				$no_kk = ltrim($no_kk, ',');
			}
	        if (substr($no_kk, -1, 1) === ','){
	            $no_kk = rtrim($no_kk, ',');
	        }
			$r = $this->scr->cek_kk($no_kk,$this->session->userdata(S_NO_KEC));
			$data = array(
		 		"stitle"=>'Cek Status KK',
		 		"mtitle"=>'Cek Status KK',
		 		"my_url"=>'KK',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>0,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Cek Status KK',
		 		"mtitle"=>'Cek Status KK',
		 		"my_url"=>'KK',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('cekkk/index',$data);
	}
	
	
	public function do_push()
	{
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('no_kk') != null){
			$no_kk = $this->input->post('no_kk');
			if (substr($no_kk, 0, 1) === ','){
				$no_kk = ltrim($no_kk, ',');
			}
      $this->scr->push_data_kel($no_kk);
      $this->scr->push_data_bsre($no_kk);
			$r = $this->scr->cek_kk($no_kk);
			$data = array(
		 		"stitle"=>'Re-Push Status KK',
		 		"mtitle"=>'Re-Push Status KK',
		 		"my_url"=>'Push',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Re-Push Status KK',
		 		"mtitle"=>'Re-Push Status KK',
		 		"my_url"=>'Push',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('cekkk/index',$data);
	}
}