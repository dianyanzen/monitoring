<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Login','lgn');
		$this->load->model('M_Activity','act');
		$this->load->model('M_Shared','shr');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
				redirect('/','refresh');
			}
		}
	}
	public function Absensi()
	{
			$menu_id = 139;
			$is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
			if ($is_akses == 0){
				redirect('404Notfound','refresh');
			}
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'My Absensi',
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_kantor"=>$this->session->userdata(S_NAMA_KANTOR),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Absensi/index',$data);
		
	}
	public function Absensi_ci($user_id)
	{
			$menu_id = 139;
			$is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
			if ($is_akses == 0){
				redirect('404Notfound','refresh');
			}
			if($user_id != null){
			$user_id = base64_decode($user_id);
			$r = $this->act->report_clock_in($user_id);
			if (count($r) > 0) {
				$r = $r[0];
				$data["user_id"] = $r->USER_ID;
				$data["nama"] = $r->NAMA;
				$data["tanggal"] = $r->TANGGAL;
				$data["jam_masuk"] = $r->JAM_MASUK;
				$data["jam_keluar"] = $r->JAM_KELUAR;
				$data["keterangan"] = $r->KETERANGAN;
				$data["ip_address"] = $r->IP_ADDRESS;
				$data["is_ci"] = 1;
			}else{
				$data["user_id"] = $this->session->userdata(S_USER_ID);
				$data["nama"] = $this->session->userdata(S_NAMA_LGKP);
				$data["tanggal"] = date('d-m-Y');
				$data["jam_masuk"] = date("H:i");
				$data["jam_keluar"] = date("H:i");
				$date1 = DateTime::createFromFormat('H:i', date("H:i"));
				$date2 = DateTime::createFromFormat('H:i', "8:00");
				if ($date1 > $date2 ){
					$ket = 'Terlambat';
				}else{
					$ket = 'Tepat Waktu';		
				}	
				$data["keterangan"] = $ket;
				$data["ip_address"] = $this->session->userdata(S_IP_ADDRESS);
				$data["is_ci"] = 0;
				
			}
			
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'My Absensi',
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$data,
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_kantor"=>$this->session->userdata(S_NAMA_KANTOR),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Absensi_ci/index',$data);
			}else{
				redirect('/','refresh');
			}
		
	}
	public function Absensi_co($user_id)
	{
			$menu_id = 139;
			$is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
			if ($is_akses == 0){
				redirect('404Notfound','refresh');
			}
			if($user_id != null){
			$user_id = base64_decode($user_id);
			$r = $this->act->report_clock_in($user_id);
			if (count($r) > 0) {
				$r = $r[0];
				$data["user_id"] = $r->USER_ID;
				$data["nama"] = $r->NAMA;
				$data["tanggal"] = $r->TANGGAL;
				$data["jam_masuk"] = $r->JAM_MASUK;
				$data["jam_keluar"] = date("H:i");
				$data["keterangan"] = $r->KETERANGAN;
				$data["ip_address"] = $r->IP_ADDRESS;
				$data["is_ci"] = 1;
			}else{
				$data["user_id"] = $this->session->userdata(S_USER_ID);
				$data["nama"] = $this->session->userdata(S_NAMA_LGKP);
				$data["tanggal"] = date('d-m-Y');
				$data["jam_masuk"] = date("H:i");
				$data["jam_keluar"] = date("H:i");
				$date1 = DateTime::createFromFormat('H:i', date("H:i"));
				$date2 = DateTime::createFromFormat('H:i', "8:00");
				if ($date1 > $date2 ){
					$ket = 'Terlambat';
				}else{
					$ket = 'Tepat Waktu';		
				}	
				$data["keterangan"] = $ket;
				$data["ip_address"] = $this->session->userdata(S_IP_ADDRESS);
				$data["is_ci"] = 0;
				
			}
			
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'My Absensi',
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$data,
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_kantor"=>$this->session->userdata(S_NAMA_KANTOR),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Absensi_co/index',$data);
			}else{
				redirect('/','refresh');
			}
		
	}
	public function do_ci() 
	{
		if($this->input->post('absen_user_id') != null){
			$absen_user_id = $this->input->post('absen_user_id');
			$absen_nama = $this->input->post('absen_nama');
			$absen_tanggal = $this->input->post('absen_tanggal');
			$absen_masuk = $this->input->post('absen_masuk');
			$absen_keluar = $this->input->post('absen_keluar');
			$absen_ket = $this->input->post('absen_ket');
			$absen_ip = $this->input->post('absen_ip');
			
			$r = $this->act->report_clock_in($absen_user_id);
			$j = $this->act->report_is_absen($absen_user_id);
			if (count($j) > 0) {
				$absen_nama =  $j[0]->NAMA_LGKP;
				if (count($r) == 0) {
					$this->act->do_clockin($absen_user_id,$absen_nama,$absen_tanggal,$absen_masuk,$absen_keluar,$absen_ket,$absen_ip);
					$data["success"] = TRUE;
					$data["is_save"] = 0;
	        		$data["message"] = "$absen_user_id a Telah Berhasil Clock In.";
	        		echo json_encode($data);
				}else{
					$data["success"] = FALSE;
					$data["is_save"] = 1;
	        		$data["message"] = "Anda Sudah Clock In";
	        		echo json_encode($data);
				}
			}else{
				$data["success"] = FALSE;
				$data["is_save"] = 1;
        		$data["message"] = "Anda Tidak Memiliki Hak Akses Untuk Absen";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function do_co() 
	{
		if($this->input->post('absen_user_id') != null){
			$absen_user_id = $this->input->post('absen_user_id');
			$absen_tanggal = $this->input->post('absen_tanggal');
			$absen_keluar = $this->input->post('absen_keluar');
			$r = $this->act->report_clock_in($absen_user_id);
			$j = $this->act->report_is_absen($absen_user_id);
			if (count($j) > 0) {
				$absen_nama =  $j[0]->NAMA_LGKP;
				if (count($r) > 0) {
					$this->act->do_clockout($absen_user_id,$absen_keluar,$absen_tanggal);
					$data["success"] = TRUE;
					$data["is_save"] = 0;
	        		$data["message"] = "$absen_user_id Jam Keluar Telah Di Update, Selamat Beristirahat.";
	        		echo json_encode($data);
				}else{
					$data["success"] = FALSE;
					$data["is_save"] = 1;
	        		$data["message"] = "Anda Harus Clock In Terlebih Dahulu";
	        		echo json_encode($data);
				}
			}else{
				$data["success"] = FALSE;
				$data["is_save"] = 1;
        		$data["message"] = "Anda Tidak Memiliki Hak Akses Untuk Absen";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function rekap() 
	{
			$menu_id = 139;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Rekap My Absensi ',
		 		"mtitle"=>'Rekap My Absensi ',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Rekap_absensi/index',$data);
	}
	public function rekap_daily() 
	{
			$menu_id = 139;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Rekap My Daily Report ',
		 		"mtitle"=>'Rekap My Daily Report ',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Rekap_activity/index',$data);
	}
	public function laporan_absensi() 
	{
			$menu_id = 142;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Rekap Laporan Absensi ',
		 		"mtitle"=>'Rekap Laporan Absensi ',
		 		"my_url"=>'laporan_absensi',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('rekap_laporan_absensi/index',$data);
	}
	public function detail_absensi($id) 
	{
			$menu_id = 142;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    $user_id = base64_decode($id);
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Rekap Absensi '.$user_id,
		 		"mtitle"=>'Rekap Absensi '.$user_id,
		 		"my_url"=>'Detail_absensi',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"check_id"=>$user_id,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('rekap_laporan_absensi_detail/index',$data);
	}
	public function detail_activity($id) 
	{
			$menu_id = 142;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
		    $user_id = base64_decode($id);
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Rekap Activit '.$user_id,
		 		"mtitle"=>'Rekap Activit '.$user_id,
		 		"my_url"=>'Detail_activity',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"check_id"=>$user_id,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('rekap_laporan_activity_detail/index',$data);
	}
	public function laporan_activity() 
	{
			$menu_id = 143;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Rekap Laporan Activity ',
		 		"mtitle"=>'Rekap Laporan Activity ',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('rekap_laporan_daily/index',$data);
	}
	public function absensi_bulanan() 
	{
			$menu_id = 144;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Rekap Laporan Absensi Bulanan ',
		 		"mtitle"=>'Rekap Laporan Absensi Bulanan ',
		 		"my_url"=>'laporan_bulanan',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('rekap_bulanan_absensi/index',$data);
	}
	public function get_daily_harian() 
	{
			$menu_id = 144;
		    $is_akses = $this->shr->cek_is_akses($this->session->userdata(S_USER_LEVEL),$menu_id);
		    if ($is_akses == 0){
		      redirect('404Notfound','refresh');
		    }
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$isakses_kec = $this->shr->get_give_kec();
			$isakses_kel = $this->shr->get_give_kel();
			$data = array(
		 		"stitle"=>'Rekap Laporan Activity Harian ',
		 		"mtitle"=>'Rekap Laporan Activity Harian ',
		 		"my_url"=>'GetDaily',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('rekap_harian_activity/index',$data);
	}
	public function get_rekap_data()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $user_id = $this->input->post('user_id');
          $rekap_siak = $this->act->get_rekap_absensi($user_id);
          $data = array();
          $i = 0;
          foreach($rekap_siak->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->USER_ID,
                    $r->NAMA_LGKP,
                    $r->DAY,
                    $r->JAM_MASUK,
                    $r->JAM_KELUAR,
                    $r->KETERANGAN
                    
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $rekap_siak->num_rows(),
                 "recordsFiltered" => $rekap_siak->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}
	public function get_daily_ttd()
     {
           $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $user_id = $this->input->post('user_id');
          $tgl = $this->input->post('tanggal');
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $rekap_daily = $this->act->rekap_daily_ttd($user_id,$tgl_start,$tgl_end);
          $data = array();
          $i = 0;
          foreach($rekap_daily->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->USER_ID,
                    $r->TANGGAL,
                    $r->DESCRIPTION,
                    $r->JUMLAH.' Aktivitas'
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $rekap_daily->num_rows(),
                 "recordsFiltered" => $rekap_daily->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
      }
	public function get_rekap_bulan()
     {
          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $tgl = $this->input->post('tanggal');
          $rekap_siak = $this->act->get_rekap_absen_bulan($tgl);
          $data = array();
          $i = 0;
          foreach($rekap_siak->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->USER_ID,
                    $r->NAMA,
                    $r->BULAN,
                    $r->T1,
					$r->T2,
					$r->T3,
					$r->T4,
					$r->T5,
					$r->T6,
					$r->T7,
					$r->T8,
					$r->T9,
					$r->T10,
					$r->T11,
					$r->T12,
					$r->T13,
					$r->T14,
					$r->T15,
					$r->T16,
					$r->T17,
					$r->T18,
					$r->T19,
					$r->T20,
					$r->T21,
					$r->T22,
					$r->T23,
					$r->T24,
					$r->T25,
					$r->T26,
					$r->T27,
					$r->T28,
					$r->T29,
					$r->T30,
					$r->T31,
					$r->KEHADIRAN,
					$r->TEPAT_WAKTU,
					$r->TERLAMBAT
                    
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $rekap_siak->num_rows(),
                 "recordsFiltered" => $rekap_siak->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}
	public function get_rekap_daily()
     {

          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $user_id = $this->input->post('user_id');
          $rekap_daily = $this->act->daily_rekap($user_id);
          $data = array();
          $i = 0;
          foreach($rekap_daily->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->USER_ID,
                    $r->TANGGAL,
                    $r->ACTIVITY,
                    $r->DESCRIPTION,
                    $r->START_ACTIVITY.'-'.$r->END_ACTIVITY,
                    $r->NUMBER_ACTIVITY,
                    '<button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->DAILY_ID.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
                    
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $rekap_daily->num_rows(),
                 "recordsFiltered" => $rekap_daily->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}
	public function get_rekap_daily_tgl()
     {

          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $user_id = $this->input->post('user_id');
          $tgl = $this->input->post('tanggal');
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $rekap_daily = $this->act->daily_rekap_tgl($user_id,$tgl_start,$tgl_end);
          $data = array();
          $i = 0;
          foreach($rekap_daily->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->USER_ID,
                    $r->TANGGAL,
                    $r->ACTIVITY,
                    $r->DESCRIPTION,
                    $r->START_ACTIVITY.'-'.$r->END_ACTIVITY,
                    $r->NUMBER_ACTIVITY,
                    '<button class="btn btn-danger btn-xs" onclick="hapus('.$r->DAILY_ID.');">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
                    
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $rekap_daily->num_rows(),
                 "recordsFiltered" => $rekap_daily->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}
	public function get_rekap_absen()
     {

          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $tgl = $this->input->post('tanggal');
          $rekap_daily = $this->act->rekap_absensi($tgl);
          $data = array();
          $i = 0;
          foreach($rekap_daily->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->USER_ID,
                    $r->NAMA_LGKP,
                    $r->JUMLAH_HADIR.' Hari',
                    $r->TEPAT_WAKTU.' Hari',
                    $r->TERLAMBAT.' Hari',
                    $r->EXCUSE.' Hari',
                    $r->MINUTE_WORK.' Menit',
                    '<button  class="btn btn-info btn-xs" onclick="lihat(\''.base64_encode($r->USER_ID).'\');"><i class="fa fa-desktop  fa-fw" id=></i> Detail</button>'
                    
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $rekap_daily->num_rows(),
                 "recordsFiltered" => $rekap_daily->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}
	public function get_rekap_activity()
     {

          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $tgl = $this->input->post('tanggal');
          $rekap_daily = $this->act->rekap_daily($tgl);
          $data = array();
          $i = 0;
          foreach($rekap_daily->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->USER_ID,
                    $r->NAMA_LGKP,
                    $r->AKTIVITAS.' Kriteria',
                    $r->TASK_PER_AKTIVITAS.' Task',
                    $r->JUMLAH_AKTIVITAS.' Aktivitas',
                    '<button  class="btn btn-info btn-xs" onclick="lihat(\''.base64_encode($r->USER_ID).'\');"><i class="fa fa-desktop  fa-fw" id=></i> Detail</button>'
                    
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $rekap_daily->num_rows(),
                 "recordsFiltered" => $rekap_daily->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}
	public function get_rekap_absen_detail()
     {

          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $tgl = $this->input->post('tanggal');
          $user_id = $this->input->post('user_id');
          $res = $this->act->get_rekap_absensi_detail($tgl,$user_id);
          $data = array();
          $i = 0;
          foreach($res->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->USER_ID,
                    $r->NAMA_LGKP,
                    $r->DAY,
                    $r->JAM_MASUK,
                    $r->JAM_KELUAR,
                    $r->KETERANGAN
                    
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $res->num_rows(),
                 "recordsFiltered" => $res->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}
	public function get_rekap_daily_detail()
     {

          $draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));
          $tgl = $this->input->post('tanggal');
          $user_id = $this->input->post('user_id');
          $res = $this->act->get_rekap_activity_detail($tgl,$user_id);
          $data = array();
          $i = 0;
          foreach($res->result() as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                   	$r->USER_ID,
                    $r->TANGGAL,
                    $r->ACTIVITY,
                    $r->DESCRIPTION,
                    $r->START_ACTIVITY.'-'.$r->END_ACTIVITY,
                    $r->NUMBER_ACTIVITY
                    
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $res->num_rows(),
                 "recordsFiltered" => $res->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
		}
	public function option_excuse()
	{
		header("Content-Type: application/json", true);
		$r = $this->act->excuse_option();
		echo json_encode($r);
	}
	public function option_daily()
	{
		header("Content-Type: application/json", true);
		$user_id = $this->input->post('user_id');
		$r = $this->act->daily_option($user_id);
		echo json_encode($r);
	}
	public function excuse() 
	{
		header("Content-Type: application/json", true);
		if($this->input->post('get_in') != null){
			$user_id = $this->input->post('user_id');
			$tanggal_absen = $this->input->post('tanggal_absen');
			$r = $this->act->check_excuse($user_id,$tanggal_absen);
			if ($r > 0){
				$r2 = $this->act->excuse_getdate($user_id,$tanggal_absen);
				$clock_in = $r2[0]->JAM_MASUK;
				$clock_out = $r2[0]->JAM_KELUAR;
				$output = array(
	    			"message_type"=>1,
	                "clock_in"=> $clock_in,
	    			"clock_out"=> $clock_out,
	                "message"=> '',
	    			"sql"=> ''
    			);
			}else{
				$output = array(
	    			"message_type"=>2,
	    			"clock_in"=>"00:00",
	                "clock_out"=> "00:00",
	    			"message"=>"Maaf Pada Tanggal $tanggal_absen Anda Tidak Clock-In Atau Clock Out",
	                "sql"=> "SQL"  
    			);
			}
			echo json_encode($output);
		}else if ($this->input->post('get_date') != null) {
			$tanggal_absen = $this->input->post('tanggal_absen');
			$r = $this->act->cekdate($tanggal_absen);
			if ($r > 0){
				$output = array(
	                "message_type"=>1,
	                "message"=> "Anda Tidak Bisa Mengajukan Excuse Lebih Dari Hari Ini"
            	);
			}else{
				$output = array(
	                "message_type"=>2,
	                "message"=>"Okay Dude"
            	);
			}
			echo json_encode($output);
		}else if ($this->input->post('get_out') != null){
			$user_id = $this->input->post('user_id');
			$tanggal_absen = $this->input->post('tanggal_absen');
			$r = $this->act->check_excuse($user_id,$tanggal_absen);
			if ($r > 0){
				$r2 = $this->act->excuse_getdate($user_id,$tanggal_absen);
				$clock_in = $r2[0]->JAM_MASUK;
				$clock_out = $r2[0]->JAM_KELUAR;
				$output = array(
	    			"message_type"=>1,
	                "clock_in"=> $clock_in,
	    			"clock_out"=> $clock_out,
	                "message"=> '',
	    			"sql"=> ''
    			);
			}else{
				$output = array(
	    			"message_type"=>2,
	    			"clock_in"=>"00:00",
	                "clock_out"=> "00:00",
	    			"message"=>"Maaf Pada Tanggal $tanggal_absen Anda Tidak Clock-In Atau Clock Out",
	                "sql"=> "SQL"  
    			);
			}
			echo json_encode($output);
		}else if ($this->input->post('get_absen') != null){
			$user_id = $this->input->post('user_id');
			$tanggal_absen = $this->input->post('tanggal_absen');
			$r = $this->act->check_excuse($user_id,$tanggal_absen);
			if ($r > 0){
				$r2 = $this->act->excuse_getdate($user_id,$tanggal_absen);
				$clock_in = $r2[0]->JAM_MASUK;
				$clock_out = $r2[0]->JAM_KELUAR;
				$output = array(
	    			"message_type"=>1,
	                "clock_in"=> $clock_in,
	                "clock_out"=> $clock_out,
	                "message"=>"Maaf Pada Tanggal $tanggal_absen Anda Telah Clock-In Atau Clock Out",
	                "sql"=> ''
    			);
			}else{
				$output = array(
	    			"message_type"=>2,
	                "clock_in"=>"08:00",
	                "clock_out"=> "17:00",
	                "message"=>"-",
	                "sql"=> "-"   
    			);
			}
			echo json_encode($output);
		}else if ($this->input->post('get_excuse') != null){
			$user_id = $this->input->post('user_id');
			$tanggal_absen = $this->input->post('tanggal_absen');
			$r = $this->act->check_excuse($user_id,$tanggal_absen);
			if ($r > 0){
				$r2 = $this->act->excuse_getdate($user_id,$tanggal_absen);
				$clock_in = $r2[0]->JAM_MASUK;
				$clock_out = $r2[0]->JAM_KELUAR;
				$output = array(
	    			"message_type"=>1,
	                "clock_in"=> $clock_in,
	                "clock_out"=> $clock_out,
	                "message"=>"Maaf Pada Tanggal $tanggal_absen Anda Telah Clock-In Atau Clock Out",
	                "sql"=> ''
    			);
			}else{
				$output = array(
	    			"message_type"=>2,
	                "clock_in"=>"08:00",
	                "clock_out"=> "17:00",
	                "message"=>"-",
	                "sql"=> "-"   
    			);
			}
			echo json_encode($output);
		}else if ($this->input->post('excuse_save') != null){
			$user_id = $this->input->post('user_id');
			$nama_lgkp = $this->input->post('nama_lgkp');
			$excuse_tanggal = $this->input->post('excuse_tanggal');
			$excuse_keterangan = $this->input->post('excuse_keterangan');
			$excuse_ci = $this->input->post('excuse_ci');
			$excuse_co = $this->input->post('excuse_co');
			$descrip = ucwords(strtolower($this->act->get_des_excuse($excuse_keterangan)));
			$j = $this->act->report_is_absen($user_id);
			if (count($j) > 0) {
			if($excuse_keterangan == 1 || $excuse_keterangan == 4 || $excuse_keterangan == 5 || $excuse_keterangan == 6){
				$this->act->insert_excuse($user_id,$nama_lgkp,$excuse_tanggal,$excuse_ci,$excuse_co,$descrip,$this->session->userdata(S_IP_ADDRESS));
				$output = array(
	                "message_type"=>1,
	                "message"=> "Data Berhasil Di Insert",
	                "SQL"=> ""
	            );
			}else if($excuse_keterangan == 2 ||$excuse_keterangan == 3 ){
				$this->act->update_excuse($user_id,$nama_lgkp,$excuse_tanggal,$excuse_ci,$excuse_co,$descrip,$this->session->userdata(S_IP_ADDRESS));
				$output = array(
	                "message_type"=>1,
	                "message"=> "Data Berhasil Di Update",
	                "SQL"=> ""

	            );
			}else{
	            $output = array(
	                "message_type"=>2,
	                "message"=> "Maaf Terjadi Kesalahan"
	            );
        	}
        }else{
        	 $output = array(
	                "message_type"=>2,
	                "message"=> "Maaf Anda Tidak Memiliki Hak Akses Untuk Absen"
	            );
        }
        echo json_encode($output);
		}else{
			redirect('/','refresh');
		}
	}
	public function daily() 
	{
		header("Content-Type: application/json", true);
		if($this->input->post('daily_save') != null){
			$aktivitas =  $this->input->post('aktivitas');
	    	$tanggal_report =  $this->input->post('tanggal_report');
	    	$jumlah =  $this->input->post('jumlah');
	    	$daily_dari =  $this->input->post('daily_dari');
	    	$daily_sampai =  $this->input->post('daily_sampai');
	    	$daily_keterangan =  $this->clean($this->input->post('daily_keterangan'));
			$user_id =  $this->input->post('user_id');
			$today = date('d-m-Y');
			$r = $this->act->check_daily($user_id,$aktivitas,$tanggal_report);
			if ($r == 0){
				$this->act->insert_daily($aktivitas,$user_id,$daily_dari,$daily_sampai,$jumlah,$daily_keterangan,$tanggal_report);
				$output = array(
	    			"message_type"=>1,
	    			"aktivitas"=>$aktivitas,
	    			"message"=> "Terima Kasih Sudah Mengisi Laporan Harian <span class='fa fa-smile-o'></span>"
	    		);
			}else{
				$output = array(
	    			"message_type"=>2,
	    			"aktivitas"=>$aktivitas,
	    			"message"=>"Maaf Anda Tidak Bisa Mengisi Aktivitas Yang Sama Di Hari Yang Sama <span class='fa fa-meh-o'></span>"
	    		);
			}
			echo json_encode($output);
		}else if ($this->input->post('get_date') != null) {
			$tanggal_report =  $this->input->post('tanggal_report');
			$r = $this->act->cekdate($tanggal_report);
			if ($r > 0){
				$output = array(
	               "message_type"=>1,
                	"message"=> "Anda Tidak Bisa Membuat Daily Report Lebih Dari Hari Ini"
            	);
			}else{
				$output = array(
	                "message_type"=>2,
                	"message"=>"Okay Dude"
            	);
			}
			echo json_encode($output);
		}else if ($this->input->post('do_delete') != null) {
			$activity_delete =  $this->input->post('activity_delete');
			$this->act->delete_daily($activity_delete);
			$output = array(
                "message_type"=>2,
                "aktivitas"=>"",
                "message"=>"Activitas Sudah Di Delete <span class='fa fa-smile-o'></span>"
            );
			echo json_encode($output);
		}

	}
	public function delete_daily_id(){
		header('Content-type: application/json');
		if ($this->input->post('daily_id') != null){
			$daily_id = $this->input->post('daily_id');
			$this->act->delete_daily($daily_id);
			$output = array(
	    			"message_type"=>1,
	    			"message"=>"Aktivity Ini Telah Dihapus !"
	    	);
			echo json_encode($output);	
		}
	}
	public function get_daily_harian_table()
	{
		header("Content-Type: application/json", true);
		$user_id = $this->input->post('user_id');
        $tgl = $this->input->post('tanggal');
		$tgl_start = substr($tgl, 0, 10);
		$tgl_end = substr($tgl,13, 10);
		$r = $this->act->rekap_daily_ttd_table($user_id,$tgl_start,$tgl_end);
		echo json_encode($r);
	}
	public function do_pdf() {
		if($this->input->post('pdf_tanggal') != null){
			$pdf_userid = $this->session->userdata(S_USER_ID);
			$tgl = $this->input->post('pdf_tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);

        $fileName = strtoupper($this->session->userdata(S_USER_ID)).'_activity_data_'.time().'.xlsx';  
		$activity_data =  $this->act->rekap_daily_ttd_pdf($pdf_userid,$tgl_start,$tgl_end);
		$atasan =  $this->act->get_atasan($pdf_userid);
        // set document information
        PDF::SetCreator('Dian Yanzen');
		PDF::SetAuthor('Dian Yanzen');
		PDF::SetTitle('PDF Monitoring');
        PDF::SetSubject('YZ PDF');
        PDF::SetKeywords('YZ, PDF, Monitoring, DisdukCapil, Kota Bandung');
		// set auto page breaks
		PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		PDF::setImageScale(PDF_IMAGE_SCALE_RATIO);

		

// ---------------------------------------------------------

// set font

        // set default header data
        PDF::SetMargins(PDF_MARGIN_LEFT, 45, PDF_MARGIN_RIGHT);
		PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
		PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
        // set font
        PDF::SetFont('times', '', 12);
        PDF::AddPage();
        // add a page
        $html = '
				<!-- EXAMPLE OF CSS STYLE -->
				<style>

				    table.first {
				    	padding: 0px;    
				    	margin: 0px;    
				        font-family: helvetica;
				        font-size: 7pt;
				        
				    }
				</style>
				<table class="first" cellpadding="0" cellspacing="5" border="0">
				 <tr>
				  <td width="50" align="left">Nama</td>
				  <td width="20" align="center">:</td>
				  <td width="140" align="left"><b>'.$this->session->userdata(S_NAMA_LGKP).'</b></td>
				  <td width="80" align="center"></td>
				  <td width="60" align="center"></td>
				  <td width="20" align="center"></td>
				  <td width="220" align="left"></td>
				 </tr>
				 <tr>
				  <td width="50" align="left">Nik</td>
				  <td width="20" align="center">:</td>
				  <td width="140" align="left">'.$this->session->userdata(S_NIK).'</td>
				  <td width="80" align="center"></td>
				  <td width="60" align="center"></td>
				  <td width="20" align="center"></td>
				  <td width="220" align="left"></td>
				 </tr>
				 <tr>
				  <td width="50" align="left">Perihal</td>
				  <td width="20" align="center">:</td>
				  <td width="210" align="left">Laporan Pelaksanaan Tugas</td>
				  <td width="10" align="center"></td>
				  <td width="60" align="right"></td>
				  <td width="20" align="center"></td>
				  <td width="220" align="left"></td>
				 </tr>
				 <tr>
				  <td width="50" align="left">Tanggal</td>
				  <td width="20" align="center">:</td>
				  <td width="210" align="left">'.$tgl.'</td>
				  <td width="10" align="center"></td>
				  <td width="60" align="right"></td>
				  <td width="20" align="center"></td>
				  <td width="220" align="left"></td>
				 </tr>
				</table>
				<br>
				<br>
				<table id="demo-foo-row-toggler" class="first" data-page-size="100" cellpadding="10">
				                                        <thead>
                                            <tr>
                                                <th width="5%" style="border: 2px solid #e4e7ea; text-align: center;">No</th>
                                                <th width="15%" style="border: 2px solid #e4e7ea;text-align: center;">Tanggal</th>
                                                <th width="40%" style="border: 2px solid #e4e7ea;text-align: center;">Aktivitas</th>
                                                <th width="20%" style="border: 2px solid #e4e7ea;text-align: center;">Jumlah Aktivitas</th>
                                                <th width="20%" style="border: 2px solid #e4e7ea;text-align: center;">Paraf</th>
                                            </tr>
                                        </thead>
                                        <tbody id="my_data" style="border: 1px solid #e4e7ea;">
';
$style = array('border' => 0, 'vpadding' => 'auto', 'hpadding' => 'auto', 'fgcolor' => array(0, 0, 0), 'bgcolor' => false, 'module_width' => 1, 'module_height' => 1);
$i = 0;
        foreach ($activity_data as $val) 
        {
        // new style
$i++;
// QRCODE,H : QR-CODE Best error correction

        
        // print a line using Cell()
        // PDF::Cell(0, 12, strtoupper($this->session->userdata(S_USER_ID)).'_activity_data_'.time(), 0, 1, 'C');
        $html .= '

 <tr style="border: 1px solid #e4e7ea;">
<td width="5%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;">'.$i.'</td>
<td width="15%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;">'.$val->TANGGAL.'</td>
<td width="40%" valign="center" style="border: 1px solid #e4e7ea; text-align: left;">'.str_replace("//line","<br/>",$val->DESX).'<br /></td>
<td width="20%" valign="center" style="border: 1px solid #e4e7ea; text-align: left;">'.str_replace("//line","<br/>",$val->JUMLAH).' Aktivitas <br /></td>
<td width="20%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;"></td>
</tr>
 
   
';
	



// embed image, masked with previously embedded mask

// PDF::Image(base_url('assets/upload/pp/'.$this->session->userdata(S_NIK).'.jpg'), 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);

// PDF::Image($image_file_bg, 10, 120, 180, '', '', '', '', false, 300, '', false, 300);
}
        $html .= '
        </tbody>
                                    </table>
                                    <table class="first" cellpadding="0" cellspacing="5" border="0">

        <tr>
            <td width="45%"></td>	
            <td width="10%"></td>											
            <td width="45%" align="center"></td>
		</tr>
		<tr>
          <td align="center">Mengetahui : </td>
          <td></td>
          <td align="center">Mengetahui : </td>
        </tr>
     
        <tr>
          <td height="80" valign="top" align="center">'.$atasan->JABATAN_SATU.'</td>
          <td height="80" valign="top" align="center"></td>
          <td height="80" valign="top" align="center">'.$atasan->JABATAN_DUA.'</td>
        </tr>        
        <tr>
            <td align="center"><u>'.$atasan->NAMA_SATU.'</u></td>
            <td></td>
            <td align="center"><u>'.$atasan->NAMA_DUA.'</u></td>
        </tr>
        <tr style="line-height:7px">
            <td align="center">NIP. '.$atasan->NIP_SATU.'</td>
            <td></td>
            <td align="center">NIP. '.$atasan->NIP_DUA.'</td>
        </tr>
    </table>
';
        PDF::writeHTML($html, true, false, true, false, '');
        //Close and output PDF document
        PDF::Output(strtoupper($this->session->userdata(S_USER_ID)).'_activity_data_'.time().'.pdf', 'I');
         } 
        else 
        {
           redirect('/','refresh');
        }
    }
	public function clean($string) {
	   	$string = str_replace(' ', ' ', $string); // Replaces all spaces with hyphens.
	   	return preg_replace('/[^A-Za-z0-9\- ,.]/', '', $string); // Removes special chars.
	}

}