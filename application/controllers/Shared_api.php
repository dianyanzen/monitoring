<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shared_api extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Shared','shr');
	}
	public function index()
	{
		redirect('/','refresh');
	}
	public function get_kecamatan()
	{
		header("Content-Type: application/json", true);
		$j = $this->shr->get_give_kec();
		if ($j > 0){
			$r = $this->shr->get_all_kecamatan(0);
		}else{
			$r = $this->shr->get_all_kecamatan($this->session->userdata(S_NO_KEC));
		}
		
		echo json_encode($r);
	}
	public function get_master_provinsi()
	{
		header("Content-Type: application/json", true);
		$no_prop = $this->input->post('no_prop');
		$r = $this->shr->get_prop($no_prop);
		echo json_encode($r);
	}
	public function get_master_kabupaten()
	{
		header("Content-Type: application/json", true);
		$no_prop = $this->input->post('no_prop');
		$no_kab = $this->input->post('no_kab');
		$r = $this->shr->get_kab($no_prop,$no_kab);
		echo json_encode($r);
	}
	public function get_master_kecamatan()
	{
		header("Content-Type: application/json", true);
		$no_prop = $this->input->post('no_prop');
		$no_kab = $this->input->post('no_kab');
		$no_kec = $this->input->post('no_kec');
		$r = $this->shr->get_kec($no_prop,$no_kab,$no_kec);
		echo json_encode($r);
	}
	public function get_master_kelurahan()
	{
		header("Content-Type: application/json", true);
		$no_prop = $this->input->post('no_prop');
		$no_kab = $this->input->post('no_kab');
		$no_kec = $this->input->post('no_kec');
		$no_kel = $this->input->post('no_kel');
		$r = $this->shr->get_kel($no_prop,$no_kab,$no_kec,$no_kel);
		echo json_encode($r);
	}
	public function get_one_kecamatan()
	{
		header("Content-Type: application/json", true);
		$no_kec = $this->input->post('no_kec');
		
		$j = $this->shr->get_give_kec();
		if ($j > 0){
			$r = $this->shr->get_one_kecamatan(0);
		}else{
			$r = $this->shr->get_one_kecamatan($no_kec);
		}
		echo json_encode($r);
	}
	public function get_kelurahan()
	{
		header("Content-Type: application/json", true);
		$no_kec = $this->input->post('no_kec');
		$j = $this->shr->get_give_kec();
		if ($j > 0){
			$j = $this->shr->get_give_kel();
			if ($j > 0){
				$r = $this->shr->get_all_kelurahan($no_kec,0);
			}else{
				$r = $this->shr->get_all_kelurahan($no_kec,$this->session->userdata(S_NO_KEL));
			}
		}else{
			$j = $this->shr->get_give_kel();
			if ($j > 0){
				$r = $this->shr->get_all_kelurahan($this->session->userdata(S_NO_KEC),0);
			}else{
				$r = $this->shr->get_all_kelurahan($this->session->userdata(S_NO_KEC),$this->session->userdata(S_NO_KEL));
			}
		}
		
		
		echo json_encode($r);
		
	}
	public function get_rw()
	{
		header("Content-Type: application/json", true);
		$no_kec = $this->input->post('no_kec');
		$no_kel = $this->input->post('no_kel');
		$r = $this->shr->get_all_rw($no_kec,$no_kel,$this->session->userdata(S_NO_RW));
		echo json_encode($r);
		
	}
	public function get_rt()
	{
		header("Content-Type: application/json", true);
		$no_kec = $this->input->post('no_kec');
		$no_kel = $this->input->post('no_kel');
		$no_rw = $this->input->post('no_rw');
		$r = $this->shr->get_all_rt($no_kec,$no_kel,$no_rw,$this->session->userdata(S_NO_RT));
		echo json_encode($r);
		
	}
	public function get_will()
	{
		header("Content-Type: application/json", true);
		$r = $this->shr->get_all_wil($this->session->userdata(S_NO_KEC));
		echo json_encode($r);
	}
	public function get_master_menu()
	{
		header("Content-Type: application/json", true);
		$r = $this->shr->get_master_menu();
		echo json_encode($r);
	}
	public function get_master_usia()
	{
		header("Content-Type: application/json", true);
		$r = $this->shr->get_master_usia();
		echo json_encode($r);
	}
	public function get_master_cakupan()
	{
		header("Content-Type: application/json", true);
		$r = $this->shr->get_master_cakupan();
		echo json_encode($r);
	}
	public function get_user_daily()
	{
		header("Content-Type: application/json", true);
		$r = $this->shr->get_user_daily();
		echo json_encode($r);
	}
	public function get_user_absensi()
	{
		header("Content-Type: application/json", true);
		$r = $this->shr->get_user_absensi();
		echo json_encode($r);
	}
	public function get_user_cpltte()
	{
		header("Content-Type: application/json", true);
		$r = $this->shr->get_user_cpltte();
		echo json_encode($r);
	}
	public function get_default_pass()
	{
		header("Content-Type: application/json", true);
		$r = $this->shr->get_default_pass();
		echo json_encode($r);
	}
}